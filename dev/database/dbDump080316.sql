-- MySQL dump 10.13  Distrib 5.6.27, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: bitnow
-- ------------------------------------------------------
-- Server version	5.6.27-0ubuntu0.15.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `user` bigint(20) NOT NULL,
  `id` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `number` varchar(255) NOT NULL,
  `owner` varchar(255) NOT NULL,
  `web_username` varchar(255) NOT NULL,
  `web_password` varchar(255) NOT NULL,
  `balance` double NOT NULL DEFAULT '0',
  `balance_available` double NOT NULL DEFAULT '0',
  `latest_payment` bigint(20) NOT NULL,
  `daily_balance` double NOT NULL DEFAULT '0',
  `last_read_status` int(8) NOT NULL DEFAULT '-1',
  `fee` double NOT NULL DEFAULT '0',
  `created` bigint(20) NOT NULL,
  `last_read` bigint(20) NOT NULL,
  `last_modified` bigint(20) NOT NULL,
  PRIMARY KEY (`user`,`id`,`type`),
  CONSTRAINT `fk_account_user` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `btc_address`
--

DROP TABLE IF EXISTS `btc_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `btc_address` (
  `user` bigint(20) NOT NULL,
  `address` varchar(255) NOT NULL,
  `created` bigint(20) NOT NULL,
  PRIMARY KEY (`address`),
  KEY `fk_address_user_idx` (`user`),
  CONSTRAINT `fk_address_user` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `btc_address`
--

LOCK TABLES `btc_address` WRITE;
/*!40000 ALTER TABLE `btc_address` DISABLE KEYS */;
/*!40000 ALTER TABLE `btc_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `configuration`
--

DROP TABLE IF EXISTS `configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuration` (
  `key` varchar(45) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 KEY_BLOCK_SIZE=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuration`
--

LOCK TABLES `configuration` WRITE;
/*!40000 ALTER TABLE `configuration` DISABLE KEYS */;
INSERT INTO `configuration` VALUES ('ADDRESS_LINK','http://tbtc.blockr.io/address/info/'),('COGE_POLLING_INTERVAL','60000'),('CONTEXT_PATH','http://www.bitnow.it/'),('ENVIRONMENT','DEV'),('EXCHANGE_RATE_API','https://api.kraken.com/0/public/Ticker?pair=XXBTZEUR'),('EXCHANGE_RATE_EXCHANGE','KRAKEN'),('FEE_BUY','6'),('FEE_BUYER','2'),('FEE_SELL','-4'),('FEE_SELLER','4'),('MAX_BUY_PAYMENT','997,99'),('MAX_SELL_PAYMENT','500,00'),('NOT_ALLOWED_TRANSACTIONS','POSTEPAY.IT,APP,ATM,BPCLICK,BPOL,ON LINE,PUNTORICARICA,'),('REST_PRICE_KEY','CQJ4FPS2FPLNMMOD'),('TRANSACTION_EXPIRY_TIME','900000'),('TRANSACTION_LINK','http://tbtc.blockr.io/tx/info/'),('UI_POLLING_INTERVAL','15'),('WALLET_FEE_ACCOUNT_BUY','feeB'),('WALLET_FEE_ACCOUNT_SELL','feeS'),('WALLET_PASSPHRASE','YOUR WALLET ENCRYPTED PASSPHRASE HERE'),('WALLET_SEND_FEE','0.001'),('WALLET_VALIDITY_API','http://tbtc.blockr.io/api/v1/address/info/');
/*!40000 ALTER TABLE `configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `file` bigint(20) NOT NULL,
  `user` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_document_user_idx` (`user`),
  KEY `fk_document_file_idx` (`file`),
  CONSTRAINT `fk_document_file` FOREIGN KEY (`file`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_document_user` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document`
--

LOCK TABLES `document` WRITE;
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
/*!40000 ALTER TABLE `document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file`
--

DROP TABLE IF EXISTS `file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `data` mediumblob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file`
--

LOCK TABLES `file` WRITE;
/*!40000 ALTER TABLE `file` DISABLE KEYS */;
/*!40000 ALTER TABLE `file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `code` varchar(16) NOT NULL,
  `type` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `btc` double NOT NULL,
  `euro` double NOT NULL,
  `fee` double NOT NULL,
  `status` int(11) NOT NULL,
  `transaction_hash` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `modified` bigint(20) NOT NULL,
  `created` bigint(20) NOT NULL,
  `account_user` bigint(20) NOT NULL,
  `account_id` varchar(255) NOT NULL,
  `account_type` int(11) NOT NULL,
  PRIMARY KEY (`code`),
  UNIQUE KEY `index_seller_status_euro_unique` (`status`,`euro`,`account_id`,`account_type`,`account_user`),
  KEY `fk_order_seller_idx` (`account_user`,`account_id`,`account_type`),
  CONSTRAINT `fk_order_account` FOREIGN KEY (`account_user`, `account_id`, `account_type`) REFERENCES `account` (`user`, `id`, `type`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `readings`
--

DROP TABLE IF EXISTS `readings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `readings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `read` blob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65025 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `readings`
--

LOCK TABLES `readings` WRITE;
/*!40000 ALTER TABLE `readings` DISABLE KEYS */;
/*!40000 ALTER TABLE `readings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receipt`
--

DROP TABLE IF EXISTS `receipt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receipt` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `file` bigint(20) NOT NULL,
  `order` varchar(16) DEFAULT NULL,
  `transaction` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_receipt_order_idx` (`order`),
  KEY `fk_receipt_transaction_idx` (`transaction`),
  KEY `fk_receipt_file_idx` (`file`),
  CONSTRAINT `fk_receipt_file` FOREIGN KEY (`file`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_receipt_order` FOREIGN KEY (`order`) REFERENCES `order` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_receipt_transaction` FOREIGN KEY (`transaction`) REFERENCES `transaction` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receipt`
--

LOCK TABLES `receipt` WRITE;
/*!40000 ALTER TABLE `receipt` DISABLE KEYS */;
/*!40000 ALTER TABLE `receipt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  `name` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (-2,'Default role for all Users','ROLE_USER'),(-1,'Administrator role (can edit Users)','ROLE_ADMIN');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `code` varchar(16) NOT NULL,
  `type` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `btc` double NOT NULL,
  `euro` double NOT NULL,
  `fee` double NOT NULL,
  `status` int(11) NOT NULL,
  `transaction_hash` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `modified` bigint(20) NOT NULL,
  `created` bigint(20) NOT NULL,
  `account_user` bigint(20) NOT NULL,
  `account_id` varchar(255) NOT NULL,
  `account_type` int(11) NOT NULL,
  PRIMARY KEY (`code`),
  KEY `fk_transaction_1_idx` (`account_user`,`account_id`,`account_type`),
  CONSTRAINT `fk_transaction_account` FOREIGN KEY (`account_user`, `account_id`, `account_type`) REFERENCES `account` (`user`, `id`, `type`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_wallet`
--

DROP TABLE IF EXISTS `transaction_wallet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_wallet` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `btc` double NOT NULL,
  `confirmations` int(11) NOT NULL,
  `transaction_hash` varchar(255) DEFAULT NULL,
  `created` bigint(20) NOT NULL,
  `account_user` bigint(20) NOT NULL,
  `account_id` varchar(255) NOT NULL,
  `account_type` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_transaction_account_idx` (`account_user`,`account_id`,`account_type`),
  CONSTRAINT `fk_transaction_wallet_account` FOREIGN KEY (`account_user`, `account_id`, `account_type`) REFERENCES `account` (`user`, `id`, `type`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_wallet`
--

LOCK TABLES `transaction_wallet` WRITE;
/*!40000 ALTER TABLE `transaction_wallet` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction_wallet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_expired` bit(1) NOT NULL,
  `account_locked` bit(1) NOT NULL,
  `credentials_expired` bit(1) NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 NOT NULL,
  `account_enabled` bit(1) DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 NOT NULL,
  `created` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `theme` varchar(45) NOT NULL,
  `secret` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (30,'\0','\0','\0','amministratore@bitnow.it','','','admin',1456356515,102,'bootstrap','');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FK143BF46A49A62B3D` (`role_id`) USING BTREE,
  KEY `FK143BF46AEED0EF1D` (`user_id`) USING BTREE,
  CONSTRAINT `FK143BF46A49A62B3D` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `FK143BF46AEED0EF1D` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (30,-1);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wallet`
--

DROP TABLE IF EXISTS `wallet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wallet` (
  `type` int(11) NOT NULL,
  `balance` double NOT NULL DEFAULT '0',
  `balance_available` double NOT NULL DEFAULT '0',
  `exchange_rate` double NOT NULL DEFAULT '0',
  `created` bigint(20) NOT NULL,
  `last_read` bigint(20) NOT NULL,
  `last_modified` bigint(20) NOT NULL,
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wallet`
--

LOCK TABLES `wallet` WRITE;
/*!40000 ALTER TABLE `wallet` DISABLE KEYS */;
INSERT INTO `wallet` VALUES (2,1.0758362,0.07583619999999991,373.32,1454934961,0,1457458348),(5,500,500,0.002678667095253402,1455561259,0,1457458348);
/*!40000 ALTER TABLE `wallet` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-08 21:15:20
