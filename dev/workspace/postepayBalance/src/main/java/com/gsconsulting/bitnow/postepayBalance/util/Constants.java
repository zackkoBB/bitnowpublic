package com.gsconsulting.bitnow.postepayBalance.util;

public class Constants {

//	public final static String URL_BASE = "https://myposte.poste.it/portal/login?username=$username&password=$password";
	public final static String URL1 = "https://securelogin.poste.it/jod-fcc/fcc-authentication.html";
//	public final static String URL2 = "https://bancopostaonline.poste.it/bpol/cartepre/servizi/cartapostepay/cartapostepay.aspx?PnlStart=Benvenuto";
	public final static String URL2 = "https://bancopostaonline.poste.it/bpol/cartepre/servizi/cartapostepay/cartapostepay.aspx?pnlstart=listamovimenti";
//	public final static String URL_BALANCE = "https://postepay.poste.it/portalppay/startListaMovimentiAction.do";
	public final static String FORM2_NAME = "Form1";
//	public final static String FORM_NAME = "brk:form=listamovimentiform";
	public final static String ENTERCARD_NAME = "CartaPostePaySelezionaCarta1:txtNrCartaPre";
	public final static String BUTTON1_VALUE = "Accedi";
	public final static String BUTTON2_VALUE = "Esegui";

	/**
	 * General Info IDs
	 */
	public final static String NUMERO_CARTA_ID = "CartaPostePayListaMovimenti1_lblNumeroCarta";
	public final static String INTESTATARIO_ID = "CartaPostePayListaMovimenti1_lblIntestatari";
	public final static String CF_ID = "CartaPostePayListaMovimenti1_lblCF";
	public final static String SALDO_AL_ID = "CartaPostePayListaMovimenti1_lblSaldoAl";
	public final static String SALDO_CONTABILE_ID = "CartaPostePayListaMovimenti1_lblSaldoContabile";
	public final static String SALDO_DISPONIBILE_ID = "CartaPostePayListaMovimenti1_lblSaldoDisponibile";

	public final static String DATA_INIZIO_DECORRENZA_ID = "CartaPostePayListaMovimenti1_lblDataInizioDecorrenza";
	public final static String DATA_DECORRENZA_PLAFOND_ID = "CartaPostePayListaMovimenti1_lblDataAzzeramentoPlafond";
	public final static String IMPORTO_RICARICATO_ID = "CartaPostePayListaMovimenti1_lblImportoRicaricato";
	public final static String SALDO_RESIDUO_RICARICABILE_ID = "CartaPostePayListaMovimenti1_lblSaldoResiduoRicaricabile";
	public final static String NUMERO_RICARICHE_RICEVUTE_ID = "CartaPostePayListaMovimenti1_lblNumeroRicaricheRicevute";

	/**
	 * Transaction IDs
	 */
	public final static String DATA_CONTABILE_ID = "CartaPostePayListaMovimenti1_dgListaMovimenti__ctl{0}_lblDataContabile";
	public final static String DATA_VALUTA_ID = "CartaPostePayListaMovimenti1_dgListaMovimenti__ctl{0}_lblDataValuta";
	public final static String ADDEBITO_ID = "CartaPostePayListaMovimenti1_dgListaMovimenti__ctl{0}_lblAddebito";
	public final static String ACCREDITO_ID = "CartaPostePayListaMovimenti1_dgListaMovimenti__ctl{0}_lblAccredito";
	public final static String DESCRIZIONE_ID = "CartaPostePayListaMovimenti1_dgListaMovimenti__ctl{0}_lblDescrizione";
}
