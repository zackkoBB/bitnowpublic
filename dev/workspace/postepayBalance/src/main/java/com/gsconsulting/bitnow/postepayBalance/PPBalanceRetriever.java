package com.gsconsulting.bitnow.postepayBalance;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import com.gsconsulting.bitnow.postepayBalance.exception.PPBalanceRetrieverException;
import com.gsconsulting.bitnow.postepayBalance.model.Configuration;
import com.gsconsulting.bitnow.postepayBalance.model.GeneralInfo;
import com.gsconsulting.bitnow.postepayBalance.model.Transaction;
import com.gsconsulting.bitnow.postepayBalance.util.Constants;

public class PPBalanceRetriever {

	protected final Log log = LogFactory.getLog(getClass());

	private Configuration configuration;
	private GeneralInfo generalInfo;
	private List<Transaction> transactions;
	private WebClient webClient;

	public PPBalanceRetriever() {
		super();
		init();
	}

	public PPBalanceRetriever(Configuration configuration) {
		super();
		this.configuration = configuration;
		init();
	}

	private void init() {

		webClient = new WebClient(BrowserVersion.CHROME);
		webClient.getOptions().setJavaScriptEnabled(false);
		webClient.getOptions().setCssEnabled(false);
		webClient.getOptions().setUseInsecureSSL(true);
		webClient.getOptions().setThrowExceptionOnFailingStatusCode(true);
		webClient.getCookieManager().setCookiesEnabled(true);
		webClient.setAjaxController(new NicelyResynchronizingAjaxController());
		webClient.getOptions().setThrowExceptionOnScriptError(true);
		webClient.getCookieManager().setCookiesEnabled(true);
		generalInfo = new GeneralInfo();
		transactions = new ArrayList<>();
	}

	private void getGeneralInfo(HtmlPage page) {

		generalInfo = new GeneralInfo();
		generalInfo.setNumero(page.getElementById(Constants.NUMERO_CARTA_ID)
				.getTextContent());
		generalInfo.setIntestatario(page.getElementById(
				Constants.INTESTATARIO_ID).getTextContent());
		generalInfo
				.setCf(page.getElementById(Constants.CF_ID).getTextContent());
		generalInfo.setSaldoAl(page.getElementById(Constants.SALDO_AL_ID)
				.getTextContent());
		generalInfo.setSaldoContabile(page.getElementById(
				Constants.SALDO_CONTABILE_ID).getTextContent());
		generalInfo.setSaldoDisponibile(page.getElementById(
				Constants.SALDO_DISPONIBILE_ID).getTextContent());
		generalInfo.setInizioDecorrenza(page.getElementById(
				Constants.DATA_INIZIO_DECORRENZA_ID).getTextContent());
		generalInfo.setAzzeramentoPlafond(page.getElementById(
				Constants.DATA_DECORRENZA_PLAFOND_ID).getTextContent());
		generalInfo.setImportoRicaricato(page.getElementById(
				Constants.IMPORTO_RICARICATO_ID).getTextContent());
		generalInfo.setSaldoResiduo(page.getElementById(
				Constants.SALDO_RESIDUO_RICARICABILE_ID).getTextContent());
		generalInfo.setNumeroRicariche(page.getElementById(
				Constants.NUMERO_RICARICHE_RICEVUTE_ID).getTextContent());
	}

	private void getTransactions(HtmlPage page) {

		transactions = new ArrayList<>();
		for (int i = 2; i < configuration.getTransactions(); i++) {
			if (page.getElementById(Constants.DATA_CONTABILE_ID.replace("{0}",
					(new Integer(i)).toString())) != null) {
				transactions.add(new Transaction(page.getElementById(
						Constants.DATA_CONTABILE_ID.replace("{0}",
								(new Integer(i)).toString())).getTextContent(),
						page.getElementById(
								Constants.DATA_VALUTA_ID.replace("{0}",
										(new Integer(i)).toString()))
								.getTextContent(), page.getElementById(
								Constants.ADDEBITO_ID.replace("{0}",
										(new Integer(i)).toString()))
								.getTextContent(), page.getElementById(
								Constants.ACCREDITO_ID.replace("{0}",
										(new Integer(i)).toString()))
								.getTextContent(), page.getElementById(
								Constants.DESCRIZIONE_ID.replace("{0}",
										(new Integer(i)).toString()))
								.getTextContent()));
			} else {
				break;
			}
		}
	}

	public void refresh() throws PPBalanceRetrieverException {

		try {
			HtmlPage page1 = webClient.getPage(Constants.URL1);
			HtmlInput username = (HtmlInput) page1.getElementById("username");
			username.setValueAttribute(configuration.getUsername());
			HtmlInput password = (HtmlInput) page1.getElementsByName("password").get(0);
			password.setValueAttribute(configuration.getPassword());
			HtmlForm form1 = page1.getForms().get(1);
			HtmlSubmitInput submitButton1 = form1.getInputByValue(Constants.BUTTON1_VALUE);
			HtmlPage page2 = submitButton1.click();
			HtmlPage page3 = webClient.getPage(Constants.URL2);			
			HtmlForm form2 = page3.getFormByName(Constants.FORM2_NAME);
			HtmlTextInput cardNumber = form2
					.getInputByName(Constants.ENTERCARD_NAME);
			cardNumber.setValueAttribute(configuration.getNumber());
			HtmlSubmitInput submitButton2 = form2
					.getInputByValue(Constants.BUTTON2_VALUE);
			HtmlPage page4 = submitButton2.click();
			getGeneralInfo(page4);
			getTransactions(page4);
		} catch (FailingHttpStatusCodeException | IOException
				| NullPointerException | ElementNotFoundException e) {
			throw new PPBalanceRetrieverException(e);
		}
	}

	public void close() {
		webClient.close();
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	public GeneralInfo getGeneralInfo() {
		return generalInfo;
	}

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public WebClient getWebClient() {
		return webClient;
	}

	public void setWebClient(WebClient webClient) {
		this.webClient = webClient;
	}
}
