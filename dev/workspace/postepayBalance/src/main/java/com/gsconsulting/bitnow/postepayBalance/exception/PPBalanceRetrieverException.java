package com.gsconsulting.bitnow.postepayBalance.exception;

public class PPBalanceRetrieverException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5508388693789341960L;

	// Parameterless Constructor
	public PPBalanceRetrieverException() {
	}

	// Constructor that accepts a message
	public PPBalanceRetrieverException(String message) {
		super(message);
	}

	// Constructor that accepts an exception
	public PPBalanceRetrieverException(Exception e) {
		super(e);
	}
}
