package com.gsconsulting.bitnow.postepayBalance.model;

public class Configuration {

	private String username;
	private String password;
	private String number;
	private Integer transactions;

	public Configuration() {
		super();
	}

	public Configuration(String username, String password, String number,
			Integer transactions) {
		super();
		this.username = username;
		this.password = password;
		this.number = number;
		this.transactions = transactions;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Integer getTransactions() {
		return transactions;
	}

	public void setTransactions(Integer transactions) {
		this.transactions = transactions;
	}

}
