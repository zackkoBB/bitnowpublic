package com.gsconsulting.bitnow.postepayBalance.model;

public class Transaction {

	private String dataContabile;
	private String dataValuta;
	private String addebiti;
	private String accrediti;
	private String descrizione;

	public Transaction() {
		super();
	}

	public Transaction(String dataContabile, String dataValuta,
			String addebiti, String accrediti, String descrizione) {
		super();
		this.dataContabile = dataContabile;
		this.dataValuta = dataValuta;
		this.addebiti = addebiti;
		this.accrediti = accrediti;
		this.descrizione = descrizione;
	}

	public String getDataContabile() {
		return dataContabile;
	}

	public void setDataContabile(String dataContabile) {
		this.dataContabile = dataContabile;
	}

	public String getDataValuta() {
		return dataValuta;
	}

	public void setDataValuta(String dataValuta) {
		this.dataValuta = dataValuta;
	}

	public String getAddebiti() {
		return addebiti;
	}

	public void setAddebiti(String addebiti) {
		this.addebiti = addebiti;
	}

	public String getAccrediti() {
		return accrediti;
	}

	public void setAccrediti(String accrediti) {
		this.accrediti = accrediti;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

}
