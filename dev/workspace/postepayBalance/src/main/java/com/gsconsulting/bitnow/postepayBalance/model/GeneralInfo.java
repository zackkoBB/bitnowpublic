package com.gsconsulting.bitnow.postepayBalance.model;

public class GeneralInfo {

	private String numero;
	private String intestatario;
	private String cf;
	private String saldoAl;
	private String saldoContabile;
	private String saldoDisponibile;
	private String inizioDecorrenza;
	private String azzeramentoPlafond;
	private String importoRicaricato;
	private String saldoResiduo;
	private String numeroRicariche;

	public GeneralInfo() {
		super();
	}

	public GeneralInfo(String numero, String intestatario, String cf,
			String saldoAl, String saldoContabile, String saldoDisponibile,
			String inizioDecorrenza, String azzeramentoPlafond,
			String importoRicaricato, String saldoResiduo,
			String numeroRicariche) {
		super();
		this.numero = numero;
		this.intestatario = intestatario;
		this.cf = cf;
		this.saldoAl = saldoAl;
		this.saldoContabile = saldoContabile;
		this.saldoDisponibile = saldoDisponibile;
		this.inizioDecorrenza = inizioDecorrenza;
		this.azzeramentoPlafond = azzeramentoPlafond;
		this.importoRicaricato = importoRicaricato;
		this.saldoResiduo = saldoResiduo;
		this.numeroRicariche = numeroRicariche;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getIntestatario() {
		return intestatario;
	}

	public void setIntestatario(String intestatario) {
		this.intestatario = intestatario;
	}

	public String getCf() {
		return cf;
	}

	public void setCf(String cf) {
		this.cf = cf;
	}

	public String getSaldoAl() {
		return saldoAl;
	}

	public void setSaldoAl(String saldoAl) {
		this.saldoAl = saldoAl;
	}

	public String getSaldoContabile() {
		return saldoContabile;
	}

	public void setSaldoContabile(String saldoContabile) {
		this.saldoContabile = saldoContabile;
	}

	public String getSaldoDisponibile() {
		return saldoDisponibile;
	}

	public void setSaldoDisponibile(String saldoDisponibile) {
		this.saldoDisponibile = saldoDisponibile;
	}

	public String getInizioDecorrenza() {
		return inizioDecorrenza;
	}

	public void setInizioDecorrenza(String inizioDecorrenza) {
		this.inizioDecorrenza = inizioDecorrenza;
	}

	public String getAzzeramentoPlafond() {
		return azzeramentoPlafond;
	}

	public void setAzzeramentoPlafond(String azzeramentoPlafond) {
		this.azzeramentoPlafond = azzeramentoPlafond;
	}

	public String getImportoRicaricato() {
		return importoRicaricato;
	}

	public void setImportoRicaricato(String importoRicaricato) {
		this.importoRicaricato = importoRicaricato;
	}

	public String getSaldoResiduo() {
		return saldoResiduo;
	}

	public void setSaldoResiduo(String saldoResiduo) {
		this.saldoResiduo = saldoResiduo;
	}

	public String getNumeroRicariche() {
		return numeroRicariche;
	}

	public void setNumeroRicariche(String numeroRicariche) {
		this.numeroRicariche = numeroRicariche;
	}

}
