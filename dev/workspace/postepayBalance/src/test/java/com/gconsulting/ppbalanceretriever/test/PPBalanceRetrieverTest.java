package com.gconsulting.ppbalanceretriever.test;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gsconsulting.bitnow.postepayBalance.PPBalanceRetriever;
import com.gsconsulting.bitnow.postepayBalance.exception.PPBalanceRetrieverException;
import com.gsconsulting.bitnow.postepayBalance.model.Configuration;
import com.gsconsulting.bitnow.postepayBalance.model.Transaction;

public class PPBalanceRetrieverTest {

	protected static final Log log = LogFactory
			.getLog(PPBalanceRetrieverTest.class);

	private static PPBalanceRetriever balanceRetriever;

	@BeforeClass
	public static void setUpAll() {

		Configuration configuration = new Configuration(TestData.USERNAME,
				TestData.PASSWORD, TestData.CARD_NUMBER, TestData.TRANSACTIONS);
		balanceRetriever = new PPBalanceRetriever(configuration);
		try {
			balanceRetriever.refresh();
		} catch (PPBalanceRetrieverException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
	}

	@AfterClass
	public static void tearDownAll() {
		balanceRetriever.close();
	}

	@Test
	public void testCardNumber() {
		Assert.assertEquals(TestData.CARD_NUMBER, balanceRetriever
				.getGeneralInfo().getNumero());
	}

	@Test
	public void testIntestatario() {
		Assert.assertEquals(TestData.INTESTATARIO, balanceRetriever
				.getGeneralInfo().getIntestatario());
	}

	@Test
	public void testCF() {
		Assert.assertEquals(TestData.CF, balanceRetriever.getGeneralInfo()
				.getCf());
	}

	@Test
	public void testSaldoAl() {
		Assert.assertEquals(TestData.SALDO_AL, balanceRetriever
				.getGeneralInfo().getSaldoAl());
	}

	@Test
	public void testSaldoContabile() {
		Assert.assertEquals(TestData.SALDO_CONTABILE, balanceRetriever
				.getGeneralInfo().getSaldoContabile());
	}

	@Test
	public void testSaldoDisponibile() {
		Assert.assertEquals(TestData.SALDO_DISPONIBILE, balanceRetriever
				.getGeneralInfo().getSaldoDisponibile());
	}

	@Test
	public void testDataInizioDecorrenza() {
		Assert.assertEquals(TestData.DATA_INIZIO_DECORRENZA, balanceRetriever
				.getGeneralInfo().getInizioDecorrenza());
	}

	@Test
	public void testDataFineDecorrenza() {
		Assert.assertEquals(TestData.DATA_FINE_DECORRENZA, balanceRetriever
				.getGeneralInfo().getAzzeramentoPlafond());
	}

	@Test
	public void testImportoRicaricato() {
		Assert.assertEquals(TestData.IMPORTO_RICARICATO, balanceRetriever
				.getGeneralInfo().getImportoRicaricato());
	}

	@Test
	public void testResiduoRicaricabile() {
		Assert.assertEquals(TestData.RESIDUO_RICARICABILE, balanceRetriever
				.getGeneralInfo().getSaldoResiduo());
	}

	@Test
	public void testRicaricheRicevute() {
		Assert.assertEquals(TestData.RICARICHE_RICEVUTE, balanceRetriever
				.getGeneralInfo().getNumeroRicariche());
	}

//	@Test
	public void testGeneralInfo() {

		testCardNumber();
		testIntestatario();
		testCF();
		testSaldoAl();
		testSaldoContabile();
		testSaldoDisponibile();
		testDataInizioDecorrenza();
		testDataFineDecorrenza();
		testImportoRicaricato();
		testResiduoRicaricabile();
		testRicaricheRicevute();
	}

	@Test
	public void showTransactions() {

		for (Transaction transaction : balanceRetriever.getTransactions()) {
			log.info(transaction.getDataContabile() + " "
					+ transaction.getDataValuta() + " "
					+ transaction.getAddebiti() + " "
					+ transaction.getAccrediti() + " "
					+ transaction.getDescrizione());
		}
	}
}
