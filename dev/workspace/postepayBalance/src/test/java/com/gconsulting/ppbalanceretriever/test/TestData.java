package com.gconsulting.ppbalanceretriever.test;

public class TestData {

	/**
	 * Configuration
	 */
	public static final String USERNAME = "username";// "put your username";
	public static final String PASSWORD = "password";// "put your password";
	public static final String CARD_NUMBER = "CC NUMBER";// "put your ppnumber";
	public static final Integer TRANSACTIONS = 5;

	/**
	 * General Info
	 */
	public static final String INTESTATARIO = "CC NAME";
	public static final String CF = "CC NAME SSN";
	public static final String SALDO_AL = "01/07/2015";
	public static final String SALDO_CONTABILE = "+1,74";
	public static final String SALDO_DISPONIBILE = "+1,74";
	public static final String DATA_INIZIO_DECORRENZA = "10/03/2015";
	public static final String DATA_FINE_DECORRENZA = "10/03/2016";
	public static final String IMPORTO_RICARICATO = "13.704,58";
	public static final String RESIDUO_RICARICABILE = "86.295,42";
	public static final String RICARICHE_RICEVUTE = "128";

}
