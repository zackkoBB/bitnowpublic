package com.gconsulting.sorter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class Sorter {

	private static String RELATIVE_PATH = File.separator + "Documents"
			+ File.separator + "gsit80@gmail.com" + File.separator + "Work"
			+ File.separator + "eBay" + File.separator + "Docs"
			+ File.separator;
	private static String FILE_NAME1 = "IClassiciDelPensiero.txt";
	private static String FILE_NAME2 = "IGrandiClassiciLatiniEGreci.txt";

	public static void main(String[] args) {

		try {
			String selectedFile = FILE_NAME2;
			String userPath = System.getProperty("user.home");
			String path = userPath + RELATIVE_PATH + selectedFile;
			System.out.println(path);
			List<String> lines = Files.readAllLines(Paths.get(path),
					Charset.forName("UTF-8"));
			if (lines != null) {
				if (lines.size() > 0) {
					lines.remove(0);
					lines.remove(1);
					lines.remove(2);
					Collections.sort(lines);
					for (String line : lines) {
						System.out.println(line);
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Exception: " + e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Exception: " + e.getMessage());
		}
	}

}
