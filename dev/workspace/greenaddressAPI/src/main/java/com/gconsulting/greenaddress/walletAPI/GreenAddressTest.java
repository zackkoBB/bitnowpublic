package com.gconsulting.greenaddress.walletAPI;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.bitcoinj.core.NetworkParameters;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import ws.wamp.jawampa.WampClient;
import ws.wamp.jawampa.WampClientBuilder;
import ws.wamp.jawampa.WampError;

public class GreenAddressTest {

	private final static Logger log = Log.getLogger(GreenAddressTest.class);

	private static Subscription addProcSubscription;
	private static Subscription eventPublication;
	private static Subscription eventSubscription;

	private static final int eventInterval = 2000;
	private static int lastEventValue = 0;
	private static final String MNEMONIC = "culture trick token tenant sadness mind about fold adapt sail crane cancel song hire corn embark autumn secret brass trial intact menu long artwork";
    public final static NetworkParameters NETWORK = NetworkParameters.fromID(NetworkParameters.ID_MAINNET);
    public final static String GAIT_TOKEN_URL = "https://greenaddress.it/token/";
    public final static String GAIT_WAMP_URL = "wss://prodwss.greenaddress.it/ws/inv";
    public final static String BLOCKEXPLORER = "https://insight.bitpay.com/tx/";
    public final static String depositPubkey = "0322c5f5c9c4b9d1c3e22ca995e200d724c2d7d8b6953f7b38fddf9296053c961f";
    public final static String depositChainCode = "e9a563d68686999af372a33157209c6860fe79197a4dafd9ec1dbaa49523351d";

	public static void main(String[] args) {

		final WampClient client;
		try {
			// Create a builder and configure the client
			WampClientBuilder builder = new WampClientBuilder();
			builder.withUri("wss://prodwss.greenaddress.it/ws/")
					.withRealm("inv").withInfiniteReconnects()
					.withReconnectInterval(5, TimeUnit.SECONDS);
			// Create a client through the builder. This will not immediatly
			// start
			// a connection attempt
			client = builder.build();
			client.statusChanged().subscribe(new Action1<WampClient.Status>() {
				@Override
				public void call(WampClient.Status t1) {
					if (t1 == WampClient.Status.Connected) {
						// Client got connected to the remote router
						// and the session was established
						log.info("Session1 status changed to " + t1);
					} else if (t1 == WampClient.Status.Disconnected) {
						// Client got disconnected from the remoute router
						// or the last possible connect attempt failed
						log.info("Session1 status changed to " + t1);
					} else if (t1 == WampClient.Status.Connecting) {
						// Client starts connecting to the remote router
						log.info("Session1 status changed to " + t1);
					}
				}
			}, new Action1<Throwable>() {
				@Override
				public void call(Throwable t) {
					log.info("Session1 ended with error " + t);
				}
			}, new Action0() {
				@Override
				public void call() {
					log.info("Session1 ended normally");
				}
			});
			client.open();
			// Publish an event regularly
			eventPublication = Schedulers.computation().createWorker()
					.schedulePeriodically(new Action0() {
						@Override
						public void call() {
							client.publish("test.event", "Hello "
									+ lastEventValue);
							lastEventValue++;
						}
					}, eventInterval, eventInterval, TimeUnit.MILLISECONDS);
			waitUntilKeypressed();
			log.info("Stopping subscription");
			if (eventSubscription != null)
				eventSubscription.unsubscribe();
			log.info("Stopping publication");
			eventPublication.unsubscribe();
			log.info("Closing the client");
			client.close();
		} catch (WampError e) {
			// Catch exceptions that will be thrown in case of invalid
			// configuration
			log.info("Error: " + e.getMessage());
			e.printStackTrace();
		}
	}

	private static void waitUntilKeypressed() {
		try {
			System.in.read();
			while (System.in.available() > 0) {
				System.in.read();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
