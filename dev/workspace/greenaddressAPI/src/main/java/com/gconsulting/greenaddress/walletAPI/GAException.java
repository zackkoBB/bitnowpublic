package com.gconsulting.greenaddress.walletAPI;

public class GAException extends Exception {
	public GAException(final String message) {
		super(message);
	}
}
