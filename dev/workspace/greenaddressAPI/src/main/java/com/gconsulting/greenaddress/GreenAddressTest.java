package com.gconsulting.greenaddress;

import java.util.concurrent.Executors;

import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import com.gconsulting.greenaddress.walletAPI.INotificationHandler;
import com.gconsulting.greenaddress.walletAPI.WalletClient;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

public class GreenAddressTest {

	private final static Logger log = Log.getLogger(GreenAddressTest.class);

	private static WalletClient client;
	public static final ListeningExecutorService es = MoreExecutors
			.listeningDecorator(Executors.newFixedThreadPool(3));

	public static void main(String[] args) {

		client = new WalletClient(new INotificationHandler() {
			@Override
			public void onNewBlock(final long count) {
				log.info("GaService", "onNewBlock");
			}

			@Override
			public void onNewTransaction(final long wallet_id,
					final long[] subaccounts, final long value,
					final String txhash) {
				log.info("GaService", "onNewTransactions");
			}

			@Override
			public void onConnectionClosed(final int code) {
				log.info("GaService", "onConnectionClosed");
			}
		}, es);
		client.connect();
//		client.login(mnemonics_str, device_id)
	}
}
