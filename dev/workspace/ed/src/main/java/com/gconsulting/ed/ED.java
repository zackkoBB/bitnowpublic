package com.gconsulting.ed;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.generators.PKCS5S2ParametersGenerator;
import org.bouncycastle.util.encoders.Base64;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.keygen.KeyGenerators;

public class ED {

	private static final String TEXT = "eh1lV8iZ9eHiekenPpDhqC6RM4Vsvo3ZlhZsomw77QA=";

	public static void main(String[] args) throws NoSuchAlgorithmException,
			InvalidKeySpecException, UnsupportedEncodingException,
			InvalidKeyException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException {

		String salt = getSalt();
		// System.out.println("Salt: " + salt);
		String generatedSecuredPasswordHash = generateStorngPasswordHashJDK(
				Constants.PASSWORD, salt, Constants.KEY_SIZE1);
		// System.out.println("Key1: " + generatedSecuredPasswordHash);
		generatedSecuredPasswordHash = generateStorngPasswordHashJDK(
				generatedSecuredPasswordHash, salt, Constants.KEY_SIZE2);
//		System.out.println("Key: " + generatedSecuredPasswordHash);
		String encrypt = encrypt(TEXT, generatedSecuredPasswordHash);
		 System.out.println("D: "
		 + TEXT);
		System.out.println("E: " + encrypt);
		System.out.println("D: "
				+ decrypt(TEXT, generatedSecuredPasswordHash));
	}

	public static void testSpring() {

		final String salt = KeyGenerators.string().generateKey();
		TextEncryptor encryptor = Encryptors.text(Constants.PASSWORD, salt);
		System.out.println("Salt: \"" + salt + "\"");
		// String textToEncrypt = "*royal secrets*";
		System.out.println("Original text: \"" + TEXT + "\"");
		String encryptedText = encryptor.encrypt(TEXT);
		System.out.println("Encrypted text: \"" + encryptedText + "\"");
		// Could reuse encryptor but wanted to show reconstructing TextEncryptor
		TextEncryptor decryptor = Encryptors.text(Constants.PASSWORD, salt);
		String decryptedText = decryptor.decrypt(encryptedText);
		System.out.println("Decrypted text: \"" + decryptedText + "\"");
		if (TEXT.equals(decryptedText)) {
			System.out.println("Success: decrypted text matches");
		} else {
			System.out.println("Failed: decrypted text does not match");
		}
	}

	private static String generateStorngPasswordHashBouncyCastly(
			String password, String salt, Integer key)
			throws NoSuchAlgorithmException, InvalidKeySpecException,
			UnsupportedEncodingException {

		PKCS5S2ParametersGenerator gen = new PKCS5S2ParametersGenerator(
				new SHA256Digest());
		gen.init(password.getBytes("UTF-8"), salt.getBytes(),
				Constants.ITARATIONS);
		byte[] dk = ((KeyParameter) gen.generateDerivedParameters(key))
				.getKey();
		return toHex(dk);
	}

	private static String generateStorngPasswordHashJDK(String password,
			String salt, Integer key) throws NoSuchAlgorithmException,
			InvalidKeySpecException {

		char[] chars = password.toCharArray();
		PBEKeySpec spec = new PBEKeySpec(chars, salt.getBytes(),
				Constants.ITARATIONS, key);
		SecretKeyFactory skf = SecretKeyFactory
				.getInstance("PBKDF2WithHmacSHA256");
		byte[] hash = skf.generateSecret(spec).getEncoded();
		return toHex(hash);
	}

	private static String getSalt() throws NoSuchAlgorithmException {

		SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
		byte[] salt = new byte[16];
		sr.nextBytes(salt);
		return salt.toString();
	}

	private static String toHex(byte[] array) throws NoSuchAlgorithmException {

		BigInteger bi = new BigInteger(1, array);
		String hex = bi.toString(16);
		int paddingLength = (array.length * 2) - hex.length();
		if (paddingLength > 0) {
			return String.format("%0" + paddingLength + "d", 0) + hex;
		} else {
			return hex;
		}
	}

	private static String encrypt(String input, String key)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

		byte[] crypted = null;
		SecretKeySpec skey = new SecretKeySpec(Base64.decode(key), "AES");
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, skey);
		crypted = cipher.doFinal(input.getBytes());
		return new String(Base64.encode(crypted));
	}

	private static String decrypt(String input, String key)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

		byte[] output = null;
		SecretKeySpec skey = new SecretKeySpec(Base64.decode(key), "AES");
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, skey);
		output = cipher.doFinal(Base64.decode(input));
		return new String(output);
	}
}
