package com.gconsulting.ed;

import java.util.Arrays;
import java.util.Random;

import org.apache.commons.codec.binary.Base32;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorConfig;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;
import com.warrenstrange.googleauth.GoogleAuthenticatorQRGenerator;

public class TOTP {

	private static final Log log = LogFactory.getLog(TOTP.class);

	private static final Integer SECRET_SIZE = 40;
	private static final Integer NUMBER_OF_SCRATCH_CODES = 1;
	private static final Integer SCRATCH_CODE_SIZE = 6;
	private static final String KEY = "DPCHFIEFYFXAPQZX";

	public static void main(String[] args) {

		// Allocating the buffer
		byte[] buffer = new byte[SECRET_SIZE];

		// Filling the buffer with random numbers.
		// Notice: you want to reuse the same random generator
		// while generating larger random number sequences.
		new Random().nextBytes(buffer);
		// Getting the key and converting it to Base32
		Base32 codec = new Base32();
		byte[] secretKey = Arrays.copyOf(buffer, SECRET_SIZE);
		byte[] bEncodedKey = codec.encode(secretKey);
		String encodedKey = new String(bEncodedKey);
		log.info("Secret: " + encodedKey);
		GoogleAuthenticator gAuth = new GoogleAuthenticator();
		GoogleAuthenticatorKey key1 = gAuth.createCredentials();
		log.info("QR: " + GoogleAuthenticatorQRGenerator.getOtpAuthTotpURL("provider", "user", key1));
		log.info("QR: " + GoogleAuthenticatorQRGenerator.getOtpAuthURL("provider", "user", key1));
		log.info("Key: " + key1.getKey()); 
		log.info("Is valid: " + gAuth.authorize(KEY, 740117));
	}
}
