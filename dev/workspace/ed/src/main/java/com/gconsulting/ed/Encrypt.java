package com.gconsulting.ed;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.util.encoders.Base64;

public class Encrypt {

	private static String TEXT_TO_ENCRYPT = "TEXT";
	
	public static void main(String[] args)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		
		System.out.println("Text to encrypt: " + TEXT_TO_ENCRYPT);
		System.out.println("Encrypted text: " + encrypt(TEXT_TO_ENCRYPT));
	}

	private static String encrypt(String input)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

		byte[] crypted = null;
		SecretKeySpec skey = new SecretKeySpec(Base64.decode(Constants.KEY_NEW), "AES");
		Cipher cipher = Cipher.getInstance("https://docs.oracle.com/javase/8/docs/api/index.html?javax/crypto/Cipher.html");
		cipher.init(Cipher.ENCRYPT_MODE, skey);
		crypted = cipher.doFinal(input.getBytes());
		return new String(Base64.encode(crypted));
	}
}
