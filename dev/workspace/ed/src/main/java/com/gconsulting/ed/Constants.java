package com.gconsulting.ed;

public class Constants {
	
	public static final String PASSWORD = "password";
	public static final String KEY_OLD = "YOUR MASTER PASSWORD HERE";
	public static final String KEY_NEW = "YOUR MASTER PASSWORD HERE";
	public static final String TEXT = "TEXT";
	public static final Integer KEY_SIZE1 = 128;
	public static final Integer KEY_SIZE2 = 128;
	public static final Integer ITARATIONS = 1024;
}
