package com.gconsulting.ed;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.util.encoders.Base64;

public class Decrypt {

	private static String TEXT_TO_DECRYPT = "TEXT";
	
	public static void main(String[] args)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		
		System.out.println("Text to encrypt: " + TEXT_TO_DECRYPT);
		System.out.println("Decrypted text: " + decrypt(TEXT_TO_DECRYPT));
	}
	
	private static String decrypt(String input)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

		byte[] output = null;
		SecretKeySpec skey = new SecretKeySpec(Base64.decode(Constants.KEY_OLD), "AES");
		Cipher cipher = Cipher.getInstance("https://docs.oracle.com/javase/8/docs/api/index.html?javax/crypto/Cipher.html");
		cipher.init(Cipher.DECRYPT_MODE, skey);
		byte[] inputByte = Base64.decode(input);
		output = cipher.doFinal(inputByte);
		return new String(output);
	}
}
