package com.gsconsulting.bitnow.server.restInterface;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gsconsulting.bitnow.model.Market;
import com.gsconsulting.bitnow.server.model.ExchangeRate;

public class KRAKENInterface implements RESTInterface {

	@Override
	public List<Market> getPairs(JSONObject source) {

		List<Market> result = new ArrayList<>();
		JSONObject data = source.getJSONObject("pairs");
		JSONArray names = data.names();
		for (int i = 0; i < names.length(); i++) {
			Market market = new Market(
					(new String((String) names.get(i))).toUpperCase(),
					names.get(i) + " market");
			result.add(market);
		}
		return result;
	}

	public ExchangeRate getTicker(JSONObject source) throws JSONException {

		ExchangeRate result = new ExchangeRate();
		JSONObject data = source.getJSONObject("result");
		JSONObject returnObject = data.getJSONObject("XXBTZEUR");
		JSONArray lastPrice = returnObject.getJSONArray("c");
		double price = new Double((String) lastPrice.get(0));
		result.setRate(new BigDecimal(price));
		result.setTime(Calendar.getInstance().getTimeInMillis() / 1000);
		return result;
	}

}
