package com.gsconsulting.bitnow.server.util;

public class Constants {

	public static final String[] APPLICATION_CONTEXT = {
			"applicationContext-dao.xml", "applicationContext-resources.xml",
			"applicationContext-service.xml" };
	public static final String WALLET_NODE_PROPERTIES_FILE_NAME = "node.properties";
	public static final String REST_BASE_PACKAGE = "com.gsconsulting.bitnow.server.restInterface";
	public static final Integer TRANSACTION_DONE_CONFIRMATIONS = 6;
	public static final Integer TRANSACTION_SELL_DONE_CONFIRMATIONS = 3;
}
