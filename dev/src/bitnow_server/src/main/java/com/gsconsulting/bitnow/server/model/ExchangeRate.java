package com.gsconsulting.bitnow.server.model;

import java.math.BigDecimal;

public class ExchangeRate {

	private Long time;
	private BigDecimal rate;

	public ExchangeRate() {
		super();
	}

	public ExchangeRate(Long time, BigDecimal rate) {
		super();
		this.time = time;
		this.rate = rate;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

}
