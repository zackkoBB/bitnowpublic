package com.gsconsulting.bitnow.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.Thread.State;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.json.JSONObject;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.BTCAddress;
import com.gsconsulting.bitnow.model.Order;
import com.gsconsulting.bitnow.model.Receipt;
import com.gsconsulting.bitnow.model.Transaction;
import com.gsconsulting.bitnow.model.TransactionWallet;
import com.gsconsulting.bitnow.model.Wallet;
import com.gsconsulting.bitnow.model.enums.AccountStatus;
import com.gsconsulting.bitnow.model.enums.AccountType;
import com.gsconsulting.bitnow.model.enums.TransactionStatus;
import com.gsconsulting.bitnow.model.enums.TransactionType;
import com.gsconsulting.bitnow.model.enums.TransactionWalletType;
import com.gsconsulting.bitnow.model.service.AccountManager;
import com.gsconsulting.bitnow.model.service.BTCAddressManager;
import com.gsconsulting.bitnow.model.service.ConfigurationManager;
import com.gsconsulting.bitnow.model.service.OrderManager;
import com.gsconsulting.bitnow.model.service.ReadingsManager;
import com.gsconsulting.bitnow.model.service.ReceiptManager;
import com.gsconsulting.bitnow.model.service.TransactionManager;
import com.gsconsulting.bitnow.model.service.TransactionWalletManager;
import com.gsconsulting.bitnow.model.service.WalletManager;
import com.gsconsulting.bitnow.model.util.Constants;
import com.gsconsulting.bitnow.model.util.CryptService;
import com.gsconsulting.bitnow.postepayBalance.PPBalanceRetriever;
import com.gsconsulting.bitnow.postepayBalance.exception.PPBalanceRetrieverException;
import com.gsconsulting.bitnow.postepayBalance.model.Configuration;
import com.gsconsulting.bitnow.server.exception.ServerException;
import com.gsconsulting.bitnow.server.model.ExchangeRate;
import com.gsconsulting.bitnow.server.restInterface.RESTInterface;
import com.neemre.btcdcli4j.core.BitcoindException;
import com.neemre.btcdcli4j.core.CommunicationException;
import com.neemre.btcdcli4j.core.client.BtcdClient;
import com.neemre.btcdcli4j.core.client.BtcdClientImpl;
import com.neemre.btcdcli4j.core.domain.PaymentOverview;
import com.neemre.btcdcli4j.daemon.BtcdDaemon;
import com.neemre.btcdcli4j.daemon.BtcdDaemonImpl;
import com.neemre.btcdcli4j.daemon.event.WalletListener;

@SuppressWarnings("unused")
public class COGE {

	protected final Log log = LogFactory.getLog(getClass());

	private Thread cogeThread;
	private COGERunnable cogeRunnable;
	private Client RESTClient;
	private BtcdClient walletClient;
	private BtcdDaemon walletDaemon;
	private Wallet BTCWallet;
	private Wallet EURWallet;
	private ConfigurationManager configurationManager;
	private OrderManager orderManager;
	private TransactionManager transactionManager;
	private AccountManager accountManager;
	private WalletManager walletManager;
	private ReadingsManager readingsManager;
	private ReceiptManager receiptManager;
	private BTCAddressManager btcAddressManager;
	private TransactionWalletManager transactionWalletManager;
	private ClassPathXmlApplicationContext context;

	private String walletFeeBuyAccount;
	private String walletFeeSellAccount;
	private String walletPassphrase;
	private PPBalanceRetriever ppBalanceRetriever;
	private List<Account> accounts;
	private String exchange;
	private String exchangeRateAPI;
	private Integer COGEPollingInterval;
	private Long transactionExpiryTime;
	private String feeSend;
	private List<String> notAllowedTransactions;
	private SimpleDateFormat dateFormatter;

	/**
	 * single instance of COGE
	 */
	private static final COGE INSTANCE = new COGE();

	private COGE() {

		try {
			initManagers();
			initRESTClient();
			initBTCWallet();
			initEURWallet();
			initCOGE();
		} catch (ServerException e) {
			log.error("Starting COGE: " + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Refresh all the balances
	 * 
	 */
	private void checkBalancesEUR(AccountType type) {

		ppBalanceRetriever = new PPBalanceRetriever();
		accounts = accountManager.getAccountByType(type.ordinal(),
				AccountStatus.ACCOUNT_NOT_ACTIVE);
		for (Account account : accounts) {
			try {
				ppBalanceRetriever.setConfiguration(new Configuration(
						CryptService.decrypt(account.getWebUsername()),
						CryptService.decrypt(account.getWebPassword()),
						CryptService.decrypt(account.getNumber()),
						Constants.MAX_POSTEPAY_RETRIEVER_TRANSACTIONS));
				if (account.getLastReadStatus() != AccountStatus.ACCOUNT_NOT_ACTIVE
						.ordinal()) {
					ppBalanceRetriever.refresh();
					account.setBalance(new Double(ppBalanceRetriever
							.getGeneralInfo().getSaldoDisponibile()
							.replace(",", ".")));
					account.setDailyBalance(0D);
					account.setLastRead(Calendar.getInstance()
							.getTimeInMillis() / 1000);
					account.setLastReadStatus(AccountStatus.ACCOUNT_LAST_READ_OK
							.ordinal());
					accountManager.update(account);
				}
			} catch (PPBalanceRetrieverException | InvalidKeyException
					| NoSuchAlgorithmException | NoSuchPaddingException
					| IllegalBlockSizeException | BadPaddingException e) {
				log.error("Exception: " + e.getMessage());
				e.printStackTrace();
				account.setLastReadStatus(AccountStatus.ACCOUNT_LAST_READ_ERROR
						.ordinal());
				accountManager.update(account);
			}
		}
	}

	private void initManagers() throws ServerException {

		dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
		context = new ClassPathXmlApplicationContext(
				com.gsconsulting.bitnow.server.util.Constants.APPLICATION_CONTEXT);
		configurationManager = (ConfigurationManager) context
				.getBean("configurationManager");
		if (configurationManager != null) {
			walletFeeBuyAccount = new String(configurationManager
					.getConfigurationByKey(Constants.WALLET_FEE_ACCOUNT_BUY)
					.getValue());
			walletFeeSellAccount = new String(configurationManager
					.getConfigurationByKey(Constants.WALLET_FEE_ACCOUNT_SELL)
					.getValue());
			walletPassphrase = new String(configurationManager
					.getConfigurationByKey(Constants.WALLET_PASSPHRASE)
					.getValue());
			feeSend = new String(configurationManager.getConfigurationByKey(
					Constants.WALLET_SEND_FEE).getValue());
			exchange = configurationManager.getConfigurationByKey(
					Constants.EXCHANGE_RATE_EXCHANGE).getValue();
			exchangeRateAPI = configurationManager.getConfigurationByKey(
					Constants.EXCHANGE_RATE_API).getValue();
			COGEPollingInterval = new Integer(configurationManager
					.getConfigurationByKey(Constants.COGE_POLLING_INTERVAL)
					.getValue());
			transactionExpiryTime = new Long(configurationManager
					.getConfigurationByKey(Constants.TRANSACTION_EXPIRY_TIME)
					.getValue());
			notAllowedTransactions = new ArrayList<>(
					Arrays.asList(configurationManager
							.getConfigurationByKey(
									Constants.NOT_ALLOWED_TRANSACTIONS)
							.getValue().split(",")));
		} else {
			throw new ServerException("configurationManager is Null");
		}
		orderManager = (OrderManager) context.getBean("orderManager");
		if (orderManager != null) {
		} else {
			throw new ServerException("orderManager is Null");
		}
		transactionManager = (TransactionManager) context
				.getBean("transactionHistoryManager");
		if (transactionManager != null) {
		} else {
			throw new ServerException("transactionManager is Null");
		}
		accountManager = (AccountManager) context.getBean("accountManager");
		if (accountManager != null) {
			checkBalancesEUR(AccountType.EUR_POSTEPAY);
		} else {
			throw new ServerException("accountManager is Null");
		}
		walletManager = (WalletManager) context.getBean("walletManager");
		if (walletManager != null) {
		} else {
			throw new ServerException("walletManager is Null");
		}
		readingsManager = (ReadingsManager) context.getBean("readingsManager");
		if (readingsManager != null) {
		} else {
			throw new ServerException("readingsManager is Null");
		}
		receiptManager = (ReceiptManager) context.getBean("receiptManager");
		if (receiptManager != null) {
		} else {
			throw new ServerException("receiptManager is Null");
		}
		btcAddressManager = (BTCAddressManager) context
				.getBean("btcAddressManager");
		if (btcAddressManager != null) {
		} else {
			throw new ServerException("btcAddressManager is Null");
		}
		transactionWalletManager = (TransactionWalletManager) context
				.getBean("transactionWalletManager");
		if (transactionWalletManager != null) {
		} else {
			throw new ServerException("transactionWalletManager is Null");
		}
	}

	private void initRESTClient() {

		ClientConfig config = new ClientConfig();
		config = config.property(ClientProperties.CONNECT_TIMEOUT,
				Constants.REST_CONNECTION_TIMEOUT);
		config = config.property(ClientProperties.READ_TIMEOUT,
				Constants.REST_READ_TIMEOUT);
		RESTClient = ClientBuilder.newClient(config);
	}

	/**
	 * Refresh Default receiving address for EUR accounts
	 * 
	 */
	private String refreshReceivingAddress(Account account, boolean create)
			throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException, BitcoindException, CommunicationException {

		String address = CryptService.encrypt(walletClient
				.getAccountAddress(CryptService.decrypt(account.getId())));
		if (create) {
			if (btcAddressManager.getBTCAddressById(CryptService
					.decrypt(account.getNumber())) == null) {
				btcAddressManager.create(new BTCAddress(account.getUser(),
						CryptService.decrypt(account.getNumber()), Calendar
								.getInstance().getTimeInMillis() / 1000));
			}
		}
		account.setNumber(address);
		accountManager.update(account);
		return address;
	}

	private void initBTCWallet() throws ServerException {

		if (walletManager != null) {
			BTCWallet = walletManager.getWalletByKey(AccountType.BTC.ordinal());
		}
		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
		CloseableHttpClient httpProvider = HttpClients.custom()
				.setConnectionManager(cm).build();
		Properties nodeConfig = new Properties();
		InputStream is;
		is = ClassLoader
				.getSystemResourceAsStream(com.gsconsulting.bitnow.server.util.Constants.WALLET_NODE_PROPERTIES_FILE_NAME);
		try {
			nodeConfig.load(is);
			// log.info("1######################################");
			walletClient = new BtcdClientImpl(httpProvider, nodeConfig);
			// log.info("2######################################");
			is.close();
			if (walletClient != null) {
				/*
				 * Set default fee
				 */
				walletClient.setTxFee(new BigDecimal(feeSend));
				for (Account account : accountManager.getAccountByType(
						AccountType.EUR.ordinal(),
						AccountStatus.ACCOUNT_NOT_ACTIVE)) {
					refreshReceivingAddress(account, false);
				}
				/*
				 * Set daemon
				 */
				walletDaemon = new BtcdDaemonImpl(walletClient);
				walletDaemon.addWalletListener(new WalletListener() {

					@Override
					public void walletChanged(
							com.neemre.btcdcli4j.core.domain.Transaction transaction) {

						/*
						 * 1. get the Account that belongs
						 * 
						 * 2. get order (SELL, account, WAITING_PAYMENT)
						 * 
						 * 3. update account address (number)
						 * 
						 * TODO: UNIQUE ON
						 * BTC!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
						 */
						String id = transaction.getTxId();
						String toAddress = transaction.getTo();
						for (PaymentOverview p : transaction.getDetails()) {
							toAddress = p.getAddress();
						}
						String accountId = new String();
						Integer confirmations = transaction.getConfirmations();
						BigDecimal amount = transaction.getAmount();
						log.info("New transaction detected!: " + id + " "
								+ toAddress + " " + amount.toString());
						try {
							accountId = walletClient.getAccount(toAddress);
						} catch (BitcoindException | CommunicationException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						log.info("Transaction account: " + accountId);
						try {
							if (accountId != null) {
								if (accountId.length() > 0) {
									Account account = null;
									for (Account eurAccount : accountManager.getAccountByType(
											AccountType.EUR.ordinal(),
											AccountStatus.ACCOUNT_NOT_ACTIVE)) {
										if (CryptService.decrypt(
												eurAccount.getId()).equals(
												accountId)) {
											account = eurAccount;
											break;
										}
									}
									if (account != null) {
										/*
										 * Check if new or existing: create
										 * update TransactionWallet
										 * 
										 * Only for INCOMING
										 */
										if ((confirmations > 0)
												&& (amount.doubleValue() > 0)) {
											for (TransactionWallet transactionWallet : transactionWalletManager
													.getTransactionByConfirmations(com.gsconsulting.bitnow.server.util.Constants.TRANSACTION_DONE_CONFIRMATIONS)) {
												if (transactionWallet
														.getTransactionHash()
														.equals(id)) {
													/*
													 * That is the one
													 */
													transactionWallet
															.setConfirmations(confirmations);
													transactionWalletManager
															.update(transactionWallet);
													break;
												}
											}
										} else if (amount.doubleValue() > 0) {
											transactionWalletManager
													.create(new TransactionWallet(
															null,
															TransactionWalletType.INCOMING
																	.ordinal(),
															CryptService
																	.decrypt(account
																			.getNumber()),
															amount.doubleValue(),
															confirmations,
															id,
															Calendar.getInstance()
																	.getTimeInMillis() / 1000,
															account));
											account.setLatestPayment(Calendar
													.getInstance()
													.getTimeInMillis() / 1000);
											accountManager.update(account);
										}
										/*
										 * Search if we have an order for that
										 * transaction
										 */
										for (Order order : orderManager
												.getOrderTypeByAccountAndStatus(
														TransactionType.SELL,
														account,
														TransactionStatus.WAITING_PAYMENT)) {
											Double error = Math.abs(order
													.getBtc()
													- amount.doubleValue());
											if (error <= Constants.BTC_PRECISION) {
												/*
												 * we have a transaction
												 */
												order.setTransactionHash(id);
												order.setStatus(TransactionStatus.PAYMENT_RECEIVED_WAITING_CONFIRMATION
														.ordinal());
												order.setModified(Calendar
														.getInstance()
														.getTimeInMillis() / 1000);
												orderManager.update(order);
											}
										}
										/*
										 * Update account address only for
										 * INCOMING
										 */
										if ((confirmations == 0)
												&& (amount.doubleValue() > 0)) {
											refreshReceivingAddress(account,
													true);
										}
									}
								}
							}
							/*
							 * add a transaction for outgoing (WITHDRAWS)
							 */
							if (amount.doubleValue() < 0) {
							}
						} catch (InvalidKeyException | NoSuchAlgorithmException
								| NoSuchPaddingException
								| IllegalBlockSizeException
								| BadPaddingException | BitcoindException
								| CommunicationException e) {
							log.error("New transaction" + e.getMessage());
							// e.printStackTrace();
							/*
							 * TODO: send email
							 */
						}
					}
				});
			} else {
				throw new ServerException("walletClient is Null");
			}
		} catch (BitcoindException | CommunicationException | IOException e) {
			log.error("Init node: " + e.getMessage());
			throw new ServerException(e);
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException e) {
			log.error("Init node: " + e.getMessage());
			throw new ServerException(e);
		}
	}

	private void initEURWallet() {

		if (walletManager != null) {
			EURWallet = walletManager.getWalletByKey(AccountType.EUR.ordinal());
		}
	}

	private void initCOGE() throws ServerException {

		cogeRunnable = new COGERunnable();
		cogeThread = new Thread(cogeRunnable);
		cogeThread
				.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
					@Override
					public void uncaughtException(Thread t, Throwable e) {
						log.error("ERROR! An exception occurred in "
								+ t.getName() + ". Cause: " + e.getMessage());
						e.printStackTrace();
					}
				});
		cogeThread.start();
	}

	public static COGE getInstance() {
		return INSTANCE;
	}

	/**
	 * Read a GET API
	 * 
	 * @param source
	 *            API for that Text API
	 * @return String result
	 */
	private String getAPI(String URI) {

		WebTarget target = RESTClient.target(getBaseURI(URI));
		Builder builder = target.request(MediaType.APPLICATION_JSON);
		Response response = builder.get();
		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}
		return response.readEntity(String.class);
	}

	private static URI getBaseURI(String url) {
		return UriBuilder.fromUri(url).build();
	}

	/**
	 * Get exchange rate marketPrefix from web
	 * 
	 * @return Map<String, List<ExchangeRate>> exchange rate marketPrefix
	 *         retrieved
	 */
	private ExchangeRate getExchangeRate(String exchange, String api) {

		try {
			// log.info(") " + api);
			JSONObject response = new JSONObject(getAPI(api));
			Class<?> restInterfaceClass = Class
					.forName(com.gsconsulting.bitnow.server.util.Constants.REST_BASE_PACKAGE
							+ "." + exchange + "Interface");
			RESTInterface restInterface = (RESTInterface) restInterfaceClass
					.newInstance();
			return restInterface.getTicker(response);
		} catch (Exception e) {
			log.error("Cannot get the ExchangeRate: " + api + " "
					+ e.getMessage());
			// e.printStackTrace();
			return null;
		}
	}

	/**
	 * Get wallet balance
	 * 
	 * @return ExchangeRate wallet balance retrieved
	 */
	private ExchangeRate getBTCWalletBalance() {

		BigDecimal result;
		try {
			result = walletClient.getWalletInfo().getBalance();
		} catch (Exception e) {
			log.error("Cannot get the WalletBalance: " + e.getMessage());
			return null;
		}
		return new ExchangeRate(
				Calendar.getInstance().getTimeInMillis() / 1000, result);
	}

	/**
	 * Adjust wallet Balance BTC based on current Transactions Orders
	 */
	private ExchangeRate adjustWalletBalanceBTC(TransactionType type,
			Double balance) {

		ExchangeRate result = new ExchangeRate(Calendar.getInstance()
				.getTimeInMillis() / 1000, new BigDecimal(balance));
		Double balanceInProcessing = orderManager.getOrderTypeVolumeBTC(type);
		if (balanceInProcessing != null) {
			// balanceInProcessing = balanceInProcessing;
			// * Constants.BTC_TO_SATOSHI;
			if ((result.getRate().doubleValue() - balanceInProcessing) < 0) {
				result.setRate(new BigDecimal(0d));
			} else {
				result.setRate(new BigDecimal(result.getRate().doubleValue()
						- balanceInProcessing));
			}
		}
		return result;
	}

	/**
	 * Adjust wallet Balance EUR based on current Transactions Orders
	 */
	private ExchangeRate adjustWalletBalanceEUR(TransactionType type,
			Double balance) {

		ExchangeRate result = new ExchangeRate(Calendar.getInstance()
				.getTimeInMillis() / 1000, new BigDecimal(balance));
		Double balanceInProcessing = orderManager.getOrderTypeVolumeEUR(type);
		if (balanceInProcessing != null) {
			// balanceInProcessing = balanceInProcessing;
			// * Constants.BTC_TO_SATOSHI;
			if ((result.getRate().doubleValue() - balanceInProcessing) < 0) {
				result.setRate(new BigDecimal(0d));
			} else {
				result.setRate(new BigDecimal(result.getRate().doubleValue()
						- balanceInProcessing));
			}
		}
		return result;
	}

	/**
	 * 
	 */
	public synchronized void read() {

		StringBuilder reading = new StringBuilder();
		reading.append("S [T " + Calendar.getInstance().getTimeInMillis()
				/ 1000 + "] ");
		/*
		 * Exchange rates
		 */
		ExchangeRate exchangeRateBTCEUR = getExchangeRate(exchange,
				exchangeRateAPI);
		ExchangeRate exchangeRateEURBTC = new ExchangeRate(0l,
				new BigDecimal(0));
		if (exchangeRateBTCEUR != null) {
			reading.append("[ER "
					+ exchangeRateBTCEUR.getRate().setScale(2,
							RoundingMode.CEILING) + "] ");
			exchangeRateEURBTC = new ExchangeRate(exchangeRateBTCEUR.getTime(),
					new BigDecimal(1d / exchangeRateBTCEUR.getRate()
							.doubleValue()));
			if (exchangeRateEURBTC != null) {
				reading.append("[ER "
						+ exchangeRateEURBTC.getRate().setScale(2,
								RoundingMode.CEILING) + "] ");
			}
		}
		/*
		 * Refresh BTC, EUR Wallet Balance
		 */
		if (walletManager != null) {
			/*
			 * BTC Wallet Balance
			 */
			ExchangeRate btcWalletBalance = getBTCWalletBalance();
			ExchangeRate btcWalletBalanceAvailable = adjustWalletBalanceBTC(
					TransactionType.BUY, BTCWallet.getBalance());
			if (BTCWallet != null) {
				BTCWallet.setBalance(btcWalletBalance.getRate().doubleValue());
				if (exchangeRateBTCEUR != null) {
					BTCWallet.setExchangeRate(exchangeRateBTCEUR.getRate()
							.doubleValue());
				}
				BTCWallet.setBalanceAvailable(btcWalletBalanceAvailable
						.getRate().doubleValue());
				BTCWallet.setLastModified(Calendar.getInstance()
						.getTimeInMillis() / 1000);
				walletManager.update(BTCWallet);
				reading.append("[W(" + BTCWallet.getType() + ") "
						+ BTCWallet.getBalance() + " "
						+ BTCWallet.getBalanceAvailable() + "] ");
			}
			/*
			 * EUR's Accounts balances
			 */
			Double totalEURBalance = 0d;
			for (Account account : accountManager
					.getAccountByType(AccountType.EUR.ordinal(),
							AccountStatus.ACCOUNT_NOT_ACTIVE)) {
				/*
				 * Check if it's a new (newly activated) account (number = null:
				 * no receiving address)
				 */
				if (account.getNumber().length() == 0) {
					try {
						account.setNumber(refreshReceivingAddress(account,
								false));
					} catch (InvalidKeyException | NoSuchAlgorithmException
							| NoSuchPaddingException
							| IllegalBlockSizeException | BadPaddingException
							| BitcoindException | CommunicationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				Double balance = account.getBalance();
				totalEURBalance += balance;
				Double balanceInProcessing = orderManager
						.getOrderTypeVolumeEURByUser(TransactionType.SELL,
								account.getUser());
				if (balanceInProcessing == null) {
					balanceInProcessing = 0d;
				}
				account.setBalanceAvailable(balance.doubleValue()
						- balanceInProcessing);
				account.setLastModified(Calendar.getInstance()
						.getTimeInMillis() / 1000);
				if (account.getLastReadStatus() == AccountStatus.ACCOUNT_SELLER
						.ordinal()) {
				} else {
					account.setLastReadStatus(AccountStatus.ACCOUNT_LAST_READ_OK
							.ordinal());
				}
				accountManager.update(account);
				reading.append("[A(" + account.getUser().getId() + ","
						+ account.getId() + "," + account.getType() + ") "
						+ account.getBalance() + " "
						+ account.getBalanceAvailable() + "] ");
			}
			/*
			 * EUR Wallet Balance
			 */
			Double eurWalletBalance = totalEURBalance;
			ExchangeRate eurWalletBalanceAvailable = adjustWalletBalanceEUR(
					TransactionType.SELL, eurWalletBalance);
			if (EURWallet != null) {
				EURWallet.setBalance(eurWalletBalance);
				if (exchangeRateEURBTC != null) {
					EURWallet.setExchangeRate(exchangeRateEURBTC.getRate()
							.doubleValue());
				}
				EURWallet.setBalanceAvailable(eurWalletBalanceAvailable
						.getRate().doubleValue());
				EURWallet.setLastModified(Calendar.getInstance()
						.getTimeInMillis() / 1000);
				walletManager.update(EURWallet);
				reading.append("[W(" + EURWallet.getType() + ") "
						+ EURWallet.getBalance() + " "
						+ EURWallet.getBalanceAvailable() + "] ");
			}
		}
		/*
		 * BTC's Accounts balances
		 */
		for (Account account : accountManager.getAccountByType(
				AccountType.BTC.ordinal(), AccountStatus.ACCOUNT_NOT_ACTIVE)) {
			try {
				BigDecimal balance;
				balance = walletClient.getBalance(CryptService.decrypt(account
						.getId()));
				account.setBalance(balance.doubleValue());
				Double balanceInProcessing = orderManager
						.getOrderTypeVolumeBTCByUser(TransactionType.BUY,
								account.getUser());
				if (balanceInProcessing == null) {
					balanceInProcessing = 0d;
				}
				/*
				 * Set account inactive if balance <= B0.4000
				 * && balanceInProcessing == 0
				 */
				account.setBalanceAvailable(balance.doubleValue()
						- balanceInProcessing);
				account.setLastModified(Calendar.getInstance()
						.getTimeInMillis() / 1000);
				account.setLastReadStatus(AccountStatus.ACCOUNT_LAST_READ_OK
						.ordinal());
				accountManager.update(account);
				reading.append("[A(" + account.getUser().getId() + ","
						+ account.getId() + "," + account.getType() + ") "
						+ account.getBalance() + " "
						+ account.getBalanceAvailable() + "] ");
			} catch (InvalidKeyException | NoSuchAlgorithmException
					| NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException | BitcoindException
					| CommunicationException e) {
				log.error("Getting Account Balance: "
						+ account.getUser().getId() + " " + account.getId()
						+ " " + account.getType());
				// e.printStackTrace();
			}
		}
		/*
		 * Save readings
		 */
		readingsManager.create(reading.toString());
	}

	public boolean cogeThreadIsAlive() {
		return cogeThread.isAlive();
	}

	public boolean cogeThreadIsInterrupted() {
		return cogeThread.isInterrupted();
	}

	public void cogeThreadCheckAccess() {
		cogeThread.checkAccess();
	}

	public State cogeThreadState() {
		return cogeThread.getState();
	}

	public void close() {
		cogeRunnable.close();
	}

	public void cogeThreadRestart() throws ServerException {

		COGERunnable cogeRunnable = new COGERunnable();
		cogeThread = new Thread(cogeRunnable);
		cogeThread
				.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
					@Override
					public void uncaughtException(Thread t, Throwable e) {
						log.error("ERROR! An exception occurred in "
								+ t.getName() + ". Cause: " + e.getMessage());
						e.printStackTrace();
					}
				});
		cogeThread.start();
	}

	public Wallet getBTCWallet() {
		return BTCWallet;
	}

	public void setBTCWallet(Wallet bTCWallet) {
		BTCWallet = bTCWallet;
	}

	public Map<String, BigDecimal> listAccounts() throws ServerException {

		try {
			return walletClient.listAccounts();
		} catch (BitcoindException | CommunicationException e) {
			log.error("ListAccounts: " + e.getMessage());
			throw new ServerException(e);
			// e.printStackTrace();
		}
	}

	public BtcdClient getWalletClient() {
		return walletClient;
	}

	public boolean isRunning() {
		return cogeThread.isAlive();
	}

	/**
	 * COGERunnable for retrieving info and keep accountability of the
	 * application GET: BTC/EUR exchange rate GET: BTC Wallet balance GET: EUR
	 * Wallet balance DOES: accounting
	 * 
	 * @author zackko
	 *
	 */
	private class COGERunnable implements Runnable {

		private boolean stop = false;

		/**
		 * Init usdMarket and sources for retrieving arbs
		 * 
		 * @throws UnsupportedEncodingException
		 * 
		 */
		private COGERunnable() {

		}

		/**
		 * Check Accounts:
		 * 
		 * 1.
		 * 
		 * 2.
		 * 
		 * 3.
		 * 
		 * @throws ServerException
		 * 
		 */
		private void checkAccountsAndOrders() {

			checkAccounts(AccountType.EUR_POSTEPAY);
			checkDoneOrders(TransactionType.BUY, TransactionStatus.BTC_SENT);
			checkDoneOrders(TransactionType.SELL, TransactionStatus.EUR_SENT);
			checkPaymentReceived();
			checkPaymentReceivedWaitingForConfirmationOrders();
			checkExpiredOrders();
		}

		/**
		 * Send bitcoins
		 * 
		 * @param account
		 *            from
		 * @param order
		 *            to send for
		 * @throws ServerException
		 */
		private void sendB(Account fromAccountEUR, Order order)
				throws ServerException {

			/*
			 * TODO: double check use order.getFee()
			 * 
			 * unlock/lock wallet
			 */
			Account fromAccountBTC = null;
			try {
				for (Account account : accountManager.getAccountByUser(
						fromAccountEUR.getUser(),
						AccountStatus.ACCOUNT_NOT_ACTIVE)) {
					if (account.getType() == AccountType.BTC.ordinal()) {
						fromAccountBTC = account;
						// fromAccountID = CryptService.decrypt(fromAccount
						// .getId());
						break;
					}
				}
				if (fromAccountBTC != null) {
					walletClient.walletPassphrase(
							CryptService.decrypt(walletPassphrase), 10);
					String transactionHash = walletClient.sendFrom(CryptService
							.decrypt(fromAccountBTC.getId()), order
							.getAddress(), (new BigDecimal(order.getBtc()
							.doubleValue()))
							.setScale(8, RoundingMode.HALF_DOWN), 6, "");
					walletClient.walletLock();
					/*
					 * Send fee
					 */
					walletClient.move(CryptService.decrypt(fromAccountBTC
							.getId()), walletFeeBuyAccount, (new BigDecimal(
							order.getFee())).setScale(8, RoundingMode.HALF_UP));
					fromAccountEUR.setLatestPayment(Calendar.getInstance()
							.getTimeInMillis() / 1000);
					fromAccountBTC.setLatestPayment(Calendar.getInstance()
							.getTimeInMillis() / 1000);
					accountManager.update(fromAccountEUR);
					accountManager.update(fromAccountBTC);
					log.info("Sent BTC. BTC: " + order.getAddress()
							+ " transaction hash: " + transactionHash
							+ " note: " + order.getNote());
					order.setTransactionHash(transactionHash);
					order.setModified(Calendar.getInstance().getTimeInMillis() / 1000);
					order.setStatus(TransactionStatus.BTC_SENT.ordinal());
					orderManager.update(order);
					transactionWalletManager.create(new TransactionWallet(null,
							TransactionWalletType.OUTGOING.ordinal(), order
									.getAddress(), order.getBtc()
									+ order.getFee(), 0, transactionHash,
							Calendar.getInstance().getTimeInMillis() / 1000,
							fromAccountEUR));
				}
			} catch (InvalidKeyException | NoSuchAlgorithmException
					| NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException e) {
				log.error("Accessing BTC account: " + fromAccountBTC.getId()
						+ " " + e.getMessage());
				// e.printStackTrace();
				throw new ServerException(e);
			} catch (BitcoindException | CommunicationException e) {
				log.error("SendFrom: " + e.getMessage());
				// e.printStackTrace();
				throw new ServerException(e);
			}

		}

		/**
		 * Check Accounts and process payments
		 * 
		 * @param type
		 *            account type to check
		 */
		private void checkAccounts(AccountType type) {

			if (ppBalanceRetriever == null) {
				ppBalanceRetriever = new PPBalanceRetriever();
			}
			for (Account account : accountManager.getAccountByType(
					type.ordinal(), AccountStatus.ACCOUNT_NOT_ACTIVE)) {
				try {
					if (account.getLastReadStatus() != AccountStatus.ACCOUNT_NOT_ACTIVE
							.ordinal()) {
						ppBalanceRetriever.setConfiguration(new Configuration(
								CryptService.decrypt(account.getWebUsername()),
								CryptService.decrypt(account.getWebPassword()),
								CryptService.decrypt(account.getNumber()),
								Constants.MAX_POSTEPAY_RETRIEVER_TRANSACTIONS));
						ppBalanceRetriever.refresh();
						account.setBalance(new Double(ppBalanceRetriever
								.getGeneralInfo().getSaldoDisponibile()
								.replace(",", ".")));
						account.setLastRead(Calendar.getInstance()
								.getTimeInMillis() / 1000);
						account.setLastReadStatus(AccountStatus.ACCOUNT_LAST_READ_OK
								.ordinal());
						/*
						 * TODO: check SaldoContabile for Performances
						 */
						for (com.gsconsulting.bitnow.postepayBalance.model.Transaction transaction : ppBalanceRetriever
								.getTransactions()) {
							// log.info(transaction.getDescrizione());
							if (transaction.getAccrediti() != null) {
								if (transaction.getAccrediti().length() > 0) {
									Double accrediti = new Double(transaction
											.getAccrediti().replace(",", "."));
									if (accrediti > 0d) {
										/*
										 * We have a new payment
										 * 
										 * Check for same Euro amount on Status
										 * WAITING_PAYMENT
										 * 
										 * TODO: check exchange rate (double
										 * check)
										 * 
										 * TODO: check for transaction (with
										 * accrediti) with no corresponding
										 * order
										 */
										for (Order order : orderManager
												.getOrderTypeByAccountAndStatus(
														TransactionType.BUY,
														account,
														TransactionStatus.WAITING_PAYMENT)) {
											Double error = Math.abs(order
													.getEuro() - accrediti);
											if (error <= Constants.VOLUME_PRECISION) {
												/*
												 * We have a corresponding
												 * transaction
												 */
												Date currentDate = Calendar
														.getInstance()
														.getTime();
												Date paymentDate;
												try {
													paymentDate = dateFormatter
															.parse(transaction
																	.getDataValuta());
													log.info("Payment Date: "
															+ paymentDate);
													/*
													 * Check Dates (2 DAYs
													 * interval)
													 */
													if ((currentDate.getTime() - paymentDate
															.getTime()) > Constants.LAST_TRANSACTION_SAME_AMOUNT_IN_MILLIS) {
														/*
														 * Payments comes from
														 * more that Constants.
														 * LAST_TRANSACTION_SAME_AMOUNT_IN_MILLIS
														 * milliseconds ago
														 * 
														 * CAN'T ACCEPT
														 */
														log.error("NOT ALLOWED TRANSACTION (TOO FAR): "
																+ transaction
																		.getDescrizione()
																+ " "
																+ transaction
																		.getDataValuta());
														/*
														 * TODO: send email
														 */
														order.setModified(currentDate
																.getTime() / 1000);
														order.setStatus(TransactionStatus.PAYMENT_RECEIVED_NOT_CONFIRMED
																.ordinal());
														order.setNote(transaction
																.getDescrizione());
														orderManager
																.update(order);
													} else {
														/*
														 * Check NOT ALLOWED
														 * TRANSACTIONS
														 */
														boolean allowed = true;
														for (String string : notAllowedTransactions) {
															if (transaction
																	.getDescrizione()
																	.contains(
																			string)) {
																allowed = false;
																break;
															}
														}
														if (allowed) {
															/*
															 * TODO: Check time
															 * (date + time) on
															 * transaction
															 * description date
															 * + time >
															 * order.getCreated
															 * () +
															 * TRANSACTION_EXPIRY_TIME
															 * =>
															 * PAYMENT_RECEIVED_NOT_CONFIRMED
															 */
															Integer iDataDescrizione = transaction
																	.getDescrizione()
																	.indexOf(
																			transaction
																					.getDataValuta());
															String time = transaction
																	.getDescrizione()
																	.substring(
																			iDataDescrizione + 11,
																			iDataDescrizione + 16);
															log.info("Payment Time: "
																	+ time);
															order.setModified(currentDate
																	.getTime() / 1000);
															order.setStatus(TransactionStatus.PAYMENT_RECEIVED
																	.ordinal());
															order.setNote(transaction
																	.getDescrizione());
															orderManager
																	.update(order);
															sendB(account,
																	order);
														} else {
															log.error("NOT ALLOWED TRANSACTION: "
																	+ transaction
																			.getDescrizione()
																	+ " "
																	+ order.getCode());
															/*
															 * TODO: send email
															 */order.setModified(currentDate
																	.getTime() / 1000);
															order.setStatus(TransactionStatus.PAYMENT_RECEIVED_NOT_CONFIRMED
																	.ordinal());
															order.setNote(transaction
																	.getDescrizione());
															orderManager
																	.update(order);
														}
													}
												} catch (ParseException e) {
													log.error("Parsing DataValuta: "
															+ transaction
																	.getDataValuta()
															+ " "
															+ e.getMessage());
													// e.printStackTrace();
												}
											}
										}
									}
								}
							}
						}
						accountManager.update(account);
					}
				} catch (PPBalanceRetrieverException e) {
					log.error("ppBalanceRetriever: " + e.getMessage());
					// e.printStackTrace();
					ppBalanceRetriever.close();
					ppBalanceRetriever = new PPBalanceRetriever();
					account.setLastReadStatus(AccountStatus.ACCOUNT_LAST_READ_ERROR
							.ordinal());
					accountManager.update(account);
				} catch (InvalidKeyException | NoSuchAlgorithmException
						| NoSuchPaddingException | IllegalBlockSizeException
						| BadPaddingException e) {
					log.error("Accessing " + account.getType() + " account: "
							+ e.getMessage());
					// e.printStackTrace();
				} catch (ServerException e) {
					log.error("Sending B " + account.getType() + " account: "
							+ e.getMessage());
					e.printStackTrace();
				}
			}
		}

		/**
		 * Check and process DONE Orders move to Transaction
		 * 
		 * 
		 * 
		 */
		private void checkDoneOrders(TransactionType type,
				TransactionStatus status) {

			for (Order order : orderManager.getOrderTypeByStatus(type, status)) {
				try {
					com.neemre.btcdcli4j.core.domain.Transaction transaction = walletClient
							.getTransaction(order.getTransactionHash());
					if (transaction.getConfirmations() >= com.gsconsulting.bitnow.server.util.Constants.TRANSACTION_DONE_CONFIRMATIONS) {
						if (type.equals(TransactionType.SELL)) {
							walletClient
									.move(walletClient.getAccount(order
											.getAddress()),
											walletFeeSellAccount,
											(new BigDecimal(order.getFee()))
													.setScale(
															8,
															RoundingMode.HALF_UP));
							/*
							 * Update account balance done in Dashboard
							 */
							// Account account = order.getAccount();
							// account.setBalance(account.getBalance()
							// - order.getEuro());
							// accountManager.update(account);
						}
						Transaction transactionDB = new Transaction(
								order.getCode()
										+ " "
										+ RandomStringUtils.randomAlphanumeric(
												9).toUpperCase(),
								order.getType(),
								order.getAddress(),
								order.getEmail(),
								order.getTransactionHash(),
								order.getBtc(),
								order.getEuro(),
								order.getFee(),
								order.getCreated(),
								Calendar.getInstance().getTimeInMillis() / 1000,
								order.getNote(), TransactionStatus.DONE
										.ordinal(), order.getAccount());
						transactionManager.create(transactionDB);
						/*
						 * UPDATE RECEIPT REFERENCES
						 */
						for (Receipt receipt : receiptManager
								.getReceiptByOrder(order)) {
							receipt.setOrder(null);
							receipt.setTransaction(transactionDB);
							receiptManager.update(receipt);
						}
						orderManager.delete(order);
					}
				} catch (BitcoindException | CommunicationException e) {
					log.error("DONE Transaction: " + e.getMessage());
					e.printStackTrace();
				}
			}
		}

		/**
		 * Check and process PAYMENT_RECEIVED_WAITING_CONFIRMATION Orders move
		 * to PAYMENT_RECEIVED
		 * 
		 * SELL orders only
		 * 
		 * TODO: send e-mail to seller/administrator
		 * 
		 */
		private void checkPaymentReceivedWaitingForConfirmationOrders() {

			for (Order order : orderManager.getOrderTypeByStatus(
					TransactionType.SELL,
					TransactionStatus.PAYMENT_RECEIVED_WAITING_CONFIRMATION)) {
				try {
					com.neemre.btcdcli4j.core.domain.Transaction transaction = walletClient
							.getTransaction(order.getTransactionHash());
					if (transaction.getConfirmations() >= com.gsconsulting.bitnow.server.util.Constants.TRANSACTION_SELL_DONE_CONFIRMATIONS) {
						order.setStatus(TransactionStatus.PAYMENT_RECEIVED
								.ordinal());
						order.setModified(Calendar.getInstance()
								.getTimeInMillis() / 1000);
						orderManager.update(order);
					}
				} catch (BitcoindException | CommunicationException e) {
					log.error("PAYMENT_RECEIVED_WAITING_CONFIRMATION Transaction: "
							+ e.getMessage());
					// e.printStackTrace();
				}
			}
		}

		/**
		 * Check and process PAYMENT_RECEIVED orders (problems in sending/order
		 * not allowed)
		 * 
		 * BUY orders only
		 * 
		 * TODO: send e-mail to seller/administrator
		 * 
		 */
		private void checkPaymentReceived() {

			for (Order order : orderManager.getOrderTypeByStatus(
					TransactionType.BUY, TransactionStatus.PAYMENT_RECEIVED)) {
				try {
					sendB(order.getAccount(), order);
				} catch (ServerException e) {
					log.error("Sending B " + order.getAccount().getType()
							+ " account: " + e.getMessage());
					e.printStackTrace();
				}
			}
		}

		/**
		 * Process expired orders
		 * 
		 * TODO: check orders in
		 * 
		 * 1. PAYMENT_RECEIVED && TransactionType.BUY: error can't be send email
		 * to Administrator
		 * 
		 * 2. PAYMENT_RECEIVED && TransactionType.SELL: waiting for seller to
		 * set MONEY_SENT
		 * 
		 */
		private void checkExpiredOrders() {

			Long timestamp = Calendar.getInstance().getTimeInMillis();
			timestamp -= transactionExpiryTime;
			timestamp /= 1000;
			for (Order order : orderManager.getOrderByStatusAndTimestampLesser(
					TransactionStatus.UNCONFIRMED, timestamp)) {
				Transaction transaction = new Transaction(
						order.getCode()
								+ " "
								+ RandomStringUtils.randomAlphanumeric(9)
										.toUpperCase(), order.getType(),
						order.getAddress(), order.getEmail(), "",
						order.getBtc(), order.getEuro(), order.getFee(),
						order.getCreated(), Calendar.getInstance()
								.getTimeInMillis() / 1000, order.getNote(),
						TransactionStatus.EXPIRED.ordinal(), order.getAccount());
				transactionManager.create(transaction);
				orderManager.delete(order);
			}
			for (Order order : orderManager.getOrderByStatusAndTimestampLesser(
					TransactionStatus.WAITING_PAYMENT, timestamp)) {
				Transaction transaction = new Transaction(
						order.getCode()
								+ " "
								+ RandomStringUtils.randomAlphanumeric(9)
										.toUpperCase(), order.getType(),
						order.getAddress(), order.getEmail(), "",
						order.getBtc(), order.getEuro(), order.getFee(),
						order.getCreated(), Calendar.getInstance()
								.getTimeInMillis() / 1000, order.getNote(),
						TransactionStatus.EXPIRED.ordinal(), order.getAccount());
				transactionManager.create(transaction);
				orderManager.delete(order);
			}
		}

		/**
		 * SendMany from wallet
		 * 
		 * @return String transactionHash
		 * @throws CommunicationException
		 * @throws BitcoindException
		 * @throws ServerException
		 */
		private String sendMany(String fromAccount,
				Map<String, BigDecimal> toAddresses, String comment)
				throws BitcoindException, CommunicationException {

			String result = new String();
			result = walletClient
					.sendMany(fromAccount, toAddresses, 1, comment);
			log.info("Return Message: " + result);
			return result;
		}

		/**
		 * Update unconfirmed Wallet Transactions
		 */
		private void checkWalletTransactions() {

			for (TransactionWallet walletTransaction : transactionWalletManager
					.getTransactionByConfirmations(com.gsconsulting.bitnow.server.util.Constants.TRANSACTION_SELL_DONE_CONFIRMATIONS)) {
				try {
					com.neemre.btcdcli4j.core.domain.Transaction transaction = walletClient
							.getTransaction(walletTransaction
									.getTransactionHash());
					if (transaction.getConfirmations() >= com.gsconsulting.bitnow.server.util.Constants.TRANSACTION_SELL_DONE_CONFIRMATIONS) {
						walletTransaction.setConfirmations(transaction
								.getConfirmations());
						transactionWalletManager.update(walletTransaction);
					}
				} catch (BitcoindException | CommunicationException e) {
					log.error("checkWalletTransactions Transaction: "
							+ e.getMessage());
					// e.printStackTrace();
				}
			}
		}

		/**
		 * 
		 */
		private void checkExchangeRateAndBalances() {
			read();
		}

		public synchronized void close() {
			stop = true;
			this.notify();
		}

		/**
		 * Run method of this thread
		 */
		public synchronized void run() {

			while (!stop) {
				try {
					checkAccountsAndOrders();
					checkExchangeRateAndBalances();
					checkWalletTransactions();
					this.wait(COGEPollingInterval);
					// System.gc();
				} catch (Throwable e) {
					log.error("Throwable: " + e.getMessage());
					e.printStackTrace();
					/*
					 * TODO: send email
					 */
				}
			}
			log.info("Stopping COGE...");
			ppBalanceRetriever.close();
			RESTClient.close();
			walletDaemon.shutdown();
			walletClient.close();
			log.info("- COGE Stopped -");
		}
	}
}
