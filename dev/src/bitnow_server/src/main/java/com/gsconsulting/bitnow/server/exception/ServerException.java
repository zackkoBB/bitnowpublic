package com.gsconsulting.bitnow.server.exception;

public class ServerException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3427527634265992831L;

	public ServerException() {
	}
	
	public ServerException(Exception e) {
		super(e);
	}

	public ServerException(String message) {
		super(message);
	}
}
