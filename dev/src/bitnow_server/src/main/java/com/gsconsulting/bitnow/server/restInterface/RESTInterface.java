package com.gsconsulting.bitnow.server.restInterface;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.gsconsulting.bitnow.model.Market;
import com.gsconsulting.bitnow.server.model.ExchangeRate;

public interface RESTInterface {

	public List<Market> getPairs(JSONObject source);
	public ExchangeRate getTicker(JSONObject source) throws JSONException;
}
