package com.gsconsulting.bitnow.server;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.gsconsulting.bitnow.server.exception.ServerException;
import com.neemre.btcdcli4j.core.BitcoindException;
import com.neemre.btcdcli4j.core.CommunicationException;

/**
 * Hello world!
 *
 */
public class Server {

	private final static Log log = LogFactory.getLog(Server.class);

	public static void main(String[] args) throws ServerException,
			InterruptedException, BitcoindException, CommunicationException {

		Scanner scan = new Scanner(System.in);
		log.info("- Server started -");
		System.out.print("Key from (1) File (2) Key ");
		String choose = scan.next();
		if (choose.equalsIgnoreCase("1")) {
			try {
				String key = FileUtils.readFileToString(new File(System
						.getProperty("user.dir") + File.separator + "key"));
				System.setProperty("key", key);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (choose.equalsIgnoreCase("2")) {
			System.out.print("Key: ");
			String key = scan.next();
			System.setProperty("key", key);
		}
		log.info("- Connecting to Bitcoin deamon...");
		COGE.getInstance();
		while (true) {
			System.out.println("");
			System.out.println(" - BTC Wallet - ");
			System.out.println("Balance: B"
					+ COGE.getInstance().getBTCWallet().getBalance());
			System.out.println("Balance Available: B"
					+ COGE.getInstance().getBTCWallet().getBalanceAvailable());
			System.out.println("Exchange Rate: €/B"
					+ COGE.getInstance().getBTCWallet().getExchangeRate());
			System.out.println("");
			System.out.println(" - Options -");
			System.out.println("1. List Accounts");
			System.out.println("2. Refresh");
			System.out.println("x. Exit");
			System.out.println("");
			System.out.print("Choice: ");
			String input = scan.next();
			if (input.equalsIgnoreCase("1")) {
				Map<String, BigDecimal> accounts = COGE.getInstance()
						.listAccounts();
				System.out.println("");
				System.out.println(" - Accounts -");
				for (String account : accounts.keySet()) {
					System.out.println("Account: " + account + " B"
							+ accounts.get(account));
				}
				System.out.println("");
			} else if (input.equalsIgnoreCase("2")) {
				COGE.getInstance().read();
			} else if (input.equalsIgnoreCase("x")) {
				scan.close();
				COGE.getInstance().close();
				while (COGE.getInstance().cogeThreadIsAlive()) {
				}
				break;
			}
		}
		log.info("- Server stopped -");
		System.exit(0);
	}
}
