package com.gsconsulting.bitnow.model.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.Wallet;

/**
 * Wallet Data Access Object (GenericDao) interface.
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
public interface WalletDao extends GenericDao<Wallet, String> {

    /**
     * Gets Wallet information based on Key.
     * 
     * @param Key the Wallet to be retrieved
     * @return Wallet retrieved
     * 
     * @throws 
     */
	@Transactional
	Wallet getWalletByKey(Integer key);	

    /**
     * Gets all Wallet entities in the db
     * 
     * @return List<Wallet> retrieved
     * 
     * @throws 
     */
    @Transactional
    List<Wallet> getAll();

    /**
     * Create a new Wallet
     * 
     * @throws 
     */
    @Transactional
    void create(Wallet wallet);

    /**
     * Update an existing Wallet instance
     * 
     * @throws 
     */
    @Transactional
    void update(Wallet wallet);

    /**
     * Delete an existing Wallet instance
     * 
     * @throws 
     */
    @Transactional
    void delete(Wallet wallet);
}
