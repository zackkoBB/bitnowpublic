package com.gsconsulting.bitnow.model.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.TransactionWallet;
import com.gsconsulting.bitnow.model.dao.TransactionWalletDao;
import com.gsconsulting.bitnow.model.service.TransactionWalletManager;

/**
 * Implementation of UserManager interface.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Service("transactionWalletManager")
public class TransactionWalletManagerImpl extends
		GenericManagerImpl<TransactionWallet, String> implements
		TransactionWalletManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2555587220430391189L;
	private TransactionWalletDao transactionDao;

	@Override
	@Autowired
	public void setTransactionWalletDao(
			final TransactionWalletDao transactionDao) {
		this.dao = transactionDao;
		this.transactionDao = transactionDao;
	}

	/**
	 * {@inheritDoc}
	 */
	public TransactionWallet getTransactionById(Long id) {
		return this.transactionDao.getTransactionById(id);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<TransactionWallet> getTransactionByAccount(Account account,
			Long timestamp) {
		return this.transactionDao.getTransactionByAccount(account, timestamp);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<TransactionWallet> getTransactionByAddress(String address) {
		return transactionDao.getTransactionByAddress(address);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<TransactionWallet> getTransactionByConfirmations(
			Integer confirmations) {
		return this.transactionDao.getTransactionByConfirmations(confirmations);
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(TransactionWallet transactionWallet) {
		this.transactionDao.create(transactionWallet);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(TransactionWallet transactionWallet) {
		this.transactionDao.update(transactionWallet);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(TransactionWallet transactionWallet) {
		this.transactionDao.delete(transactionWallet);
	}
}
