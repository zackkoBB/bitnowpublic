package com.gsconsulting.bitnow.model.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.BTCAddress;
import com.gsconsulting.bitnow.model.User;

/**
 * Seller Data Access Object (GenericDao) interface.
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
public interface BTCAddressDao extends GenericDao<BTCAddress, String> {

	/**
	 * Gets BTCAddress entity based on BTCAddress id
	 * 
	 * @param String
	 *            id of BTCAddress to be retreived
	 * @return BTCAddress retreived
	 * 
	 * @throws
	 */
	@Transactional
	public BTCAddress getBTCAddressById(String id);

	/**
	 * Get all BTCAddress owned by a user
	 * 
	 * @param User
	 *            user for whom the List<BTCAdress> needs to be retrieved
	 * @return List<BTCAddress> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<BTCAddress> getBTCAddressByUser(User user);

	/**
	 * Gets all BTCAddress entities in the db
	 * 
	 * @return List<BTCAddress> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<BTCAddress> getAll();

	/**
	 * Create a new BTCAddress
	 * 
	 * @throws
	 */
	@Transactional
	public void create(BTCAddress btcAddress);

	/**
	 * Update an existing BTCAddress instance
	 * 
	 * @throws
	 */
	@Transactional
	public void update(BTCAddress btcAddress);

	/**
	 * Delete an existing BTCAddress instance
	 * 
	 * @throws
	 */
	@Transactional
	public void delete(BTCAddress btcAddress);
}
