package com.gsconsulting.bitnow.model.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.gsconsulting.bitnow.model.dao.PaymentDao;
import com.gsconsulting.bitnow.model.Payment;
import com.gsconsulting.bitnow.model.service.PaymentManager;

public class PaymentManagerImpl implements PaymentManager {

	private PaymentDao paymentDao;

	public PaymentManagerImpl() {
	}

	public PaymentManagerImpl(PaymentDao paymentDao) {
		this.paymentDao = paymentDao;
	}

	@Override
	@Autowired
	public void setPaymentDao(final PaymentDao paymentDao) {
		this.paymentDao = paymentDao;
	}

	/**
	 * {@inheritDoc}
	 */
	public Payment getPaymentByKey(String key) {
		return paymentDao.getPaymentByKey(key);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Payment> getAllPayment() {
		return paymentDao.getAllPayment();
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(Payment payment) {
		paymentDao.create(payment);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Payment payment) {
		paymentDao.update(payment);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Payment payment) {
		paymentDao.delete(payment);
	}

}
