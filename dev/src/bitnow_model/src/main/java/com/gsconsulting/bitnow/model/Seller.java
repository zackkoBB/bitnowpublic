package com.gsconsulting.bitnow.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
@Entity
@Table(name = "seller")
public class Seller extends BaseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6284872808436538825L;
	private String cf;
	private String name;
	private String surname;
	private String postepay;
	private String pusername;
	private String ppassword;
	private Double balanceBTC;
	private Long latestPayment;
	private Double balanceEur;
	private Double balanceEurDaily;
	private Double fee;
	private Integer lastRead;

	public Seller() {
		super();
	}

	public Seller(String cf, String name, String surname, String postepay,
			String pusername, String ppassword, Double balanceBTC,
			Long latestPayment, Double balanceEur, Double balanceEurDaily,
			Double fee, Integer lastRead) {
		super();
		this.cf = cf;
		this.name = name;
		this.surname = surname;
		this.postepay = postepay;
		this.pusername = pusername;
		this.ppassword = ppassword;
		this.balanceBTC = balanceBTC;
		this.latestPayment = latestPayment;
		this.balanceEur = balanceEur;
		this.balanceEurDaily = balanceEurDaily;
		this.fee = fee;
		this.lastRead = lastRead;
	}

	@Id
	@Column(name = "CF")
	public String getCf() {
		return cf;
	}

	public void setCf(String cf) {
		this.cf = cf;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "surname")
	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	@Column(name = "postepay")
	public String getPostepay() {
		return postepay;
	}

	public void setPostepay(String postepay) {
		this.postepay = postepay;
	}

	@Column(name = "pusername")
	public String getPusername() {
		return pusername;
	}

	public void setPusername(String pusername) {
		this.pusername = pusername;
	}

	@Column(name = "ppassword")
	public String getPpassword() {
		return ppassword;
	}

	public void setPpassword(String ppassword) {
		this.ppassword = ppassword;
	}

	@Column(name = "balanceBTC")
	public Double getBalanceBTC() {
		return balanceBTC;
	}

	public void setBalanceBTC(Double balanceBTC) {
		this.balanceBTC = balanceBTC;
	}

	@Column(name = "latestPayment")
	public Long getLatestPayment() {
		return latestPayment;
	}

	public void setLatestPayment(Long latestPayment) {
		this.latestPayment = latestPayment;
	}

	@Column(name = "balanceEur")
	public Double getBalanceEur() {
		return balanceEur;
	}

	public void setBalanceEur(Double balanceEur) {
		this.balanceEur = balanceEur;
	}

	@Column(name = "balanceEurDaily")
	public Double getBalanceEurDaily() {
		return balanceEurDaily;
	}

	public void setBalanceEurDaily(Double balanceEurDaily) {
		this.balanceEurDaily = balanceEurDaily;
	}

	@Column(name = "fee")
	public Double getFee() {
		return fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	@Column(name = "last_read")
	public Integer getLastRead() {
		return lastRead;
	}

	public void setLastRead(Integer lastRead) {
		this.lastRead = lastRead;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
