package com.gsconsulting.bitnow.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
@Entity
@Table(name = "transaction")
public class Transaction extends BaseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2037091926629417218L;
	private String code;
	private Integer type;
	private String address;
	private String email;
	private String transactionHash;
	private Double btc;
	private Double euro;
	private Double fee;
	private Long created;
	private Long modified;
	private String note;
	private Integer status;
	private Account account;

	public Transaction() {
		super();
	}

	public Transaction(String code, Integer type, String address, String email,
			String transactionHash, Double btc, Double euro, Double fee,
			Long created, Long modified, String note, Integer status,
			Account account) {
		super();
		this.code = code;
		this.type = type;
		this.address = address;
		this.email = email;
		this.transactionHash = transactionHash;
		this.btc = btc;
		this.euro = euro;
		this.fee = fee;
		this.created = created;
		this.modified = modified;
		this.note = note;
		this.status = status;
		this.account = account;
	}

	@Id
	@Column(name = "code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "type")
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "transaction_hash")
	public String getTransactionHash() {
		return transactionHash;
	}

	public void setTransactionHash(String transactionHash) {
		this.transactionHash = transactionHash;
	}

	@Column(name = "btc")
	public Double getBtc() {
		return btc;
	}

	public void setBtc(Double btc) {
		this.btc = btc;
	}

	@Column(name = "euro")
	public Double getEuro() {
		return euro;
	}

	public void setEuro(Double euro) {
		this.euro = euro;
	}

	@Column(name = "fee")
	public Double getFee() {
		return fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	@Column(name = "created")
	public Long getCreated() {
		return created;
	}

	public void setCreated(Long created) {
		this.created = created;
	}

	@Column(name = "modified")
	public Long getModified() {
		return modified;
	}

	public void setModified(Long modified) {
		this.modified = modified;
	}

	@Column(name = "note")
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "account_user", referencedColumnName = "user"),
			@JoinColumn(name = "account_id", referencedColumnName = "id"),
			@JoinColumn(name = "account_type", referencedColumnName = "type") })
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
