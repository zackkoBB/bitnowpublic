package com.gsconsulting.bitnow.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
@Entity
@Table(name = "transaction_wallet")
public class TransactionWallet extends BaseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6350495176769206344L;
	private Long id;
	private Integer type;
	private String address;
	private Double btc;
	private Integer confirmations;
	private String transactionHash;
	private Long created;
	private Account account;

	public TransactionWallet() {
		super();
	}

	public TransactionWallet(Long id, Integer type, String address,
			Double btc, Integer confirmations, String transactionHash,
			Long created, Account account) {
		super();
		this.id = id;
		this.type = type;
		this.address = address;
		this.btc = btc;
		this.confirmations = confirmations;
		this.transactionHash = transactionHash;
		this.created = created;
		this.account = account;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "type")
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "btc")
	public Double getBtc() {
		return btc;
	}

	public void setBtc(Double btc) {
		this.btc = btc;
	}

	@Column(name = "confirmations")
	public Integer getConfirmations() {
		return confirmations;
	}

	public void setConfirmations(Integer confirmations) {
		this.confirmations = confirmations;
	}

	@Column(name = "transaction_hash")
	public String getTransactionHash() {
		return transactionHash;
	}

	public void setTransactionHash(String transactionHash) {
		this.transactionHash = transactionHash;
	}

	@Column(name = "created")
	public Long getCreated() {
		return created;
	}

	public void setCreated(Long created) {
		this.created = created;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "account_user", referencedColumnName = "user"),
			@JoinColumn(name = "account_id", referencedColumnName = "id"),
			@JoinColumn(name = "account_type", referencedColumnName = "type") })
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
