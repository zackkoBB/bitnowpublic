package com.gsconsulting.bitnow.model.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.dao.PaymentDao;
import com.gsconsulting.bitnow.model.Payment;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler </a>
 */
public interface PaymentManager {

	/**
	 * Convenience method for testing - allows you to mock the DAO and set it on
	 * an interface.
	 * 
	 * @param userDao
	 *            the UserDao implementation to use
	 */
	void setPaymentDao(PaymentDao paymentDao);
	
	/**
	 * Gets Payment information based on code.
	 * 
	 * @param String
	 *            key of the payment to be retrieved
	 * @return Payment retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public Payment getPaymentByKey(String key);

	/**
	 * Gets all Payment entities in the db
	 * 
	 * @return List<Payment> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<Payment> getAllPayment();

	/**
	 * Create a new Payment
	 * 
	 * @throws
	 */
	@Transactional
	public void create(Payment payment);

	/**
	 * Update an existing Payment instance
	 * 
	 * @throws
	 */
	@Transactional
	public void update(Payment payment);

	/**
	 * Delete an existing Payment instance
	 * 
	 * @throws
	 */
	@Transactional
	public void delete(Payment payment);
}
