package com.gsconsulting.bitnow.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.gsconsulting.bitnow.model.ids.SellerTransactionKey;

/**
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
@Entity
@IdClass(SellerTransactionKey.class)
@Table(name = "seller_transaction")
public class SellerTransaction extends BaseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6974125745410859795L;
	private Seller seller;
	private Transaction transaction;
		
	public SellerTransaction() {
		super();
	}

	public SellerTransaction(Seller seller, Transaction transaction) {
		super();
		this.seller = seller;
		this.transaction = transaction;
	}

	@Id
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="seller", referencedColumnName = "CF")
	public Seller getSeller() {
		return seller;
	}

	public void setSeller(Seller seller) {
		this.seller = seller;
	}

	@Id
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="transaction", referencedColumnName = "code")
	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}
}
