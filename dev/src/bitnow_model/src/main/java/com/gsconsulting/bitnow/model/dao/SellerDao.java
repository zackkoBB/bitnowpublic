package com.gsconsulting.bitnow.model.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.Seller;

/**
 * Seller Data Access Object (GenericDao) interface.
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
public interface SellerDao extends GenericDao<Seller, String> {

    /**
     * Gets Seller information based on code.
     * 
     * @param Cf code of the Seller to be retrieved
     * @return Seller retrieved
     * 
     * @throws 
     */
    @Transactional
    Seller getSellerByCf(String cf);

	/**
	 * Gets all Seller entities in the db based on BTCs available
	 * 
	 * @param Double btc min value
	 * @return List<Seller> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Seller> getSellerByBtc(Double btc);

    /**
     * Gets all Seller entities in the db
     * 
     * @return List<Seller> retrieved
     * 
     * @throws 
     */
    @Transactional
    List<Seller> getAll();

    /**
     * Create a new Seller
     * 
     * @throws 
     */
    @Transactional
    void create(Seller seller);

    /**
     * Update an existing Seller instance
     * 
     * @throws 
     */
    @Transactional
    void update(Seller seller);

    /**
     * Delete an existing Seller instance
     * 
     * @throws 
     */
    @Transactional
    void delete(Seller seller);
}
