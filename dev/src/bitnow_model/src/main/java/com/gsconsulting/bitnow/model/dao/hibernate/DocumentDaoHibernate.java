package com.gsconsulting.bitnow.model.dao.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.gsconsulting.bitnow.model.Document;
import com.gsconsulting.bitnow.model.User;
import com.gsconsulting.bitnow.model.dao.DocumentDao;

/**
 * This class interacts with Hibernate session to save/delete and retrieve User
 * objects.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler</a> Extended to
 *         implement Acegi UserDetailsService interface by David Carter
 *         david@carter.net Modified by <a href="mailto:bwnoll@gmail.com">Bryan
 *         Noll</a> to work with the new BaseDaoHibernate implementation that
 *         uses generics. Modified by jgarcia (updated to hibernate 4)
 */
@Repository("documentDao")
@SuppressWarnings("unchecked")
public class DocumentDaoHibernate extends GenericDaoHibernate<Document, Long>
		implements DocumentDao {

	/**
	 * Constructor that sets the entity to User.class.
	 */
	public DocumentDaoHibernate() {
		super(Document.class);
	}

	/**
	 * {@inheritDoc}
	 */
	public Document getDocumentById(Long id) {
		return (Document) getSession().get(Document.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Document> getDocumentByUser(User user) {

		Criteria documentCriteria = getSession().createCriteria(Document.class,
				"d");
		Criteria userCriteria = documentCriteria.createCriteria("user");
		userCriteria.add(Restrictions.eq("id", user.getId()));
		return documentCriteria.list();
	}

	/**
	 * {@inheritDoc}
	 */
	public Long create(Document document) {
		return (Long) getSession().save(document);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Document document) {
		getSession().update(document);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Document document) {
		getSession().delete(document);
	}

}
