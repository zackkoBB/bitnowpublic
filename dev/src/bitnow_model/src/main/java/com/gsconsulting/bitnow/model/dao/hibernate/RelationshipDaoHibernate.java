package com.gsconsulting.bitnow.model.dao.hibernate;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Repository;

import com.gsconsulting.bitnow.model.dao.RelationshipDao;
import com.gsconsulting.bitnow.model.SellerOrder;
import com.gsconsulting.bitnow.model.SellerTransaction;
import com.gsconsulting.bitnow.model.ids.SellerOrderKey;
import com.gsconsulting.bitnow.model.ids.SellerTransactionKey;

@Repository("relationshipDao")
public class RelationshipDaoHibernate implements RelationshipDao {

	/**
	 * Log variable for all child classes. Uses LogFactory.getLog(getClass())
	 * from Commons Logging
	 */
	protected final Log log = LogFactory.getLog(RelationshipDaoHibernate.class);
	@Resource
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}

	public Session getSession() throws HibernateException {

		Session sess = getSessionFactory().getCurrentSession();
		if (sess == null) {
			sess = getSessionFactory().openSession();
		}
		return sess;
	}

	@Autowired
	@Required
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public RelationshipDaoHibernate() {
	}

	@Override
	public SellerOrder getSellerOrderByKey(SellerOrderKey key) {
		return (SellerOrder) getSession().get(SellerOrder.class, key);
	}

	@Override
	public List<SellerOrder> getAllSellerOrder() {
		return (List<SellerOrder>) getSession().createCriteria(
				SellerOrder.class).list();
	}

	@Override
	public void create(SellerOrder sellerOrder) {
		getSession().save(sellerOrder);
	}

	@Override
	public void update(SellerOrder sellerOrder) {
		getSession().update(sellerOrder);
	}

	@Override
	public void delete(SellerOrder sellerOrder) {
		getSession().delete(sellerOrder);
	}

	@Override
	public SellerTransaction getSellerTransactionByKey(SellerTransactionKey key) {
		return (SellerTransaction) getSession().get(SellerTransaction.class,
				key);
	}

	@Override
	public List<SellerTransaction> getAllSellerTransaction() {
		return (List<SellerTransaction>) getSession().createCriteria(
				SellerTransaction.class).list();
	}

	@Override
	public void create(SellerTransaction sellerTransaction) {
		getSession().save(sellerTransaction);
	}

	@Override
	public void update(SellerTransaction sellerTransaction) {
		getSession().update(sellerTransaction);
	}

	@Override
	public void delete(SellerTransaction sellerTransaction) {
		getSession().delete(sellerTransaction);
	}

}
