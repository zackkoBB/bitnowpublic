package com.gsconsulting.bitnow.model.enums;

public enum AccountStatus {
	ACCOUNT_NOT_ACTIVE, ACCOUNT_LAST_READ_ERROR, ACCOUNT_LAST_READ_OK, ACCOUNT_SELLER;
}
