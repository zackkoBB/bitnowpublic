package com.gsconsulting.bitnow.model.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.TransactionWallet;

/**
 * TransactionWallet Data Access Object (GenericDao) interface.
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
public interface TransactionWalletDao extends
		GenericDao<TransactionWallet, String> {

	/**
	 * Gets TransactionWallet information based on code.
	 * 
	 * @param Id
	 *            id of the TransactionWallet to be retrieved
	 * @return TransactionWallet retrieved
	 * 
	 * @throws
	 */
	@Transactional
	TransactionWallet getTransactionById(Long id);

	/**
	 * Gets TransactionWallet information based on account.
	 * 
	 * @param Account
	 *            account of the Order to be retrieved
	 * @return List<TransactionWallet> orders retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<TransactionWallet> getTransactionByAccount(Account account,
			Long timestamp);

	/**
	 * Gets TransactionWallet information based on address.
	 * 
	 * @param address
	 *            address of the TransactionWallet to be retrieved
	 * @return List<TransactionWallet> transactions retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<TransactionWallet> getTransactionByAddress(String address);

	/**
	 * Gets TransactionWallet information based on confirmations (less than).
	 * 
	 * @param Integer
	 *            max number of confirmations allowed for the query
	 * @return List<TransactionWallet> transactions retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<TransactionWallet> getTransactionByConfirmations(Integer confirmations);

	/**
	 * Gets all TransactionWallet entities in the db
	 * 
	 * @return List<TransactionWallet> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<TransactionWallet> getAll();

	/**
	 * Create a new TransactionWallet
	 * 
	 * @throws
	 */
	@Transactional
	void create(TransactionWallet transactionWallet);

	/**
	 * Update an existing TransactionWallet instance
	 * 
	 * @throws
	 */
	@Transactional
	void update(TransactionWallet transactionWallet);

	/**
	 * Delete an existing TransactionWallet instance
	 * 
	 * @throws
	 */
	@Transactional
	void delete(TransactionWallet transactionWallet);
}
