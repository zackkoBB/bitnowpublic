package com.gsconsulting.bitnow.model.dao.hibernate;

import org.springframework.stereotype.Repository;

import com.gsconsulting.bitnow.model.Wallet;
import com.gsconsulting.bitnow.model.dao.WalletDao;

/**
 * This class interacts with Hibernate session to save/delete and retrieve User
 * objects.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler</a> Extended to
 *         implement Acegi UserDetailsService interface by David Carter
 *         david@carter.net Modified by <a href="mailto:bwnoll@gmail.com">Bryan
 *         Noll</a> to work with the new BaseDaoHibernate implementation that
 *         uses generics. Modified by jgarcia (updated to hibernate 4)
 */
@Repository("walletDao")
public class WalletDaoHibernate extends GenericDaoHibernate<Wallet, String>
		implements WalletDao {

	/**
	 * Constructor that sets the entity to User.class.
	 */
	public WalletDaoHibernate() {
		super(Wallet.class);
	}

	/**
	 * {@inheritDoc}
	 */
	public Wallet getWalletByKey(Integer key) {

		return (Wallet) getSession().get(Wallet.class, key);
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(Wallet wallet) {
		getSession().save(wallet);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Wallet wallet) {
		getSession().update(wallet);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Wallet wallet) {
		getSession().delete(wallet);
	}

}
