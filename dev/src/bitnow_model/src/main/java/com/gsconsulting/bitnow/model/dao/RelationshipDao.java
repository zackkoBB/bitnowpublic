package com.gsconsulting.bitnow.model.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.SellerOrder;
import com.gsconsulting.bitnow.model.SellerTransaction;
import com.gsconsulting.bitnow.model.ids.SellerOrderKey;
import com.gsconsulting.bitnow.model.ids.SellerTransactionKey;

public interface RelationshipDao {
	
	/**
	 * Gets SellerOrder information based on code.
	 * 
	 * @param SellerOrderKey
	 *            key of the sellerOrder to be retrieved
	 * @return SellerOrder retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public SellerOrder getSellerOrderByKey(SellerOrderKey key);

	/**
	 * Gets all SellerOrder entities in the db
	 * 
	 * @return List<SellerOrder> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<SellerOrder> getAllSellerOrder();

	/**
	 * Create a new SellerOrder
	 * 
	 * @throws
	 */
	@Transactional
	public void create(SellerOrder sellerOrder);

	/**
	 * Update an existing SellerOrder instance
	 * 
	 * @throws
	 */
	@Transactional
	public void update(SellerOrder sellerOrder);

	/**
	 * Delete an existing SellerOrder instance
	 * 
	 * @throws
	 */
	@Transactional
	public void delete(SellerOrder sellerOrder);
	
	/**
	 * Gets SellerTransaction information based on code.
	 * 
	 * @param SellerTransactionKey
	 *            key of the sellerTransaction to be retrieved
	 * @return SellerTransaction retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public SellerTransaction getSellerTransactionByKey(SellerTransactionKey key);

	/**
	 * Gets all SellerTransaction entities in the db
	 * 
	 * @return List<SellerTransaction> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<SellerTransaction> getAllSellerTransaction();

	/**
	 * Create a new SellerTransaction
	 * 
	 * @throws
	 */
	@Transactional
	public void create(SellerTransaction sellerTransaction);

	/**
	 * Update an existing SellerTransaction instance
	 * 
	 * @throws
	 */
	@Transactional
	public void update(SellerTransaction sellerTransaction);

	/**
	 * Delete an existing SellerTransaction instance
	 * 
	 * @throws
	 */
	@Transactional
	public void delete(SellerTransaction sellerTransaction);
}
