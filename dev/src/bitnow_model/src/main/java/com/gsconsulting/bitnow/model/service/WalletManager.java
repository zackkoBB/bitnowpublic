package com.gsconsulting.bitnow.model.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.Wallet;
import com.gsconsulting.bitnow.model.dao.WalletDao;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler </a>
 */
public interface WalletManager extends
		GenericManager<Wallet, String>, Serializable {

	/**
	 * Convenience method for testing - allows you to mock the DAO and set it on
	 * an interface.
	 * 
	 * @param userDao
	 *            the UserDao implementation to use
	 */
	public void setWalletDao(WalletDao walletDao);

    /**
     * Gets Wallet information based on Key.
     * 
     * @param Key the Wallet to be retrieved
     * @return Wallet retrieved
     * 
     * @throws 
     */
	@Transactional
	public Wallet getWalletByKey(Integer key);	
	
    /**
     * Gets all Wallet entities in the db
     * 
     * @return List<Wallet> retrieved
     * 
     * @throws 
     */
    @Transactional
    public List<Wallet> getAll();
	
    /**
	 * Create a new Wallet
	 * 
	 * @throws
	 */
	@Transactional
	public void create(Wallet wallet);

	/**
	 * Update an existing Wallet instance
	 * 
	 * @throws
	 */
	@Transactional
	public void update(Wallet wallet);

	/**
	 * Delete an existing Wallet instance
	 * 
	 * @throws
	 */
	@Transactional
	public void delete(Wallet wallet);
}
