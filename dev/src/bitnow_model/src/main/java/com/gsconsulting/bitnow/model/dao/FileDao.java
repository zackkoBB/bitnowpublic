package com.gsconsulting.bitnow.model.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.File;

/**
 * File Data Access Object (GenericDao) interface.
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
public interface FileDao extends GenericDao<File, Long> {

	/**
	 * Gets File information based on id.
	 * 
	 * @param Id
	 *            the File to be retrieved
	 * @return File retrieved
	 * 
	 * @throws
	 */
	@Transactional
	File getFileById(Long id);

	/**
	 * Gets all File entities in the db
	 * 
	 * @return List<File> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<File> getAll();

	/**
	 * Create a new File
	 * 
	 * @throws
	 */
	@Transactional
	Long create(byte[] data);

	/**
	 * Update an existing File instance
	 * 
	 * @throws
	 */
	@Transactional
	void update(Long id, byte[] data);

	/**
	 * Delete an existing File instance
	 * 
	 * @throws
	 */
	@Transactional
	void delete(Long id);
}
