package com.gsconsulting.bitnow.model.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.Document;
import com.gsconsulting.bitnow.model.User;

/**
 * Document Data Access Object (GenericDao) interface.
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
public interface DocumentDao extends GenericDao<Document, Long> {

	/**
	 * Gets Document information based on Key.
	 * 
	 * @param Key
	 *            the Document to be retrieved
	 * @return Document retrieved
	 * 
	 * @throws
	 */
	@Transactional
	Document getDocumentById(Long id);

	/**
	 * Gets Document information based on Order.
	 * 
	 * @param Order
	 *            the Document to be retrieved
	 * @return List<Document> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Document> getDocumentByUser(User user);

	/**
	 * Gets all Document entities in the db
	 * 
	 * @return List<Document> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Document> getAll();

	/**
	 * Create a new Document
	 * 
	 * @throws
	 */
	@Transactional
	Long create(Document document);

	/**
	 * Update an existing Document instance
	 * 
	 * @throws
	 */
	@Transactional
	void update(Document document);

	/**
	 * Delete an existing Document instance
	 * 
	 * @throws
	 */
	@Transactional
	void delete(Document document);
}
