package com.gsconsulting.bitnow.model.ids;

import java.io.Serializable;

import com.gsconsulting.bitnow.model.BaseObject;
import com.gsconsulting.bitnow.model.User;

/**
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
public class AccountKey extends BaseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8611381650861089978L;
	private User user;
	private String id;
	private Integer type;
	
	public AccountKey() {
		super();
	}

	public AccountKey(User user, String id, Integer type) {
		super();
		this.user = user;
		this.id = id;
		this.type = type;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}
}
