package com.gsconsulting.bitnow.model.dao.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.TransactionWallet;
import com.gsconsulting.bitnow.model.dao.TransactionWalletDao;
import com.gsconsulting.bitnow.model.util.Constants;

/**
 * This class interacts with Hibernate session to save/delete and retrieve User
 * objects.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler</a> Extended to
 *         implement Acegi UserDetailsService interface by David Carter
 *         david@carter.net Modified by <a href="mailto:bwnoll@gmail.com">Bryan
 *         Noll</a> to work with the new BaseDaoHibernate implementation that
 *         uses generics. Modified by jgarcia (updated to hibernate 4)
 */
@Repository("transactionWalletDao")
@SuppressWarnings("unchecked")
public class TransactionWalletDaoHibernate extends
		GenericDaoHibernate<TransactionWallet, String> implements
		TransactionWalletDao {

	/**
	 * Constructor that sets the entity to User.class.
	 */
	public TransactionWalletDaoHibernate() {
		super(TransactionWallet.class);
	}

	/**
	 * {@inheritDoc}
	 */
	public TransactionWallet getTransactionById(Long id) {

		Query qry = getSession().createQuery(
				"from " + TransactionWallet.class.getCanonicalName()
						+ " t where t.id=" + id);
		if (qry.list() != null) {
			if (qry.list().size() > 0) {
				return (TransactionWallet) qry.list().get(0);
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<TransactionWallet> getTransactionByAccount(Account account,
			Long timestamp) {

		Criteria transactionCriteria = getSession().createCriteria(
				TransactionWallet.class, "t");
		transactionCriteria.setMaxResults(Constants.MAX_HISTORY_TRANSACTION);
		transactionCriteria.add(Restrictions.ge("created", timestamp));
		Criteria accountCriteria = transactionCriteria
				.createCriteria("account");
		Criteria userCriteria = accountCriteria.createCriteria("user");
		userCriteria.add(Restrictions.eq("id", account.getUser().getId()));
		accountCriteria.add(Restrictions.eq("id", account.getId()));
		accountCriteria.add(Restrictions.eq("type", account.getType()));
		return transactionCriteria.list();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<TransactionWallet> getTransactionByAddress(String address) {

		return (List<TransactionWallet>) getSession().createQuery(
				"from " + TransactionWallet.class.getCanonicalName()
						+ " t where t.address='" + address + "'").list();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<TransactionWallet> getTransactionByConfirmations(
			Integer confirmations) {

		return (List<TransactionWallet>) getSession().createQuery(
				"from " + TransactionWallet.class.getCanonicalName()
						+ " t where t.confirmations<" + confirmations).list();
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(TransactionWallet transactionWallet) {
		getSession().save(transactionWallet);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(TransactionWallet transactionWallet) {
		getSession().update(transactionWallet);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(TransactionWallet transactionWallet) {
		getSession().delete(transactionWallet);
	}

}
