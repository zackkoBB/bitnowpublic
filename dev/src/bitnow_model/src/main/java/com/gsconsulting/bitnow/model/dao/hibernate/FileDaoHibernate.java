package com.gsconsulting.bitnow.model.dao.hibernate;

import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import com.gsconsulting.bitnow.model.File;
import com.gsconsulting.bitnow.model.dao.FileDao;

/**
 * This class interacts with Hibernate session to save/delete and retrieve User
 * objects.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler</a> Extended to
 *         implement Acegi UserDetailsService interface by David Carter
 *         david@carter.net Modified by <a href="mailto:bwnoll@gmail.com">Bryan
 *         Noll</a> to work with the new BaseDaoHibernate implementation that
 *         uses generics. Modified by jgarcia (updated to hibernate 4)
 */
@Repository("fileDao")
public class FileDaoHibernate extends GenericDaoHibernate<File, Long> implements
		FileDao {

	/**
	 * Constructor that sets the entity to User.class.
	 */
	public FileDaoHibernate() {
		super(File.class);
	}

	/**
	 * {@inheritDoc}
	 */
	public File getFileById(Long id) {
		return (File) getSession().get(File.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	public Long create(byte[] data) {
		return (Long) getSession().save(
				new File(null, Hibernate.getLobCreator(getSession())
						.createBlob(data)));
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Long id, byte[] data) {

		File file = (File) getSession().get(File.class, id);
		file.setData(Hibernate.getLobCreator(getSession()).createBlob(data));
		getSession().update(file);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Long id) {
		getSession().delete((File) getSession().get(File.class, id));
	}

}
