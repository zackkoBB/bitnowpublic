package com.gsconsulting.bitnow.model.util;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base32;
import org.bouncycastle.util.encoders.Base64;

import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;
import com.warrenstrange.googleauth.GoogleAuthenticatorQRGenerator;

public class CryptService {

	private static final String SYSTEM_PROPERTY_KEY_NAME = "key";
	private static final String GOOGLE_QR_BASE_URL = "https://www.google.com/chart?chs=widthxheight&cht=qr&chl=";
	/**
	 * single instance of CryptService
	 */
	private static final CryptService INSTANCE = new CryptService();
	private static final GoogleAuthenticator googleAuthenticator = new GoogleAuthenticator();

	private static String key;

	public static CryptService getInstance() {
		return INSTANCE;
	}

	private CryptService() {

		if (System.getProperty(SYSTEM_PROPERTY_KEY_NAME) != null) {
			key = System.getProperty(SYSTEM_PROPERTY_KEY_NAME);
		} else {
			System.out.print("Password: ");
			Scanner scan = new Scanner(System.in);
			String input = scan.next();
			key = input;
			scan.close();
		}
	}

	public static String encrypt(String input) throws NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidKeyException,
			IllegalBlockSizeException, BadPaddingException {

		byte[] crypted = null;
		SecretKeySpec skey = new SecretKeySpec(Base64.decode(key), "AES");
		Cipher cipher = Cipher.getInstance("https://docs.oracle.com/javase/8/docs/api/index.html?javax/crypto/Cipher.html");
		cipher.init(Cipher.ENCRYPT_MODE, skey);
		crypted = cipher.doFinal(input.getBytes());
		return new String(Base64.encode(crypted));
	}

	public static String decrypt(String input) throws NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidKeyException,
			IllegalBlockSizeException, BadPaddingException {

		byte[] output = null;
		SecretKeySpec skey = new SecretKeySpec(Base64.decode(key), "AES");
		Cipher cipher = Cipher.getInstance("https://docs.oracle.com/javase/8/docs/api/index.html?javax/crypto/Cipher.html");
		cipher.init(Cipher.DECRYPT_MODE, skey);
		byte[] inputByte = Base64.decode(input);
		output = cipher.doFinal(inputByte);
		return new String(output);
	}

	public static String encodedSecret(int lenght) {

		byte[] buffer = new byte[lenght];
		// Filling the buffer with random numbers.
		// Notice: you want to reuse the same random generator
		// while generating larger random number sequences.
		new Random().nextBytes(buffer);
		// Getting the key and converting it to Base32
		Base32 codec = new Base32();
		byte[] secretKey = Arrays.copyOf(buffer, lenght);
		byte[] bEncodedKey = codec.encode(secretKey);
		return new String(bEncodedKey);
	}

	public static boolean authorize(String secret, int code) {
		return googleAuthenticator.authorize(secret, code);
	}

	public static GoogleAuthenticatorKey getKey() {
		return googleAuthenticator.createCredentials();
	}

	public static String qrURL(Integer width, Integer height, String issuer,
			String user, GoogleAuthenticatorKey key) {

		String baseURL = GOOGLE_QR_BASE_URL.replace("width", width.toString());
		baseURL = baseURL.replace("height", height.toString());
		return baseURL
				+ GoogleAuthenticatorQRGenerator.getOtpAuthTotpURL(issuer,
						user, key);
	}

	public static String qrURL(String issuer, String user,
			GoogleAuthenticatorKey key) {
		return GoogleAuthenticatorQRGenerator.getOtpAuthURL(issuer, user, key);
	}
}
