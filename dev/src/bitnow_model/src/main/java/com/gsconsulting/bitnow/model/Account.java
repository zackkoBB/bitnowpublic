package com.gsconsulting.bitnow.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.gsconsulting.bitnow.model.ids.AccountKey;

/**
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
@Entity
@IdClass(AccountKey.class)
@Table(name = "account")
public class Account extends BaseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 565504882179934497L;
	private User user;
	private String id;
	private Integer type;
	private String number;
	private String owner;
	private String webUsername;
	private String webPassword;
	private Double balance;
	private Double balanceAvailable;
	private Long latestPayment;
	private Double dailyBalance;
	private Integer lastReadStatus;
	private Double fee;
	private Long created;
	private Long lastRead;
	private Long lastModified;

	public Account() {
		super();
	}

	public Account(User user, String id, Integer type, String number,
			String owner, String webUsername, String webPassword,
			Double balance, Double balanceAvailable, Long latestPayment, Double dailyBalance,
			Integer lastReadStatus, Double fee,
			Long created, Long lastRead, Long lastModified) {
		super();
		this.user = user;
		this.id = id;
		this.type = type;
		this.number = number;
		this.owner = owner;
		this.webUsername = webUsername;
		this.webPassword = webPassword;
		this.balance = balance;
		this.balanceAvailable = balanceAvailable;
		this.latestPayment = latestPayment;
		this.dailyBalance = dailyBalance;
		this.lastReadStatus = lastReadStatus;
		this.fee = fee;
		this.created = created;
		this.lastRead = lastRead;
		this.lastModified = lastModified;
	}

	@Id
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="user", referencedColumnName = "id")	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Id
	@Column(name = "id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Id
	@Column(name = "type")
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "number")
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Column(name = "owner")
	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	@Column(name = "web_username")
	public String getWebUsername() {
		return webUsername;
	}

	public void setWebUsername(String webUsername) {
		this.webUsername = webUsername;
	}

	@Column(name = "web_password")
	public String getWebPassword() {
		return webPassword;
	}

	public void setWebPassword(String webPassword) {
		this.webPassword = webPassword;
	}

	@Column(name = "balance")
	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	@Column(name = "balance_available")
	public Double getBalanceAvailable() {
		return balanceAvailable;
	}

	public void setBalanceAvailable(Double balanceAvailable) {
		this.balanceAvailable = balanceAvailable;
	}
	@Column(name = "latest_payment")
	public Long getLatestPayment() {
		return latestPayment;
	}

	public void setLatestPayment(Long latestPayment) {
		this.latestPayment = latestPayment;
	}

	@Column(name = "daily_balance")
	public Double getDailyBalance() {
		return dailyBalance;
	}

	public void setDailyBalance(Double dailyBalance) {
		this.dailyBalance = dailyBalance;
	}

	@Column(name = "last_read_status")
	public Integer getLastReadStatus() {
		return lastReadStatus;
	}

	public void setLastReadStatus(Integer lastReadStatus) {
		this.lastReadStatus = lastReadStatus;
	}

	@Column(name = "fee")
	public Double getFee() {
		return fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	@Column(name = "created")
	public Long getCreated() {
		return created;
	}

	public void setCreated(Long created) {
		this.created = created;
	}

	@Column(name = "last_read")
	public Long getLastRead() {
		return lastRead;
	}

	public void setLastRead(Long lastRead) {
		this.lastRead = lastRead;
	}

	@Column(name = "last_modified")
	public Long getLastModified() {
		return lastModified;
	}

	public void setLastModified(Long lastModified) {
		this.lastModified = lastModified;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
