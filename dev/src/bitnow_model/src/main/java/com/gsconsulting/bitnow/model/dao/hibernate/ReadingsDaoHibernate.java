package com.gsconsulting.bitnow.model.dao.hibernate;

import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import com.gsconsulting.bitnow.model.Readings;
import com.gsconsulting.bitnow.model.dao.ReadingsDao;

/**
 * This class interacts with Hibernate session to save/delete and retrieve User
 * objects.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler</a> Extended to
 *         implement Acegi UserDetailsService interface by David Carter
 *         david@carter.net Modified by <a href="mailto:bwnoll@gmail.com">Bryan
 *         Noll</a> to work with the new BaseDaoHibernate implementation that
 *         uses generics. Modified by jgarcia (updated to hibernate 4)
 */
@Repository("readingsDao")
public class ReadingsDaoHibernate extends GenericDaoHibernate<Readings, Long>
		implements ReadingsDao {

	/**
	 * Constructor that sets the entity to User.class.
	 */
	public ReadingsDaoHibernate() {
		super(Readings.class);
	}

	/**
	 * {@inheritDoc}
	 */
	public Readings getReadingsByKey(Long key) {

		return (Readings) getSession().get(Readings.class, key);
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(String readings) {
		getSession().save(
				new Readings(null, Hibernate.getLobCreator(getSession())
						.createBlob(readings.getBytes())));
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Long id, String readings) {

		Readings reading = (Readings) getSession().get(Readings.class, id);
		reading.setRead(Hibernate.getLobCreator(getSession()).createBlob(
				readings.getBytes()));
		getSession().update(reading);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Long id) {
		getSession().delete((Readings) getSession().get(Readings.class, id));
	}

}
