package com.gsconsulting.bitnow.model.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gsconsulting.bitnow.model.dao.RelationshipDao;
import com.gsconsulting.bitnow.model.SellerOrder;
import com.gsconsulting.bitnow.model.SellerTransaction;
import com.gsconsulting.bitnow.model.ids.SellerOrderKey;
import com.gsconsulting.bitnow.model.ids.SellerTransactionKey;
import com.gsconsulting.bitnow.model.service.RelationshipManager;

@Service("relationshipManager")
public class RelationshipManagerImpl implements RelationshipManager {

	private RelationshipDao relationshipDao;

	public RelationshipManagerImpl() {
	}

	public RelationshipManagerImpl(RelationshipDao relationshipDao) {
		this.relationshipDao = relationshipDao;
	}

	@Override
	@Autowired
	public void setRelationshipDao(final RelationshipDao relationshipDao) {
		this.relationshipDao = relationshipDao;
	}

	@Override
	public SellerOrder getSellerOrderByKey(SellerOrderKey key) {
		return relationshipDao.getSellerOrderByKey(key);
	}

	@Override
	public List<SellerOrder> getAllSellerOrder() {
		return relationshipDao.getAllSellerOrder();
	}

	@Override
	public void create(SellerOrder sellerOrder) {
		relationshipDao.create(sellerOrder);
	}

	@Override
	public void update(SellerOrder sellerOrder) {
		relationshipDao.update(sellerOrder);
	}

	@Override
	public void delete(SellerOrder sellerOrder) {
		relationshipDao.delete(sellerOrder);
	}

	@Override
	public SellerTransaction getSellerTransactionByKey(SellerTransactionKey key) {
		return relationshipDao.getSellerTransactionByKey(key);
	}

	@Override
	public List<SellerTransaction> getAllSellerTransaction() {
		return relationshipDao.getAllSellerTransaction();
	}

	@Override
	public void create(SellerTransaction sellerTransaction) {
		relationshipDao.create(sellerTransaction);
	}

	@Override
	public void update(SellerTransaction sellerTransaction) {
		relationshipDao.update(sellerTransaction);
	}

	@Override
	public void delete(SellerTransaction sellerTransaction) {
		relationshipDao.delete(sellerTransaction);
	}
}
