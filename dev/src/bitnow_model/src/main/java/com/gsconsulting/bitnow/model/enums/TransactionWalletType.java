package com.gsconsulting.bitnow.model.enums;

public enum TransactionWalletType {
	INCOMING, OUTGOING;
}
