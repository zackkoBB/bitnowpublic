package com.gsconsulting.bitnow.model.dao.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.gsconsulting.bitnow.model.Order;
import com.gsconsulting.bitnow.model.Receipt;
import com.gsconsulting.bitnow.model.Transaction;
import com.gsconsulting.bitnow.model.dao.ReceiptDao;

/**
 * This class interacts with Hibernate session to save/delete and retrieve User
 * objects.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler</a> Extended to
 *         implement Acegi UserDetailsService interface by David Carter
 *         david@carter.net Modified by <a href="mailto:bwnoll@gmail.com">Bryan
 *         Noll</a> to work with the new BaseDaoHibernate implementation that
 *         uses generics. Modified by jgarcia (updated to hibernate 4)
 */
@Repository("receiptDao")
@SuppressWarnings("unchecked")
public class ReceiptDaoHibernate extends GenericDaoHibernate<Receipt, Long>
		implements ReceiptDao {

	/**
	 * Constructor that sets the entity to User.class.
	 */
	public ReceiptDaoHibernate() {
		super(Receipt.class);
	}

	/**
	 * {@inheritDoc}
	 */
	public Receipt getReceiptByKey(Long key) {
		return (Receipt) getSession().get(Receipt.class, key);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Receipt> getReceiptByOrder(Order order) {

		Criteria receiptCriteria = getSession().createCriteria(Receipt.class, "r");
		Criteria orderCriteria = receiptCriteria.createCriteria("order");
		orderCriteria.add(Restrictions.eq("code", order.getCode()));
		return receiptCriteria.list();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<Receipt> getReceiptByTransaction(Transaction transaction) {

		Criteria receiptCriteria = getSession().createCriteria(Receipt.class, "r");
		Criteria transactionCriteria = receiptCriteria.createCriteria("transaction");
		transactionCriteria.add(Restrictions.eq("code", transaction.getCode()));
		return receiptCriteria.list();
	}

	/**
	 * {@inheritDoc}
	 */
	public Long create(Receipt receipt) {
		return (Long) getSession().save(receipt);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Receipt receipt) {
		getSession().update(receipt);		
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Receipt receipt) {
		getSession().delete(receipt);
	}

}
