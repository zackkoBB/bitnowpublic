package com.gsconsulting.bitnow.model.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.Readings;

/**
 * Readings Data Access Object (GenericDao) interface.
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
public interface ReadingsDao extends GenericDao<Readings, Long> {

    /**
     * Gets Readings information based on Key.
     * 
     * @param Key the Readings to be retrieved
     * @return Readings retrieved
     * 
     * @throws 
     */
	@Transactional
	Readings getReadingsByKey(Long key);	

    /**
     * Gets all Readings entities in the db
     * 
     * @return List<Readings> retrieved
     * 
     * @throws 
     */
    @Transactional
    List<Readings> getAll();

    /**
     * Create a new Readings
     * 
     * @throws 
     */
    @Transactional
    void create(String readings);

    /**
     * Update an existing Readings instance
     * 
     * @throws 
     */
    @Transactional
    void update(Long id, String readings);

    /**
     * Delete an existing Readings instance
     * 
     * @throws 
     */
    @Transactional
    void delete(Long id);
}
