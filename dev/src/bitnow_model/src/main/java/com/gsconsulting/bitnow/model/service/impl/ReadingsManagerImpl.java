package com.gsconsulting.bitnow.model.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gsconsulting.bitnow.model.Readings;
import com.gsconsulting.bitnow.model.dao.ReadingsDao;
import com.gsconsulting.bitnow.model.service.ReadingsManager;

/**
 * Implementation of UserManager interface.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Service("readingsManager")
public class ReadingsManagerImpl extends GenericManagerImpl<Readings, Long>
		implements ReadingsManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4488457924385953401L;
	private ReadingsDao readingsDao;

	@Override
	@Autowired
	public void setReadingsDao(final ReadingsDao readingsDao) {
		this.dao = readingsDao;
		this.readingsDao = readingsDao;
	}

	/**
	 * {@inheritDoc}
	 */
	public Readings getReadingsByKey(Long key) {
		return readingsDao.getReadingsByKey(key);
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(String readings) {
		this.readingsDao.create(readings);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Long id, String readings) {
		this.readingsDao.update(id, readings);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Long id) {
		this.readingsDao.delete(id);
	}
}
