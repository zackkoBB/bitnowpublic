package com.gsconsulting.bitnow.model.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.User;
import com.gsconsulting.bitnow.model.dao.AccountDao;
import com.gsconsulting.bitnow.model.enums.AccountStatus;
import com.gsconsulting.bitnow.model.ids.AccountKey;

/**
 * This class interacts with Hibernate session to save/delete and retrieve User
 * objects.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler</a> Extended to
 *         implement Acegi UserDetailsService interface by David Carter
 *         david@carter.net Modified by <a href="mailto:bwnoll@gmail.com">Bryan
 *         Noll</a> to work with the new BaseDaoHibernate implementation that
 *         uses generics. Modified by jgarcia (updated to hibernate 4)
 */
@Repository("accountDao")
@SuppressWarnings("unchecked")
public class AccountDaoHibernate extends GenericDaoHibernate<Account, String>
		implements AccountDao {

	/**
	 * Constructor that sets the entity to User.class.
	 */
	public AccountDaoHibernate() {
		super(Account.class);
	}

	/**
	 * {@inheritDoc}
	 */
	public Account getAccountByKey(AccountKey key) {

		return (Account) getSession().get(Account.class, key);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Account> getAccountByType(Integer type, AccountStatus status) {

		Query qry = null;
		if (status == null) {
			qry = getSession().createQuery(
					"from " + Account.class.getCanonicalName()
							+ " a where a.type=" + type);
		} else {
			qry = getSession().createQuery(
					"from " + Account.class.getCanonicalName()
							+ " a where a.type=" + type
							+ " and a.lastReadStatus!=" + status.ordinal());
		}
		return (List<Account>) qry.list();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Account> getAccountByUser(User user, AccountStatus status) {

		Query qry = null;
		if (status == null) {
			qry = getSession().createQuery(
					"from " + Account.class.getCanonicalName()
							+ " a where a.user.id=" + user.getId());
		} else {
			qry = getSession().createQuery(
					"from " + Account.class.getCanonicalName()
							+ " a where a.user.id=" + user.getId()
							+ " and a.lastReadStatus!=" + status.ordinal());
		}
		return (List<Account>) qry.list();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Account> getAccountByBalanceAvailable(Double minBalance,
			AccountStatus status) {

		Query qry = null;
		if (status == null) {
			qry = getSession().createQuery(
					"from " + Account.class.getCanonicalName()
							+ " a where a.balanceAvailable >= " + minBalance);
		} else {
			qry = getSession().createQuery(
					"from " + Account.class.getCanonicalName()
							+ " a where a.balanceAvailable >= " + minBalance
							+ " and a.lastReadStatus!=" + status.ordinal());
		}
		return (List<Account>) qry.list();
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(Account account) {
		getSession().save(account);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Account account) {
		getSession().update(account);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Account account) {
		getSession().delete(account);
	}

}
