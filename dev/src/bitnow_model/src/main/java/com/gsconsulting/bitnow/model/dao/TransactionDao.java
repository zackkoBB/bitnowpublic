package com.gsconsulting.bitnow.model.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.Transaction;
import com.gsconsulting.bitnow.model.enums.TransactionStatus;
import com.gsconsulting.bitnow.model.enums.TransactionType;

/**
 * Transaction Data Access Object (GenericDao) interface.
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
public interface TransactionDao extends GenericDao<Transaction, String> {

	/**
	 * Gets Transaction information based on code.
	 * 
	 * @param Code
	 *            code of the Transaction to be retrieved
	 * @return Transaction retrieved
	 * 
	 * @throws
	 */
	@Transactional
	Transaction getTransactionByCode(String code);

	/**
	 * Gets Transaction information based on account.
	 * 
	 * @param Account
	 *            account of the Order to be retrieved
	 * @return List<Transaction> orders retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Transaction> getTransactionByAccount(Account account, Long timestamp);

	/**
	 * Gets Transaction information based on account/status.
	 * 
	 * @param Account
	 *            account of the Transaction to be retrieved
	 * @param TransactionStatus
	 *            status of the Transaction to be retrieved
	 * @return List<Transaction> orders retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Transaction> getTransactionByAccountAndStatus(Account account,
			TransactionStatus status);

	/**
	 * Gets Transaction information based on status/euro.
	 * 
	 * @param Status
	 *            status of the Transaction to be retrieved
	 * @param Euro
	 *            euro of the Transaction to be retrieved
	 * @return Transactions retrieved
	 * 
	 * @throws
	 */
	@Transactional
	Transaction getTransactionByStatusAndEuro(TransactionStatus status,
			Double euro);

	/**
	 * Gets Transaction/Type information based on Status/Timestamp (greater).
	 * 
	 * @param TransactionType
	 *            type of the Transaction to be retrieved
	 * @param Account
	 *            of the transaction to be retrieved
	 * @param TransactionStatus
	 *            status of the Transaction to be retrieved
	 * @param Long
	 *            timestamp of the Transaction to be retrieved
	 * @return Transactions retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Transaction> getTransactionTypeByAccountStatusAndTimestampGreater(
			TransactionType type, Account account, TransactionStatus status,
			Long timestamp);

	/**
	 * Gets Transaction Volume
	 * 
	 * @return Double total volume of transaction
	 * 
	 * @throws
	 */
	@Transactional
	Double getTransactionVolume();

	/**
	 * Gets Transaction information based on address.
	 * 
	 * @param address
	 *            address of the Transaction to be retrieved
	 * @return List<Transaction> transactions retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Transaction> getTransactionByAddress(String address);

	/**
	 * Gets all Transaction entities in the db
	 * 
	 * @return List<Transaction> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Transaction> getAll();

	/**
	 * Create a new Transaction
	 * 
	 * @throws
	 */
	@Transactional
	void create(Transaction transaction);

	/**
	 * Update an existing Transaction instance
	 * 
	 * @throws
	 */
	@Transactional
	void update(Transaction transaction);

	/**
	 * Delete an existing Transaction instance
	 * 
	 * @throws
	 */
	@Transactional
	void delete(Transaction transaction);
}
