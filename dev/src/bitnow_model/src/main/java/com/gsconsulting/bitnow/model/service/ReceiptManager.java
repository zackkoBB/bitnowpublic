package com.gsconsulting.bitnow.model.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.Order;
import com.gsconsulting.bitnow.model.Receipt;
import com.gsconsulting.bitnow.model.Transaction;
import com.gsconsulting.bitnow.model.dao.ReceiptDao;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler </a>
 */
public interface ReceiptManager extends
		GenericManager<Receipt, Long>, Serializable {

	/**
	 * Convenience method for testing - allows you to mock the DAO and set it on
	 * an interface.
	 * 
	 * @param userDao
	 *            the UserDao implementation to use
	 */
	public void setReceiptDao(ReceiptDao receiptDao);

	/**
	 * Gets Receipt information based on Key.
	 * 
	 * @param Key
	 *            the Receipt to be retrieved
	 * @return Receipt retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public Receipt getReceiptByKey(Long key);

	/**
	 * Gets Receipt information based on Order.
	 * 
	 * @param Order
	 *            the Receipt to be retrieved
	 * @return List<Receipt> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<Receipt> getReceiptByOrder(Order order);

	/**
	 * Gets Receipt information based on Transaction.
	 * 
	 * @param Transaction
	 *            the Receipt to be retrieved
	 * @return List<Receipt> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<Receipt> getReceiptByTransaction(Transaction transaction);

	/**
	 * Gets all Receipt entities in the db
	 * 
	 * @return List<Receipt> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<Receipt> getAll();

	/**
	 * Create a new Receipt
	 * 
	 * @throws
	 */
	@Transactional
	public Long create(Receipt receipt);

	/**
	 * Update an existing Receipt instance
	 * 
	 * @throws
	 */
	@Transactional
	public void update(Receipt receipt);

	/**
	 * Delete an existing Receipt instance
	 * 
	 * @throws
	 */
	@Transactional
	public void delete(Receipt receipt);
}
