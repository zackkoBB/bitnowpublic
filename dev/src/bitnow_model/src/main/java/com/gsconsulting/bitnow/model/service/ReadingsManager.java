package com.gsconsulting.bitnow.model.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.Readings;
import com.gsconsulting.bitnow.model.dao.ReadingsDao;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler </a>
 */
public interface ReadingsManager extends
		GenericManager<Readings, Long>, Serializable {

	/**
	 * Convenience method for testing - allows you to mock the DAO and set it on
	 * an interface.
	 * 
	 * @param userDao
	 *            the UserDao implementation to use
	 */
	public void setReadingsDao(ReadingsDao readingsDao);

    /**
     * Gets Readings information based on Key.
     * 
     * @param Key the Readings to be retrieved
     * @return Readings retrieved
     * 
     * @throws 
     */
	@Transactional
	public Readings getReadingsByKey(Long key);	
	
    /**
     * Gets all Readings entities in the db
     * 
     * @return List<Readings> retrieved
     * 
     * @throws 
     */
    @Transactional
    public List<Readings> getAll();
	
    /**
	 * Create a new Readings
	 * 
	 * @throws
	 */
	@Transactional
	public void create(String readings);

	/**
	 * Update an existing Readings instance
	 * 
	 * @throws
	 */
	@Transactional
	public void update(Long id, String readings);

	/**
	 * Delete an existing Readings instance
	 * 
	 * @throws
	 */
	@Transactional
	public void delete(Long id);
}
