package com.gsconsulting.bitnow.model.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.Order;
import com.gsconsulting.bitnow.model.User;
import com.gsconsulting.bitnow.model.enums.TransactionStatus;
import com.gsconsulting.bitnow.model.enums.TransactionType;

/**
 * Order Data Access Object (GenericDao) interface.
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
public interface OrderDao extends GenericDao<Order, String> {

	/**
	 * Gets Order information based on code.
	 * 
	 * @param Code
	 *            code of the Order to be retrieved
	 * @return Order retrieved
	 * 
	 * @throws
	 */
	@Transactional
	Order getOrderByCode(String code);

	/**
	 * Gets Order/Type information based on account and status.
	 * 
	 * @param TransactionType
	 *            type of the order to be retrieved
	 * @param Account
	 *            account of the Order to be retrieved
	 * @param TransactionStatus
	 *            status of the Order to be retrieved
	 * @return List<Order> orders retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Order> getOrderTypeByAccountAndStatus(TransactionType type,
			Account account, TransactionStatus status);

	/**
	 * Gets Order/Type information based on account.
	 * 
	 * @param TransactionType
	 *            type of the Order to be retrieved
	 * @param Account
	 *            account of the Order to be retrieved
	 * @return List<Order> orders retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Order> getOrderTypeByAccount(TransactionType type, Account account);

	/**
	 * Gets Order information based on status.
	 * 
	 * @param TransactionType
	 *            type of the Order to be retrieved
	 * @param TransactionStatus
	 *            status of the Order to be retrieved
	 * @return List<Order> orders retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Order> getOrderTypeByStatus(TransactionType type,
			TransactionStatus status);

	/**
	 * Gets Order information based on status.
	 * 
	 * @param TransactionStatus
	 *            status of the Order to be retrieved
	 * @return List<Order> orders retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Order> getOrderByStatus(TransactionStatus status);

	/**
	 * Gets Order information based on status/btc.
	 * 
	 * @param TransactionStatus
	 *            status of the Order to be retrieved
	 * @param Double
	 *            btc of the Order to be retrieved
	 * @return List<Order> orders retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Order> getOrderByStatusAndBtc(TransactionStatus status, Double btc);

	/**
	 * Gets Order information based on status/euro.
	 * 
	 * @param TransactionStatus
	 *            status of the Order to be retrieved
	 * @param Double
	 *            euro of the Order to be retrieved
	 * @return Orders retrieved
	 * 
	 * @throws
	 */
	@Transactional
	Order getOrderByStatusAndEuro(TransactionStatus status, Double euro);

	/**
	 * Gets Order information based on status/Timestamp.
	 * 
	 * @param OrderStatus
	 *            status of the Order to be retrieved
	 * @param Long
	 *            timestamp of the Order to be retrieved
	 * @return Order retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Order> getOrderByStatusAndTimestampLesser(TransactionStatus status,
			Long timestamp);

	/**
	 * Gets Order/Type BTC Volume
	 * 
	 * @param TransactionType
	 *            type to sum up
	 * @return Double total volume of order
	 * 
	 * @throws
	 */
	@Transactional
	Double getOrderTypeVolumeBTC(TransactionType type);

	/**
	 * Gets Order/Type BTC Volume by User
	 * 
	 * @param TransactionType
	 *            type to sum up
	 * @param User
	 *            to sum up
	 * @return Double total volume of order
	 * 
	 * @throws
	 */
	@Transactional
	Double getOrderTypeVolumeBTCByUser(TransactionType type, User user);

	/**
	 * Gets Order/Type EUR Volume
	 * 
	 * @param TransactionType
	 *            type to sum up
	 * @return Double total volume of order
	 * 
	 * @throws
	 */
	@Transactional
	Double getOrderTypeVolumeEUR(TransactionType type);

	/**
	 * Gets Order/Type EUR Volume by User
	 * 
	 * @param TransactionType
	 *            type to sum up
	 * @param User
	 *            to sum up
	 * @return Double total volume of order
	 * 
	 * @throws
	 */
	@Transactional
	Double getOrderTypeVolumeEURByUser(TransactionType type, User user);

	/**
	 * Gets Order information based on address.
	 * 
	 * @param address
	 *            address of the Order to be retrieved
	 * @return List<Order> orders retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Order> getOrderByAddress(String address);

	/**
	 * Gets all Order entities in the db
	 * 
	 * @return List<Order> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Order> getAll();

	/**
	 * Create a new Order
	 * 
	 * @throws
	 */
	@Transactional
	void create(Order order);

	/**
	 * Update an existing Order instance
	 * 
	 * @throws
	 */
	@Transactional
	void update(Order order);

	/**
	 * Delete an existing Order instance
	 * 
	 * @throws
	 */
	@Transactional
	void delete(Order order);
}
