package com.gsconsulting.bitnow.model.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.gsconsulting.bitnow.model.dao.SellerDao;
import com.gsconsulting.bitnow.model.Seller;

/**
 * This class interacts with Hibernate session to save/delete and retrieve User
 * objects.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler</a> Extended to
 *         implement Acegi UserDetailsService interface by David Carter
 *         david@carter.net Modified by <a href="mailto:bwnoll@gmail.com">Bryan
 *         Noll</a> to work with the new BaseDaoHibernate implementation that
 *         uses generics. Modified by jgarcia (updated to hibernate 4)
 */
@Repository("sellerDao")
public class SellerDaoHibernate extends GenericDaoHibernate<Seller, String>
		implements SellerDao {

	/**
	 * Constructor that sets the entity to User.class.
	 */
	public SellerDaoHibernate() {
		super(Seller.class);
	}

	/**
	 * {@inheritDoc}
	 */
	public Seller getSellerByCf(String cf) {

		Query qry = getSession().createQuery(
				"from " + Seller.class.getCanonicalName() + " s where s.CF='"
						+ cf + "'");
		if (qry.list() != null) {
			if (qry.list().size() > 0) {
				return (Seller) qry.list().get(0);
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Seller> getSellerByBtc(Double btc) {

		Query qry = getSession().createQuery(
				"from " + Seller.class.getCanonicalName()
						+ " s where s.balanceBTC >= " + btc);
		return (List<Seller>) qry.list();
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(Seller seller) {
		getSession().save(seller);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Seller seller) {
		getSession().update(seller);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Seller seller) {
		getSession().delete(seller);
	}

}
