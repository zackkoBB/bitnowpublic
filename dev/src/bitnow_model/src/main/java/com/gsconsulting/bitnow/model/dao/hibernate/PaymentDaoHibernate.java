package com.gsconsulting.bitnow.model.dao.hibernate;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Repository;

import com.gsconsulting.bitnow.model.dao.PaymentDao;
import com.gsconsulting.bitnow.model.Payment;

@Repository("paymentDao")
public class PaymentDaoHibernate implements PaymentDao {

	/**
	 * Log variable for all child classes. Uses LogFactory.getLog(getClass())
	 * from Commons Logging
	 */
	protected final Log log = LogFactory.getLog(PaymentDaoHibernate.class);
	@Resource
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}

	public Session getSession() throws HibernateException {

		Session sess = getSessionFactory().getCurrentSession();
		if (sess == null) {
			sess = getSessionFactory().openSession();
		}
		return sess;
	}

	@Autowired
	@Required
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public PaymentDaoHibernate() {
	}

	/**
	 * {@inheritDoc}
	 */
	public Payment getPaymentByKey(String key) {
		Query qry = getSession().createQuery(
				"from " + Payment.class.getCanonicalName()
						+ " as p where p.seller.cf='" + key + "'");
		if (qry.list() != null) {
			if (qry.list().size() > 0) {
				return (Payment) qry.list().get(0);
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Payment> getAllPayment() {
		return (List<Payment>) getSession().createCriteria(
				Payment.class).list();
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(Payment payment) {
		getSession().save(payment);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Payment payment) {
		getSession().update(payment);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Payment payment) {
		getSession().delete(payment);
	}

}
