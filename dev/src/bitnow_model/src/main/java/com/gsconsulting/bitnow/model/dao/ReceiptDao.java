package com.gsconsulting.bitnow.model.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.Order;
import com.gsconsulting.bitnow.model.Receipt;
import com.gsconsulting.bitnow.model.Transaction;

/**
 * Receipt Data Access Object (GenericDao) interface.
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
public interface ReceiptDao extends GenericDao<Receipt, Long> {

	/**
	 * Gets Receipt information based on Key.
	 * 
	 * @param Key
	 *            the Receipt to be retrieved
	 * @return Receipt retrieved
	 * 
	 * @throws
	 */
	@Transactional
	Receipt getReceiptByKey(Long key);

	/**
	 * Gets Receipt information based on Order.
	 * 
	 * @param Order
	 *            the Receipt to be retrieved
	 * @return List<Receipt> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Receipt> getReceiptByOrder(Order order);

	/**
	 * Gets Receipt information based on Transaction.
	 * 
	 * @param Transaction
	 *            the Receipt to be retrieved
	 * @return List<Receipt> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Receipt> getReceiptByTransaction(Transaction transaction);

	/**
	 * Gets all Receipt entities in the db
	 * 
	 * @return List<Receipt> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Receipt> getAll();

	/**
	 * Create a new Receipt
	 * 
	 * @throws
	 */
	@Transactional
	Long create(Receipt receipt);

	/**
	 * Update an existing Receipt instance
	 * 
	 * @throws
	 */
	@Transactional
	void update(Receipt receipt);

	/**
	 * Delete an existing Receipt instance
	 * 
	 * @throws
	 */
	@Transactional
	void delete(Receipt receipt);
}
