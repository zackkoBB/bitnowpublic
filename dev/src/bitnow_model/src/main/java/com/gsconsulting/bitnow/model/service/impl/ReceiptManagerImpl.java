package com.gsconsulting.bitnow.model.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gsconsulting.bitnow.model.Order;
import com.gsconsulting.bitnow.model.Receipt;
import com.gsconsulting.bitnow.model.Transaction;
import com.gsconsulting.bitnow.model.dao.ReceiptDao;
import com.gsconsulting.bitnow.model.service.ReceiptManager;

/**
 * Implementation of UserManager interface.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Service("receiptManager")
public class ReceiptManagerImpl extends GenericManagerImpl<Receipt, Long>
		implements ReceiptManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8223704164439794075L;
	private ReceiptDao receiptDao;

	@Override
	@Autowired
	public void setReceiptDao(final ReceiptDao receiptDao) {
		this.dao = receiptDao;
		this.receiptDao = receiptDao;
	}

	/**
	 * {@inheritDoc}
	 */
	public Receipt getReceiptByKey(Long key) {
		return this.receiptDao.getReceiptByKey(key);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Receipt> getReceiptByOrder(Order order) {
		return this.receiptDao.getReceiptByOrder(order);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Receipt> getReceiptByTransaction(Transaction transaction) {
		return this.receiptDao.getReceiptByTransaction(transaction);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Receipt> getAll() {
		return this.receiptDao.getAll();
	}

	/**
	 * {@inheritDoc}
	 */
	public Long create(Receipt receipt) {
		return this.receiptDao.create(receipt);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Receipt receipt) {
		this.receiptDao.update(receipt);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Receipt receipt) {
		this.receiptDao.delete(receipt);
	}
}
