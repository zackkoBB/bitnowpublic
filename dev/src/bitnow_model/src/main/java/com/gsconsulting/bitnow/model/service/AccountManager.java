package com.gsconsulting.bitnow.model.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.dao.AccountDao;
import com.gsconsulting.bitnow.model.enums.AccountStatus;
import com.gsconsulting.bitnow.model.ids.AccountKey;
import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.User;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler </a>
 */
public interface AccountManager extends GenericManager<Account, String>,
		Serializable {

	/**
	 * Convenience method for testing - allows you to mock the DAO and set it on
	 * an interface.
	 * 
	 * @param userDao
	 *            the UserDao implementation to use
	 */
	public void setAccountDao(AccountDao accountDao);

	/**
	 * Gets Account information based on Key.
	 * 
	 * @param Key
	 *            the Account to be retrieved
	 * @return Account retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public Account getAccountByKey(AccountKey key);

	/**
	 * Gets Account information based on type.
	 * 
	 * @param Integer
	 *            type of the Accounts to be retrieved
	 * @return List<Account> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<Account> getAccountByType(Integer type, AccountStatus status);

	/**
	 * Gets Account information based on user.
	 * 
	 * @param User
	 *            of the Accounts to be retrieved
	 * @return List<Account> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<Account> getAccountByUser(User user, AccountStatus status);

	/**
	 * Gets Account information based on user and type.
	 * 
	 * @param User
	 *            of the Accounts to be retrieved
	 * @param Integer
	 *            type of the acctount to be retrieved
	 * @return Account retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public Account getAccountByUserAndType(User user, Integer type,
			AccountStatus status);

	/**
	 * Gets all Account entities in the db based on min BTC balance available
	 * 
	 * @param Double
	 *            min BTC balance value
	 * @return List<Account> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<Account> getAccountByBTCBalanceAvailable(Double minBTCBalance,
			AccountStatus status);

	/**
	 * Gets all Account entities in the db based on min EUR balance available
	 * 
	 * @param Double
	 *            min EUR balance value
	 * @param AccountStatus
	 *            to avoid
	 * @param AccountStatus
	 *            to avoid
	 * @return List<Account> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<Account> getAccountByEURBalanceAvailable(Double minBTCBalance,
			AccountStatus status1, AccountStatus status2);

	/**
	 * Gets all Account entities in the db
	 * 
	 * @return List<Account> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<Account> getAll();

	/**
	 * Create a new Account
	 * 
	 * @throws
	 */
	@Transactional
	public void create(Account account);

	/**
	 * Update an existing Account instance
	 * 
	 * @throws
	 */
	@Transactional
	public void update(Account account);

	/**
	 * Delete an existing Account instance
	 * 
	 * @throws
	 */
	@Transactional
	public void delete(Account account);
}
