package com.gsconsulting.bitnow.model.dao.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.Order;
import com.gsconsulting.bitnow.model.User;
import com.gsconsulting.bitnow.model.dao.OrderDao;
import com.gsconsulting.bitnow.model.enums.TransactionStatus;
import com.gsconsulting.bitnow.model.enums.TransactionType;

/**
 * This class interacts with Hibernate session to save/delete and retrieve User
 * objects.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler</a> Extended to
 *         implement Acegi UserDetailsService interface by David Carter
 *         david@carter.net Modified by <a href="mailto:bwnoll@gmail.com">Bryan
 *         Noll</a> to work with the new BaseDaoHibernate implementation that
 *         uses generics. Modified by jgarcia (updated to hibernate 4)
 */
@Repository("orderDao")
@SuppressWarnings("unchecked")
public class OrderDaoHibernate extends GenericDaoHibernate<Order, String>
		implements OrderDao {

	/**
	 * Constructor that sets the entity to User.class.
	 */
	public OrderDaoHibernate() {
		super(Order.class);
	}

	/**
	 * {@inheritDoc}
	 */
	public Order getOrderByCode(String code) {

		Query qry = getSession().createQuery(
				"from " + Order.class.getCanonicalName()
						+ " as o where o.code='" + code + "'");
		if (qry.list() != null) {
			if (qry.list().size() > 0) {
				return (Order) qry.list().get(0);
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public Double getOrderTypeVolumeBTC(TransactionType type) {

		Query qry = getSession().createQuery(
				"select sum(o.btc) from "
						+ Order.class.getCanonicalName()
						+ " as o where o.type="
						+ type.ordinal()
						+ " and (o.status="
						+ TransactionStatus.UNCONFIRMED.ordinal()
						+ " or o.status="
						+ TransactionStatus.WAITING_PAYMENT.ordinal()
						+ " or o.status="
						+ TransactionStatus.PAYMENT_RECEIVED.ordinal()
						+ " or o.status="
						+ TransactionStatus.PAYMENT_RECEIVED_NOT_CONFIRMED
								.ordinal() + ")");
		if (qry.list() != null) {
			if (qry.list().size() > 0) {
				return (Double) qry.list().get(0);
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public Double getOrderTypeVolumeBTCByUser(TransactionType type, User user) {

		Query qry = getSession().createQuery(
				"select sum(o.btc) from "
						+ Order.class.getCanonicalName()
						+ " as o where o.account.user.id="
						+ user.getId()
						+ " and o.type="
						+ type.ordinal()
						+ " and (o.status="
						+ TransactionStatus.UNCONFIRMED.ordinal()
						+ " or o.status="
						+ TransactionStatus.WAITING_PAYMENT.ordinal()
						+ " or o.status="
						+ TransactionStatus.PAYMENT_RECEIVED.ordinal()
						+ " or o.status="
						+ TransactionStatus.PAYMENT_RECEIVED_NOT_CONFIRMED
								.ordinal() + ")");
		if (qry.list() != null) {
			if (qry.list().size() > 0) {
				return (Double) qry.list().get(0);
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public Double getOrderTypeVolumeEUR(TransactionType type) {

		Query qry = getSession()
				.createQuery(
						"select sum(o.euro) from "
								+ Order.class.getCanonicalName()
								+ " as o where o.type="
								+ type.ordinal()
								+ " and (o.status="
								+ TransactionStatus.UNCONFIRMED.ordinal()
								+ " or o.status="
								+ TransactionStatus.WAITING_PAYMENT.ordinal()
								+ " or o.status="
								+ TransactionStatus.PAYMENT_RECEIVED.ordinal()
								+ " or o.status="
								+ TransactionStatus.PAYMENT_RECEIVED_WAITING_CONFIRMATION
										.ordinal() + ")");
		if (qry.list() != null) {
			if (qry.list().size() > 0) {
				return (Double) qry.list().get(0);
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public Double getOrderTypeVolumeEURByUser(TransactionType type, User user) {

		Query qry = getSession()
				.createQuery(
						"select sum(o.euro) from "
								+ Order.class.getCanonicalName()
								+ " as o where o.account.user.id="
								+ user.getId()
								+ " and o.type="
								+ type.ordinal()
								+ " and (o.status="
								+ TransactionStatus.UNCONFIRMED.ordinal()
								+ " or o.status="
								+ TransactionStatus.WAITING_PAYMENT.ordinal()
								+ " or o.status="
								+ TransactionStatus.PAYMENT_RECEIVED.ordinal()
								+ " or o.status="
								+ TransactionStatus.PAYMENT_RECEIVED_WAITING_CONFIRMATION
										.ordinal() + ")");
		if (qry.list() != null) {
			if (qry.list().size() > 0) {
				return (Double) qry.list().get(0);
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Order> getOrderTypeByAccount(TransactionType type,
			Account account) {

		Criteria orderCriteria = getSession().createCriteria(Order.class, "o");
		orderCriteria.add(Restrictions.eq("o.type", type.ordinal()));
		Criteria accountCriteria = orderCriteria.createCriteria("account");
		Criteria userCriteria = accountCriteria.createCriteria("user");
		userCriteria.add(Restrictions.eq("id", account.getUser().getId()));
		accountCriteria.add(Restrictions.eq("id", account.getId()));
		accountCriteria.add(Restrictions.eq("type", account.getType()));
		return orderCriteria.list();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Order> getOrderTypeByStatus(TransactionType type,
			TransactionStatus status) {

		Criteria orderCriteria = getSession().createCriteria(Order.class, "o");
		orderCriteria.add(Restrictions.eq("type", type.ordinal()));
		orderCriteria.add(Restrictions.eq("status", status.ordinal()));
		return orderCriteria.list();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Order> getOrderByStatus(TransactionStatus status) {

		Criteria orderCriteria = getSession().createCriteria(Order.class, "o");
		orderCriteria.add(Restrictions.eq("status", status.ordinal()));
		return orderCriteria.list();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Order> getOrderTypeByAccountAndStatus(TransactionType type,
			Account account, TransactionStatus status) {

		Criteria orderCriteria = getSession().createCriteria(Order.class, "o");
		orderCriteria.add(Restrictions.eq("o.type", type.ordinal()));
		orderCriteria.add(Restrictions.eq("o.status", status.ordinal()));
		Criteria accountCriteria = orderCriteria.createCriteria("account");
		Criteria userCriteria = accountCriteria.createCriteria("user");
		userCriteria.add(Restrictions.eq("id", account.getUser().getId()));
		accountCriteria.add(Restrictions.eq("id", account.getId()));
		accountCriteria.add(Restrictions.eq("type", account.getType()));
		return orderCriteria.list();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Order> getOrderByStatusAndBtc(TransactionStatus status,
			Double btc) {

		Query qry = getSession().createQuery(
				"from " + Order.class.getCanonicalName()
						+ " as o where o.status=" + status.ordinal()
						+ " and o.btc=" + btc);
		return (List<Order>) qry.list();
	}

	/**
	 * {@inheritDoc}
	 */
	public Order getOrderByStatusAndEuro(TransactionStatus status, Double euro) {

		Query qry = getSession().createQuery(
				"from " + Order.class.getCanonicalName()
						+ " as o where o.status=" + status.ordinal()
						+ " and o.euro=" + euro);
		if (qry.list() != null) {
			if (qry.list().size() > 0) {
				return (Order) qry.list().get(0);
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Order> getOrderByStatusAndTimestampLesser(
			TransactionStatus status, Long timestamp) {

		Query qry = getSession().createQuery(
				"from " + Order.class.getCanonicalName()
						+ " as o where o.status=" + status.ordinal()
						+ " and o.created<" + timestamp);
		return (List<Order>) qry.list();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Order> getOrderByAddress(String address) {

		return (List<Order>) getSession().createQuery(
				"from " + Order.class.getCanonicalName()
						+ " as o where o.address='" + address + "'").list();
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(Order order) {
		getSession().save(order);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Order order) {
		getSession().update(order);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Order order) {
		getSession().delete(order);
	}

}
