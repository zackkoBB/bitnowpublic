package com.gsconsulting.bitnow.model.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.Order;
import com.gsconsulting.bitnow.model.User;
import com.gsconsulting.bitnow.model.dao.OrderDao;
import com.gsconsulting.bitnow.model.enums.TransactionStatus;
import com.gsconsulting.bitnow.model.enums.TransactionType;
import com.gsconsulting.bitnow.model.service.OrderManager;

/**
 * Implementation of UserManager interface.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Service("orderManager")
public class OrderManagerImpl extends GenericManagerImpl<Order, String>
		implements OrderManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8253446758121224955L;
	private OrderDao orderDao;

	@Override
	@Autowired
	public void setOrderDao(final OrderDao orderDao) {
		this.dao = orderDao;
		this.orderDao = orderDao;
	}

	/**
	 * {@inheritDoc}
	 */
	public Order getOrderByCode(String code) {
		return this.orderDao.getOrderByCode(code);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Order> getOrderTypeByAccount(TransactionType type,
			Account account) {
		return this.orderDao.getOrderTypeByAccount(type, account);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Order> getOrderTypeByStatus(TransactionType type,
			TransactionStatus status) {
		return this.orderDao.getOrderTypeByStatus(type, status);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Order> getOrderByStatus(TransactionStatus status) {
		return this.orderDao.getOrderByStatus(status);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Order> getOrderTypeByAccountAndStatus(TransactionType type,
			Account account, TransactionStatus status) {
		return orderDao.getOrderTypeByAccountAndStatus(type, account, status);
	}

	/**
	 * {@inheritDoc}
	 */
	public Double getOrderTypeVolumeBTC(TransactionType type) {
		return orderDao.getOrderTypeVolumeBTC(type);
	}

	/**
	 * {@inheritDoc}
	 */
	public Double getOrderTypeVolumeBTCByUser(TransactionType type, User user) {
		return orderDao.getOrderTypeVolumeBTCByUser(type, user);
	}

	/**
	 * {@inheritDoc}
	 */
	public Double getOrderTypeVolumeEUR(TransactionType type) {
		return orderDao.getOrderTypeVolumeEUR(type);
	}

	/**
	 * {@inheritDoc}
	 */
	public Double getOrderTypeVolumeEURByUser(TransactionType type, User user) {
		return orderDao.getOrderTypeVolumeEURByUser(type, user);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Order> getOrderByStatusAndBtc(TransactionStatus status,
			Double btc) {
		return orderDao.getOrderByStatusAndBtc(status, btc);
	}

	/**
	 * {@inheritDoc}
	 */
	public Order getOrderByStatusAndEuro(TransactionStatus status, Double euro) {
		return orderDao.getOrderByStatusAndEuro(status, euro);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Order> getOrderByStatusAndTimestampLesser(
			TransactionStatus status, Long timestamp) {
		return orderDao.getOrderByStatusAndTimestampLesser(status, timestamp);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Order> getOrderByAddress(String address) {
		return orderDao.getOrderByAddress(address);
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(Order order) {
		this.orderDao.create(order);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Order order) {
		this.orderDao.update(order);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Order order) {
		this.orderDao.delete(order);
	}
}
