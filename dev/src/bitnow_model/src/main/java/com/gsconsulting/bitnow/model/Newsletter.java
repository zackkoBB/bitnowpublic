package com.gsconsulting.bitnow.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

/**
 * This class represents the basic "user" object in AppFuse that allows for authentication
 * and user management.  It implements Acegi Security's UserDetails interface.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 *         Updated by Dan Kibler (dan@getrolling.com)
 *         Extended to implement Acegi UserDetails interface
 *         by David Carter david@carter.net
 */
@Entity
@Table(name = "newsletter")
@Indexed
public class Newsletter extends BaseObject implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8487541071528697956L;
	private String email;

    /**
     * Default constructor - creates a new instance with no values set.
     */
    public Newsletter() {
    }

    /**
     * Create a new instance and set the username.
     *
     * @param username login name for user.
     */
    public Newsletter(final String email) {
        this.email = email;
    }

    @Id
    @Column(name="email", nullable = false, length = 255, unique = true)
    @Field
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * {@inheritDoc}
     */
    public int hashCode() {
        return (email != null ? email.hashCode() : 0);
    }

    /**
     * {@inheritDoc}
     */
    public String toString() {
        ToStringBuilder sb = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
                .append("email", this.email);
        return sb.toString();
    }

	@Override
	public boolean equals(Object o) {
		return false;
	}
}
