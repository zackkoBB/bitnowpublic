package com.gsconsulting.bitnow.model.enums;

public enum AccountType {
	EUR_POSTEPAY, EUR_POSTAGIRO, BTC, LTC, DOGE, EUR;
}
