package com.gsconsulting.bitnow.model.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.gsconsulting.bitnow.model.dao.BTCAddressDao;
import com.gsconsulting.bitnow.model.BTCAddress;
import com.gsconsulting.bitnow.model.User;

/**
 * This class interacts with Hibernate session to save/delete and retrieve User
 * objects.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler</a> Extended to
 *         implement Acegi UserDetailsService interface by David Carter
 *         david@carter.net Modified by <a href="mailto:bwnoll@gmail.com">Bryan
 *         Noll</a> to work with the new BaseDaoHibernate implementation that
 *         uses generics. Modified by jgarcia (updated to hibernate 4)
 */
@Repository("btcAddressDao")
@SuppressWarnings("unchecked")
public class BTCAddressDaoHibernate extends
		GenericDaoHibernate<BTCAddress, String> implements BTCAddressDao {

	/**
	 * Constructor that sets the entity to User.class.
	 */
	public BTCAddressDaoHibernate() {
		super(BTCAddress.class);
	}

	/**
	 * {@inheritDoc}
	 */
	public BTCAddress getBTCAddressById(String id) {

		Query qry = getSession().createQuery(
				"from " + BTCAddress.class.getCanonicalName()
						+ " ba where ba.address='" + id + "'");
		if (qry.list() != null) {
			if (qry.list().size() > 0) {
				return (BTCAddress) qry.list().get(0);
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<BTCAddress> getBTCAddressByUser(User user) {

		Query qry = getSession().createQuery(
				"from " + BTCAddress.class.getCanonicalName()
						+ " ba where ba.user.id = " + user.getId());
		return (List<BTCAddress>) qry.list();
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(BTCAddress btcAddress) {
		getSession().save(btcAddress);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(BTCAddress btcAddress) {
		getSession().update(btcAddress);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(BTCAddress btcAddress) {
		getSession().delete(btcAddress);
	}

}
