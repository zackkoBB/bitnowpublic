package com.gsconsulting.bitnow.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
@Entity
@Table(name = "wallet")
public class Wallet extends BaseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8403271718097552628L;
	private Integer type;
	private Double balance;
	private Double balanceAvailable;
	private Double exchangeRate;
	private Long created;
	private Long lastRead;
	private Long lastModified;

	public Wallet() {
		super();
	}

	public Wallet(Integer type, Double balance, Double balanceAvailable,
			Double exchangeRate, Long created, Long lastRead, Long lastModified) {
		super();
		this.type = type;
		this.balance = balance;
		this.exchangeRate = exchangeRate;
		this.created = created;
		this.lastRead = lastRead;
		this.lastModified = lastModified;
	}

	@Id
	@Column(name = "type")
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "balance")
	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	@Column(name = "balance_available")
	public Double getBalanceAvailable() {
		return balanceAvailable;
	}

	public void setBalanceAvailable(Double balanceAvailable) {
		this.balanceAvailable = balanceAvailable;
	}

	@Column(name = "exchange_rate")
	public Double getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(Double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	@Column(name = "created")
	public Long getCreated() {
		return created;
	}

	public void setCreated(Long created) {
		this.created = created;
	}

	@Column(name = "last_read")
	public Long getLastRead() {
		return lastRead;
	}

	public void setLastRead(Long lastRead) {
		this.lastRead = lastRead;
	}

	@Column(name = "last_modified")
	public Long getLastModified() {
		return lastModified;
	}

	public void setLastModified(Long lastModified) {
		this.lastModified = lastModified;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
