package com.gsconsulting.bitnow.model.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.dao.SellerDao;
import com.gsconsulting.bitnow.model.Seller;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler </a>
 */
public interface SellerManager extends
		GenericManager<Seller, String>, Serializable {

	/**
	 * Convenience method for testing - allows you to mock the DAO and set it on
	 * an interface.
	 * 
	 * @param userDao
	 *            the UserDao implementation to use
	 */
	void setSellerDao(SellerDao sellerDao);

	/**
	 * Gets Seller information based on code.
	 * 
	 * @param Code
	 *            code of the Seller to be retrieved
	 * @return Seller retrieved
	 * 
	 * @throws
	 */
	@Transactional
	Seller getSellerByCf(String cf);

	/**
	 * Gets all Seller entities in the db based on BTCs available
	 * 
	 * @param Double btc min value
	 * @return List<Seller> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Seller> getSellerByBtc(Double btc);

	/**
	 * Gets all Seller entities in the db
	 * 
	 * @return List<Seller> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Seller> getAll();

	/**
	 * Create a new Seller
	 * 
	 * @throws
	 */
	@Transactional
	void create(Seller seller);

	/**
	 * Update an existing Seller instance
	 * 
	 * @throws
	 */
	@Transactional
	void update(Seller seller);

	/**
	 * Delete an existing Seller instance
	 * 
	 * @throws
	 */
	@Transactional
	void delete(Seller seller);
}
