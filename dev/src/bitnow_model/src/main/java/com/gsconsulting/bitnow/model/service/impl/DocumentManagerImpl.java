package com.gsconsulting.bitnow.model.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gsconsulting.bitnow.model.Document;
import com.gsconsulting.bitnow.model.User;
import com.gsconsulting.bitnow.model.dao.DocumentDao;
import com.gsconsulting.bitnow.model.service.DocumentManager;

/**
 * Implementation of UserManager interface.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Service("documentManager")
public class DocumentManagerImpl extends GenericManagerImpl<Document, Long>
		implements DocumentManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6018765227082556752L;
	private DocumentDao documentDao;

	@Override
	@Autowired
	public void setDocumentDao(final DocumentDao documentDao) {
		this.dao = documentDao;
		this.documentDao = documentDao;
	}

	/**
	 * {@inheritDoc}
	 */
	public Document getDocumentById(Long id) {
		return this.documentDao.getDocumentById(id);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Document> getDocumentByUser(User user) {
		return this.documentDao.getDocumentByUser(user);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Document> getAll() {
		return this.documentDao.getAll();
	}

	/**
	 * {@inheritDoc}
	 */
	public Long create(Document document) {
		return this.documentDao.create(document);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Document document) {
		this.documentDao.update(document);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Document document) {
		this.documentDao.delete(document);
	}
}
