package com.gsconsulting.bitnow.model.enums;

public enum TransactionType {
	BUY, SELL;
}
