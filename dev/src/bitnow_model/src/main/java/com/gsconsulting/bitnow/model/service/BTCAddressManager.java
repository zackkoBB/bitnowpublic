package com.gsconsulting.bitnow.model.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.dao.BTCAddressDao;
import com.gsconsulting.bitnow.model.BTCAddress;
import com.gsconsulting.bitnow.model.User;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler </a>
 */
public interface BTCAddressManager extends GenericManager<BTCAddress, String>,
		Serializable {

	/**
	 * Convenience method for testing - allows you to mock the DAO and set it on
	 * an interface.
	 * 
	 * @param userDao
	 *            the UserDao implementation to use
	 */
	public void setBTCAddressDao(BTCAddressDao btcAddressDao);

	/**
	 * Gets BTCAddress entity based on BTCAddress id
	 * 
	 * @param String
	 *            id of BTCAddress to be retreived
	 * @return BTCAddress retreived
	 * 
	 * @throws
	 */
	@Transactional
	public BTCAddress getBTCAddressById(String id);

	/**
	 * Get all BTCAddress owned by a user
	 * 
	 * @param User
	 *            user for whom the List<BTCAdress> needs to be retrieved
	 * @return List<BTCAddress> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<BTCAddress> getBTCAddressByUser(User user);

	/**
	 * Gets all BTCAddress entities in the db
	 * 
	 * @return List<BTCAddress> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<BTCAddress> getAll();

	/**
	 * Create a new BTCAddress
	 * 
	 * @throws
	 */
	@Transactional
	public void create(BTCAddress btcAddress);

	/**
	 * Update an existing BTCAddress instance
	 * 
	 * @throws
	 */
	@Transactional
	public void update(BTCAddress btcAddress);

	/**
	 * Delete an existing BTCAddress instance
	 * 
	 * @throws
	 */
	@Transactional
	public void delete(BTCAddress btcAddress);

}
