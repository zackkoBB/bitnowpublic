package com.gsconsulting.bitnow.model.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.User;
import com.gsconsulting.bitnow.model.dao.AccountDao;
import com.gsconsulting.bitnow.model.enums.AccountStatus;
import com.gsconsulting.bitnow.model.enums.AccountType;
import com.gsconsulting.bitnow.model.ids.AccountKey;
import com.gsconsulting.bitnow.model.service.AccountManager;

/**
 * Implementation of UserManager interface.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Service("accountManager")
public class AccountManagerImpl extends GenericManagerImpl<Account, String>
		implements AccountManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2287502353670690392L;
	private AccountDao accountDao;

	@Override
	@Autowired
	public void setAccountDao(final AccountDao accountDao) {
		this.dao = accountDao;
		this.accountDao = accountDao;
	}

	/**
	 * {@inheritDoc}
	 */
	public Account getAccountByKey(AccountKey key) {
		return accountDao.getAccountByKey(key);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Account> getAccountByType(Integer type, AccountStatus status) {
		return accountDao.getAccountByType(type, status);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Account> getAccountByUser(User user, AccountStatus status) {
		return accountDao.getAccountByUser(user, status);
	}

	/**
	 * {@inheritDoc}
	 */
	public Account getAccountByUserAndType(User user, Integer type,
			AccountStatus status) {

		Account result = null;
		List<Account> accountsByUser = accountDao
				.getAccountByUser(user, status);
		if (accountsByUser != null) {
			for (Account account : accountsByUser) {
				if (account.getType() == type) {
					result = account;
					break;
				}
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Account> getAccountByBTCBalanceAvailable(Double minBTCBalance,
			AccountStatus status) {

		List<Account> result = new ArrayList<>();
		for (Account account : this.accountDao.getAccountByBalanceAvailable(
				minBTCBalance, status)) {
			if (account.getType() == AccountType.BTC.ordinal()) {
				result.add(account);
			}
		}
		if (result.size() > 0)
			return result;
		else
			return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Account> getAccountByEURBalanceAvailable(Double minEURBalance,
			AccountStatus status1, AccountStatus status2) {

		List<Account> result = new ArrayList<>();
		for (Account account : this.accountDao.getAccountByBalanceAvailable(
				minEURBalance, status1)) {
			if ((account.getLastReadStatus() != status2.ordinal())
					&& (account.getType() == AccountType.EUR.ordinal())) {
				result.add(account);
			}
		}
		if (result.size() > 0)
			return result;
		else
			return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(Account account) {
		this.accountDao.create(account);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Account account) {
		this.accountDao.update(account);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Account account) {
		this.accountDao.delete(account);
	}
}
