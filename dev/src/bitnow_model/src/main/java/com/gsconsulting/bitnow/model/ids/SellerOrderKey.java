package com.gsconsulting.bitnow.model.ids;

import java.io.Serializable;

import com.gsconsulting.bitnow.model.BaseObject;
import com.gsconsulting.bitnow.model.Order;
import com.gsconsulting.bitnow.model.Seller;

/**
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
public class SellerOrderKey extends BaseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2057819674738621223L;
	private Seller seller;
	private Order order;

	public SellerOrderKey() {
		super();
	}

	public SellerOrderKey(Seller seller, Order order) {
		super();
		this.seller = seller;
		this.order = order;
	}

	public Seller getSeller() {
		return seller;
	}

	public void setSeller(Seller seller) {
		this.seller = seller;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}
}
