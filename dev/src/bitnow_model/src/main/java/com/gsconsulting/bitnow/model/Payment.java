package com.gsconsulting.bitnow.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "payment")
public class Payment extends BaseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -856586396330387091L;
	private Seller seller;
	private Double balanceEur;
	private Double saldoDisponibile;
	private Long timestamp;

	public Payment() {
		super();
	}

	public Payment(Seller seller, Double balanceEur, Double saldoDisponibile,
			Long timestamp) {
		super();
		this.seller = seller;
		this.balanceEur = balanceEur;
		this.saldoDisponibile = saldoDisponibile;
		this.timestamp = timestamp;
	}

	public Seller getSeller() {
		return seller;
	}

	public void setSeller(Seller seller) {
		this.seller = seller;
	}

	public Double getBalanceEur() {
		return balanceEur;
	}

	public void setBalanceEur(Double balanceEur) {
		this.balanceEur = balanceEur;
	}

	public Double getSaldoDisponibile() {
		return saldoDisponibile;
	}

	public void setSaldoDisponibile(Double saldoDisponibile) {
		this.saldoDisponibile = saldoDisponibile;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}
}
