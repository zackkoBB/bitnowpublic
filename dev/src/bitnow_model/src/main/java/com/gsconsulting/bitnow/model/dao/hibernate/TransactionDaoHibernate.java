package com.gsconsulting.bitnow.model.dao.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.Transaction;
import com.gsconsulting.bitnow.model.dao.TransactionDao;
import com.gsconsulting.bitnow.model.enums.TransactionStatus;
import com.gsconsulting.bitnow.model.enums.TransactionType;
import com.gsconsulting.bitnow.model.util.Constants;

/**
 * This class interacts with Hibernate session to save/delete and retrieve User
 * objects.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler</a> Extended to
 *         implement Acegi UserDetailsService interface by David Carter
 *         david@carter.net Modified by <a href="mailto:bwnoll@gmail.com">Bryan
 *         Noll</a> to work with the new BaseDaoHibernate implementation that
 *         uses generics. Modified by jgarcia (updated to hibernate 4)
 */
@Repository("transactionHistoryDao")
@SuppressWarnings("unchecked")
public class TransactionDaoHibernate extends
		GenericDaoHibernate<Transaction, String> implements TransactionDao {

	/**
	 * Constructor that sets the entity to User.class.
	 */
	public TransactionDaoHibernate() {
		super(Transaction.class);
	}

	/**
	 * {@inheritDoc}
	 */
	public Transaction getTransactionByCode(String code) {

		Query qry = getSession().createQuery(
				"from " + Transaction.class.getCanonicalName()
						+ " t where t.code='" + code + "'");
		if (qry.list() != null) {
			if (qry.list().size() > 0) {
				return (Transaction) qry.list().get(0);
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Transaction> getTransactionByAccount(Account account,
			Long timestamp) {

		Criteria transactionCriteria = getSession().createCriteria(
				Transaction.class, "t");
		transactionCriteria.setMaxResults(Constants.MAX_HISTORY_TRANSACTION);
		transactionCriteria.add(Restrictions.ge("created", timestamp));
		Criteria accountCriteria = transactionCriteria
				.createCriteria("account");
		Criteria userCriteria = accountCriteria.createCriteria("user");
		userCriteria.add(Restrictions.eq("id", account.getUser().getId()));
		accountCriteria.add(Restrictions.eq("id", account.getId()));
		accountCriteria.add(Restrictions.eq("type", account.getType()));
		return transactionCriteria.list();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Transaction> getTransactionByAccountAndStatus(Account account,
			TransactionStatus status) {

		Criteria transactionCriteria = getSession().createCriteria(
				Transaction.class, "t");
		transactionCriteria.add(Restrictions.eq("t.status", status.ordinal()));
		Criteria accountCriteria = transactionCriteria
				.createCriteria("account");
		Criteria userCriteria = accountCriteria.createCriteria("user");
		userCriteria.add(Restrictions.eq("id", account.getUser().getId()));
		accountCriteria.add(Restrictions.eq("id", account.getId()));
		accountCriteria.add(Restrictions.eq("type", account.getType()));
		return transactionCriteria.list();
	}

	/**
	 * {@inheritDoc}
	 */
	public Double getTransactionVolume() {

		Query qry = getSession().createQuery(
				"select sum(t.btc) from "
						+ Transaction.class.getCanonicalName() + " t");
		if (qry.list() != null) {
			if (qry.list().size() > 0) {
				return (Double) qry.list().get(0);
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public Transaction getTransactionByStatusAndEuro(TransactionStatus status,
			Double euro) {

		Integer statusInt = status.ordinal();
		Query qry = getSession().createQuery(
				"from " + Transaction.class.getCanonicalName()
						+ " t where t.status=" + statusInt + " and t.euro="
						+ euro + "");
		if (qry.list() != null) {
			if (qry.list().size() > 0) {
				return (Transaction) qry.list().get(0);
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Transaction> getTransactionTypeByAccountStatusAndTimestampGreater(
			TransactionType type, Account account, TransactionStatus status,
			Long timestamp) {

		Criteria transactionCriteria = getSession().createCriteria(
				Transaction.class, "t");
		transactionCriteria.add(Restrictions.eq("t.status", status.ordinal()));
		transactionCriteria.add(Restrictions.ge("created", timestamp));
		Criteria accountCriteria = transactionCriteria
				.createCriteria("account");
		Criteria userCriteria = accountCriteria.createCriteria("user");
		userCriteria.add(Restrictions.eq("id", account.getUser().getId()));
		accountCriteria.add(Restrictions.eq("id", account.getId()));
		accountCriteria.add(Restrictions.eq("type", account.getType()));
		return transactionCriteria.list();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Transaction> getTransactionByAddress(String address) {

		return (List<Transaction>) getSession().createQuery(
				"from " + Transaction.class.getCanonicalName()
						+ " t where t.address='" + address + "'").list();
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(Transaction transaction) {
		getSession().save(transaction);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Transaction transaction) {
		getSession().update(transaction);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Transaction transaction) {
		getSession().delete(transaction);
	}

}
