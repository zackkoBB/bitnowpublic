package com.gsconsulting.bitnow.model.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.User;
import com.gsconsulting.bitnow.model.enums.AccountStatus;
import com.gsconsulting.bitnow.model.ids.AccountKey;

/**
 * Account Data Access Object (GenericDao) interface.
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
public interface AccountDao extends GenericDao<Account, String> {

    /**
     * Gets Account information based on Key.
     * 
     * @param Key the Account to be retrieved
     * @return Account retrieved
     * 
     * @throws 
     */
	@Transactional
	Account getAccountByKey(AccountKey key);	

	/**
     * Gets Account information based on type.
     * 
     * @param Integer type of the Accounts to be retrieved
     * @return List<Account> retrieved
     * 
     * @throws 
     */
    @Transactional
    List<Account> getAccountByType(Integer type, AccountStatus status);

	/**
     * Gets Account information based on id.
     * 
     * @param User of the Accounts to be retrieved
     * @return List<Account> retrieved
     * 
     * @throws 
     */
    @Transactional
    List<Account> getAccountByUser(User user, AccountStatus status);

	/**
	 * Gets all Account entities in the db based on minimum balance available
	 * 
	 * @param Double min balance value
	 * @return List<Account> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<Account> getAccountByBalanceAvailable(Double minBalance, AccountStatus status);

    /**
     * Gets all Account entities in the db
     * 
     * @return List<Account> retrieved
     * 
     * @throws 
     */
    @Transactional
    List<Account> getAll();

    /**
     * Create a new Account
     * 
     * @throws 
     */
    @Transactional
    void create(Account account);

    /**
     * Update an existing Account instance
     * 
     * @throws 
     */
    @Transactional
    void update(Account account);

    /**
     * Delete an existing Account instance
     * 
     * @throws 
     */
    @Transactional
    void delete(Account account);
}
