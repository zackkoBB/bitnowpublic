package com.gsconsulting.bitnow.model.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Constant values used throughout the application.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
public final class Constants {

	private Constants() {
		// hide me
	}

	// ~ Static fields/initializers
	// =============================================


	public static final String[] APPLICATION_CONTEXT = {
			"applicationContext-dao.xml", "applicationContext-resources.xml",
			"applicationContext-service.xml" };
	/**
	 * Dashboard
	 */
	public static final String ADMINISTRATOR_EMAIL = "amministratore@bitnow.it";
//	public static final Long LAST_TRANSACTION_SAME_AMOUNT_IN_MILLIS = 259200000l; //3 Days
	public static final Long LAST_TRANSACTION_SAME_AMOUNT_IN_MILLIS = 172800000l; //2 Days
	public static final Long MAX_TRANSACTION_HISTORY_TIME_IN_MILLIS = 5184000000l; //60 Days
	public static final Integer TRANSACTION_WALLET_INCOMING_DONE_CONFIRMATIONS = 3;
	public static final int SECOND_TO_MILLISECOND = 1000;
	public static final String MARKET_BTC_USD = "BTC_USD";
	public static final String MARKET_BTC_EUR = "BTC_EUR";
	public static final String MARKET_BTC_CNY = "BTC_CNY";
	public static final String MARKET_BTC_RUB = "BTC_RUB";
	public static final String MARKET_BTC_GBP = "BTC_GBP";
	public static final String MARKET_USD_EUR = "USD_EUR";
	public static final String MARKET_CNY_EUR = "CNY_EUR";
	public static final String MARKET_RUB_EUR = "RUB_EUR";
	public static final String MARKET_GBP_EUR = "GBP_EUR";
	public static final String FEE_TRADE1 = "FIAT";
	public static final String FEE_TRADE2 = "CRYPTO";
	public static final Double FEE_BTC_NETWORK_FEE = 0.0005;
	public static final String FEE_BTC_NETWORK_FEE_NOTE = "Default BTC Network Fee";
	public static final String MARKET_USD = "USD";
	public static final String MARKET_EUR = "EUR";
	public static final String MARKET_CNY = "CNY";
	public static final String MARKET_RUB = "RUB";
	public static final String MARKET_GBP = "GBP";

	public static final String WALLET_VALIDITY_API = "WALLET_VALIDITY_API";
	public static final String WALLET_SEND_FEE = "WALLET_SEND_FEE";
	public static final String WALLET_FEE_ACCOUNT_SELL = "WALLET_FEE_ACCOUNT_SELL";
	public static final String WALLET_FEE_ACCOUNT_BUY = "WALLET_FEE_ACCOUNT_BUY";
//	public static final String WALLET_FEE_ADDRESS = "WALLET_FEE_ADDRESS";
//	public static final String WALLET_FEE_ACCOUNT = "WALLET_FEE_ACCOUNT";
	public static final String EXCHANGE_RATE_EXCHANGE = "EXCHANGE_RATE_EXCHANGE";
	public static final String EXCHANGE_RATE_API = "EXCHANGE_RATE_API";
	public static final String FEE_BUY = "FEE_BUY";
	public static final String FEE_SELL = "FEE_SELL";
	public static final String FEE_SELLER = "FEE_SELLER";
	public static final String FEE_BUYER = "FEE_BUYER";
	public static final String COGE_POLLING_INTERVAL = "COGE_POLLING_INTERVAL";
	public static final String TRANSACTION_EXPIRY_TIME = "TRANSACTION_EXPIRY_TIME";
	public static final String UI_POLLING_INTERVAL = "UI_POLLING_INTERVAL";
	public static final String ENVIRONMENT = "ENVIRONMENT";
	public static final String REST_PRICE_KEY = "REST_PRICE_KEY";
	public static final String WALLET_PASSPHRASE = "WALLET_PASSPHRASE";
	public static final String MAX_BUY_PAYMENT = "MAX_BUY_PAYMENT";
	public static final String MAX_SELL_PAYMENT = "MAX_SELL_PAYMENT";
	public static final String TRANSACTION_LINK = "TRANSACTION_LINK";
	public static final String ADDRESS_LINK = "ADDRESS_LINK";
	public static final String CONTEXT_PATH = "CONTEXT_PATH";	
	public static final String NOT_ALLOWED_TRANSACTIONS = "NOT_ALLOWED_TRANSACTIONS";
	public static final Integer MAX_POSTEPAY_RETRIEVER_TRANSACTIONS = 20;
	public static final Integer MAX_RECEIPTS_TRANSACTION = 10;
	public static final Integer MAX_HISTORY_TRANSACTION = 1000;

	/**
	 * REST error messages
	 */
	public final static String KEY_WRONG_MESSAGE = "Wrong Key!";
	public final static String PRICE_SELL_LABEL = "sell";
	public final static String PRICE_BUY_LABEL = "buy";
	public final static String PRICE_WRONG_MESSAGE = "No Price!";
	public final static String LANGUAGE_IT_LABEL = "IT";
	public final static String LIQUIDITY_COIN_WRONG_MESSAGE = "No Coin!";

	public final static Double MIN_BUY_ORDER = 0.01;
	public final static Double MIN_SELL_ORDER = 1.00;
	public static final Double WALLET_BALANCE_MINIMUM_AMOUNT = 0.00050000;
	public static final int REST_CONNECTION_TIMEOUT = 180000;
	public static final int REST_READ_TIMEOUT = 180000;
	public static final double VOLUME_PRECISION = 0.00;
	public static final double EXCHANGE_RATE_PRECISION = 0.01;
	public static final Double BTC_PRECISION = 0.00000001;
	public static final int SKIPPED_SUMMARY_LENGTH = 10;
	public static final String CSV_SUFFIX = ".csv";
	public static final String EXCHANGES_IMPORT_HEADER1 = "Code";
	public static final String EXCHANGES_IMPORT_HEADER2 = "Name";
	public static final String EXCHANGES_IMPORT_HEADER3 = "Description";
	public static final String EXCHANGES_IMPORT_HEADER4 = "Website";
	public static final String EXCHANGES_IMPORT_HEADER5 = "Api";
	public static final String EXCHANGES_IMPORT_HEADER6 = "Fee";
	public static final String EXCHANGES_IMPORT_HEADER7 = "Interface";
	public static final String TYPES_IMPORT_HEADER1 = "Market";
	public static final String TYPES_IMPORT_HEADER2 = "Type";
	public static final String APIS_IMPORT_HEADER1 = "Exchange";
	public static final String APIS_IMPORT_HEADER2 = "Market";
	public static final String APIS_IMPORT_HEADER3 = "Type";
	public static final String APIS_IMPORT_HEADER4 = "Address";
	public static final String FEES_IMPORT_HEADER1 = "Exchange";
	public static final String FEES_IMPORT_HEADER2 = "Market";
	public static final String FEES_IMPORT_HEADER3 = "Type";
	public static final String FEES_IMPORT_HEADER4 = "Unit";
	public static final String FEES_IMPORT_HEADER5 = "Value";
	public static final String FEES_IMPORT_HEADER6 = "Note";
	public static final String MARKET_CODE_NONE = "NONE";
	public static final String API_TYPE_PAIRS = "PAIRS";
	public static final String API_TYPE_ORDERBOOK = "ORDERBOOK";
	public static final String API_TYPE_TICKER = "TICKER";
	public static final String API_TYPE_FUNDING = "FUNDING";
	public static final String FEE_TYPE_TRADE = "TRADE";
	public static final String FEE_TYPE_DEPOSIT = "DEPOSIT";
	public static final String FEE_TYPE_WITHDRAW = "WITHDRAW";
	public static final String FEE_UNIT_EUR = "€";
	public static final String FEE_UNIT_USD = "$";
	public static final String FEE_UNIT_CNY = "¥";
	public static final String FEE_UNIT_RUB = "₽";
	public static final String FEE_UNIT_GBP = "£";
	public static final String FEE_UNIT_BTC = "BTC";
	public static final String FEE_UNIT_BTCKB = "BTC/kB";
	public static final String FEE_UNIT_PERCENTAGE = "%";
	public static final String DEFAULT_THEME = "bootstrap";
	public static final String[] POSSIBLE_THEMES = { "bootstrap", "afterdark",
			"afternoon", "afterwork", "black-tie", "blitzer", "bluesky",
			"casablanca", "cruze", "cupertino", "dark-hive", "dot-luv",
			"eggplant", "excite-bike", "flick", "glass-x", "home",
			"hot-sneaks", "humanity", "le-frog", "midnight", "mint-choc",
			"overcast", "pepper-grinder", "redmond", "rocket", "sam",
			"smoothness", "south-street", "start", "sunny", "swanky-purse",
			"trontastic", "ui-darkness", "ui-lightness", "vader" };

	public static List<String> getTypes() {

		List<String> types = new ArrayList<>();
		types.add(Constants.API_TYPE_PAIRS);
		types.add(Constants.API_TYPE_ORDERBOOK);
		types.add(Constants.API_TYPE_FUNDING);
		types.add(Constants.API_TYPE_TICKER);
		types.add(Constants.FEE_TYPE_TRADE);
		types.add(Constants.FEE_TYPE_DEPOSIT);
		types.add(Constants.FEE_TYPE_WITHDRAW);
		return types;
	}

	public static List<String> getFeeUnits() {

		List<String> types = new ArrayList<>();
		types.add(Constants.FEE_UNIT_EUR);
		types.add(Constants.FEE_UNIT_USD);
		types.add(Constants.FEE_UNIT_BTC);
		types.add(Constants.FEE_UNIT_BTCKB);
		types.add(Constants.FEE_UNIT_PERCENTAGE);
		return types;
	}

	public static List<String> getCurrencies() {

		List<String> types = new ArrayList<>();
		types.add(Constants.MARKET_USD);
		types.add(Constants.MARKET_EUR);
		types.add(Constants.MARKET_CNY);
		types.add(Constants.MARKET_RUB);
		types.add(Constants.MARKET_GBP);
		return types;
	}

	public static List<String> getTradingFeeMarketCode() {

		List<String> types = new ArrayList<>();
		types.add(Constants.FEE_TRADE1);
		types.add(Constants.FEE_TRADE2);
		return types;
	}

	/**
	 * Assets Version constant
	 */
	public static final String ASSETS_VERSION = "assetsVersion";
	/**
	 * The name of the ResourceBundle used in this application
	 */
	public static final String BUNDLE_KEY = "ApplicationResources";

	/**
	 * File separator from System properties
	 */
	public static final String FILE_SEP = System.getProperty("file.separator");

	/**
	 * User home from System properties
	 */
	public static final String USER_HOME = System.getProperty("user.home")
			+ FILE_SEP;

	/**
	 * The name of the configuration hashmap stored in application scope.
	 */
	public static final String CONFIG = "appConfig";

	/**
	 * Session scope attribute that holds the locale set by the user. By setting
	 * this key to the same one that Struts uses, we get synchronization in
	 * Struts w/o having to do extra work or have two session-level variables.
	 */
	public static final String PREFERRED_LOCALE_KEY = "org.apache.struts2.action.LOCALE";

	/**
	 * The request scope attribute under which an editable user form is stored
	 */
	public static final String USER_KEY = "userForm";

	/**
	 * The request scope attribute that holds the user list
	 */
	public static final String USER_LIST = "userList";

	/**
	 * The request scope attribute for indicating a newly-registered user
	 */
	public static final String REGISTERED = "registered";

	/**
	 * The name of the Administrator role, as specified in web.xml
	 */
	public static final String ADMIN_ROLE = "ROLE_ADMIN";

	/**
	 * The name of the User role, as specified in web.xml
	 */
	public static final String USER_ROLE = "ROLE_USER";

	/**
	 * The name of the user's role list, a request-scoped attribute when
	 * adding/editing a user.
	 */
	public static final String USER_ROLES = "userRoles";

	/**
	 * The name of the available roles list, a request-scoped attribute when
	 * adding/editing a user.
	 */
	public static final String AVAILABLE_ROLES = "availableRoles";

	/**
	 * The name of the CSS Theme setting.
	 * 
	 * @deprecated No longer used to set themes.
	 */
	public static final String CSS_THEME = "csstheme";
}
