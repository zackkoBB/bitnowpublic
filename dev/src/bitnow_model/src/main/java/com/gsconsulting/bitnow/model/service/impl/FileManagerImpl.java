package com.gsconsulting.bitnow.model.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gsconsulting.bitnow.model.File;
import com.gsconsulting.bitnow.model.dao.FileDao;
import com.gsconsulting.bitnow.model.service.FileManager;

/**
 * Implementation of UserManager interface.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Service("fileManager")
public class FileManagerImpl extends GenericManagerImpl<File, Long> implements
		FileManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4488457924385953401L;
	private FileDao fileDao;

	@Override
	@Autowired
	public void setFileDao(final FileDao fileDao) {
		this.dao = fileDao;
		this.fileDao = fileDao;
	}

	/**
	 * {@inheritDoc}
	 */
	public File getFileById(Long id) {
		return fileDao.getFileById(id);
	}

	/**
	 * {@inheritDoc}
	 */
	public Long create(byte[] data) {
		return this.fileDao.create(data);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Long id, byte[] data) {
		this.fileDao.update(id, data);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Long id) {
		this.fileDao.delete(id);
	}
}
