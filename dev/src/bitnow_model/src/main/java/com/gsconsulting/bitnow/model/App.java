package com.gsconsulting.bitnow.model;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.gsconsulting.bitnow.model.dao.ConfigurationDao;
import com.gsconsulting.bitnow.model.dao.SellerDao;
import com.gsconsulting.bitnow.model.dao.TransactionDao;
import com.gsconsulting.bitnow.model.dao.UserDao;

/**
 * Hello world!
 *
 */
public class App {

	private final static Log log = LogFactory.getLog(App.class);

	private static String[] appContext = { "applicationContext-dao.xml",
			"applicationContext-resources.xml",
			"applicationContext-service.xml" };

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				appContext);
		ConfigurationDao configurationDAO = (ConfigurationDao) context
				.getBean("configurationDao");
		for (Configuration configuration : configurationDAO.getAll()) {
			log.info(configuration.getKey() + ":" + configuration.getValue());
		}
		UserDao userDAO = (UserDao) context.getBean("userDao");
		for (User user : userDAO.getUsers()) {
			log.info("User: " + user.getUsername() + user.getEmail());
		}
//		SellerDao sellerDAO = (SellerDao) context.getBean("sellerDao");
//		TransactionDao transactionDAO = (TransactionDao) context.getBean("transactionHistoryDao");
//		Transaction transactionTemplate = new Transaction();
//		for (Seller seller : sellerDAO.getAll()) {
//			log.info("Seller: " + seller.getCf() + " " + seller.getPostepay());
//			for (Transaction transaction : transactionDAO.getTransactionBySeller(seller)) {
//				log.info("Transaction: " + transaction.getCode() + " " + transaction.getBtc());
//				transactionTemplate = transaction;
//			}
//
//		}
//		transactionTemplate.setCode("TMP 0001");
//		transactionDAO.create(transactionTemplate);
//		transactionTemplate.setAddress("this is a try");
//		transactionDAO.update(transactionTemplate);
//		transactionDAO.delete(transactionTemplate);
		context.close();
	}
}
