package com.gsconsulting.bitnow.model.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.Document;
import com.gsconsulting.bitnow.model.User;
import com.gsconsulting.bitnow.model.dao.DocumentDao;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler </a>
 */
public interface DocumentManager extends
		GenericManager<Document, Long>, Serializable {

	/**
	 * Convenience method for testing - allows you to mock the DAO and set it on
	 * an interface.
	 * 
	 * @param userDao
	 *            the UserDao implementation to use
	 */
	public void setDocumentDao(DocumentDao documentDao);

	/**
	 * Gets Document information based on Key.
	 * 
	 * @param Key
	 *            the Document to be retrieved
	 * @return Document retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public Document getDocumentById(Long id);

	/**
	 * Gets Document information based on Order.
	 * 
	 * @param Order
	 *            the Document to be retrieved
	 * @return List<Document> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<Document> getDocumentByUser(User user);

	/**
	 * Gets all Document entities in the db
	 * 
	 * @return List<Document> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<Document> getAll();

	/**
	 * Create a new Document
	 * 
	 * @throws
	 */
	@Transactional
	public Long create(Document document);

	/**
	 * Update an existing Document instance
	 * 
	 * @throws
	 */
	@Transactional
	public void update(Document document);

	/**
	 * Delete an existing Document instance
	 * 
	 * @throws
	 */
	@Transactional
	public void delete(Document document);
}
