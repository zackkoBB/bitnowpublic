package com.gsconsulting.bitnow.model.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.Transaction;
import com.gsconsulting.bitnow.model.dao.TransactionDao;
import com.gsconsulting.bitnow.model.enums.TransactionStatus;
import com.gsconsulting.bitnow.model.enums.TransactionType;
import com.gsconsulting.bitnow.model.service.TransactionManager;

/**
 * Implementation of UserManager interface.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Service("transactionHistoryManager")
public class TransactionManagerImpl extends
		GenericManagerImpl<Transaction, String> implements TransactionManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8253446758121224955L;
	private TransactionDao transactionDao;

	@Override
	@Autowired
	public void setTransactionDao(final TransactionDao transactionDao) {
		this.dao = transactionDao;
		this.transactionDao = transactionDao;
	}

	/**
	 * {@inheritDoc}
	 */
	public Transaction getTransactionByCode(String code) {

		// Query qry = getSession().createQuery(
		// "from Exchange e where e.code='" + code + "'");
		// if (qry.list() != null) {
		// if (qry.list().size() > 0) {
		// return (Exchange) qry.list().get(0);
		// }
		// }
		// return null;
		return this.transactionDao.getTransactionByCode(code);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Transaction> getTransactionByAccount(Account account,
			Long timestamp) {
		return this.transactionDao.getTransactionByAccount(account, timestamp);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Transaction> getTransactionByAccountAndStatus(Account account,
			TransactionStatus status) {
		return this.transactionDao.getTransactionByAccountAndStatus(account,
				status);
	}

	/**
	 * {@inheritDoc}
	 */
	public Double getTransactionVolume() {
		return transactionDao.getTransactionVolume();
	}

	/**
	 * {@inheritDoc}
	 */
	public Transaction getTransactionByStatusAndEuro(TransactionStatus status,
			Double euro) {
		return transactionDao.getTransactionByStatusAndEuro(status, euro);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Transaction> getTransactionTypeByAccountStatusAndTimestampGreater(
			TransactionType type, Account account, TransactionStatus status,
			Long timestamp) {
		return transactionDao
				.getTransactionTypeByAccountStatusAndTimestampGreater(type,
						account, status, timestamp);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Transaction> getTransactionByAddress(String address) {
		return transactionDao.getTransactionByAddress(address);
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(Transaction transaction) {
		this.transactionDao.create(transaction);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Transaction transaction) {
		this.transactionDao.update(transaction);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Transaction transaction) {
		this.transactionDao.delete(transaction);
	}
}
