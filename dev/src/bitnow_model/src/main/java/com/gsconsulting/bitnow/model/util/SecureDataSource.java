package com.gsconsulting.bitnow.model.util;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SecureDataSource extends BasicDataSource {

	protected final Log log = LogFactory.getLog(getClass());

	@Override
	public synchronized void setUrl(String url) {

		String urlD = new String();
		try {
			urlD = CryptService.getInstance().decrypt(url);
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException e) {
			e.printStackTrace();
		}
		super.setUrl(urlD);
	}

	@Override
	public void setUsername(String username) {

		String usernameD = new String();
		try {
			usernameD = CryptService.getInstance().decrypt(username);
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException e) {
			e.printStackTrace();
		}
		super.setUsername(usernameD);
	}

	@Override
	public void setPassword(String password) {

		String passwordD = new String();
		try {
			passwordD = CryptService.getInstance().decrypt(password);
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException e) {
			e.printStackTrace();
		}
		super.setPassword(passwordD);
	}
}
