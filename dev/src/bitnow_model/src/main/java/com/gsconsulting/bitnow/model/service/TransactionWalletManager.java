package com.gsconsulting.bitnow.model.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.TransactionWallet;
import com.gsconsulting.bitnow.model.dao.TransactionWalletDao;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler </a>
 */
public interface TransactionWalletManager extends
		GenericManager<TransactionWallet, String>, Serializable {

	/**
	 * Convenience method for testing - allows you to mock the DAO and set it on
	 * an interface.
	 * 
	 * @param userDao
	 *            the UserDao implementation to use
	 */
	void setTransactionWalletDao(TransactionWalletDao transactionDao);

	/**
	 * Gets TransactionWallet information based on code.
	 * 
	 * @param Id
	 *            id of the TransactionWallet to be retrieved
	 * @return TransactionWallet retrieved
	 * 
	 * @throws
	 */
	@Transactional
	TransactionWallet getTransactionById(Long id);

	/**
	 * Gets TransactionWallet information based on account.
	 * 
	 * @param Account
	 *            account of the TransactionWallet to be retrieved
	 * @return List<TransactionWallet> transactions retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<TransactionWallet> getTransactionByAccount(Account account,
			Long timestamp);

	/**
	 * Gets TransactionWallet information based on address.
	 * 
	 * @param address
	 *            address of the TransactionWallet to be retrieved
	 * @return List<TransactionWallet> transactions retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<TransactionWallet> getTransactionByAddress(String address);

	/**
	 * Gets TransactionWallet information based on confirmations (less than).
	 * 
	 * @param Integer
	 *            max number of confirmations allowed for the query
	 * @return List<TransactionWallet> transactions retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<TransactionWallet> getTransactionByConfirmations(Integer confirmations);

	/**
	 * Gets all TransactionWallet entities in the db
	 * 
	 * @return List<TransactionWallet> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	List<TransactionWallet> getAll();

	/**
	 * Create a new TransactionWallet
	 * 
	 * @throws
	 */
	@Transactional
	void create(TransactionWallet transactionWallet);

	/**
	 * Update an existing TransactionWallet instance
	 * 
	 * @throws
	 */
	@Transactional
	void update(TransactionWallet transactionWallet);

	/**
	 * Delete an existing TransactionWallet instance
	 * 
	 * @throws
	 */
	@Transactional
	void delete(TransactionWallet transactionWallet);
}
