package com.gsconsulting.bitnow.model.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.Payment;

public interface PaymentDao {
	
	/**
	 * Gets Payment information based on code.
	 * 
	 * @param String
	 *            key of the payment to be retrieved
	 * @return Payment retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public Payment getPaymentByKey(String key);

	/**
	 * Gets all Payment entities in the db
	 * 
	 * @return List<Payment> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<Payment> getAllPayment();

	/**
	 * Create a new Payment
	 * 
	 * @throws
	 */
	@Transactional
	public void create(Payment payment);

	/**
	 * Update an existing Payment instance
	 * 
	 * @throws
	 */
	@Transactional
	public void update(Payment payment);

	/**
	 * Delete an existing Payment instance
	 * 
	 * @throws
	 */
	@Transactional
	public void delete(Payment payment);
}
