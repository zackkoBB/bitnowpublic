package com.gsconsulting.bitnow.model.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gsconsulting.bitnow.model.Wallet;
import com.gsconsulting.bitnow.model.dao.WalletDao;
import com.gsconsulting.bitnow.model.service.WalletManager;

/**
 * Implementation of UserManager interface.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Service("walletManager")
public class WalletManagerImpl extends GenericManagerImpl<Wallet, String>
		implements WalletManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7878131660435609809L;
	private WalletDao walletDao;

	@Override
	@Autowired
	public void setWalletDao(final WalletDao walletDao) {
		this.dao = walletDao;
		this.walletDao = walletDao;
	}

	/**
	 * {@inheritDoc}
	 */
	public Wallet getWalletByKey(Integer key) {
		return walletDao.getWalletByKey(key);
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(Wallet wallet) {
		this.walletDao.create(wallet);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Wallet wallet) {
		this.walletDao.update(wallet);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Wallet wallet) {
		this.walletDao.delete(wallet);
	}
}
