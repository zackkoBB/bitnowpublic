package com.gsconsulting.bitnow.model.ids;

import java.io.Serializable;

import com.gsconsulting.bitnow.model.BaseObject;
import com.gsconsulting.bitnow.model.Seller;
import com.gsconsulting.bitnow.model.Transaction;

/**
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
public class SellerTransactionKey extends BaseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2076882144713860909L;
	private Seller seller;
	private Transaction transaction;
	

	public SellerTransactionKey() {
		super();
	}

	public SellerTransactionKey(Seller seller, Transaction transaction) {
		super();
		this.seller = seller;
		this.transaction = transaction;
	}

	public Seller getSeller() {
		return seller;
	}

	public void setSeller(Seller seller) {
		this.seller = seller;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}
}
