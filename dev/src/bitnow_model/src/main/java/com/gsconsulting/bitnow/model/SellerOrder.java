package com.gsconsulting.bitnow.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.gsconsulting.bitnow.model.ids.SellerOrderKey;

/**
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
@Entity
@IdClass(SellerOrderKey.class)
@Table(name = "seller_order")
public class SellerOrder extends BaseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -485674358042536660L;
	private Seller seller;
	private Order order;
		
	public SellerOrder() {
		super();
	}

	public SellerOrder(Seller seller, Order order) {
		super();
		this.seller = seller;
		this.order = order;
	}

	@Id
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="seller", referencedColumnName = "CF")
	public Seller getSeller() {
		return seller;
	}

	public void setSeller(Seller seller) {
		this.seller = seller;
	}

	@Id
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="order", referencedColumnName = "code")
	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}
}
