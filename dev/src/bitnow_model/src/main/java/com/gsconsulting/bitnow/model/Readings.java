package com.gsconsulting.bitnow.model;

import java.io.Serializable;
import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
@Entity
@Table(name = "readings")
public class Readings extends BaseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8075989090700547985L;
	private Long id;
	private Blob read;

	public Readings() {
		super();
	}

	public Readings(Long id, Blob read) {
		super();
		this.id = id;
		this.read = read;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "`read`")
	public Blob getRead() {
		return read;
	}

	public void setRead(Blob read) {
		this.read = read;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
