package com.gsconsulting.bitnow.model.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gsconsulting.bitnow.model.dao.BTCAddressDao;
import com.gsconsulting.bitnow.model.BTCAddress;
import com.gsconsulting.bitnow.model.User;
import com.gsconsulting.bitnow.model.service.BTCAddressManager;

/**
 * Implementation of UserManager interface.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Service("btcAddressManager")
public class BTCAddressManagerImpl extends
		GenericManagerImpl<BTCAddress, String> implements BTCAddressManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1797413353678475958L;
	private BTCAddressDao btcAddressDao;

	@Override
	@Autowired
	public void setBTCAddressDao(final BTCAddressDao btcAddressDao) {
		this.dao = btcAddressDao;
		this.btcAddressDao = btcAddressDao;
	}

	/**
	 * {@inheritDoc}
	 */
	public BTCAddress getBTCAddressById(String id) {
		return this.btcAddressDao.getBTCAddressById(id);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<BTCAddress> getBTCAddressByUser(User user) {
		return this.btcAddressDao.getBTCAddressByUser(user);
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(BTCAddress btcAddress) {
		this.btcAddressDao.create(btcAddress);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(BTCAddress btcAddress) {
		this.btcAddressDao.update(btcAddress);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(BTCAddress btcAddress) {
		this.btcAddressDao.delete(btcAddress);
	}
}
