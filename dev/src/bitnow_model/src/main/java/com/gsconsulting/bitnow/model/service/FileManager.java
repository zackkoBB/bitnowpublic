package com.gsconsulting.bitnow.model.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gsconsulting.bitnow.model.File;
import com.gsconsulting.bitnow.model.dao.FileDao;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler </a>
 */
public interface FileManager extends GenericManager<File, Long>, Serializable {

	/**
	 * Convenience method for testing - allows you to mock the DAO and set it on
	 * an interface.
	 * 
	 * @param userDao
	 *            the UserDao implementation to use
	 */
	public void setFileDao(FileDao fileDao);

	/**
	 * Gets File information based on Key.
	 * 
	 * @param Key
	 *            the File to be retrieved
	 * @return File retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public File getFileById(Long id);

	/**
	 * Gets all File entities in the db
	 * 
	 * @return List<File> retrieved
	 * 
	 * @throws
	 */
	@Transactional
	public List<File> getAll();

	/**
	 * Create a new File
	 * 
	 * @throws
	 */
	@Transactional
	public Long create(byte[] data);

	/**
	 * Update an existing File instance
	 * 
	 * @throws
	 */
	@Transactional
	public void update(Long id, byte[] data);

	/**
	 * Delete an existing File instance
	 * 
	 * @throws
	 */
	@Transactional
	public void delete(Long id);
}
