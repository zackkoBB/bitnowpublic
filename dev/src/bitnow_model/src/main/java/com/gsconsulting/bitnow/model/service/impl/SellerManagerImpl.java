package com.gsconsulting.bitnow.model.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gsconsulting.bitnow.model.dao.SellerDao;
import com.gsconsulting.bitnow.model.Seller;
import com.gsconsulting.bitnow.model.service.SellerManager;

/**
 * Implementation of UserManager interface.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Service("sellerManager")
public class SellerManagerImpl extends GenericManagerImpl<Seller, String>
		implements SellerManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8532727980481589583L;
	private SellerDao sellerDao;

	@Override
	@Autowired
	public void setSellerDao(final SellerDao sellerDao) {
		this.dao = sellerDao;
		this.sellerDao = sellerDao;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Seller> getSellerByBtc(Double btc) {
		return this.sellerDao.getSellerByBtc(btc);
	}

	/**
	 * {@inheritDoc}
	 */
	public Seller getSellerByCf(String cf) {
		return this.sellerDao.getSellerByCf(cf);
	}

	/**
	 * {@inheritDoc}
	 */
	public void create(Seller seller) {
		this.sellerDao.create(seller);
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(Seller seller) {
		this.sellerDao.update(seller);
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Seller seller) {
		this.sellerDao.delete(seller);
	}
}
