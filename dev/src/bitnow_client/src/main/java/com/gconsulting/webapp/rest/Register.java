package com.gconsulting.webapp.rest;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.faces.FactoryFinder;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.context.FacesContextFactory;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gconsulting.webapp.exception.ClientException;
import com.gsconsulting.bitnow.model.User;
import com.gsconsulting.bitnow.model.service.ConfigurationManager;
import com.gsconsulting.bitnow.model.service.UserManager;
import com.gsconsulting.bitnow.model.util.Constants;

@RestController
public class Register {

	protected final Log log = LogFactory.getLog(getClass());

	private ConfigurationManager configurationManager;
	protected UserManager userManager;
	private String key;

	@Autowired
	public void setConfigurationManager(
			@Qualifier("configurationManager") ConfigurationManager configurationManager) {
		this.configurationManager = configurationManager;
	}

	@Autowired
	public void setUserManager(@Qualifier("userManager") UserManager userManager) {
		this.userManager = userManager;
	}

	/**
	 * Init method called after construct
	 * 
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	@PostConstruct
	public void init() throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException {

		if (configurationManager != null) {
			log.info("Environment: "
					+ configurationManager.getConfigurationByKey(
							Constants.ENVIRONMENT).getValue());
			key = configurationManager.getConfigurationByKey(
					Constants.REST_PRICE_KEY).getValue();
		}
	}

	private void returnError(HttpServletResponse response, String text)
			throws IOException {
		// GradientPaint gradientPaint = new GradientPaint(10, 5, Color.GREEN,
		// 20,
		// 10, Color.RED, true);
		drawImage(response, text, Color.RED);
	}

	private void drawImage(HttpServletResponse response, String text,
			Paint paint) throws IOException {

		// response.setContentType(MediaType.IMAGE_JPEG_VALUE);
		response.setContentType(MediaType.IMAGE_PNG_VALUE);
		ServletOutputStream out = response.getOutputStream();
		// BufferedImage image = new BufferedImage(200, 40,
		// BufferedImage.TYPE_BYTE_INDEXED);
		BufferedImage image = new BufferedImage(200, 40,
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D graphics = image.createGraphics();
		graphics.setComposite(AlphaComposite.Clear);
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, 200, 40);
		graphics.setPaint(paint);
		// Font font = new Font("Comic Sans MS", Font.PLAIN, 30);
		Font font = new Font("Verdana", Font.PLAIN, 30);
		graphics.setFont(font);
		graphics.setComposite(AlphaComposite.Src);
		graphics.drawString(text, 5, 30);
		graphics.dispose();
		ImageIO.write(image, "png", out);
		out.close();
	}

	private FacesContext getFacesContext(HttpServletRequest request,
			HttpServletResponse response) {

		// Get current FacesContext.
		FacesContext facesContext = FacesContext.getCurrentInstance();
		// Check current FacesContext.
		if (facesContext == null) {
			// Create new Lifecycle.
			LifecycleFactory lifecycleFactory = (LifecycleFactory) FactoryFinder
					.getFactory(FactoryFinder.LIFECYCLE_FACTORY);
			Lifecycle lifecycle = lifecycleFactory
					.getLifecycle(LifecycleFactory.DEFAULT_LIFECYCLE);
			// Create new FacesContext.
			FacesContextFactory contextFactory = (FacesContextFactory) FactoryFinder
					.getFactory(FactoryFinder.FACES_CONTEXT_FACTORY);
			facesContext = contextFactory.getFacesContext(request.getSession()
					.getServletContext(), request, response, lifecycle);
			// Create new View.
			UIViewRoot view = facesContext.getApplication().getViewHandler()
					.createView(facesContext, "");
			facesContext.setViewRoot(view);
			// Set current FacesContext.
			// FacesContextWrapper.setCurrentInstance(facesContext);
		}
		return facesContext;
	}

	/**
	 * Servlet API Convenience method
	 * 
	 * @return HttpServletRequest from the FacesContext
	 */
	private HttpServletRequest getRequest(FacesContext context) {
		return (HttpServletRequest) context.getExternalContext().getRequest();
	}

	private String getBundleName(FacesContext context) {
		return context.getApplication().getMessageBundle();
	}

	private ResourceBundle getBundle(FacesContext context) {
		ClassLoader classLoader = Thread.currentThread()
				.getContextClassLoader();
		return ResourceBundle.getBundle(getBundleName(context),
				getRequest(context).getLocale(), classLoader);
	}

	private String getText(FacesContext context, String key) {
		String message;

		try {
			message = getBundle(context).getString(key);
		} catch (java.util.MissingResourceException mre) {
			log.warn("Missing key for '" + key + "'");
			return "???" + key + "???";
		}
		return message;
	}

	/**
	 * After 1 Confirmation: Moves order to transaction, moves account_order to
	 * account_transaction, update accountBalance
	 * 
	 * 
	 * @param response
	 * @param value
	 * @param transaction_hash
	 * @param input_address
	 * @param confirmations
	 * @param keyReceived
	 * @throws ClientException
	 * @throws IOException
	 */
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public void confirmRegistration(
			@RequestParam(value = "user") String username,
			@RequestParam(value = "otp") String otp,
			@RequestParam(value = "key") String keyReceived,
			HttpServletRequest request, HttpServletResponse response)
			throws ClientException, IOException {

		log.info("Parameters: " + username + " " + otp + " " + keyReceived);
		if (keyReceived.equalsIgnoreCase(key)) {
			if (username != null) {
				User user = userManager.getUserByUsername(username);
				if (user != null) {
					if (!user.isEnabled()) {
						if (user.getSecret().equals(otp)) {
							user.setSecret("");
							user.setEnabled(true);
							userManager.update(user);
							/*
							 * Login
							 */
							FacesContext context = getFacesContext(request,
									response);
							context.addMessage(
									"messages",
									new FacesMessage(
											FacesMessage.SEVERITY_INFO, "", // "Info",
											getText(context,
													"register.accountActivated")));
//							context.getExternalContext().getFlash()
//							 .setKeepMessages(true);
							context.getExternalContext().redirect(
									context.getExternalContext()
											.getApplicationContextPath()
											+ "/login");
						}
					}
				}
			}
		} else {
			// wrong secret
			returnError(response, Constants.KEY_WRONG_MESSAGE);
		}
	}

}