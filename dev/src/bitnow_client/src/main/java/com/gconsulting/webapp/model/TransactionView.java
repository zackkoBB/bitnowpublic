package com.gconsulting.webapp.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.Receipt;

/**
 *
 * @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
 */
public class TransactionView implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -33897399403257092L;
	private String code;
	private Integer type;
	private String address;
	private String email;
	private String transactionHash;
	private Double btc;
	private Double eur;
	private Double price;
	private Double fee;
	private Date created;
	private Date modified;
	private String note;
	private Integer status;
	private Account account;
	private List<Receipt> receipts;

	public TransactionView() {
		super();
	}

	public TransactionView(String code, Integer type, String address, String email, String transactionHash,
			Double btc, Double eur, Double price, Double fee, Date created,
			Date modified, String note, Integer status, Account account) {
		super();
		this.code = code;
		this.type = type;
		this.address = address;
		this.email = email;
		this.transactionHash = transactionHash;
		this.btc = btc;
		this.eur = eur;
		this.price = price;
		this.fee = fee;
		this.created = created;
		this.modified = modified;
		this.note = note;
		this.status = status;
		this.account = account;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTransactionHash() {
		return transactionHash;
	}

	public void setTransactionHash(String transactionHash) {
		this.transactionHash = transactionHash;
	}

	public Double getBtc() {
		return btc;
	}

	public void setBtc(Double btc) {
		this.btc = btc;
	}

	public Double getEur() {
		return eur;
	}

	public void setEur(Double eur) {
		this.eur = eur;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getFee() {
		return fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public List<Receipt> getReceipts() {
		return receipts;
	}

	public void setReceipts(List<Receipt> receipts) {
		this.receipts = receipts;
	}
}
