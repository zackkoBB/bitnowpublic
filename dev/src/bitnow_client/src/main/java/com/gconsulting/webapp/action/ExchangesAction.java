package com.gconsulting.webapp.action;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gsconsulting.bitnow.model.util.Constants;
import com.gsconsulting.bitnow.model.Exchange;
import com.gsconsulting.bitnow.model.service.ExchangeManager;
import com.gconsulting.webapp.util.ImportUtils;

@SuppressWarnings("unchecked")
@Scope("request")
@Component("exchangeAction")
public class ExchangesAction extends BasePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4820984471948349094L;

	private ExchangeManager exchangeManager;
	private Exchange selectedExchange = new Exchange();
	private UploadedFile importFile;
	private String code;

	@Autowired
	public void setExchangeManager(
			@Qualifier("exchangeManager") ExchangeManager exchangeManager) {
		this.exchangeManager = exchangeManager;
	}

	public ExchangesAction() {
		setSortColumn("code"); // sets the default sort column
		// isUpdate = false;
	}

	public List<Exchange> getExchanges() {
		return sort(exchangeManager.getAllExchange());
	}

	public Exchange getSelectedExchange() {
		return selectedExchange;
	}

	public void setSelectedExchange(Exchange selectedExchange) {
		this.selectedExchange = selectedExchange;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public UploadedFile getImportFile() {
		return importFile;
	}

	public void setImportFile(UploadedFile importFile) {
		this.importFile = importFile;
	}

	public String importExchanges() {

		HttpServletRequest request = getRequest();
		// write the file to the filesystem
		// the directory to upload to
		String uploadDir = getFacesContext().getExternalContext().getRealPath(
				"/resources");
		// The following seems to happen when running jetty:run
		if (uploadDir == null) {
			uploadDir = new File("src/main/webapp/resources").getAbsolutePath();
		}
		uploadDir += "/" + request.getRemoteUser() + "/";
		try {
			String filename = importFile.getFileName();
			if (filename.endsWith(Constants.CSV_SUFFIX)) {
				// Create the CSVFormat object
				CSVFormat format = CSVFormat.RFC4180.withHeader()
						.withDelimiter(';');
				CSVParser parser = new CSVParser(new FileReader(
						ImportUtils.saveFile(importFile, uploadDir)), format);
				int i = 0, total = 0;
				for (CSVRecord csvExchange : parser.getRecords()) {
					Exchange exchange = new Exchange();
					exchange.setCode(csvExchange
							.get(Constants.EXCHANGES_IMPORT_HEADER1));
					exchange.setName(csvExchange
							.get(Constants.EXCHANGES_IMPORT_HEADER2));
					exchange.setDescription(csvExchange
							.get(Constants.EXCHANGES_IMPORT_HEADER3));
					exchange.setWebsiteAddress(csvExchange
							.get(Constants.EXCHANGES_IMPORT_HEADER4));
					exchange.setApiAddress(csvExchange
							.get(Constants.EXCHANGES_IMPORT_HEADER5));
					exchange.setFeeAddress(csvExchange
							.get(Constants.EXCHANGES_IMPORT_HEADER6));
					exchange.setRestInterface(new Boolean(csvExchange
							.get(Constants.EXCHANGES_IMPORT_HEADER7)));
					if (exchangeManager.getExchangeByCode(exchange.getCode()) == null) {
						exchangeManager.create(exchange);
						i++;
					}
					total++;
				}
				// close the parser
				parser.close();
//		        getFacesContext().addMessage("messages", 
//		        		new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "PrimeFaces Rocks."));
//		        getFacesContext().getExternalContext().getFlash().setKeepMessages(true);
//				addMessage("exchanges.import.imported");
//				addMessage("exchanges.import.importedSummary", new Object[] {
//						i, total - i });
				addFacesMessage("exchanges.import.imported");
				addFacesMessage("exchanges.import.importedSummary", new Object[] {
						i, total - i });
			} else {
				addFacesError("exchanges.import.error");
			}
		} catch (IOException e) {
			addFacesError("exchanges.import.error");
		}
		return "list";
	}

	public String delete() {

		exchangeManager.delete(selectedExchange);
		addFacesMessage("exchanges.deleted");
		return "list";
	}

	public String edit() {

		if (code == null) {
			code = new String(getParameter("code"));
		}
		selectedExchange = exchangeManager.getExchangeByCode(code);
		return "edit";
	}

	public String save() {

		boolean isUpdate = (exchangeManager.getExchangeByCode(selectedExchange
				.getCode()) == null) ? false : true;
		if (!isUpdate) {
			exchangeManager.create(selectedExchange);
			addFacesMessage("exchanges.added");
		} else {
			exchangeManager.update(selectedExchange);
			addFacesMessage("exchanges.updated");
		}
		return "list";
	}
}
