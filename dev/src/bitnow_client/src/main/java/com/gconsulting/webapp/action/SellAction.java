package com.gconsulting.webapp.action;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.mail.MessagingException;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

import com.gconsulting.webapp.coge.COGE;
import com.gconsulting.webapp.model.ExchangeRate;
import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.Order;
import com.gsconsulting.bitnow.model.Wallet;
import com.gsconsulting.bitnow.model.enums.AccountStatus;
import com.gsconsulting.bitnow.model.enums.AccountType;
import com.gsconsulting.bitnow.model.enums.TransactionStatus;
import com.gsconsulting.bitnow.model.enums.TransactionType;
import com.gsconsulting.bitnow.model.service.AccountManager;
import com.gsconsulting.bitnow.model.service.ConfigurationManager;
import com.gsconsulting.bitnow.model.service.MailEngine;
import com.gsconsulting.bitnow.model.service.OrderManager;
import com.gsconsulting.bitnow.model.util.Constants;
import com.gsconsulting.bitnow.model.util.CryptService;

/**
 * Managed Bean to send password hints to registered users.
 *
 * <p>
 * <a href="PasswordHint.java.html"><i>View Source</i></a>
 * </p>
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Scope("session")
@Component("sellAction")
@SuppressWarnings("unused")
public class SellAction extends BasePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4581810403934633386L;
	private boolean walletEUREmpty;
	private boolean oversize;
	private Double walletEURBalance;
	// private Double walletBTCBalanceEUR;
	private Double sellMAXPayment;
	private Double btcQuantityDouble;
	private Double eurQuantityDouble;
	private String code;
	private String btcQuantity;
	private String eurQuantity;
	private String address;
	private OrderManager orderManager;
	private ConfigurationManager configurationManager;
	private AccountManager accountManager;
	private Double priceSell;
	private Double feeSell;
	private Double feeBuyer;
	private String priceSellString;
	private String email;
	private String orderSubmittedSummary;
	private String orderSubmittedDetail;
	private String lastAddress;
	private String lastEmail;
	private String lastCode;
	private String orderBTCQuantity;
	private String orderEURQuantity;
	private List<Account> accounts;
	private Account selectedAccount;
	private DecimalFormat df2;

	/**
	 * "Constants"
	 */
	private Integer pollingInterval;
	private String postePayNumber;
	private String postePayName;
	private String postePaySSN;
	private String lastPostePayNumber;
	private String lastPostePayName;
	private String lastPostePaySSN;

	/**
	 * Default constructor
	 */
	public SellAction() {
		super();
	}

	/**
	 * Init method called after construct
	 */
	@PostConstruct
	public void init() {

		if (configurationManager != null) {
			log.info("Environment: "
					+ configurationManager.getConfigurationByKey(
							Constants.ENVIRONMENT).getValue());
			pollingInterval = new Integer(configurationManager
					.getConfigurationByKey(Constants.UI_POLLING_INTERVAL)
					.getValue());
			// feeBuy = new Double(configurationManager.getConfigurationByKey(
			// Constants.FEE_BUY).getValue());
			feeSell = new Double(configurationManager.getConfigurationByKey(
					Constants.FEE_SELL).getValue());
			feeBuyer = new Double(configurationManager.getConfigurationByKey(
					Constants.FEE_BUYER).getValue());
			sellMAXPayment = new Double(configurationManager
					.getConfigurationByKey(Constants.MAX_SELL_PAYMENT)
					.getValue().replace(",", "."));
		}
		if (accountManager != null) {
			// accounts = accountManager.getAll();
		}
		df2 = new DecimalFormat(".##");
		btcQuantity = new String();
		eurQuantity = new String("0.00");
		btcQuantityDouble = new Double(0d);
		eurQuantityDouble = new Double(0d);
		walletEUREmpty = true;
		oversize = false;
		walletEURBalance = new Double(0D);
		// walletBTCBalanceEUR = new Double(0D);
		priceSellString = new String();
		priceSell = new Double(0D);
		getData();
	}

	@Autowired
	public void setOrderManager(
			@Qualifier("orderManager") OrderManager orderManager) {
		this.orderManager = orderManager;
	}

	@Autowired
	public void setConfigurationManager(
			@Qualifier("configurationManager") ConfigurationManager configurationManager) {
		this.configurationManager = configurationManager;
	}

	@Autowired
	public void setAccountManager(
			@Qualifier("accountManager") AccountManager accountManager) {
		this.accountManager = accountManager;
	}

	@Autowired
	public void setMailEngine(@Qualifier("mailEngine") MailEngine mailEngine) {
		this.mailEngine = mailEngine;
	}

	/**
	 * Send an email Memo
	 * 
	 * @param PINs
	 *            of the transaction
	 * @param size
	 *            total size of the transaction
	 * @throws MessagingException
	 */
	private void sendMailSell(String subject, String destination,
			String alertTime) throws MessagingException {

		// System.getProperty("line.separator")
		message = new SimpleMailMessage();
		// message.setFrom("contatti@bitnow.it");
		message.setTo(destination);
		message.setSubject(subject);
		Map<String, Object> args = new HashMap<>();
		args.put("webappURL", getText("website.url"));
		args.put("webappName", getText("webapp.name"));
		args.put("webappTagline", getText("webapp.tagline"));
		args.put("orderSummary", getText("success.orderSubmitted.orderSummary"));
		args.put("orderSummary_confirmationCode",
				getText("success.orderSubmitted.orderSummary.confirmationCode"));
		args.put("lastCode", lastCode);
		args.put("orderSummary_bitcoin",
				getText("success.orderSubmitted.orderSummary.bitcoin"));
		args.put("orderBTCQuantity", orderBTCQuantity);
		args.put("orderSummary_address",
				getText("success.orderSubmitted.orderSummary.address"));
		args.put("lastAddress", lastAddress);
		args.put("orderSubmitted_postePayTopUpDetails",
				getText("success.orderSubmitted.postePayTopUpDetails"));
		args.put("orderSubmitted_postePayTopUpDetails_euro",
				getText("success.orderSubmitted.postePayTopUpDetails.euro"));
		args.put("orderEURQuantity", orderEURQuantity);
		args.put("orderSubmitted_postePayTopUpDetails_number",
				getText("success.orderSubmitted.postePayTopUpDetails.number"));
		args.put("postePayNumber", postePayNumber);
		args.put("orderSubmitted_postePayTopUpDetails_name",
				getText("success.orderSubmitted.postePayTopUpDetails.name"));
		args.put("postePayName", postePayName);
		args.put("orderSubmitted_postePayTopUpDetails_ssn",
				getText("success.orderSubmitted.postePayTopUpDetails.ssn"));
		args.put("postePaySSN", postePaySSN);
		args.put("orderSubmitted_alertTime", alertTime);
		args.put("orderSubmitted_emailClose",
				getText("success.orderSubmitted.emailClose"));
		mailEngine
				.sendHTML(
						message,
						com.gconsulting.webapp.util.Constants.SELL_ORDER_EMAIL_TEMPLATE,
						args);
	}

	/**
	 * Handle btcQuantity change
	 * 
	 */
	public void btcQuantityChange() {

		try {
			btcQuantity = btcQuantity.replace("_", "0");
			eurQuantity = (df2.format(new Double(new Double(btcQuantity)
					* priceSell))).toString();
		} catch (NumberFormatException e) {
		}
	}

	/**
	 * Buy action
	 * 
	 * @return String label of the next view
	 */
	public String cancel() {
		reset();
		return "cancel";
	}

	/**
	 * Reset everything after a successfull buy
	 */
	private void reset() {

		address = new String();
		email = new String();
		selectedAccount = null;
		btcQuantity = new String();
		eurQuantity = new String("0.00");
		btcQuantityDouble = new Double(0d);
		eurQuantityDouble = new Double(0d);
		postePayNumber = new String();
		postePayName = new String();
		postePaySSN = new String();
		getData();
	}

	/**
	 * Check for a valid PostePay transaction
	 * 
	 * @return boolean true if is a valid PostePay transaction
	 */
	private boolean validSellTransaction() {

		getData();
		btcQuantityDouble = new Double(btcQuantity);
		eurQuantityDouble = new Double(eurQuantity);
		/*
		 * Check input data
		 */
		if (code != null) {
		} else {
			addFacesError("errors.general");
			return false;
		}
		if (eurQuantityDouble != null) {
			Double error = Math.abs(btcQuantityDouble
					- (eurQuantityDouble / priceSell));
			if (error <= Constants.EXCHANGE_RATE_PRECISION) {
				/*
				 * Select Accounts based on eurQuantityDouble
				 */
				selectedAccount = null;
				accounts = accountManager.getAccountByEURBalanceAvailable(
						eurQuantityDouble, AccountStatus.ACCOUNT_NOT_ACTIVE,
						AccountStatus.ACCOUNT_SELLER);
				if (accounts != null) {
					if (accounts.size() > 0) {
						/*
						 * Select Accounts based on last recent transaction
						 */
						selectedAccount = accounts.get(0);
						Long minLatestPayment = selectedAccount
								.getLatestPayment();
						for (Account account : accounts) {
							if (account.getLatestPayment() < minLatestPayment) {
								selectedAccount = account;
								minLatestPayment = selectedAccount
										.getLatestPayment();
							}
						}
						/*
						 * We have a EUR account
						 */
					}
				}
				if (selectedAccount == null) {
					/*
					 * NO SELLER
					 */
					addFacesError("errors.seller.notFound");
					getData();
					return false;
				}
			} else {
				/*
				 * HACKING Send email to Administrator they changed the
				 * btcQuantityDouble value on client
				 */
				addFacesError("errors.general");
				getData();
				return false;
			}
		} else {
			/*
			 * HACKING Send email to Administrator they changed the
			 * btcQuantityDouble value on client
			 */
			addFacesError("errors.general");
			getData();
			return false;
		}
		if ((email == null) || (postePayNumber == null)
				|| (postePayName == null) || (postePaySSN == null)) {
			/*
			 * Shouldn't be: client side validation
			 */
			addFacesError("errors.general");
			getData();
			return false;
		}
		/*
		 * Check BTC wallet balance DOUBLE CHECK!!!!!!!!!!!!!!!!!!!!!!!!
		 */
		if (COGE.getInstance().getEURWallet() != null) {
			walletEURBalance = COGE.getInstance().getEURWallet()
					.getBalanceAvailable();
			if (walletEURBalance < eurQuantityDouble) {
				addFacesError("errors.notEnough", "EURs");
				getData();
				return false;
			}
		} else {
			addFacesError("errors.general");
			getData();
			return false;
		}
		/*
		 * Check Min Order value
		 */
		if (eurQuantityDouble < Constants.MIN_SELL_ORDER) {
			addFacesError("errors.minOrder", Constants.MIN_SELL_ORDER);
			getData();
			return false;
		}
		/*
		 * Check Max Order value
		 */
		if (eurQuantityDouble > sellMAXPayment) {
			addFacesError("errors.maxOrder", sellMAXPayment.toString());
			getData();
			return false;
		}
		/*
		 * Check UNIQUE
		 * 
		 * NOT SAME TRANSACTION:
		 * 
		 * 1. IN ORDERS
		 */
		for (Order existingTransaction : orderManager.getOrderTypeByAccount(
				TransactionType.SELL, selectedAccount)) {
			Double error = Math.abs(existingTransaction.getBtc()
					- btcQuantityDouble);
			if (error <= Constants.BTC_PRECISION) {
				addFacesError("errors.existing.transaction");
				getData();
				return false;
			}
		}
		return true;
	}

	private void oldCode() {

		/*
		 * Refresh Liquidity Wallet
		 */
		if (COGE.getInstance().getEURWallet() != null) {
			Wallet EURWallet = COGE.getInstance().getEURWallet();
			Double newWalletBalance = EURWallet.getBalanceAvailable();
			newWalletBalance -= eurQuantityDouble;
			if (newWalletBalance < 0) {
				newWalletBalance = 0D;
			}
			EURWallet.setBalanceAvailable(newWalletBalance);
			COGE.getInstance().saveBTCWallet(EURWallet);
			walletEURBalance = newWalletBalance;
			/*
			 * Check MIN/MAX balance
			 */
			if (walletEURBalance < Constants.WALLET_BALANCE_MINIMUM_AMOUNT) {
				walletEUREmpty = true;
			} else {
				walletEUREmpty = false;
			}
			if (walletEURBalance > sellMAXPayment) {
				walletEURBalance = sellMAXPayment;
				oversize = true;
			} else {
				oversize = false;
			}
		} else {
			walletEUREmpty = true;
		}
		/*
		 * Refresh Liquidity Account
		 */
		Account selectedEURAccount = accountManager.getAccountByUserAndType(
				selectedAccount.getUser(), AccountType.BTC.ordinal(),
				AccountStatus.ACCOUNT_NOT_ACTIVE);
		Double balance = selectedEURAccount.getBalance();
		Double balanceInProcessing = orderManager.getOrderTypeVolumeEURByUser(
				TransactionType.SELL, selectedEURAccount.getUser());
		if (balanceInProcessing == null) {
			balanceInProcessing = 0d;
		}
		selectedEURAccount.setBalanceAvailable(balance - balanceInProcessing);
		selectedEURAccount.setLastModified(Calendar.getInstance()
				.getTimeInMillis() / 1000);
		selectedEURAccount.setLastReadStatus(AccountStatus.ACCOUNT_LAST_READ_OK
				.ordinal());
		accountManager.update(selectedEURAccount);
	}

	/**
	 * Create a PostePay transaction
	 * 
	 * @return boolean true if is a valid PostePay transaction
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	private boolean createSellTransaction() {

		/*
		 * Create transactions Fee Formula: (T - pT = q AND fee = T - q) => fee
		 * = q(p/(1-p))
		 */
		try {
			address = CryptService.decrypt(selectedAccount.getNumber());
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException e1) {
			log.error("Decoding account: " + selectedAccount.getNumber());
			return false;
		}
		BigDecimal fee = (new BigDecimal(btcQuantityDouble / 100d
				* (-feeSell - feeBuyer))).setScale(8, RoundingMode.HALF_UP);
		orderManager.create(new Order(code, TransactionType.SELL.ordinal(),
				address, email, "", btcQuantityDouble, eurQuantityDouble, fee
						.doubleValue(), Calendar.getInstance()
						.getTimeInMillis() / 1000, Calendar.getInstance()
						.getTimeInMillis() / 1000, postePayNumber + " "
						+ postePayName + " " + postePaySSN,
				TransactionStatus.WAITING_PAYMENT.ordinal(), selectedAccount));
		selectedAccount.setLatestPayment(Calendar.getInstance()
				.getTimeInMillis() / 1000);
		accountManager.update(selectedAccount);
		// oldCode();
		getData();
		/*
		 * Display SUCCESS page
		 * 
		 * Send E-Mail to Administrator
		 */
		orderBTCQuantity = btcQuantity;
		orderEURQuantity = eurQuantity;
		orderSubmittedSummary = getText(
				"success.orderSubmittedPostePaySummary", new DecimalFormat(
						"#.########").format(btcQuantityDouble));
		lastAddress = address;
		lastCode = code;
		lastPostePayNumber = postePayNumber;
		lastPostePayName = postePayName;
		lastPostePaySSN = postePaySSN;
		String subject = '[' + getText("webapp.name") + "] "
				+ getText("website.home.newOrder") + " "
				+ getText("home.dashboard.transactions.type.sell") + " " + "B"
				+ orderBTCQuantity;
		try {
			sendMailSell(subject, Constants.ADMINISTRATOR_EMAIL,
					getText("success.orderSubmitted.alertTime"));
			sendMailSell(subject,
					CryptService.decrypt(selectedAccount.getId()),
					getText("success.orderSubmitted.alertTime"));
			sendMailSell(subject, email,
					getText("success.orderSubmitted.alertTimeBTC"));
		} catch (MessagingException e) {
			addFacesError("errors.detail", e.getMessage());
			log.error("MailException: " + e.getMessage());
			// e.printStackTrace();
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	/**
	 * Buy action PostePay
	 * 
	 * @return String label of the next view
	 */
	public String sell() {

		if (validSellTransaction()) {
			if (createSellTransaction()) {
				reset();
				return "successSell";
			}
		}
		return "error";
	}

	/**
	 * Get Data from the web
	 */
	private void getData() {

		COGE.getInstance().read();
		ExchangeRate exchangeRateRetrieved = COGE.getInstance()
				.getExchangeRate();
		if (exchangeRateRetrieved != null) {
			/*
			 * Adjust exchangeRate to our value
			 */
			priceSell = exchangeRateRetrieved.getRate()
					+ exchangeRateRetrieved.getRate() / 100.00 * feeSell;
			priceSellString = df2.format(priceSell);
		}
		if (COGE.getInstance().getEURWallet() != null) {
			walletEURBalance = COGE.getInstance().getEURWallet()
					.getBalanceAvailable();
			/*
			 * Check MIN/MAX balance
			 */
			if (walletEURBalance < Constants.WALLET_BALANCE_MINIMUM_AMOUNT) {
				walletEUREmpty = true;
			} else {
				walletEUREmpty = false;
			}
			if (walletEURBalance > sellMAXPayment) {
				walletEURBalance = sellMAXPayment;
				oversize = true;
			} else {
				oversize = false;
			}
		} else {
			walletEUREmpty = true;
		}
	}

	/**
	 * Update view
	 */
	public void updateForm() {
		getData();
	}

	public boolean getWalletEUREmpty() {
		return walletEUREmpty;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Double getPriceSell() {
		return priceSell;
	}

	public void setPriceSell(Double priceSell) {
		this.priceSell = priceSell;
	}

	public Double getFeeSell() {
		return feeSell;
	}

	public void setFeeSell(Double feeSell) {
		this.feeSell = feeSell;
	}

	public Double getWalletEURBalance() {
		return walletEURBalance;
	}

	public void setWalletEURBalance(Double walletEURBalance) {
		this.walletEURBalance = walletEURBalance;
	}

	/*
	 * public Double getWalletBTCBalanceEUR() { return walletBTCBalanceEUR; }
	 * 
	 * public void setWalletBTCBalanceEUR(Double walletBalanceEur) {
	 * this.walletBTCBalanceEUR = walletBalanceEur; }
	 */

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOrderSubmittedSummary() {
		return orderSubmittedSummary;
	}

	public void setOrderSubmittedSummary(String orderSubmittedSummary) {
		this.orderSubmittedSummary = orderSubmittedSummary;
	}

	public String getOrderSubmittedDetail() {
		return orderSubmittedDetail;
	}

	public void setOrderSubmittedDetail(String orderSubmittedDetail) {
		this.orderSubmittedDetail = orderSubmittedDetail;
	}

	public Integer getPollingInterval() {
		return pollingInterval;
	}

	public void setPollingInterval(Integer pollingInterval) {
		this.pollingInterval = pollingInterval;
	}

	public String getPostePayName() {
		return postePayName;
	}

	public void setPostePayName(String postePayName) {
		this.postePayName = postePayName;
	}

	public String getPostePayNumber() {
		return postePayNumber;
	}

	public void setPostePayNumber(String postePayNumber) {
		this.postePayNumber = postePayNumber;
	}

	public String getPostePaySSN() {
		return postePaySSN;
	}

	public void setPostePaySSN(String postePaySSN) {
		this.postePaySSN = postePaySSN;
	}

	public String getLastAddress() {
		return lastAddress;
	}

	public void setLastAddress(String lastAddress) {
		this.lastAddress = lastAddress;
	}

	public String getLastEmail() {
		return lastEmail;
	}

	public void setLastEmail(String lastEmail) {
		this.lastEmail = lastEmail;
	}

	public String getLastCode() {
		return lastCode;
	}

	public void setLastCode(String lastCode) {
		this.lastCode = lastCode;
	}

	public String getBtcQuantity() {
		return btcQuantity;
	}

	public void setBtcQuantity(String btcQuantity) {
		this.btcQuantity = btcQuantity;
	}

	public String getEurQuantity() {
		return eurQuantity;
	}

	public void setEurQuantity(String eurQuantity) {
		this.eurQuantity = eurQuantity;
	}

	public String getPriceSellString() {
		return priceSellString;
	}

	public void setPriceSellString(String priceSellString) {
		this.priceSellString = priceSellString;
	}

	public String getOrderBTCQuantity() {
		return orderBTCQuantity;
	}

	public void setOrderBTCQuantity(String orderBTCQuantity) {
		this.orderBTCQuantity = orderBTCQuantity;
	}

	public String getOrderEURQuantity() {
		return orderEURQuantity;
	}

	public void setOrderEURQuantity(String orderEURQuantity) {
		this.orderEURQuantity = orderEURQuantity;
	}

	public boolean isOversize() {
		return oversize;
	}

	public void setOversize(boolean oversize) {
		this.oversize = oversize;
	}

	public String getLastPostePayNumber() {
		return lastPostePayNumber;
	}

	public void setLastPostePayNumber(String lastPostePayNumber) {
		this.lastPostePayNumber = lastPostePayNumber;
	}

	public String getLastPostePayName() {
		return lastPostePayName;
	}

	public void setLastPostePayName(String lastPostePayName) {
		this.lastPostePayName = lastPostePayName;
	}

	public String getLastPostePaySSN() {
		return lastPostePaySSN;
	}

	public void setLastPostePaySSN(String lastPostePaySSN) {
		this.lastPostePaySSN = lastPostePaySSN;
	}

	public String getTransactionCode() {

		code = new String("");
		for (;;) {
			// code = RandomStringUtils.randomNumeric(4);
			code = RandomStringUtils.randomAlphanumeric(3).toUpperCase();
			if (orderManager.getOrderByCode(code) == null) {
				return code;
			}
		}
	}
}
