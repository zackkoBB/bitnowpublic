package com.gconsulting.webapp.coge;

import java.lang.Thread.State;
import java.net.URI;
import java.text.DecimalFormat;
import java.util.Calendar;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.json.JSONObject;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.gconsulting.webapp.exception.ClientException;
import com.gconsulting.webapp.model.ExchangeRate;
import com.gconsulting.webapp.rest.interfaces.RESTInterface;
import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.Order;
import com.gsconsulting.bitnow.model.Transaction;
import com.gsconsulting.bitnow.model.Wallet;
import com.gsconsulting.bitnow.model.enums.AccountStatus;
import com.gsconsulting.bitnow.model.enums.AccountType;
import com.gsconsulting.bitnow.model.enums.TransactionStatus;
import com.gsconsulting.bitnow.model.enums.TransactionType;
import com.gsconsulting.bitnow.model.service.AccountManager;
import com.gsconsulting.bitnow.model.service.ConfigurationManager;
import com.gsconsulting.bitnow.model.service.OrderManager;
import com.gsconsulting.bitnow.model.service.ReadingsManager;
import com.gsconsulting.bitnow.model.service.TransactionManager;
import com.gsconsulting.bitnow.model.service.WalletManager;
import com.gsconsulting.bitnow.model.util.Constants;

public class COGE {

	protected final Log log = LogFactory.getLog(getClass());

	private String exchange;
	private String exchangeRateAPI;
	private Integer COGEPollingInterval;
	private Long transactionExpiryTime;
	private ExchangeRate exchangeRate;
	private Thread cogeThread;
	private Client RESTClient;
	private Wallet BTCWallet;
	private Wallet EURWallet;
	private ConfigurationManager configurationManager;
	private AccountManager accountManager;
	private OrderManager orderManager;
	private TransactionManager transactionManager;
	private WalletManager walletManager;
	private ReadingsManager readingsManager;
	private ClassPathXmlApplicationContext context;

	/**
	 * single instance of ArbitrageScanner
	 */
	private static final COGE INSTANCE = new COGE();

	private COGE() {

		try {
			initManagers();
			initRESTClient();
			initBTCWallet();
			initEURWallet();
			initCOGE();
		} catch (ClientException e) {
			log.error("Starting COGE: " + e.getMessage());
			e.printStackTrace();
		}
	}

	private void initManagers() throws ClientException {

		context = new ClassPathXmlApplicationContext(
				Constants.APPLICATION_CONTEXT);
		configurationManager = (ConfigurationManager) context
				.getBean("configurationManager");
		if (configurationManager != null) {
			exchange = configurationManager.getConfigurationByKey(
					Constants.EXCHANGE_RATE_EXCHANGE).getValue();
			exchangeRateAPI = configurationManager.getConfigurationByKey(
					Constants.EXCHANGE_RATE_API).getValue();
			COGEPollingInterval = new Integer(configurationManager
					.getConfigurationByKey(Constants.COGE_POLLING_INTERVAL)
					.getValue());
			transactionExpiryTime = new Long(configurationManager
					.getConfigurationByKey(Constants.TRANSACTION_EXPIRY_TIME)
					.getValue());
		} else {
			throw new ClientException("configurationManager is Null");
		}
		accountManager = (AccountManager) context.getBean("accountManager");
		if (accountManager != null) {
		} else {
			throw new ClientException("accountManager is Null");
		}
		orderManager = (OrderManager) context.getBean("orderManager");
		if (orderManager != null) {
		} else {
			throw new ClientException("orderManager is Null");
		}
		transactionManager = (TransactionManager) context
				.getBean("transactionHistoryManager");
		if (transactionManager != null) {
		} else {
			throw new ClientException("transactionManager is Null");
		}
		walletManager = (WalletManager) context.getBean("walletManager");
		if (walletManager != null) {
		} else {
			throw new ClientException("walletManager is Null");
		}
		readingsManager = (ReadingsManager) context.getBean("readingsManager");
		if (readingsManager != null) {
		} else {
			throw new ClientException("readingsManager is Null");
		}
	}

	private void initRESTClient() {

		ClientConfig config = new ClientConfig();
		config = config.property(ClientProperties.CONNECT_TIMEOUT,
				Constants.REST_CONNECTION_TIMEOUT);
		config = config.property(ClientProperties.READ_TIMEOUT,
				Constants.REST_READ_TIMEOUT);
		RESTClient = ClientBuilder.newClient(config);
		exchangeRate = new ExchangeRate(Calendar.getInstance()
				.getTimeInMillis(), 0d);
	}

	private void initBTCWallet() {

		if (walletManager != null) {
			BTCWallet = walletManager.getWalletByKey(AccountType.BTC.ordinal());
			exchangeRate.setRate(BTCWallet.getExchangeRate());
		}
	}

	private void initEURWallet() {

		if (walletManager != null) {
			EURWallet = walletManager.getWalletByKey(AccountType.EUR.ordinal());
		}
	}

	private void initCOGE() throws ClientException {

		COGERunnable cogeRunnable = new COGERunnable();
		cogeThread = new Thread(cogeRunnable);
		cogeThread
				.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
					@Override
					public void uncaughtException(Thread t, Throwable e) {
						log.error("ERROR! An exception occurred in "
								+ t.getName() + ". Cause: " + e.getMessage());
						e.printStackTrace();
					}
				});
		cogeThread.start();
	}

	public static COGE getInstance() {
		return INSTANCE;
	}

	/**
	 * Read a GET API
	 * 
	 * @param source
	 *            API for that Text API
	 * @return String result
	 */
	private String getAPI(String URI) {

		WebTarget target = RESTClient.target(getBaseURI(URI));
		Builder builder = target.request(MediaType.APPLICATION_JSON);
		Response response = builder.get();
		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}
		return response.readEntity(String.class);
	}

	private static URI getBaseURI(String url) {
		return UriBuilder.fromUri(url).build();
	}

	/**
	 * Get exchange rate marketPrefix from web
	 * 
	 * @return Map<String, List<ExchangeRate>> exchange rate marketPrefix
	 *         retrieved
	 */
	private ExchangeRate getExchangeRate(String exchange, String api) {

		try {
			// log.info(") " + api);
			JSONObject response = new JSONObject(getAPI(api));
			Class<?> restInterfaceClass = Class
					.forName(com.gconsulting.webapp.util.Constants.REST_BASE_PACKAGE
							+ "." + exchange + "Interface");
			RESTInterface restInterface = (RESTInterface) restInterfaceClass
					.newInstance();
			return restInterface.getTicker(response);
		} catch (Exception e) {
			log.error("Cannot get the ExchangeRate: " + api + " "
					+ e.getMessage());
			// e.printStackTrace();
			return null;
		}
	}

	/**
	 * Adjust wallet Balance BTC based on current Transactions Orders
	 */
	private Double adjustWalletBalanceBTC(TransactionType type, Double balance) {

		Double balanceInProcessing = orderManager.getOrderTypeVolumeBTC(type);
		if (balanceInProcessing != null) {
			if ((balance - balanceInProcessing) < 0) {
				balance = 0d;
			} else {
				balance = balance - balanceInProcessing;
			}
		}
		return balance;
	}

	/**
	 * Adjust wallet Balance EUR based on current Transactions Orders
	 */
	private Double adjustWalletBalanceEUR(TransactionType type, Double balance) {

		Double balanceInProcessing = orderManager.getOrderTypeVolumeEUR(type);
		if (balanceInProcessing != null) {
			if ((balance - balanceInProcessing) < 0) {
				balance = 0d;
			} else {
				balance = balance - balanceInProcessing;
			}
		}
		return balance;
	}

	public synchronized void read() {

		DecimalFormat df2 = new DecimalFormat(".##");
		StringBuilder reading = new StringBuilder();
		reading.append("C [T " + Calendar.getInstance().getTimeInMillis()
				/ 1000 + "] ");
		/*
		 * Exchange rates
		 */
		exchangeRate = getExchangeRate(exchange, exchangeRateAPI);
		ExchangeRate exchangeRateEURBTC = new ExchangeRate(0l, 0d);
		if (exchangeRate != null) {
			reading.append("[ER " + df2.format(exchangeRate.getRate()) + "] ");
			exchangeRateEURBTC = new ExchangeRate(exchangeRate.getTime(),
					1d / exchangeRate.getRate());
			if (exchangeRateEURBTC != null) {
				reading.append("[ER "
						+ df2.format(exchangeRateEURBTC.getRate()) + "] ");
			}
		}
		/*
		 * Refresh BTC, EUR Wallet Balance
		 */
		if (walletManager != null) {
			/*
			 * BTC Wallet Balance
			 */
			BTCWallet = walletManager.getWalletByKey(AccountType.BTC.ordinal());
			Double btcWalletBalance = BTCWallet.getBalance();
			Double btcWalletBalanceAvailalbe = adjustWalletBalanceBTC(
					TransactionType.BUY, btcWalletBalance);
			if (BTCWallet != null) {
				BTCWallet.setBalance(btcWalletBalance);
				if (exchangeRate != null) {
					BTCWallet.setExchangeRate(exchangeRate.getRate()
							.doubleValue());
				}
				BTCWallet.setBalanceAvailable(btcWalletBalanceAvailalbe);
				BTCWallet.setLastModified(Calendar.getInstance()
						.getTimeInMillis() / 1000);
				walletManager.update(BTCWallet);
				reading.append("[W(" + BTCWallet.getType() + ") "
						+ BTCWallet.getBalance() + " "
						+ BTCWallet.getBalanceAvailable() + "] ");
			}
			/*
			 * EUR's Accounts balances
			 */
			Double totalEURBalance = 0d;
			for (Account account : accountManager
					.getAccountByType(AccountType.EUR.ordinal(),
							AccountStatus.ACCOUNT_NOT_ACTIVE)) {
				Double balance = account.getBalance();
				totalEURBalance += balance;
				Double balanceInProcessing = orderManager
						.getOrderTypeVolumeEURByUser(TransactionType.SELL,
								account.getUser());
				if (balanceInProcessing == null) {
					balanceInProcessing = 0d;
				}
				account.setBalanceAvailable(balance.doubleValue()
						- balanceInProcessing);
				account.setLastModified(Calendar.getInstance()
						.getTimeInMillis() / 1000);
				if (account.getLastReadStatus() == AccountStatus.ACCOUNT_SELLER
						.ordinal()) {
				} else {
					account.setLastReadStatus(AccountStatus.ACCOUNT_LAST_READ_OK
							.ordinal());
				}
				accountManager.update(account);
				reading.append("[A(" + account.getUser().getId() + ","
						+ account.getId() + "," + account.getType() + ") "
						+ account.getBalance() + " "
						+ account.getBalanceAvailable() + "] ");
			}
			/*
			 * EUR Wallet Balance
			 */
			Double eurWalletBalance = totalEURBalance;
			Double eurWalletBalanceAvailable = adjustWalletBalanceEUR(
					TransactionType.SELL, eurWalletBalance);
			if (EURWallet != null) {
				EURWallet.setBalance(eurWalletBalance);
				if (exchangeRateEURBTC != null) {
					EURWallet.setExchangeRate(exchangeRateEURBTC.getRate()
							.doubleValue());
				}
				EURWallet.setBalanceAvailable(eurWalletBalanceAvailable);
				EURWallet.setLastModified(Calendar.getInstance()
						.getTimeInMillis() / 1000);
				walletManager.update(EURWallet);
				reading.append("[W(" + EURWallet.getType() + ") "
						+ EURWallet.getBalance() + " "
						+ EURWallet.getBalanceAvailable() + "] ");
			}
		}
		/*
		 * BTC's Accounts balances
		 */
		for (Account account : accountManager.getAccountByType(
				AccountType.BTC.ordinal(), AccountStatus.ACCOUNT_NOT_ACTIVE)) {
			Double balance = account.getBalance();
			account.setBalance(balance);
			Double balanceInProcessing = orderManager
					.getOrderTypeVolumeBTCByUser(TransactionType.BUY,
							account.getUser());
			if (balanceInProcessing == null) {
				balanceInProcessing = 0d;
			}
			account.setBalanceAvailable(balance.doubleValue()
					- balanceInProcessing);
			account.setLastModified(Calendar.getInstance().getTimeInMillis() / 1000);
			account.setLastReadStatus(AccountStatus.ACCOUNT_LAST_READ_OK
					.ordinal());
			accountManager.update(account);
			reading.append("[A(" + account.getUser().getId() + ","
					+ account.getId() + "," + account.getType() + ") "
					+ account.getBalance() + " "
					+ account.getBalanceAvailable() + "] ");
		}
		/*
		 * EUR's Accounts balances
		 * 
		 * TODO: check!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! (needed?!?)
		 * 
		 * for (Account account :
		 * accountManager.getAccountByType(AccountType.EUR .ordinal())) { Double
		 * balance = account.getBalance(); Double balanceInProcessing =
		 * orderManager.getOrderTypeVolumeEURByUser( TransactionType.SELL,
		 * account.getUser()); if (balanceInProcessing == null) {
		 * balanceInProcessing = 0d; }
		 * account.setBalanceAvailable(balance.doubleValue() -
		 * balanceInProcessing);
		 * account.setLastModified(Calendar.getInstance().getTimeInMillis() /
		 * 1000); account.setLastReadStatus(AccountStatus.ACCOUNT_LAST_READ_OK
		 * .ordinal()); accountManager.update(account); reading.append("[A(" +
		 * account.getUser().getId() + "," + account.getId() + "," +
		 * account.getType() + ") " + account.getBalance() + " " +
		 * account.getBalanceAvailable() + "] "); }
		 */
		/*
		 * Save readings
		 */
		readingsManager.create(reading.toString());
	}

	public boolean cogeThreadIsAlive() {
		return cogeThread.isAlive();
	}

	public boolean cogeThreadIsInterrupted() {
		return cogeThread.isInterrupted();
	}

	public void cogeThreadCheckAccess() {
		cogeThread.checkAccess();
	}

	public State cogeThreadState() {
		return cogeThread.getState();
	}

	public void cogeThreadRestart() throws ClientException {

		COGERunnable cogeRunnable = new COGERunnable();
		cogeThread = new Thread(cogeRunnable);
		cogeThread
				.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
					@Override
					public void uncaughtException(Thread t, Throwable e) {
						log.error("ERROR! An exception occurred in "
								+ t.getName() + ". Cause: " + e.getMessage());
						e.printStackTrace();
					}
				});
		cogeThread.start();
	}

	public ExchangeRate getExchangeRate() {
		return exchangeRate;
	}

	public Wallet getBTCWallet() {
		return BTCWallet;
	}

	public synchronized void saveBTCWallet(Wallet newWallet) {

		if (walletManager != null) {
			walletManager.update(newWallet);
		}
	}

	public Wallet getEURWallet() {
		return EURWallet;
	}

	public synchronized void saveEURWallet(Wallet newWallet) {

		if (walletManager != null) {
			walletManager.update(newWallet);
		}
	}

	/**
	 * COGERunnable for retrieving info and keep accountability of the
	 * application GET: BTC/EUR exchange rate GET: BTC Wallet balance GET: EUR
	 * Wallet balance DOES: accounting
	 * 
	 * @author zackko
	 *
	 */
	private class COGERunnable implements Runnable {

		private boolean stop = false;

		public COGERunnable() {
		}

		/**
		 * Process expired orders
		 * 
		 * TODO: check orders in
		 * 
		 * 1. PAYMENT_RECEIVED && TransactionType.BUY: error can't be send email
		 * to Administrator
		 * 
		 * 2. PAYMENT_RECEIVED && TransactionType.SELL: waiting for seller to
		 * set MONEY_SENT
		 * 
		 */
		private void checkExpiredOrders() {

			Long timestamp = Calendar.getInstance().getTimeInMillis();
			timestamp -= transactionExpiryTime;
			timestamp /= 1000;
			for (Order order : orderManager.getOrderByStatusAndTimestampLesser(
					TransactionStatus.UNCONFIRMED, timestamp)) {
				Transaction transaction = new Transaction(
						order.getCode()
								+ " "
								+ RandomStringUtils.randomAlphanumeric(9)
										.toUpperCase(), order.getType(),
						order.getAddress(), order.getEmail(), "",
						order.getBtc(), order.getEuro(), order.getFee(),
						order.getCreated(), Calendar.getInstance()
								.getTimeInMillis() / 1000, order.getNote(),
						TransactionStatus.EXPIRED.ordinal(), order.getAccount());
				transactionManager.create(transaction);
				orderManager.delete(order);
			}
			for (Order order : orderManager.getOrderByStatusAndTimestampLesser(
					TransactionStatus.WAITING_PAYMENT, timestamp)) {
				Transaction transaction = new Transaction(
						order.getCode()
								+ " "
								+ RandomStringUtils.randomAlphanumeric(9)
										.toUpperCase(), order.getType(),
						order.getAddress(), order.getEmail(), "",
						order.getBtc(), order.getEuro(), order.getFee(),
						order.getCreated(), Calendar.getInstance()
								.getTimeInMillis() / 1000, order.getNote(),
						TransactionStatus.EXPIRED.ordinal(), order.getAccount());
				transactionManager.create(transaction);
				orderManager.delete(order);
			}
		}

		/**
		 * 
		 */
		public void checkExchangeRateAndBalances() {
			read();
		}

		/**
		 * Run method of this thread
		 */
		public void run() {

			while (!stop) {
				try {
					checkExpiredOrders();
					checkExchangeRateAndBalances();
					Thread.sleep(COGEPollingInterval);
					// System.gc();
				} catch (Throwable e) {
					log.error("Throwable: " + e.getMessage());
					e.printStackTrace();
					/*
					 * TODO: send email
					 */
				}
			}
			RESTClient.close();
		}
	}
}
