package com.gconsulting.webapp.action;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

import com.gconsulting.webapp.model.TransactionView;
import com.gsconsulting.bitnow.model.Order;
import com.gsconsulting.bitnow.model.Transaction;
import com.gsconsulting.bitnow.model.service.AccountManager;
import com.gsconsulting.bitnow.model.service.ConfigurationManager;
import com.gsconsulting.bitnow.model.service.MailEngine;
import com.gsconsulting.bitnow.model.service.OrderManager;
import com.gsconsulting.bitnow.model.service.TransactionManager;
import com.gsconsulting.bitnow.model.util.Constants;

/**
 * Managed Bean to send password hints to registered users.
 *
 * <p>
 * <a href="PasswordHint.java.html"><i>View Source</i></a>
 * </p>
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Scope("session")
@Component("contactAction")
public class ContactAction extends BasePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3095189826090254644L;
	private String address;
	private String contactEmail;
	private String contactSubject;
	private String contactText;
	private String transactionLinkBase;
	private OrderManager orderManager;
	private TransactionManager transactionHistoryManager;
	private ConfigurationManager configurationManager;
	private AccountManager accountManager;
	private List<TransactionView> orderHistory;
	private Integer contactFormActiveIndex;
	private Integer orderStatusFormActiveIndex;

	/**
	 * Default constructor
	 */
	public ContactAction() {
		super();
	}

	/**
	 * Init method called after construct
	 */
	@PostConstruct
	public void init() {

		if (configurationManager != null) {
			log.info("Environment: "
					+ configurationManager.getConfigurationByKey(
							Constants.ENVIRONMENT).getValue());
			transactionLinkBase = new String(configurationManager
					.getConfigurationByKey(Constants.TRANSACTION_LINK)
					.getValue());
		}
		if (accountManager != null) {
			// accounts = accountManager.getAll();
		}
	}

	@Autowired
	public void setOrderManager(
			@Qualifier("orderManager") OrderManager orderManager) {
		this.orderManager = orderManager;
	}

	@Autowired
	public void setTransactionHistoryManager(
			@Qualifier("transactionHistoryManager") TransactionManager transactionHistoryManager) {
		this.transactionHistoryManager = transactionHistoryManager;
	}

	@Autowired
	public void setConfigurationManager(
			@Qualifier("configurationManager") ConfigurationManager configurationManager) {
		this.configurationManager = configurationManager;
	}

	@Autowired
	public void setAccountManager(
			@Qualifier("accountManager") AccountManager accountManager) {
		this.accountManager = accountManager;
	}

	@Autowired
	public void setMailEngine(@Qualifier("mailEngine") MailEngine mailEngine) {
		this.mailEngine = mailEngine;
	}

	/**
	 * Send contact email
	 * 
	 * @return
	 */
	public String sendContactEmail() {

		try {
			message = new SimpleMailMessage();
			message.setTo("BitNow <contatti@bitnow.it>");
			String subject = '[' + getText("webapp.name") + "] "
					+ contactSubject;
			message.setSubject(subject);
			Map<String, Object> args = new HashMap<>();
			args.put("webappURL", getText("website.url"));
			args.put("webappName", getText("webapp.name"));
			args.put("webappTagline", getText("webapp.tagline"));
			args.put("contactEmail", contactEmail);
			args.put("contactText", contactText);
			mailEngine
					.sendHTML(
							message,
							com.gconsulting.webapp.util.Constants.CONTACT_EMAIL_TEMPLATE,
							args);
			addFacesMessage("website.contact.contactUs.success");
			contactEmail = new String();
			contactSubject = new String();
			contactText = new String();
		} catch (Exception e) {
			addFacesError("website.contact.contactUs.error", e.getMessage());
		}
		return "contact";
	}

	/**
	 * Search Order
	 * 
	 * @return
	 */
	public String searchOrder() {

		boolean transactionFound = false;
		orderHistory = new ArrayList<TransactionView>();
		for (Order order : orderManager.getOrderByAddress(address)) {
			orderHistory.add(new TransactionView(order.getCode(), order
					.getType(), order.getAddress(), order.getEmail(), order
					.getTransactionHash(), order.getBtc(), order.getEuro(),
					order.getEuro() / order.getBtc(), order.getFee(), new Date(
							order.getCreated() * 1000), new Date(order
							.getModified() * 1000), order.getNote(), order
							.getStatus(), order.getAccount()));
			transactionFound = true;
		}
		for (Transaction transaction : transactionHistoryManager
				.getTransactionByAddress(address)) {
			orderHistory.add(new TransactionView(transaction.getCode(),
					transaction.getType(), transaction.getAddress(),
					transaction.getEmail(), transaction.getTransactionHash(),
					transaction.getBtc(), transaction.getEuro(), transaction
							.getEuro() / transaction.getBtc(), transaction
							.getFee(),
					new Date(transaction.getCreated() * 1000), new Date(
							transaction.getModified() * 1000), transaction
							.getNote(), transaction.getStatus(), transaction
							.getAccount()));
			transactionFound = true;
		}
		if (transactionFound) {
			contactFormActiveIndex = 1;
			orderStatusFormActiveIndex = 0;
		} else {
			contactFormActiveIndex = 1;
			orderStatusFormActiveIndex = -1;
			addFacesError("errors.address.notFound");
		}
		return "contact";
	}

	/**
	 * Display contact form when coming from Order Search
	 * 
	 * 
	 */
	public void contactFormDisplay() {

		if (contactFormActiveIndex == 1) {
			contactFormActiveIndex = 0;
			orderStatusFormActiveIndex = -1;
			orderHistory = new ArrayList<com.gconsulting.webapp.model.TransactionView>();
			address = new String();
		}
	}

	/**
	 * On Tab Change
	 * 
	 * 
	 */
	public void contactFormOnTabChange() {

		if (contactFormActiveIndex == 0) {
			contactFormActiveIndex = 1;
			orderStatusFormActiveIndex = -1;
		} else if (contactFormActiveIndex == 1) {
			contactFormActiveIndex = 0;
			orderStatusFormActiveIndex = -1;
			orderHistory = new ArrayList<com.gconsulting.webapp.model.TransactionView>();
			address = new String();
		}
	}

	/**
	 * Buy action
	 * 
	 * @return String label of the next view
	 */
	public String cancel() {
		reset();
		return "cancel";
	}

	/**
	 * Reset everything after a successfull buy
	 */
	private void reset() {

		address = new String();
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactSubject() {
		return contactSubject;
	}

	public void setContactSubject(String contactSubject) {
		this.contactSubject = contactSubject;
	}

	public String getContactText() {
		return contactText;
	}

	public void setContactText(String contactText) {
		this.contactText = contactText;
	}

	public List<com.gconsulting.webapp.model.TransactionView> getOrderHistory() {
		return orderHistory;
	}

	public void setOrderHistory(
			List<com.gconsulting.webapp.model.TransactionView> orderHistory) {
		this.orderHistory = orderHistory;
	}

	public Integer getContactFormActiveIndex() {
		return contactFormActiveIndex;
	}

	public void setContactFormActiveIndex(Integer contactFormActiveIndex) {
		this.contactFormActiveIndex = contactFormActiveIndex;
	}

	public Integer getOrderStatusFormActiveIndex() {
		return orderStatusFormActiveIndex;
	}

	public void setOrderStatusFormActiveIndex(Integer orderStatusFormActiveIndex) {
		this.orderStatusFormActiveIndex = orderStatusFormActiveIndex;
	}

	public String getTransactionLinkBase() {
		return transactionLinkBase;
	}

	public void setTransactionLinkBase(String transactionLinkBase) {
		this.transactionLinkBase = transactionLinkBase;
	}
}
