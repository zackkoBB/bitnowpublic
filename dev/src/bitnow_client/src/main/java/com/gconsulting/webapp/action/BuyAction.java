package com.gconsulting.webapp.action;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.mail.MessagingException;
import javax.ws.rs.core.Response;

import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang.RandomStringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

import com.gconsulting.webapp.coge.COGE;
import com.gconsulting.webapp.model.ExchangeRate;
import com.gconsulting.webapp.util.RESTUtils;
import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.Order;
import com.gsconsulting.bitnow.model.Transaction;
import com.gsconsulting.bitnow.model.Wallet;
import com.gsconsulting.bitnow.model.enums.AccountStatus;
import com.gsconsulting.bitnow.model.enums.AccountType;
import com.gsconsulting.bitnow.model.enums.TransactionStatus;
import com.gsconsulting.bitnow.model.enums.TransactionType;
import com.gsconsulting.bitnow.model.service.AccountManager;
import com.gsconsulting.bitnow.model.service.ConfigurationManager;
import com.gsconsulting.bitnow.model.service.MailEngine;
import com.gsconsulting.bitnow.model.service.OrderManager;
import com.gsconsulting.bitnow.model.service.TransactionManager;
import com.gsconsulting.bitnow.model.util.Constants;
import com.gsconsulting.bitnow.model.util.CryptService;

/**
 * Managed Bean to send password hints to registered users.
 *
 * <p>
 * <a href="PasswordHint.java.html"><i>View Source</i></a>
 * </p>
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Scope("session")
@Component("buyAction")
@SuppressWarnings("unused")
public class BuyAction extends BasePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1317756065202142811L;
	private boolean walletBTCEmpty;
	private Double walletBTCBalance;
	// private Double walletBTCBalanceEUR;
	private Double buyMAXPayment;
	private Double btcQuantityDouble;
	private Double eurQuantityDouble;
	private String code;
	private String btcQuantity;
	private String eurQuantity;
	private String address;
	private OrderManager orderManager;
	private TransactionManager transactionHistoryManager;
	private ConfigurationManager configurationManager;
	private AccountManager accountManager;
	private Double priceBuy;
	private Double feeBuy;
	private Double feeSend;
	// private Double priceSell;
	// private Double feeSell;
	private Double feeSeller;
	private String priceBuyString;
	// private String priceSellString;
	private String email;
	private String orderSubmittedSummary;
	private String orderSubmittedDetail;
	private String lastAddress;
	private String lastEmail;
	private String lastCode;
	private String orderBTCQuantity;
	private String orderEURQuantity;
	private List<Account> accounts;
	private Account selectedAccount;
	private DecimalFormat df2;

	/**
	 * "Constants"
	 */
	private Integer pollingInterval;
	private String postePayName;
	private String postePayNumber;
	private String postePaySSN;

	/**
	 * Default constructor
	 */
	public BuyAction() {
		super();
	}

	/**
	 * Init method called after construct
	 */
	@PostConstruct
	public void init() {

		if (configurationManager != null) {
			log.info("Environment: "
					+ configurationManager.getConfigurationByKey(
							Constants.ENVIRONMENT).getValue());
			pollingInterval = new Integer(configurationManager
					.getConfigurationByKey(Constants.UI_POLLING_INTERVAL)
					.getValue());
			feeBuy = new Double(configurationManager.getConfigurationByKey(
					Constants.FEE_BUY).getValue());
			feeSend = new Double(configurationManager.getConfigurationByKey(
					Constants.WALLET_SEND_FEE).getValue());
			// feeSell = new Double(configurationManager.getConfigurationByKey(
			// Constants.FEE_SELL).getValue());
			feeSeller = new Double(configurationManager.getConfigurationByKey(
					Constants.FEE_SELLER).getValue());
			buyMAXPayment = new Double(configurationManager
					.getConfigurationByKey(Constants.MAX_BUY_PAYMENT)
					.getValue().replace(",", "."));
		}
		if (accountManager != null) {
			// accounts = accountManager.getAll();
		}
		df2 = new DecimalFormat(".##");
		btcQuantity = new String();
		eurQuantity = new String("0.00");
		btcQuantityDouble = new Double(0d);
		eurQuantityDouble = new Double(0d);
		walletBTCEmpty = true;
		walletBTCBalance = new Double(0D);
		// walletBTCBalanceEUR = new Double(0D);
		priceBuy = new Double(0D);
		priceBuyString = new String();
		// priceSellString = new String();
		// priceSell = new Double(0D);
		getData();
	}

	@Autowired
	public void setOrderManager(
			@Qualifier("orderManager") OrderManager orderManager) {
		this.orderManager = orderManager;
	}

	@Autowired
	public void setTransactionHistoryManager(
			@Qualifier("transactionHistoryManager") TransactionManager transactionHistoryManager) {
		this.transactionHistoryManager = transactionHistoryManager;
	}

	@Autowired
	public void setConfigurationManager(
			@Qualifier("configurationManager") ConfigurationManager configurationManager) {
		this.configurationManager = configurationManager;
	}

	@Autowired
	public void setAccountManager(
			@Qualifier("accountManager") AccountManager accountManager) {
		this.accountManager = accountManager;
	}

	@Autowired
	public void setMailEngine(@Qualifier("mailEngine") MailEngine mailEngine) {
		this.mailEngine = mailEngine;
	}

	/**
	 * Send an email Memo
	 * 
	 * @param PINs
	 *            of the transaction
	 * @param size
	 *            total size of the transaction
	 * @throws MessagingException
	 */
	private void sendMailBuy(String subject, String destination)
			throws MessagingException {

		// System.getProperty("line.separator")
		message = new SimpleMailMessage();
		// message.setFrom("contatti@bitnow.it");
		message.setTo(destination);
		message.setSubject(subject);
		Map<String, Object> args = new HashMap<>();
		args.put("webappURL", getText("website.url"));
		args.put("webappName", getText("webapp.name"));
		args.put("webappTagline", getText("webapp.tagline"));
		args.put("orderSummary", getText("success.orderSubmitted.orderSummary"));
		args.put("orderSummary_confirmationCode",
				getText("success.orderSubmitted.orderSummary.confirmationCode"));
		args.put("lastCode", lastCode);
		args.put("orderSummary_bitcoin",
				getText("success.orderSubmitted.orderSummary.bitcoin"));
		args.put("orderBTCQuantity", orderBTCQuantity);
		args.put("orderSummary_address",
				getText("success.orderSubmitted.orderSummary.address"));
		args.put("lastAddress", lastAddress);
		args.put("orderSubmitted_postePayTopUpDetails",
				getText("success.orderSubmitted.postePayTopUpDetails"));
		args.put("orderSubmitted_postePayTopUpDetails_euro",
				getText("success.orderSubmitted.postePayTopUpDetails.euro"));
		args.put("orderEURQuantity", orderEURQuantity);
		args.put("orderSubmitted_postePayTopUpDetails_number",
				getText("success.orderSubmitted.postePayTopUpDetails.number"));
		args.put("postePayNumber", postePayNumber);
		args.put("orderSubmitted_postePayTopUpDetails_name",
				getText("success.orderSubmitted.postePayTopUpDetails.name"));
		args.put("postePayName", postePayName);
		args.put("orderSubmitted_postePayTopUpDetails_ssn",
				getText("success.orderSubmitted.postePayTopUpDetails.ssn"));
		args.put("postePaySSN", postePaySSN);
		args.put("orderSubmitted_alertTime",
				getText("success.orderSubmitted.alertTime"));
		args.put("orderSubmitted_alertAmount",
				getText("success.orderSubmitted.alertAmount"));
		args.put("orderSubmitted_emailClose",
				getText("success.orderSubmitted.emailClose"));
		mailEngine.sendHTML(message,
				com.gconsulting.webapp.util.Constants.BUY_ORDER_EMAIL_TEMPLATE,
				args);
	}

	/**
	 * Handle btcQuantity change
	 * 
	 */
	public void btcQuantityChange() {

		try {
			btcQuantity = btcQuantity.replace("_", "0");
			eurQuantity = (df2.format(new Double(new Double(btcQuantity)
					* priceBuy))).toString();
		} catch (NumberFormatException e) {
		}
	}

	/**
	 * Add an E-Mail for memo
	 * 
	 * @return String label of the next view
	 * @throws MessagingException
	 */
	public String addEmailBuy() throws MessagingException {

		Order transaction = orderManager.getOrderByCode(code);
		transaction.setEmail(email);
		orderManager.update(transaction);
		lastEmail = email;
		String subject = '[' + getText("webapp.name") + "] "
				+ getText("website.home.newOrder") + " "
				+ getText("home.dashboard.transactions.type.buy") + " " + "B"
				+ orderBTCQuantity;
		sendMailBuy(subject, email);
		return "memo";
	}

	/**
	 * Buy action
	 * 
	 * @return String label of the next view
	 */
	public String cancel() {
		reset();
		return "cancel";
	}

	/**
	 * Reset everything after a successfull buy
	 */
	private void reset() {

		address = new String();
		email = new String();
		selectedAccount = null;
		btcQuantity = new String();
		eurQuantity = new String("0.00");
		btcQuantityDouble = new Double(0d);
		eurQuantityDouble = new Double(0d);
		getData();
	}

	/**
	 * Check for a valid PostePay transaction
	 * 
	 * @return boolean true if is a valid PostePay transaction
	 */
	private boolean validBuyTransaction() {

		getData();
		btcQuantityDouble = new Double(btcQuantity);
		eurQuantityDouble = new Double(eurQuantity);
		/*
		 * Check input data
		 */
		if (code != null) {
		} else {
			addFacesError("errors.general");
			return false;
		}
		if (eurQuantityDouble != null) {
			/*
			 * TODO: CHECK EXCHANGE RATE!!!!!!!!!!!!!!!!!!!!!!! move UNIQUE
			 * here, make search by account
			 */
			// log.info("btcQuantity: " + btcQuantity);
			// log.info("eurQuantity: " + eurQuantity);
			// String sizeTemp = sizePostePayString.substring(sizePostePayString
			// .indexOf("฿") + 1);
			// totalSize = new Double(sizeTemp);
			// euro = new Double(sizePostePayString.substring(
			// sizePostePayString.indexOf("€") + 1,
			// sizePostePayString.indexOf("/") - 1));
			// Double error = Math.abs(totalSize - (euro / pricePostePay));
			Double error = Math.abs(btcQuantityDouble
					- (eurQuantityDouble / priceBuy));
			if (error <= Constants.EXCHANGE_RATE_PRECISION) {
				/*
				 * Select Accounts based on totalSize
				 */
				selectedAccount = null;
				accounts = accountManager.getAccountByBTCBalanceAvailable(
						btcQuantityDouble, AccountStatus.ACCOUNT_NOT_ACTIVE);
				if (accounts != null) {
					if (accounts.size() > 0) {
						/*
						 * Select Accounts based on last recent transaction
						 */
						selectedAccount = accounts.get(0);
						Long minLatestPayment = selectedAccount
								.getLatestPayment();
						for (Account account : accounts) {
							if (account.getLatestPayment() < minLatestPayment) {
								selectedAccount = account;
								minLatestPayment = selectedAccount
										.getLatestPayment();
							}
						}
						/*
						 * We have a BTC account -> retrieve PPAccount
						 * 
						 * TODO: check balanceEurDaily
						 */
						for (Account account : accountManager.getAccountByUser(
								selectedAccount.getUser(),
								AccountStatus.ACCOUNT_NOT_ACTIVE)) {
							if (account.getType() == AccountType.EUR_POSTEPAY
									.ordinal()) {
								selectedAccount = account;
							}
						}
						try {
							postePaySSN = CryptService.decrypt(selectedAccount
									.getId());
							postePayName = CryptService.decrypt(selectedAccount
									.getOwner());
							String postePayNumberTemp = CryptService
									.decrypt(selectedAccount.getNumber());
							postePayNumberTemp = postePayNumberTemp.substring(
									0, 4)
									+ " "
									+ postePayNumberTemp.substring(4, 8)
									+ " "
									+ postePayNumberTemp.substring(8, 12)
									+ " "
									+ postePayNumberTemp.substring(12, 16);
							postePayNumber = postePayNumberTemp;
						} catch (InvalidKeyException | NoSuchAlgorithmException
								| NoSuchPaddingException
								| IllegalBlockSizeException
								| BadPaddingException e) {
							addFacesError("errors.seller.invalidCredentials");
							return false;
						}
					}
				}
				if (selectedAccount == null) {
					/*
					 * NO SELLER
					 */
					addFacesError("errors.seller.notFound");
					return false;
				}
			} else {
				/*
				 * HACKING Send email to Administrator they changed the
				 * totalSize value on client
				 */
				addFacesError("errors.general");
				return false;
			}
		} else {
			/*
			 * HACKING Send email to Administrator they changed the totalSize
			 * value on client
			 */
			addFacesError("errors.general");
			return false;
		}
		if (address != null) {
		} else {
			/*
			 * Shouldn't be: client side validation
			 */
			addFacesError("errors.general");
			return false;
		}
		/*
		 * Check BTC address Valid
		 */
		String walletBalanceAPI = configurationManager.getConfigurationByKey(
				Constants.WALLET_VALIDITY_API).getValue();
		Response response = RESTUtils.getInstance().getAPI(
				walletBalanceAPI + address);
		boolean addressValid = false;
		if (response.getStatus() != 200) {
			addFacesError("errors.validAddress", address);
			return false;
		} else {
			JSONObject responseJSON = new JSONObject(
					response.readEntity(String.class));
			if (responseJSON != null) {
				JSONObject responseData = responseJSON.getJSONObject("data");
				if (responseData != null) {
					Boolean isValid = responseData.getBoolean("is_valid");
					if (isValid.equals(Boolean.TRUE)) {
						/*
						 * Is a valid bitcoin address
						 */
						addressValid = true;
					}
				}
			}
		}
		if (!addressValid) {
			addFacesError("errors.validAddress", address);
			return false;
		}
		/*
		 * Check BTC wallet balance DOUBLE CHECK!!!!!!!!!!!!!!!!!!!!!!!!
		 */
		if (COGE.getInstance().getBTCWallet() != null) {
			walletBTCBalance = COGE.getInstance().getBTCWallet()
					.getBalanceAvailable();
			if (walletBTCBalance < btcQuantityDouble) {
				addFacesError("errors.notEnough", "BTCs");
				return false;
			}
			// walletBalanceEur = walletBalance * pricePostePay;
			// defaultSizePostePay = 5.0;
			// totalSizePostePay = defaultSizePostePay / pricePostePay;
		} else {
			addFacesError("errors.general");
			return false;
		}
		/*
		 * Check Min Order value
		 */
		if (btcQuantityDouble < Constants.MIN_BUY_ORDER) {
			addFacesError("errors.minOrder", Constants.MIN_BUY_ORDER);
			return false;
		}
		/*
		 * Check Max Order value
		 */
		if (eurQuantityDouble > buyMAXPayment) {
			addFacesError("errors.maxOrder", buyMAXPayment.toString());
			return false;
		}
		/*
		 * Check UNIQUE
		 * 
		 * NOT SAME TRANSACTION:
		 * 
		 * 1. IN ORDERS
		 * 
		 * 2. TRANSACTIONS: LAST_TRANSACTION_SAME_AMOUNT_IN_MILLIS * 2
		 */
		for (Order existingTransaction : orderManager.getOrderTypeByAccount(
				TransactionType.BUY, selectedAccount)) {
			Double error = Math.abs(existingTransaction.getEuro()
					- eurQuantityDouble);
			if (error <= Constants.VOLUME_PRECISION) {
				addFacesError("errors.existing.transaction");
				return false;
			}
		}
		/*
		 * TODO: CHECK FOR ACCOUNT
		 */
		Long referenceDate = Calendar.getInstance().getTimeInMillis();
		referenceDate -= (3 * Constants.LAST_TRANSACTION_SAME_AMOUNT_IN_MILLIS);
		referenceDate /= 1000;
		for (Transaction pastTransaction : transactionHistoryManager
				.getTransactionTypeByAccountStatusAndTimestampGreater(
						TransactionType.BUY, selectedAccount,
						TransactionStatus.DONE, referenceDate)) {
			Double error = Math.abs(pastTransaction.getEuro()
					- eurQuantityDouble);
			if (error <= Constants.VOLUME_PRECISION) {
				addFacesError("errors.existing.transaction");
				return false;
			}
		}
		return true;
	}

	private void oldCode() {

		/*
		 * Refresh Liquidity Wallet
		 */
		if (COGE.getInstance().getBTCWallet() != null) {
			Wallet BTCWallet = COGE.getInstance().getBTCWallet();
			Double newWalletBalance = BTCWallet.getBalanceAvailable();
			newWalletBalance -= btcQuantityDouble;
			if (newWalletBalance < 0) {
				newWalletBalance = 0D;
			}
			BTCWallet.setBalanceAvailable(newWalletBalance);
			COGE.getInstance().saveBTCWallet(BTCWallet);
			walletBTCBalance = newWalletBalance;
			/*
			 * adjust with account fee
			 */
			walletBTCBalance -= feeSend;
			walletBTCBalance -= walletBTCBalance / 100d * (feeBuy - feeSeller);
			if (walletBTCBalance < Constants.WALLET_BALANCE_MINIMUM_AMOUNT) {
				walletBTCEmpty = true;
			} else {
				walletBTCEmpty = false;
			}
			// walletBTCBalanceEUR = walletBTCBalance * priceBuy;
			// if (walletBTCBalanceEUR > buyMAXPayment) {
			// walletBTCBalanceEUR = buyMAXPayment;
			// }
		} else {
			walletBTCEmpty = true;
		}
		/*
		 * Refresh Liquidity Account
		 */
		Account selectedBTCAccount = accountManager.getAccountByUserAndType(
				selectedAccount.getUser(), AccountType.BTC.ordinal(),
				AccountStatus.ACCOUNT_NOT_ACTIVE);
		Double balance = selectedBTCAccount.getBalance();
		Double balanceInProcessing = orderManager.getOrderTypeVolumeBTCByUser(
				TransactionType.BUY, selectedBTCAccount.getUser());
		if (balanceInProcessing == null) {
			balanceInProcessing = 0d;
		}
		selectedBTCAccount.setBalanceAvailable(balance - balanceInProcessing);
		selectedBTCAccount.setLastModified(Calendar.getInstance()
				.getTimeInMillis() / 1000);
		selectedBTCAccount.setLastReadStatus(AccountStatus.ACCOUNT_LAST_READ_OK
				.ordinal());
		accountManager.update(selectedBTCAccount);
	}

	/**
	 * Create a PostePay transaction
	 * 
	 * @return boolean true if is a valid PostePay transaction
	 */
	private boolean createBuyTransaction() {

		/*
		 * Create transactions Fee Formula: (T - pT = q AND fee = T - q) => fee
		 * = q(p/(1-p))
		 */
		// Double feePlatform = feeBuy - feeSeller;
		BigDecimal fee = (new BigDecimal(btcQuantityDouble / 100d
				* (feeBuy - feeSeller))).setScale(8, RoundingMode.HALF_UP);
		orderManager.create(new Order(code, TransactionType.BUY.ordinal(),
				address, email, "", btcQuantityDouble, eurQuantityDouble, fee
						.doubleValue(), Calendar.getInstance()
						.getTimeInMillis() / 1000, Calendar.getInstance()
						.getTimeInMillis() / 1000, "",
				TransactionStatus.WAITING_PAYMENT.ordinal(), selectedAccount));
		/*
		 * Update LatestPayment for seller choosing
		 */
		Long currentDate = Calendar.getInstance().getTimeInMillis() / 1000;
		selectedAccount.setLatestPayment(currentDate);
		accountManager.update(selectedAccount);
		for (Account account : accountManager.getAccountByUser(
				selectedAccount.getUser(), AccountStatus.ACCOUNT_NOT_ACTIVE)) {
			if (account.getType() == AccountType.BTC.ordinal()) {
				account.setLatestPayment(currentDate);
				accountManager.update(account);
				break;
			}
		}
		// oldCode();
		getData();
		/*
		 * Display SUCCESS page
		 * 
		 * Send E-Mail to Administrator
		 */
		orderBTCQuantity = btcQuantity;
		orderEURQuantity = eurQuantity;
		orderSubmittedSummary = getText(
				"success.orderSubmittedPostePaySummary", new DecimalFormat(
						"#.########").format(btcQuantityDouble));
		lastAddress = address;
		lastCode = code;
		String subject = '[' + getText("webapp.name") + "] "
				+ getText("website.home.newOrder") + " "
				+ getText("home.dashboard.transactions.type.buy") + " " + "B"
				+ orderBTCQuantity;
		try {
			sendMailBuy(subject, Constants.ADMINISTRATOR_EMAIL);
		} catch (MessagingException e) {
			addFacesError("errors.detail", e.getMessage());
			log.error("MailException: " + e.getMessage());
			// e.printStackTrace();
		}
		return true;
	}

	/**
	 * Buy action PostePay
	 * 
	 * @return String label of the next view
	 */
	public String buy() {

		if (validBuyTransaction()) {
			if (createBuyTransaction()) {
				reset();
				return "successBuy";
			}
		}
		return "error";
	}

	/**
	 * Get Data from the web
	 */
	private void getData() {

		COGE.getInstance().read();
		ExchangeRate exchangeRateRetrieved = COGE.getInstance()
				.getExchangeRate();
		if (exchangeRateRetrieved != null) {
			/*
			 * Adjust exchangeRate to our value
			 */
			priceBuy = exchangeRateRetrieved.getRate()
					+ exchangeRateRetrieved.getRate() / 100.00 * feeBuy;
			priceBuyString = df2.format(priceBuy);
		}
		if (COGE.getInstance().getBTCWallet() != null) {
			walletBTCBalance = COGE.getInstance().getBTCWallet()
					.getBalanceAvailable();
			/*
			 * adjust with account fee
			 */
			walletBTCBalance -= feeSend;
			walletBTCBalance -= walletBTCBalance / 100d * (feeBuy - feeSeller);
			if (walletBTCBalance < Constants.WALLET_BALANCE_MINIMUM_AMOUNT) {
				walletBTCEmpty = true;
			} else {
				walletBTCEmpty = false;
			}
			// walletBTCBalanceEUR = walletBTCBalance * priceBuy;
			// if (walletBTCBalanceEUR > buyMAXPayment) {
			// walletBTCBalanceEUR = buyMAXPayment;
			// }
		} else {
			walletBTCEmpty = true;
		}
	}

	/**
	 * Update view
	 */
	public void updateForm() {
		getData();
	}

	public boolean getWalletBTCEmpty() {
		return walletBTCEmpty;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Double getPriceBuy() {
		return priceBuy;
	}

	public void setPriceBuy(Double priceBuy) {
		this.priceBuy = priceBuy;
	}

	public Double getFeeBuy() {
		return feeBuy;
	}

	public void setFeeBuy(Double feeBuy) {
		this.feeBuy = feeBuy;
	}

	public Double getWalletBTCBalance() {
		return walletBTCBalance;
	}

	public void setWalletBTCBalance(Double walletBalance) {
		this.walletBTCBalance = walletBalance;
	}

	/*
	 * public Double getWalletBTCBalanceEUR() { return walletBTCBalanceEUR; }
	 * 
	 * public void setWalletBTCBalanceEUR(Double walletBalanceEur) {
	 * this.walletBTCBalanceEUR = walletBalanceEur; }
	 */

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOrderSubmittedSummary() {
		return orderSubmittedSummary;
	}

	public void setOrderSubmittedSummary(String orderSubmittedSummary) {
		this.orderSubmittedSummary = orderSubmittedSummary;
	}

	public String getOrderSubmittedDetail() {
		return orderSubmittedDetail;
	}

	public void setOrderSubmittedDetail(String orderSubmittedDetail) {
		this.orderSubmittedDetail = orderSubmittedDetail;
	}

	public Integer getPollingInterval() {
		return pollingInterval;
	}

	public void setPollingInterval(Integer pollingInterval) {
		this.pollingInterval = pollingInterval;
	}

	public String getPostePayName() {
		return postePayName;
	}

	public void setPostePayName(String postePayName) {
		this.postePayName = postePayName;
	}

	public String getPostePayNumber() {
		return postePayNumber;
	}

	public void setPostePayNumber(String postePayNumber) {
		this.postePayNumber = postePayNumber;
	}

	public String getPostePaySSN() {
		return postePaySSN;
	}

	public void setPostePaySSN(String postePaySSN) {
		this.postePaySSN = postePaySSN;
	}

	public String getLastAddress() {
		return lastAddress;
	}

	public void setLastAddress(String lastAddress) {
		this.lastAddress = lastAddress;
	}

	public String getLastEmail() {
		return lastEmail;
	}

	public void setLastEmail(String lastEmail) {
		this.lastEmail = lastEmail;
	}

	public String getLastCode() {
		return lastCode;
	}

	public void setLastCode(String lastCode) {
		this.lastCode = lastCode;
	}

	public String getBtcQuantity() {
		return btcQuantity;
	}

	public void setBtcQuantity(String btcQuantity) {
		this.btcQuantity = btcQuantity;
	}

	public String getEurQuantity() {
		return eurQuantity;
	}

	public void setEurQuantity(String eurQuantity) {
		this.eurQuantity = eurQuantity;
	}

	public String getPriceBuyString() {
		return priceBuyString;
	}

	public void setPriceBuyString(String priceBuyString) {
		this.priceBuyString = priceBuyString;
	}

	public String getOrderBTCQuantity() {
		return orderBTCQuantity;
	}

	public void setOrderBTCQuantity(String orderBTCQuantity) {
		this.orderBTCQuantity = orderBTCQuantity;
	}

	public String getOrderEURQuantity() {
		return orderEURQuantity;
	}

	public void setOrderEURQuantity(String orderEURQuantity) {
		this.orderEURQuantity = orderEURQuantity;
	}

	public String getTransactionCode() {

		code = new String("");
		for (;;) {
			// code = RandomStringUtils.randomNumeric(4);
			code = RandomStringUtils.randomAlphanumeric(3).toUpperCase();
			if (orderManager.getOrderByCode(code) == null) {
				return code;
			}
		}
	}
}
