package com.gconsulting.webapp.action;

import java.io.Serializable;

import javax.faces.bean.ViewScoped;

import org.springframework.stereotype.Component;

import com.gconsulting.webapp.coge.COGE;
import com.gconsulting.webapp.exception.ClientException;

@ViewScoped
@Component("arbitrageScannerManagerAction")
public class COGEScannerManager extends BasePage implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -8264468867722402963L;

	public COGEScannerManager() {
		super();
	}

	public boolean getArbitrageThreadIsAlive() {
		return COGE.getInstance().cogeThreadIsAlive();
	}

	public boolean getArbitrageThreadIsInterrupted() {
		return COGE.getInstance().cogeThreadIsInterrupted();
	}

	public void cogeThreadCheckAccess() {
		COGE.getInstance().cogeThreadCheckAccess();
	}

	public String getArbitrageThreadState() {
		return COGE.getInstance().cogeThreadState().toString();
	}

	public void cogeThreadRestart() throws ClientException {
		COGE.getInstance().cogeThreadRestart();
	}
}
