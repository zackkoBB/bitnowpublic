package com.gconsulting.webapp.action;

import java.io.Serializable;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.BTCAddress;
import com.gsconsulting.bitnow.model.TransactionWallet;
import com.gsconsulting.bitnow.model.User;
import com.gsconsulting.bitnow.model.enums.AccountStatus;
import com.gsconsulting.bitnow.model.enums.AccountType;
import com.gsconsulting.bitnow.model.enums.TransactionWalletType;
import com.gsconsulting.bitnow.model.service.AccountManager;
import com.gsconsulting.bitnow.model.service.BTCAddressManager;
import com.gsconsulting.bitnow.model.service.ConfigurationManager;
import com.gsconsulting.bitnow.model.service.TransactionWalletManager;
import com.gsconsulting.bitnow.model.util.Constants;
import com.gsconsulting.bitnow.model.util.CryptService;

@Scope("session")
@Component("fundingBTCAction")
public class FundingBTC extends BasePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1227315794576877516L;
	private ConfigurationManager configurationManager;
	private AccountManager accountManager;
	private BTCAddressManager btcAddressManager;
	private TransactionWalletManager transactionWalletManager;

	private User user;
	private String selectedAddress;
	private String selectedAddressQR;
	private List<BTCAddress> addresses;
	private List<TransactionWallet> incomingTransactions;
	private List<TransactionWallet> transactions;
	private Account eurAccount;
	private Account ppAccount;

	private Integer pollingInterval;
	private String transactionLinkBase;
	private String addressLinkBase;

	public FundingBTC() {
		super();
	}

	@PostConstruct
	public void init() {

		try {
			selectedAddress = null;
			selectedAddressQR = null;
			pollingInterval = new Integer(configurationManager
					.getConfigurationByKey(Constants.UI_POLLING_INTERVAL)
					.getValue());
			transactionLinkBase = new String(configurationManager
					.getConfigurationByKey(Constants.TRANSACTION_LINK)
					.getValue());
			addressLinkBase = new String(configurationManager
					.getConfigurationByKey(Constants.ADDRESS_LINK).getValue());
			retrieveUser();
			retrieveBTCAddress();
			retrieveTransactions();
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException e) {
			log.error("Init: " + e.getMessage());
			// e.printStackTrace();
		}
	}

	@Autowired
	public void setConfigurationManager(
			@Qualifier("configurationManager") ConfigurationManager configurationManager) {
		this.configurationManager = configurationManager;
	}

	@Autowired
	public void setAccountManager(
			@Qualifier("accountManager") AccountManager accountManager) {
		this.accountManager = accountManager;
	}

	@Autowired
	public void setBTCAddressManager(
			@Qualifier("btcAddressManager") BTCAddressManager btcAddressManager) {
		this.btcAddressManager = btcAddressManager;
	}

	@Autowired
	public void setTransactionWalletManager(
			@Qualifier("transactionWalletManager") TransactionWalletManager transactionWalletManager) {
		this.transactionWalletManager = transactionWalletManager;
	}

	private void retrieveUser() {

		if (getRequest().getRemoteUser() != null) {
			user = userManager.getUserByUsername(getRequest().getRemoteUser());
		} else {
			user = null;
		}
	}

	private void retrieveBTCAddress() throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException {

		for (Account account : accountManager.getAccountByUser(user,
				AccountStatus.ACCOUNT_NOT_ACTIVE)) {
			if (account.getType() == AccountType.EUR.ordinal()) {
				eurAccount = account;
				if (account.getNumber() != null) {
					if (account.getNumber().length() > 0) {
						selectedAddress = CryptService.decrypt(eurAccount
								.getNumber());
						selectedAddressQR = com.gconsulting.webapp.util.Constants.QR_CODE_BASE_URL
								+ selectedAddress;
					}
				}
			} else if (account.getType() == AccountType.EUR_POSTEPAY.ordinal()) {
				ppAccount = account;
			}
		}
		addresses = btcAddressManager.getBTCAddressByUser(user);
		/*
		 * Fix Date format
		 */
		for (BTCAddress addressTmp : addresses) {
			addressTmp.setCreated(addressTmp.getCreated() * 1000);
		}
		/*
		 * Sort by date
		 */
		Collections.sort(addresses, new Comparator<BTCAddress>() {
			public int compare(BTCAddress o1, BTCAddress o2) {
				Long a = o1.getCreated();
				Long b = o2.getCreated();
				if (a < b)
					return -1;
				else if (a == b) // it's equals
					return 0;
				else
					return 1;
			}
		});
		Collections.reverse(addresses);
	}

	/**
	 * Retrieve all transactions in wallet: USE PER MONTH for performances
	 * 
	 */
	private void retrieveTransactions() {

		if (eurAccount != null) {
			incomingTransactions = new ArrayList<TransactionWallet>();
			transactions = new ArrayList<TransactionWallet>();
			for (TransactionWallet transaction : transactionWalletManager
					.getTransactionByAccount(
							eurAccount,
							(Calendar.getInstance().getTimeInMillis() - Constants.MAX_TRANSACTION_HISTORY_TIME_IN_MILLIS) / 1000)) {
				if (transaction.getType() == TransactionWalletType.INCOMING
						.ordinal()) {
					if (transaction.getConfirmations() < Constants.TRANSACTION_WALLET_INCOMING_DONE_CONFIRMATIONS) {
						transaction.setCreated(transaction.getCreated() * 1000);
						incomingTransactions.add(transaction);
					} else {
						transaction.setCreated(transaction.getCreated() * 1000);
						transactions.add(transaction);
					}
				}
			}
			for (TransactionWallet transaction : transactionWalletManager
					.getTransactionByAccount(
							ppAccount,
							(Calendar.getInstance().getTimeInMillis() - Constants.MAX_TRANSACTION_HISTORY_TIME_IN_MILLIS) / 1000)) {
				if (transaction.getType() == TransactionWalletType.OUTGOING
						.ordinal()) {
					transaction.setCreated(transaction.getCreated() * 1000);
					transactions.add(transaction);
				}
			}
			/*
			 * Sort by date
			 */
			Collections.sort(transactions, new Comparator<TransactionWallet>() {
				public int compare(TransactionWallet o1, TransactionWallet o2) {
					Long a = o1.getCreated();
					Long b = o2.getCreated();
					if (a < b)
						return -1;
					else if (a == b) // it's equals
						return 0;
					else
						return 1;
				}
			});
			Collections.reverse(transactions);
		}
	}

	public void refreshUI() {

		try {
			retrieveUser();
			retrieveBTCAddress();
			retrieveTransactions();
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException e) {
			log.error("refreshUI: " + e.getMessage());
			// e.printStackTrace();
		}

	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getSelectedAddress() {
		return selectedAddress;
	}

	public void setSelectedAddress(String selectedAddress) {
		this.selectedAddress = selectedAddress;
	}

	public List<BTCAddress> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<BTCAddress> addresses) {
		this.addresses = addresses;
	}

	public List<TransactionWallet> getIncomingTransactions() {
		return incomingTransactions;
	}

	public void setIncomingTransactions(
			List<TransactionWallet> incomingTransactions) {
		this.incomingTransactions = incomingTransactions;
	}

	public List<TransactionWallet> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<TransactionWallet> transactions) {
		this.transactions = transactions;
	}

	public Integer getPollingInterval() {
		return pollingInterval;
	}

	public void setPollingInterval(Integer pollingInterval) {
		this.pollingInterval = pollingInterval;
	}

	public String getTransactionLinkBase() {
		return transactionLinkBase;
	}

	public void setTransactionLinkBase(String transactionLinkBase) {
		this.transactionLinkBase = transactionLinkBase;
	}

	public String getAddressLinkBase() {
		return addressLinkBase;
	}

	public void setAddressLinkBase(String addressLinkBase) {
		this.addressLinkBase = addressLinkBase;
	}

	public String getSelectedAddressQR() {
		return selectedAddressQR;
	}

	public void setSelectedAddressQR(String selectedAddressQR) {
		this.selectedAddressQR = selectedAddressQR;
	}

}
