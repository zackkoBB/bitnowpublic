package com.gconsulting.webapp.action;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gconsulting.webapp.coge.COGE;
import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.User;
import com.gsconsulting.bitnow.model.enums.AccountStatus;
import com.gsconsulting.bitnow.model.enums.AccountType;
import com.gsconsulting.bitnow.model.service.AccountManager;
import com.gsconsulting.bitnow.model.service.ConfigurationManager;
import com.gsconsulting.bitnow.model.util.Constants;

@Scope("session")
@Component("fundingEURAction")
public class FundingEUR extends BasePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 383658173395737043L;
	private ConfigurationManager configurationManager;
	private AccountManager accountManager;

	private User user;
	private Account eurAccount;
	private boolean seller;

	private Integer pollingInterval;

	public FundingEUR() {
		super();
	}

	@PostConstruct
	public void init() {

		pollingInterval = new Integer(configurationManager
				.getConfigurationByKey(Constants.UI_POLLING_INTERVAL)
				.getValue());
		retrieveUser();
		retrieveEURAccount();
	}

	@Autowired
	public void setConfigurationManager(
			@Qualifier("configurationManager") ConfigurationManager configurationManager) {
		this.configurationManager = configurationManager;
	}

	@Autowired
	public void setAccountManager(
			@Qualifier("accountManager") AccountManager accountManager) {
		this.accountManager = accountManager;
	}

	private void retrieveUser() {

		if (getRequest().getRemoteUser() != null) {
			user = userManager.getUserByUsername(getRequest().getRemoteUser());
		} else {
			user = null;
		}
	}

	private void retrieveEURAccount() {

		eurAccount = null;
		for (Account account : accountManager.getAccountByUser(user,
				AccountStatus.ACCOUNT_NOT_ACTIVE)) {
			if (account.getType() == AccountType.EUR.ordinal()) {
				eurAccount = account;
				if (eurAccount.getLastReadStatus() == AccountStatus.ACCOUNT_SELLER
						.ordinal()) {
					seller = true;
				} else {
					seller = false;
				}
			}
		}
		if (eurAccount == null) {
			seller = true;
		}
	}

	/**
	 * Save value
	 */
	public String save() {

		/*
		 * Only numbers
		 */
		if (eurAccount.getBalance() >= com.gconsulting.webapp.util.Constants.MAX_FUNDING_EUR_BALANCE) {
			addFacesError(
					"errors.maxFundingEUR",
					com.gconsulting.webapp.util.Constants.MAX_FUNDING_EUR_BALANCE);
			return "error";
		} else {
			accountManager.update(eurAccount);
			COGE.getInstance().read();
			addFacesMessage("success.funding");
			return "success";
		}
	}

	/**
	 * Reset form
	 * 
	 * @return
	 */
	public String cancel() {

		refreshUI();
		return "cancel";
	}

	public void refreshUI() {

		retrieveUser();
		retrieveEURAccount();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getPollingInterval() {
		return pollingInterval;
	}

	public void setPollingInterval(Integer pollingInterval) {
		this.pollingInterval = pollingInterval;
	}

	public Account getEurAccount() {
		return eurAccount;
	}

	public void setEurAccount(Account eurAccount) {
		this.eurAccount = eurAccount;
	}

	public boolean isSeller() {
		return seller;
	}

	public void setSeller(boolean seller) {
		this.seller = seller;
	}
}
