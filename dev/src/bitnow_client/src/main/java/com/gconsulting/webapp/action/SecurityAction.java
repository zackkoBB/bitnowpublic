package com.gconsulting.webapp.action;

import java.io.IOException;
import java.io.Serializable;

import java.lang.NumberFormatException;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;

import com.gsconsulting.bitnow.model.User;
import com.gsconsulting.bitnow.model.service.UserExistsException;
import com.gsconsulting.bitnow.model.util.CryptService;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;

/**
 * JSF Page class to handle editing a user with a form.
 *
 * @author mraible
 */
@Scope("session")
@Component("securityAction")
public class SecurityAction extends BasePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7716617831182716060L;
	private User user;
	private GoogleAuthenticatorKey twoFAKey;
	private String twoFALink;
	private String password2FA;
	private String password2FADisable;

	@PostConstruct
	public void init() {

		loadUser();
		init2FA();
	}

	private void init2FA() {

		twoFAKey = CryptService.getKey();
		twoFALink = CryptService.qrURL(
				com.gconsulting.webapp.util.Constants.TWO_FA_ISSUER,
				user.getUsername(), twoFAKey);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Enable 2FA
	 * 
	 * @return
	 */
	public String enable2FA() {

		try {
			if (CryptService.authorize(twoFAKey.getKey(), new Integer(
					password2FA))) {
				user.setSecret(twoFAKey.getKey());
				userManager.update(user);
				addFacesMessage("user.saved");
				password2FA = null;
				password2FADisable = null;
				return "security";
			}
		} catch (NumberFormatException e) {
			log.error("Error: " + e.getMessage());
		}
		password2FA = null;
		password2FADisable = null;
		addFacesError("errors.otp");
		return "security";
	}

	/**
	 * Disable 2FA
	 * 
	 * @return
	 */
	public String disable2FA() {

		try {
			log.info("Secret: " + user.getSecret());
			log.info("OTP: " + password2FADisable);
			if (CryptService.authorize(user.getSecret(), new Integer(
					password2FADisable))) {
				user.setSecret("");
				userManager.update(user);
				addFacesMessage("user.saved");
				password2FA = null;
				password2FADisable = null;
				return "security";
			}
		} catch (NumberFormatException e) {
			log.error("Error: " + e.getMessage());
		}
		password2FA = null;
		password2FADisable = null;
		addFacesError("errors.otp");
		return "security";
	}

	public void otpRefresh() {
		log.info("OTP: " + password2FA);
	}

	/**
	 * Reset form
	 * 
	 * @return
	 */
	public String cancel() {

		if (log.isDebugEnabled()) {
			log.debug("Entering 'cancel' method");
		}
		if (!"list".equals(getParameter("from"))) {
			return "home";
		} else {
			return "cancel";
		}
	}

	/**
	 * Load user data
	 * 
	 * @return
	 */
	public String loadUser() {

		user = userManager.getUserByUsername(getRequest().getRemoteUser());
		if (user.getUsername() != null) {
			user.setConfirmPassword(user.getPassword());
		}
		return "security";
	}

	/**
	 * Save a user
	 * 
	 * @return
	 * @throws IOException
	 */
	public String save() throws IOException {

		Integer originalVersion = user.getVersion();
		/*
		 * Check password matches!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		 */
		if (!user.getPassword().equalsIgnoreCase(user.getConfirmPassword())) {
			addFacesError("errors.password.confirmPasswordMismatch");
			return "security";
		}
		try {
			user = userManager.saveUser(user);
		} catch (AccessDeniedException ade) {
			// thrown by UserSecurityAdvice configured in aop:advisor
			// userManagerSecurity
			log.warn(ade.getMessage());
			getResponse().sendError(HttpServletResponse.SC_FORBIDDEN);
			return "security";
		} catch (UserExistsException e) {
			addFacesError("errors.existing.user",
					new Object[] { user.getUsername(), user.getEmail() });
			// reset the version # to what was passed in
			user.setVersion(originalVersion);
			return "security";
		}
		user.setConfirmPassword(user.getPassword());
		addFacesMessage("user.saved");
		// return to main Menu
		return "security";
	}

	/**
	 * Check if 2FA is enabled for this user
	 * 
	 * @return
	 */

	public boolean isTwoFAEnabled() {

		if (user != null) {
			if (user.getSecret() != null) {
				if (user.getSecret().length() > 0) {
					return true;
				}
			}
		}
		return false;
	}

	public String getTwoFAKey() {
		return twoFAKey.getKey();
	}

	public String getTwoFALink() {
		return twoFALink;
	}

	public String getPassword2FA() {
		return password2FA;
	}

	public void setPassword2FA(String password2FA) {
		this.password2FA = password2FA;
	}

	public String getPassword2FADisable() {
		return password2FADisable;
	}

	public void setPassword2FADisable(String password2faDisable) {
		password2FADisable = password2faDisable;
	}
}
