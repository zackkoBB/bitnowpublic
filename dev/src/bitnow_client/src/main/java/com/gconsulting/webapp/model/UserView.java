package com.gconsulting.webapp.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;

import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.Document;
import com.gsconsulting.bitnow.model.LabelValue;
import com.gsconsulting.bitnow.model.Role;
import com.gsconsulting.bitnow.model.User;

/**
 * This class represents the basic "user" object in AppFuse that allows for
 * authentication and user management. It implements Acegi Security's
 * UserDetails interface.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Updated by
 *         Dan Kibler (dan@getrolling.com) Extended to implement Acegi
 *         UserDetails interface by David Carter david@carter.net
 */
public class UserView implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3099000778199593358L;
	private User user;
	private Set<Role> roles;
	private List<Account> accounts;
	private List<Document> documents;

	/**
	 * Default constructor - creates a new instance with no values set.
	 */
	public UserView() {
	}

	/**
	 * Create a new instance and set the username.
	 *
	 * @param username
	 *            login name for user.
	 */
	public UserView(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}

	public List<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	/**
	 * Convert user roles to LabelValue objects for convenience.
	 *
	 * @return a list of LabelValue objects with role information
	 */
	public List<LabelValue> getRoleList() {
		
		List<LabelValue> userRoles = new ArrayList<LabelValue>();
		if (this.roles != null) {
			for (Role role : roles) {
				// convert the user's roles to LabelValue Objects
				userRoles.add(new LabelValue(role.getName(), role.getName()));
			}
		}
		return userRoles;
	}

	/**
	 * @return GrantedAuthority[] an array of roles.
	 * @see org.springframework.security.core.userdetails.UserDetails#getAuthorities()
	 */
	public Set<GrantedAuthority> getAuthorities() {
		Set<GrantedAuthority> authorities = new LinkedHashSet<GrantedAuthority>();
		authorities.addAll(roles);
		return authorities;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean equals(Object o) {

		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public int hashCode() {
		return (user.getUsername() != null ? user.getUsername().hashCode() : 0);
	}

	/**
	 * {@inheritDoc}
	 */
	public String toString() {
		return "";
	}
}
