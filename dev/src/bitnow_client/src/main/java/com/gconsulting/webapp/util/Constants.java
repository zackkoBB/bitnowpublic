package com.gconsulting.webapp.util;

public class Constants {

	public static final String REST_BASE_PACKAGE = "com.gconsulting.webapp.rest.interfaces";
	public static final String BUY_ORDER_EMAIL_TEMPLATE = "buyOrder.vm";
	public static final String SELL_ORDER_EMAIL_TEMPLATE = "sellOrder.vm";
	public static final String CONTACT_EMAIL_TEMPLATE = "contact.vm";
	public static final String REGISTER_EMAIL_TEMPLATE = "register.vm";

	public static final String QR_CODE_BASE_URL = "https://chart.googleapis.com/chart?chs=200x200&chld=M%7C0&cht=qr&chl=";

	public static final Double MAX_FUNDING_EUR_BALANCE = 5000d;
	
	/**
	 * 2FA
	 */
	public static final Integer TWO_FA_WIDTH = 300;
	public static final Integer TWO_FA_HEIGHT = 300;
	public static final String TWO_FA_ISSUER = "BitNow";

}