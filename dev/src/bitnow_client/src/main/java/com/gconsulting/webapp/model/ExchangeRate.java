package com.gconsulting.webapp.model;

public class ExchangeRate {

	private Long time;
	private Double rate;

	public ExchangeRate() {
		super();
	}

	public ExchangeRate(Long time, Double rate) {
		super();
		this.time = time;
		this.rate = rate;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

}
