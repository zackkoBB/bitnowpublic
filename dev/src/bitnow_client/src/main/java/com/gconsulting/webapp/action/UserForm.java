package com.gconsulting.webapp.action;

import java.io.IOException;
import java.io.Serializable;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.mail.MailException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.gconsulting.webapp.util.RequestUtil;
import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.Role;
import com.gsconsulting.bitnow.model.User;
import com.gsconsulting.bitnow.model.enums.AccountStatus;
import com.gsconsulting.bitnow.model.enums.AccountType;
import com.gsconsulting.bitnow.model.service.AccountManager;
import com.gsconsulting.bitnow.model.service.ConfigurationManager;
import com.gsconsulting.bitnow.model.service.RoleManager;
import com.gsconsulting.bitnow.model.service.UserExistsException;
import com.gsconsulting.bitnow.model.util.Constants;
import com.gsconsulting.bitnow.model.util.ConvertUtil;
import com.gsconsulting.bitnow.model.util.CryptService;

/**
 * JSF Page class to handle editing a user with a form.
 *
 * @author mraible
 */
@Scope("request")
@Component("userForm")
public class UserForm extends BasePage implements Serializable {

	private static final long serialVersionUID = -1141119853856863204L;

	private RoleManager roleManager;
	private AccountManager accountManager;
	private ConfigurationManager configurationManager;
	private String id;
	private String accountId;
	private Double feeSeller;
	private User user;
	private Account ppAccount;
	private Map<String, String> availableRoles;
	private String[] userRoles;

	@PostConstruct
	public void init() {

		accountId = null;
		loadUser();
		feeSeller = new Double(configurationManager.getConfigurationByKey(
				Constants.FEE_SELLER).getValue());
	}

	public String[] getThemes() {
		return (Constants.POSSIBLE_THEMES);
	}

	public void setId(String id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Autowired
	public void setRoleManager(@Qualifier("roleManager") RoleManager roleManager) {
		this.roleManager = roleManager;
	}

	@Autowired
	public void setConfigurationManager(
			@Qualifier("configurationManager") ConfigurationManager configurationManager) {
		this.configurationManager = configurationManager;
	}

	@Autowired
	public void setAccountManager(
			@Qualifier("accountManager") AccountManager accountManager) {
		this.accountManager = accountManager;
	}

	/**
	 * Add a new user
	 * 
	 * @return
	 */
	public String add() {

		user = new User();
		user.setEnabled(true);
		user.addRole(new Role(Constants.USER_ROLE));
		return "userForm";
	}

	/**
	 * Reset form
	 * 
	 * @return
	 */
	public String cancel() {

		if (log.isDebugEnabled()) {
			log.debug("Entering 'cancel' method");
		}
		if (!"list".equals(getParameter("from"))) {
			return "home";
		} else {
			return "cancel";
		}
	}

	/**
	 * Load user data
	 * 
	 * @return
	 */
	public String loadUser() {

		HttpServletRequest request = getRequest();
		// if a user's id is passed in
		if (id != null) {
			log.debug("Editing user, id is: " + id);
			// lookup the user using that id
			user = userManager.getUser(id);
		} else {
			user = userManager.getUserByUsername(request.getRemoteUser());
		}
		if (user.getUsername() != null) {
			user.setConfirmPassword(user.getPassword());
			if (isRememberMe()) {
				// if user logged in with remember me, display a warning that
				// they can't change passwords
				log.debug("checking for remember me login...");
				log.trace("User '" + user.getUsername()
						+ "' logged in with cookie");
				addFacesMessage("userProfile.cookieLogin");
			}
		}
		/*
		 * Load account
		 */
		if (!isAdmin()) {
			ppAccount = accountManager.getAccountByUserAndType(user,
					AccountType.EUR_POSTEPAY.ordinal(),
					AccountStatus.ACCOUNT_NOT_ACTIVE);
			if (ppAccount != null) {
				try {
					accountId = ppAccount.getId();
					ppAccount.setId(CryptService.decrypt(ppAccount.getId()));
					ppAccount.setNumber(CryptService.decrypt(ppAccount
							.getNumber()));
					ppAccount.setOwner(CryptService.decrypt(ppAccount
							.getOwner()));
					ppAccount.setWebUsername(CryptService.decrypt(ppAccount
							.getWebUsername()));
					ppAccount.setWebPassword(CryptService.decrypt(ppAccount
							.getWebPassword()));
				} catch (InvalidKeyException | NoSuchAlgorithmException
						| NoSuchPaddingException | IllegalBlockSizeException
						| BadPaddingException e) {
					e.printStackTrace();
				}
			} else {
				accountId = null;
				Long currentDate = Calendar.getInstance().getTimeInMillis() / 1000;
				ppAccount = new Account(user, null,
						AccountType.EUR_POSTEPAY.ordinal(), null, null, null,
						null, 0d, 0d, 0l, 0d,
						AccountStatus.ACCOUNT_LAST_READ_ERROR.ordinal(),
						feeSeller, currentDate, currentDate, currentDate);
			}
		}
		return "userForm";
	}

	/**
	 * Convenience method for view templates to check if the user is logged in
	 * with RememberMe (cookies).
	 * 
	 * @return true/false - false if user interactively logged in.
	 */
	public boolean isRememberMe() {

		if (user != null && user.getId() == null)
			return false; // check for add()

		AuthenticationTrustResolver resolver = new AuthenticationTrustResolverImpl();
		SecurityContext ctx = SecurityContextHolder.getContext();

		if (ctx != null) {
			Authentication auth = ctx.getAuthentication();
			return resolver.isRememberMe(auth);
		}
		return false;
	}

	/**
	 * Save a user
	 * 
	 * @return
	 * @throws IOException
	 */
	public String save() throws IOException {

		// workaround for plain ol' HTML input tags that don't seem to set
		// properties on the managed bean
		setUserRoles(getRequest().getParameterValues("userForm:userRoles"));
		for (int i = 0; (userRoles != null) && (i < userRoles.length); i++) {
			String roleName = userRoles[i];
			user.addRole(roleManager.getRole(roleName));
		}
		// Check for Integers set to 0: happens in Tomcat, not in Jetty
		if (user.getId() != null && user.getId() == 0/*
													 * || user.getVersion() !=
													 * null && user.getVersion()
													 * == 0
													 */) {
			user.setId(null);
			user.setVersion(null);
		}
		Integer originalVersion = user.getVersion();
		// check theme
		if (user.getTheme().equalsIgnoreCase("default")) {
			user.setTheme(Constants.DEFAULT_THEME);
		}
		/*
		 * Check password matches!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		 */
		if (!user.getPassword().equalsIgnoreCase(user.getConfirmPassword())) {
			addFacesError("errors.password.confirmPasswordMismatch");
			return "userForm";
		}
		/*
		 * Save account
		 */
		if (!isAdmin()) {
			if (ppAccount == null) {
				/*
				 * Can't be
				 */
			} else {
				try {
					ppAccount.setFee(feeSeller);
					ppAccount.setId(CryptService.encrypt(ppAccount.getId()));
					ppAccount.setNumber(CryptService.encrypt(ppAccount
							.getNumber()));
					ppAccount.setOwner(CryptService.encrypt(ppAccount
							.getOwner()));
					ppAccount.setWebUsername(CryptService.encrypt(ppAccount
							.getWebUsername()));
					ppAccount.setWebPassword(CryptService.encrypt(ppAccount
							.getWebPassword()));
					ppAccount.setLastModified(Calendar.getInstance()
							.getTimeInMillis() / 1000);
					if (accountId == null) {
						accountManager.create(ppAccount);
						accountId = ppAccount.getId();
					} else {
						accountManager.update(ppAccount);
					}
				} catch (InvalidKeyException | NoSuchAlgorithmException
						| NoSuchPaddingException | IllegalBlockSizeException
						| BadPaddingException e) {
					e.printStackTrace();
				}
			}
		}
		try {
			user = userManager.saveUser(user);
		} catch (AccessDeniedException ade) {
			// thrown by UserSecurityAdvice configured in aop:advisor
			// userManagerSecurity
			log.warn(ade.getMessage());
			getResponse().sendError(HttpServletResponse.SC_FORBIDDEN);
			return "userForm";
		} catch (UserExistsException e) {
			addFacesError("errors.existing.user",
					new Object[] { user.getUsername(), user.getEmail() });
			// reset the version # to what was passed in
			user.setVersion(originalVersion);
			return "userForm";
		}
		if (!"list".equals(getParameter("from"))) {
			// add success messages
			addFacesMessage("user.saved");
			// return to main Menu
			return "userForm";
		} else {
			// add success messages
			if ("".equals(getParameter("userForm:version"))) {
				addFacesMessage("user.added", user.getUsername());
				try {
					sendUserMessage(
							user,
							getText("newuser.email.message", user.getUsername()),
							RequestUtil.getAppURL(getRequest()));
				} catch (MailException me) {
					addFacesError(me.getCause().getLocalizedMessage());
				}
				return "list"; // return to list screen
			} else {
				addFacesMessage("user.updated.byAdmin", user.getUsername());
				return "userForm"; // return to current page
			}
		}
	}

	public String delete() {

		userManager.removeUser(getUser().getId().toString());
		addFacesMessage("user.deleted", getUser().getUsername());
		return "list";
	}

	/**
	 * Convenience method to determine if the user came from the list screen
	 * 
	 * @return String
	 */
	public String getFrom() {

		if ((id != null) || (getParameter("editUser:add") != null)
				|| ("list".equals(getParameter("from")))) {
			return "list";
		}
		return "";
	}

	// Form Controls ==========================================================
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, String> getAvailableRoles() {

		if (availableRoles == null) {
			List roles = (List) getServletContext().getAttribute(
					Constants.AVAILABLE_ROLES);
			availableRoles = ConvertUtil.convertListToMap(roles);
		}
		return availableRoles;
	}

	public String[] getUserRoles() {

		userRoles = new String[user.getRoles().size()];
		int i = 0;
		if (userRoles.length > 0) {
			for (Role role : user.getRoles()) {
				userRoles[i] = role.getName();
				i++;
			}
		}

		return userRoles;
	}

	public boolean isTwoFAEnabled() {

		if (user != null) {
			if (user.getSecret() != null) {
				if (user.getSecret().length() > 0) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isAdmin() {

		if (user == null) {
			loadUser();
		}
		for (Role role : user.getRoles()) {
			if (role.getName().equalsIgnoreCase(Constants.ADMIN_ROLE)) {
				return true;
			}
		}
		return false;
	}

	public void setUserRoles(String[] userRoles) {
		this.userRoles = userRoles;
	}

	public List<String> getCurrencies() {
		return Constants.getCurrencies();
	}

	public Account getPpAccount() {
		return ppAccount;
	}

	public void setPpAccount(Account ppAccount) {
		this.ppAccount = ppAccount;
	}
}
