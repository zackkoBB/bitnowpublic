package com.gconsulting.webapp.action;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.lang.RandomStringUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gconsulting.webapp.coge.COGE;
import com.gconsulting.webapp.exception.ClientException;
import com.gconsulting.webapp.model.TransactionView;
import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.File;
import com.gsconsulting.bitnow.model.Order;
import com.gsconsulting.bitnow.model.Receipt;
import com.gsconsulting.bitnow.model.Role;
import com.gsconsulting.bitnow.model.Transaction;
import com.gsconsulting.bitnow.model.User;
import com.gsconsulting.bitnow.model.enums.AccountStatus;
import com.gsconsulting.bitnow.model.enums.AccountType;
import com.gsconsulting.bitnow.model.enums.TransactionStatus;
import com.gsconsulting.bitnow.model.enums.TransactionType;
import com.gsconsulting.bitnow.model.service.AccountManager;
import com.gsconsulting.bitnow.model.service.ConfigurationManager;
import com.gsconsulting.bitnow.model.service.FileManager;
import com.gsconsulting.bitnow.model.service.OrderManager;
import com.gsconsulting.bitnow.model.service.ReceiptManager;
import com.gsconsulting.bitnow.model.service.TransactionManager;
import com.gsconsulting.bitnow.model.service.UserManager;
import com.gsconsulting.bitnow.model.util.Constants;

@Scope("session")
@Component("dashboardAction")
public class Dashboard extends BasePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4301111659844023915L;
	private OrderManager orderManager;
	private TransactionManager transactionHistoryManager;
	private ConfigurationManager configurationManager;
	private AccountManager accountManager;
	private FileManager fileManager;
	private ReceiptManager receiptManager;

	// private List<StatisticsView> statistics;
	private List<TransactionView> transactions;
	private List<TransactionView> transactionsViewHistory;

	/**
	 * Sell statistics
	 */
	private Integer totalTransactionsSell;
	private Integer transactionsCompletedSell;
	private Double btcSold;
	private Double btcRequested;
	private Double btcRequestedNow;
	private Double eurReceived;
	private Double eurRequested;
	private Double averageSellingPrice;
	/**
	 * Buy statistics
	 */
	private Integer totalTransactionsBuy;
	private Integer transactionsCompletedBuy;
	private Double btcBought;
	private Double btcRequestedBuy;
	private Double eurRequestedBuy;
	private Double eurSent;
	private Double averageBuyingPrice;
	/**
	 * Total statistics
	 */
	private Integer totalTransactions;
	private Integer transactionsCompleted;

	private Double btcBalance;
	private Double btcBalanceAvailable;
	private Double eurBalance;
	private Double eurBalanceAvailable;
	private Integer accountLastRead;
	private TransactionView selectedTransaction;
	private Receipt selectedReceipt;

	private String transactionHash;
	private User user;
	private Integer pollingInterval;
	// private Double feeBuy;
	private String transactionLinkBase;

	public Dashboard() {
		super();
	}

	@PostConstruct
	public void init() throws ClientException {

		transactionHash = new String();
		pollingInterval = new Integer(configurationManager
				.getConfigurationByKey(Constants.UI_POLLING_INTERVAL)
				.getValue());
		// feeBuy = new Double(configurationManager.getConfigurationByKey(
		// Constants.FEE_BUY).getValue());
		transactionLinkBase = new String(configurationManager
				.getConfigurationByKey(Constants.TRANSACTION_LINK).getValue());
		retrieveUser();
		retrieveStatistics(isAdmin());
	}

	@Autowired
	public void setOrderManager(
			@Qualifier("orderManager") OrderManager orderManager) {
		this.orderManager = orderManager;
	}

	@Autowired
	public void setTransactionHistoryManager(
			@Qualifier("transactionHistoryManager") TransactionManager transactionHistoryManager) {
		this.transactionHistoryManager = transactionHistoryManager;
	}

	@Autowired
	public void setConfigurationManager(
			@Qualifier("configurationManager") ConfigurationManager configurationManager) {
		this.configurationManager = configurationManager;
	}

	@Override
	@Autowired
	public void setUserManager(@Qualifier("userManager") UserManager userManager) {
		this.userManager = userManager;
	}

	@Autowired
	public void setAccountManager(
			@Qualifier("accountManager") AccountManager accountManager) {
		this.accountManager = accountManager;
	}

	@Autowired
	public void setFileManager(@Qualifier("fileManager") FileManager fileManager) {
		this.fileManager = fileManager;
	}

	@Autowired
	public void setReceiptManager(
			@Qualifier("receiptManager") ReceiptManager receiptManager) {
		this.receiptManager = receiptManager;
	}

	private void buyStatistics(TransactionView transaction) {

		if (transaction.getStatus() == TransactionStatus.UNCONFIRMED.ordinal()) {
			totalTransactions++;
			totalTransactionsSell++;
			btcRequested += transaction.getBtc();
			btcRequestedNow += transaction.getBtc();
			eurRequested += transaction.getEur();
			averageSellingPrice += transaction.getEur() / transaction.getBtc();
		} else if (transaction.getStatus() == TransactionStatus.WAITING_PAYMENT
				.ordinal()) {
			totalTransactions++;
			totalTransactionsSell++;
			btcRequested += transaction.getBtc();
			btcRequestedNow += transaction.getBtc();
			eurRequested += transaction.getEur();
			averageSellingPrice += transaction.getEur() / transaction.getBtc();
		} else if (transaction.getStatus() == TransactionStatus.PAYMENT_RECEIVED
				.ordinal()) {
			totalTransactions++;
			totalTransactionsSell++;
			btcRequested += transaction.getBtc();
			btcRequestedNow += transaction.getBtc();
			eurReceived += transaction.getEur();
			eurRequested += transaction.getEur();
			averageSellingPrice += transaction.getEur() / transaction.getBtc();
		} else if (transaction.getStatus() == TransactionStatus.BTC_SENT
				.ordinal()) {
			totalTransactions++;
			transactionsCompleted++;
			totalTransactionsSell++;
			transactionsCompletedSell++;
			eurReceived += transaction.getEur();
			btcSold += transaction.getBtc();
			btcRequested += transaction.getBtc();
			eurRequested += transaction.getEur();
			averageSellingPrice += transaction.getEur() / transaction.getBtc();
		} else if (transaction.getStatus() == TransactionStatus.PAYMENT_RECEIVED_NOT_CONFIRMED
				.ordinal()) {
			totalTransactions++;
			totalTransactionsSell++;
			eurReceived += transaction.getEur();
			btcSold += transaction.getBtc();
			btcRequested += transaction.getBtc();
			eurRequested += transaction.getEur();
			averageSellingPrice += transaction.getEur() / transaction.getBtc();
		}
	}

	private void sellStatistics(TransactionView transaction) {

		if (transaction.getStatus() == TransactionStatus.UNCONFIRMED.ordinal()) {
			totalTransactions++;
			totalTransactionsBuy++;
			btcRequestedBuy += transaction.getBtc();
			eurRequestedBuy += transaction.getEur();
			averageBuyingPrice += transaction.getEur() / transaction.getBtc();
		} else if (transaction.getStatus() == TransactionStatus.WAITING_PAYMENT
				.ordinal()) {
			totalTransactions++;
			totalTransactionsBuy++;
			btcRequestedBuy += transaction.getBtc();
			eurRequestedBuy += transaction.getEur();
			averageBuyingPrice += transaction.getEur() / transaction.getBtc();
		} else if (transaction.getStatus() == TransactionStatus.PAYMENT_RECEIVED
				.ordinal()) {
			totalTransactions++;
			totalTransactionsBuy++;
			btcRequestedBuy += transaction.getBtc();
			btcBought += transaction.getBtc();
			eurRequestedBuy += transaction.getEur();
			averageBuyingPrice += transaction.getEur() / transaction.getBtc();
		} else if (transaction.getStatus() == TransactionStatus.PAYMENT_RECEIVED_WAITING_CONFIRMATION
				.ordinal()) {
			totalTransactions++;
			totalTransactionsBuy++;
			btcRequestedBuy += transaction.getBtc();
			btcBought += transaction.getBtc();
			eurRequestedBuy += transaction.getEur();
			averageBuyingPrice += transaction.getEur() / transaction.getBtc();
		} else if (transaction.getStatus() == TransactionStatus.EUR_SENT
				.ordinal()) {
			totalTransactions++;
			transactionsCompleted++;
			totalTransactionsBuy++;
			transactionsCompletedBuy++;
			btcBought += transaction.getBtc();
			eurSent += transaction.getEur();
			btcRequestedBuy += transaction.getBtc();
			eurRequestedBuy += transaction.getEur();
			averageBuyingPrice += transaction.getEur() / transaction.getBtc();
		}
	}

	private void buyDoneStatistics(TransactionView transaction) {

		if (transaction.getStatus() == TransactionStatus.DONE.ordinal()) {
			totalTransactions++;
			transactionsCompleted++;
			totalTransactionsSell++;
			transactionsCompletedSell++;
			btcSold += transaction.getBtc();
			eurReceived += transaction.getEur();
			btcRequested += transaction.getBtc();
			eurRequested += transaction.getEur();
			averageSellingPrice += transaction.getEur() / transaction.getBtc();
		} else if (transaction.getStatus() == TransactionStatus.EXPIRED
				.ordinal()) {
			totalTransactions++;
			totalTransactionsSell++;
			btcRequested += transaction.getBtc();
			eurRequested += transaction.getEur();
			averageSellingPrice += transaction.getEur() / transaction.getBtc();
		}
	}

	private void sellDoneStatistics(TransactionView transaction) {

		if (transaction.getStatus() == TransactionStatus.DONE.ordinal()) {
			totalTransactions++;
			transactionsCompleted++;
			totalTransactionsBuy++;
			transactionsCompletedBuy++;
			btcBought += transaction.getBtc();
			eurSent += transaction.getEur();
			btcRequestedBuy += transaction.getBtc();
			eurRequestedBuy += transaction.getEur();
			averageBuyingPrice += transaction.getEur() / transaction.getBtc();
		} else if (transaction.getStatus() == TransactionStatus.EXPIRED
				.ordinal()) {
			totalTransactions++;
			totalTransactionsBuy++;
			btcRequestedBuy += transaction.getBtc();
			eurRequestedBuy += transaction.getEur();
			averageBuyingPrice += transaction.getEur() / transaction.getBtc();
		}
	}

	/**
	 * Calculate Statistics for a given account ROLE_ADMIN
	 * 
	 * @param account
	 */
	private void calculateStatisticsAdmin() {

		for (TransactionView transaction : transactions) {
			if (transaction.getType() == TransactionType.BUY.ordinal()) {
				buyStatistics(transaction);
			} else if (transaction.getType() == TransactionType.SELL.ordinal()) {
				sellStatistics(transaction);
			}
		}
		for (TransactionView transaction : transactionsViewHistory) {
			if (transaction.getType() == TransactionType.BUY.ordinal()) {
				buyDoneStatistics(transaction);
			} else if (transaction.getType() == TransactionType.SELL.ordinal()) {
				sellDoneStatistics(transaction);
			}
		}
		if (averageSellingPrice != 0d) {
			averageSellingPrice /= totalTransactionsSell;
		}
		if (averageBuyingPrice != 0d) {
			averageBuyingPrice /= totalTransactionsBuy;
		}
		btcBalance = COGE.getInstance().getBTCWallet().getBalance();
		btcBalanceAvailable = btcBalance - btcRequestedNow;
		eurBalance = COGE.getInstance().getEURWallet().getBalance();
		eurBalanceAvailable = COGE.getInstance().getEURWallet()
				.getBalanceAvailable();
	}

	/**
	 * Calculate Statistics for a given account ROLE_USER
	 * 
	 * @param account
	 */
	private void calculateStatisticsUser() {

		for (TransactionView transaction : transactions) {
			if (transaction.getType() == TransactionType.BUY.ordinal()) {
				buyStatistics(transaction);
			} else if (transaction.getType() == TransactionType.SELL.ordinal()) {
				sellStatistics(transaction);
			}
		}
		for (TransactionView transaction : transactionsViewHistory) {
			if (transaction.getType() == TransactionType.BUY.ordinal()) {
				buyDoneStatistics(transaction);
			} else if (transaction.getType() == TransactionType.SELL.ordinal()) {
				sellDoneStatistics(transaction);
			}
		}
		if (averageSellingPrice != 0d) {
			averageSellingPrice /= totalTransactionsSell;
		}
		if (averageBuyingPrice != 0d) {
			averageBuyingPrice /= totalTransactionsBuy;
		}
		Account BTCAccount = accountManager.getAccountByUserAndType(user,
				AccountType.BTC.ordinal(), AccountStatus.ACCOUNT_NOT_ACTIVE);
		if (BTCAccount != null) {
			btcBalance = BTCAccount.getBalance();
			btcBalanceAvailable = btcBalance - btcRequestedNow;
		} else {
			btcBalance = new Double(0d);
			btcBalanceAvailable = new Double(0d);
		}
	}

	/**
	 * Retrieve transactions for ROLE_ADMIN
	 * 
	 */
	private void retrieveTransactionAdmin() {

		/*
		 * Get EUR_POSTEPAY account history
		 */
		transactions = new ArrayList<>();
		transactionsViewHistory = new ArrayList<>();
		for (User currentUser : userManager.getAll()) {
			Account account = accountManager.getAccountByUserAndType(
					currentUser, AccountType.EUR_POSTEPAY.ordinal(),
					AccountStatus.ACCOUNT_NOT_ACTIVE);
			if (account != null) {
				accountLastRead = account.getLastReadStatus();
				/*
				 * BUY Orders
				 */
				for (Order order : orderManager.getOrderTypeByAccount(
						TransactionType.BUY, account)) {
					TransactionView tmp = new TransactionView(order.getCode(),
							order.getType(), order.getAddress(),
							order.getEmail(), order.getTransactionHash(),
							order.getBtc(), order.getEuro(), order.getEuro()
									/ order.getBtc(), order.getFee(), new Date(
									order.getCreated() * 1000), new Date(
									order.getModified() * 1000),
							order.getNote(), order.getStatus(), account);
					tmp.setReceipts(receiptManager.getReceiptByOrder(order));
					transactions.add(tmp);
				}
				/*
				 * ALL Transactions
				 */
				for (Transaction transaction : transactionHistoryManager
						.getTransactionByAccount(
								account,
								(Calendar.getInstance().getTimeInMillis() - Constants.MAX_TRANSACTION_HISTORY_TIME_IN_MILLIS) / 1000)) {
					TransactionView tmp = new TransactionView(
							transaction.getCode(), transaction.getType(),
							transaction.getAddress(), transaction.getEmail(),
							transaction.getTransactionHash(),
							transaction.getBtc(), transaction.getEuro(),
							transaction.getEuro() / transaction.getBtc(),
							transaction.getFee(), new Date(
									transaction.getCreated() * 1000), new Date(
									transaction.getModified() * 1000),
							transaction.getNote(), transaction.getStatus(),
							account);
					tmp.setReceipts(receiptManager
							.getReceiptByTransaction(transaction));
					transactionsViewHistory.add(tmp);
				}
			}
			account = accountManager
					.getAccountByUserAndType(currentUser,
							AccountType.EUR.ordinal(),
							AccountStatus.ACCOUNT_NOT_ACTIVE);
			if (account != null) {
				/*
				 * SELL Orders
				 */
				for (Order order : orderManager.getOrderTypeByAccount(
						TransactionType.SELL, account)) {
					TransactionView tmp = new TransactionView(order.getCode(),
							order.getType(), order.getAddress(),
							order.getEmail(), order.getTransactionHash(),
							order.getBtc(), order.getEuro(), order.getEuro()
									/ order.getBtc(), order.getFee(), new Date(
									order.getCreated() * 1000), new Date(
									order.getModified() * 1000),
							order.getNote(), order.getStatus(), account);
					tmp.setReceipts(receiptManager.getReceiptByOrder(order));
					transactions.add(tmp);
				}
				/*
				 * ALL Transactions
				 */
				for (Transaction transaction : transactionHistoryManager
						.getTransactionByAccount(
								account,
								(Calendar.getInstance().getTimeInMillis() - Constants.MAX_TRANSACTION_HISTORY_TIME_IN_MILLIS) / 1000)) {
					TransactionView tmp = new TransactionView(
							transaction.getCode(), transaction.getType(),
							transaction.getAddress(), transaction.getEmail(),
							transaction.getTransactionHash(),
							transaction.getBtc(), transaction.getEuro(),
							transaction.getEuro() / transaction.getBtc(),
							transaction.getFee(), new Date(
									transaction.getCreated() * 1000), new Date(
									transaction.getModified() * 1000),
							transaction.getNote(), transaction.getStatus(),
							account);
					tmp.setReceipts(receiptManager
							.getReceiptByTransaction(transaction));
					transactionsViewHistory.add(tmp);
				}
			}
		}
	}

	/**
	 * Retrieve transactions for ROLE_USER
	 */
	private void retrieveTransactionUser() {

		/*
		 * Get EUR_POSTEPAY account history
		 */
		transactions = new ArrayList<>();
		transactionsViewHistory = new ArrayList<>();
		Account account = accountManager.getAccountByUserAndType(user,
				AccountType.EUR_POSTEPAY.ordinal(),
				AccountStatus.ACCOUNT_NOT_ACTIVE);
		if (account != null) {
			eurBalance = account.getBalance();
			accountLastRead = account.getLastReadStatus();
			/*
			 * BUY Orders
			 */
			for (Order order : orderManager.getOrderTypeByAccount(
					TransactionType.BUY, account)) {
				TransactionView tmp = new TransactionView(order.getCode(),
						order.getType(), order.getAddress(), order.getEmail(),
						order.getTransactionHash(), order.getBtc(),
						order.getEuro(), order.getEuro() / order.getBtc(),
						order.getFee(), new Date(order.getCreated() * 1000),
						new Date(order.getModified() * 1000), order.getNote(),
						order.getStatus(), account);
				tmp.setReceipts(receiptManager.getReceiptByOrder(order));
				transactions.add(tmp);
			}
			/*
			 * ALL Transactions
			 */
			for (Transaction transaction : transactionHistoryManager
					.getTransactionByAccount(
							account,
							(Calendar.getInstance().getTimeInMillis() - Constants.MAX_TRANSACTION_HISTORY_TIME_IN_MILLIS) / 1000)) {
				TransactionView tmp = new TransactionView(
						transaction.getCode(), transaction.getType(),
						transaction.getAddress(), transaction.getEmail(),
						transaction.getTransactionHash(), transaction.getBtc(),
						transaction.getEuro(), transaction.getEuro()
								/ transaction.getBtc(), transaction.getFee(),
						new Date(transaction.getCreated() * 1000), new Date(
								transaction.getModified() * 1000),
						transaction.getNote(), transaction.getStatus(), account);
				tmp.setReceipts(receiptManager
						.getReceiptByTransaction(transaction));
				transactionsViewHistory.add(tmp);
			}
		}
		account = accountManager.getAccountByUserAndType(user,
				AccountType.EUR.ordinal(), AccountStatus.ACCOUNT_NOT_ACTIVE);
		if (account != null) {
			/*
			 * SELL Orders
			 */
			for (Order order : orderManager.getOrderTypeByAccount(
					TransactionType.SELL, account)) {
				TransactionView tmp = new TransactionView(order.getCode(),
						order.getType(), order.getAddress(), order.getEmail(),
						order.getTransactionHash(), order.getBtc(),
						order.getEuro(), order.getEuro() / order.getBtc(),
						order.getFee(), new Date(order.getCreated() * 1000),
						new Date(order.getModified() * 1000), order.getNote(),
						order.getStatus(), account);
				tmp.setReceipts(receiptManager.getReceiptByOrder(order));
				transactions.add(tmp);
			}
			/*
			 * ALL Transactions
			 */
			for (Transaction transaction : transactionHistoryManager
					.getTransactionByAccount(
							account,
							(Calendar.getInstance().getTimeInMillis() - Constants.MAX_TRANSACTION_HISTORY_TIME_IN_MILLIS) / 1000)) {
				TransactionView tmp = new TransactionView(
						transaction.getCode(), transaction.getType(),
						transaction.getAddress(), transaction.getEmail(),
						transaction.getTransactionHash(), transaction.getBtc(),
						transaction.getEuro(), transaction.getEuro()
								/ transaction.getBtc(), transaction.getFee(),
						new Date(transaction.getCreated() * 1000), new Date(
								transaction.getModified() * 1000),
						transaction.getNote(), transaction.getStatus(), account);
				tmp.setReceipts(receiptManager
						.getReceiptByTransaction(transaction));
				transactionsViewHistory.add(tmp);
			}
		}
	}

	/**
	 * Retrieve statistics for the user Hypothesis: user cannot have more than
	 * one account (one PostePay)
	 * 
	 */
	private void retrieveStatistics(boolean admin) {

		totalTransactions = new Integer(0);
		transactionsCompleted = new Integer(0);
		totalTransactionsSell = new Integer(0);
		transactionsCompletedSell = new Integer(0);
		btcSold = new Double(0d);
		btcRequested = new Double(0d);
		btcRequestedNow = new Double(0d);
		eurReceived = new Double(0d);
		eurRequested = new Double(0d);
		averageSellingPrice = new Double(0d);
		totalTransactionsBuy = new Integer(0);
		transactionsCompletedBuy = new Integer(0);
		btcBought = new Double(0d);
		btcRequestedBuy = new Double(0d);
		eurRequestedBuy = new Double(0d);
		eurSent = new Double(0d);
		averageBuyingPrice = new Double(0d);
		btcBalance = new Double(0d);
		btcBalanceAvailable = new Double(0d);
		eurBalance = new Double(0d);
		accountLastRead = AccountStatus.ACCOUNT_NOT_ACTIVE.ordinal();
		if (user != null) {
			if (admin) {
				retrieveTransactionAdmin();
				calculateStatisticsAdmin();
			} else {
				retrieveTransactionUser();
				calculateStatisticsUser();
			}
		}
	}

	/**
	 * Retrieve User
	 * 
	 */
	private void retrieveUser() {

		if (getRequest().getRemoteUser() != null) {
			user = userManager.getUserByUsername(getRequest().getRemoteUser());
		} else {
			user = null;
		}
	}

	/**
	 * Refresh UI
	 * 
	 */
	public void refreshUI() {
		retrieveUser();
		retrieveStatistics(isAdmin());
	}

	/**
	 * Add TransactionHash for a Buy_PAYMENT_RECEIVED order
	 * 
	 */
	public void addTransactionHash() {

		Order order = orderManager
				.getOrderByCode(selectedTransaction.getCode());
		if (order != null) {
			order.setTransactionHash(transactionHash);
			orderManager.update(order);
			retrieveStatistics(isAdmin());
			addFacesMessage("success.updateOrder");
		}
	}

	/**
	 * Mark a Buy/Sell Order as BTC_SENT/EUR_SENT
	 * 
	 */
	public void forceConfirm() {

		if (selectedTransaction.getReceipts() == null) {
			addFacesError("errors.uploadReceipts.mandatory");
		} else {
			if (selectedTransaction.getReceipts().size() == 0) {
				addFacesError("errors.uploadReceipts.mandatory");
			} else {
				Order order = orderManager.getOrderByCode(selectedTransaction
						.getCode());
				if (order.getType() == TransactionType.BUY.ordinal()) {
					order.setStatus(TransactionStatus.PAYMENT_RECEIVED
							.ordinal());
				} else if (order.getType() == TransactionType.SELL.ordinal()) {
					order.setStatus(TransactionStatus.EUR_SENT.ordinal());
					/*
					 * Update account balance
					 */
					Account account = order.getAccount();
					account.setBalance(account.getBalance() - order.getEuro());
					accountManager.update(account);
				}
				orderManager.update(order);
				retrieveStatistics(isAdmin());
				addFacesMessage("success.updateOrder");
			}
		}
	}

	/**
	 * Mark a Buy/Sell Order as WAITING_PAYMENT
	 * 
	 */
	public void notConfirmed() {

		Order order = orderManager
				.getOrderByCode(selectedTransaction.getCode());
		Transaction transaction = new Transaction(
				order.getCode()
						+ " "
						+ RandomStringUtils.randomAlphanumeric(9)
								.toUpperCase(), order.getType(),
				order.getAddress(), order.getEmail(), "",
				order.getBtc(), order.getEuro(), order.getFee(),
				order.getCreated(), Calendar.getInstance()
						.getTimeInMillis() / 1000, order.getNote(),
				TransactionStatus.EXPIRED.ordinal(), order.getAccount());
		transactionHistoryManager.create(transaction);
		orderManager.delete(order);
		retrieveStatistics(isAdmin());
		addFacesMessage("success.updateOrder");
	}

	/**
	 * Delete Transaction
	 * 
	 */
	public void deleteTransaction() {

		for (Receipt receipt : selectedTransaction.getReceipts()) {
			receiptManager.delete(receipt);
			fileManager.delete(receipt.getFile());
		}
		transactionHistoryManager.delete(transactionHistoryManager
				.getTransactionByCode(selectedTransaction.getCode()));
		retrieveStatistics(isAdmin());
		addFacesMessage("success.deleteTransaction",
				selectedTransaction.getCode());
	}

	/**
	 * Delete Receipt
	 * 
	 */
	public void deleteReceipt() {

		if (selectedReceipt.getOrder() != null) {
			if ((selectedReceipt.getOrder().getStatus() == TransactionStatus.BTC_SENT
					.ordinal())
					|| (selectedReceipt.getOrder().getStatus() == TransactionStatus.EUR_SENT
							.ordinal())) {
				addFacesError("errors.deleteReceipt", selectedReceipt.getName());
				return;
			}
		}
		receiptManager.delete(selectedReceipt);
		fileManager.delete(selectedReceipt.getFile());
		retrieveStatistics(isAdmin());
		addFacesMessage("success.deleteReceipt", selectedReceipt.getName());
	}

	/**
	 * Download Receipt
	 * 
	 */

	public void downloadReceipt() {

		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		File receiptData = fileManager.get(selectedReceipt.getFile());
		ec.responseReset();
		ec.setResponseContentType(ec.getMimeType(selectedReceipt.getName()));
		try {
			ec.setResponseContentLength((new Long(receiptData.getData()
					.length())).intValue());
			ec.setResponseHeader("Content-Disposition",
					"attachment; filename=\"" + selectedReceipt.getName()
							+ "\"");
			Streams.copy(receiptData.getData().getBinaryStream(),
					ec.getResponseOutputStream(), true);
		} catch (SQLException e) {
			log.error("Downloading: " + e.getMessage());
			addFacesError("errors.downloadingReceipt",
					selectedReceipt.getName());
		} catch (IOException e) {
			log.error("Downloading: " + e.getMessage());
			addFacesError("errors.downloadingReceipt",
					selectedReceipt.getName());
		}
		fc.responseComplete();
	}

	/**
	 * Upload Receipt
	 * 
	 * @param event
	 */

	public void uploadReceipt(FileUploadEvent event) {

		if (selectedTransaction.getReceipts() == null) {
			selectedTransaction.setReceipts(new ArrayList<>());
		} else if (selectedTransaction.getReceipts().size() >= Constants.MAX_RECEIPTS_TRANSACTION) {
			addFacesError("errors.uploadReceipts.maxNumber");
		} else {
			UploadedFile receiptUploaded = event.getFile();
			/*
			 * Is an order or a transaction ?!?
			 */
			Order order = orderManager.getOrderByCode(selectedTransaction
					.getCode());
			Transaction transaction = transactionHistoryManager
					.getTransactionByCode(selectedTransaction.getCode());
			Long file = fileManager.create(receiptUploaded.getContents());
			Receipt receipt = new Receipt(null, receiptUploaded.getFileName(),
					file, order, transaction);
			receiptManager.create(receipt);
			selectedTransaction.getReceipts().add(receipt);
			addFacesMessage("success.uploadReceipts", receipt.getName());
		}
	}

	public boolean isAdmin() {

		if (user == null) {
			retrieveUser();
		}
		for (Role role : user.getRoles()) {
			if (role.getName().equalsIgnoreCase(Constants.ADMIN_ROLE)) {
				return true;
			}
		}
		return false;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public TransactionView getSelectedTransaction() {
		return selectedTransaction;
	}

	public void setSelectedTransaction(TransactionView selectedTransaction) {
		this.selectedTransaction = selectedTransaction;
	}

	public Receipt getSelectedReceipt() {
		return selectedReceipt;
	}

	public void setSelectedReceipt(Receipt selectedReceipt) {
		this.selectedReceipt = selectedReceipt;
	}

	public Integer getPollingInterval() {
		return pollingInterval;
	}

	public Integer getTotalTransactionsSell() {
		return totalTransactionsSell;
	}

	public void setTotalTransactionsSell(Integer totalTransactionsSell) {
		this.totalTransactionsSell = totalTransactionsSell;
	}

	public Integer getTransactionsCompletedSell() {
		return transactionsCompletedSell;
	}

	public void setTransactionsCompletedSell(Integer transactionsCompletedSell) {
		this.transactionsCompletedSell = transactionsCompletedSell;
	}

	public Double getBtcSold() {
		return btcSold;
	}

	public void setBtcSold(Double btcSold) {
		this.btcSold = btcSold;
	}

	public Double getBtcRequested() {
		return btcRequested;
	}

	public void setBtcRequested(Double btcRequested) {
		this.btcRequested = btcRequested;
	}

	public Double getBtcRequestedNow() {
		return btcRequestedNow;
	}

	public void setBtcRequestedNow(Double btcRequestedNow) {
		this.btcRequestedNow = btcRequestedNow;
	}

	public Double getEurReceived() {
		return eurReceived;
	}

	public void setEurReceived(Double eurReceived) {
		this.eurReceived = eurReceived;
	}

	public Double getEurRequested() {
		return eurRequested;
	}

	public void setEurRequested(Double eurRequested) {
		this.eurRequested = eurRequested;
	}

	public Double getAverageSellingPrice() {
		return averageSellingPrice;
	}

	public void setAverageSellingPrice(Double averageSellingPrice) {
		this.averageSellingPrice = averageSellingPrice;
	}

	public Double getBtcBalance() {
		return btcBalance;
	}

	public void setBtcBalance(Double btcBalance) {
		this.btcBalance = btcBalance;
	}

	public Double getBtcBalanceAvailable() {
		return btcBalanceAvailable;
	}

	public void setBtcBalanceAvailable(Double btcBalanceAvailable) {
		this.btcBalanceAvailable = btcBalanceAvailable;
	}

	public Double getEurBalance() {
		return eurBalance;
	}

	public void setEurBalance(Double eurBalance) {
		this.eurBalance = eurBalance;
	}

	public Double getEurBalanceAvailable() {
		return eurBalanceAvailable;
	}

	public void setEurBalanceAvailable(Double eurBalanceAvailable) {
		this.eurBalanceAvailable = eurBalanceAvailable;
	}

	public TransactionManager getTransactionHistoryManager() {
		return transactionHistoryManager;
	}

	public void setPollingInterval(Integer pollingInterval) {
		this.pollingInterval = pollingInterval;
	}

	public List<TransactionView> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<TransactionView> transactions) {
		this.transactions = transactions;
	}

	public List<TransactionView> getTransactionsViewHistory() {
		return transactionsViewHistory;
	}

	public void setTransactionsViewHistory(
			List<TransactionView> transactionsViewHistory) {
		this.transactionsViewHistory = transactionsViewHistory;
	}

	public String getTransactionLinkBase() {
		return transactionLinkBase;
	}

	public void setTransactionLinkBase(String transactionLinkBase) {
		this.transactionLinkBase = transactionLinkBase;
	}

	public Integer getAccountLastRead() {
		return accountLastRead;
	}

	public void setAccountLastRead(Integer accountLastRead) {
		this.accountLastRead = accountLastRead;
	}

	public String getTransactionHash() {
		return transactionHash;
	}

	public void setTransactionHash(String transactionHash) {
		this.transactionHash = transactionHash;
	}

	public Double getBtcBought() {
		return btcBought;
	}

	public void setBtcBought(Double btcBought) {
		this.btcBought = btcBought;
	}

	public Double getBtcRequestedBuy() {
		return btcRequestedBuy;
	}

	public void setBtcRequestedBuy(Double btcRequestedBuy) {
		this.btcRequestedBuy = btcRequestedBuy;
	}

	public Double getEurRequestedBuy() {
		return eurRequestedBuy;
	}

	public void setEurRequestedBuy(Double eurRequestedBuy) {
		this.eurRequestedBuy = eurRequestedBuy;
	}

	public Double getEurSent() {
		return eurSent;
	}

	public void setEurSent(Double eurSent) {
		this.eurSent = eurSent;
	}

	public Integer getTotalTransactionsBuy() {
		return totalTransactionsBuy;
	}

	public void setTotalTransactionsBuy(Integer totalTransactionsBuy) {
		this.totalTransactionsBuy = totalTransactionsBuy;
	}

	public Integer getTransactionsCompletedBuy() {
		return transactionsCompletedBuy;
	}

	public void setTransactionsCompletedBuy(Integer transactionsCompletedBuy) {
		this.transactionsCompletedBuy = transactionsCompletedBuy;
	}

	public Double getAverageBuyingPrice() {
		return averageBuyingPrice;
	}

	public void setAverageBuyingPrice(Double averageBuyingPrice) {
		this.averageBuyingPrice = averageBuyingPrice;
	}

	public Integer getTotalTransactions() {
		return totalTransactions;
	}

	public void setTotalTransactions(Integer totalTransactions) {
		this.totalTransactions = totalTransactions;
	}

	public Integer getTransactionsCompleted() {
		return transactionsCompleted;
	}

	public void setTransactionsCompleted(Integer transactionsCompleted) {
		this.transactionsCompleted = transactionsCompleted;
	}

}
