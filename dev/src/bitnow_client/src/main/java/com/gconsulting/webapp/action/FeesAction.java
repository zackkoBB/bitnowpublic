package com.gconsulting.webapp.action;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gsconsulting.bitnow.model.util.Constants;
import com.gsconsulting.bitnow.model.Exchange;
import com.gsconsulting.bitnow.model.Fee;
import com.gsconsulting.bitnow.model.FeeApiType;
import com.gsconsulting.bitnow.model.Market;
import com.gsconsulting.bitnow.model.ids.FeeApiId;
import com.gsconsulting.bitnow.model.ids.FeeApiTypeId;
import com.gsconsulting.bitnow.model.service.ExchangeManager;
import com.gconsulting.webapp.util.ImportUtils;

@SuppressWarnings("unchecked")
@Scope("request")
@Component("feesAction")
public class FeesAction extends BasePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1402337355803749427L;
	private ExchangeManager exchangeManager;
	private Fee selectedFee = new Fee();
	private UploadedFile importFile;
	private String exchangeCode;
	private String type;

	@Autowired
	public void setExchangeManager(
			@Qualifier("exchangeManager") ExchangeManager exchangeManager) {
		this.exchangeManager = exchangeManager;
	}

	public FeesAction() {
		setSortColumn("exchange.code"); // sets the default sort column
	}

	public List<Fee> getFees() {
		return sort(exchangeManager.getAllFee());
	}

	public List<String> getExchanges() {

		List<String> exchangesString = new ArrayList<String>();
		List<Exchange> exchanges = exchangeManager.getAllExchange();
		for (Exchange exchange : exchanges) {
			exchangesString.add(exchange.getCode());
		}
		return exchangesString;
	}

	public List<String> getFeeTypes() {

		List<String> typesString = new ArrayList<String>();
		List<FeeApiType> types = exchangeManager.getAllType();
		for (FeeApiType type : types) {
			typesString
					.add(type.getMarket().getCode() + " - " + type.getType());
		}
		return typesString;
	}

	public List<String> getFeeUnits() {
		return Constants.getFeeUnits();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Fee getSelectedFee() {
		return selectedFee;
	}

	public void setSelectedFee(Fee selectedFee) {
		this.selectedFee = selectedFee;
	}

	public String getExchangeCode() {
		return exchangeCode;
	}

	public void setExchangeCode(String exchangeCode) {
		this.exchangeCode = exchangeCode;
	}

	public UploadedFile getImportFile() {
		return importFile;
	}

	public void setImportFile(UploadedFile importFile) {
		this.importFile = importFile;
	}

	public String importFees() {

		HttpServletRequest request = getRequest();
		// write the file to the filesystem
		// the directory to upload to
		String uploadDir = getFacesContext().getExternalContext().getRealPath(
				"/resources");
		// The following seems to happen when running jetty:run
		if (uploadDir == null) {
			uploadDir = new File("src/main/webapp/resources").getAbsolutePath();
		}
		uploadDir += "/" + request.getRemoteUser() + "/";
		try {
			String filename = importFile.getFileName();
			if (filename.endsWith(Constants.CSV_SUFFIX)) {
				// Create the CSVFormat object
				CSVFormat format = CSVFormat.RFC4180.withHeader()
						.withDelimiter(';');
				CSVParser parser = new CSVParser(new FileReader(
						ImportUtils.saveFile(importFile, uploadDir)), format);
				int i = 0, total = 0;
				String skippedSummary = new String();
				for (CSVRecord csvType : parser.getRecords()) {
					String marketCodeTemp = csvType
							.get(Constants.FEES_IMPORT_HEADER2);
					Market market = new Market();
					if (exchangeManager.getMarketByCode(marketCodeTemp) == null) {
						market = new Market(marketCodeTemp, marketCodeTemp
								+ " market");
						exchangeManager.create(market);
					} else {
						market = exchangeManager
								.getMarketByCode(marketCodeTemp);
					}
					String typeTemp = csvType
							.get(Constants.FEES_IMPORT_HEADER3);
					FeeApiType type = new FeeApiType();
					if (exchangeManager.getTypeById(new FeeApiTypeId(typeTemp,
							market)) == null) {
						type = new FeeApiType(typeTemp, market);
						exchangeManager.create(type);
					} else {
						type = exchangeManager.getTypeById(new FeeApiTypeId(
								typeTemp, market));
					}
					Fee fee = new Fee();
					fee.setExchange(exchangeManager.getExchangeByCode(csvType
							.get(Constants.FEES_IMPORT_HEADER1)));
					fee.setFeeType(type);
					fee.setUnit(csvType.get(Constants.FEES_IMPORT_HEADER4));
					fee.setValue(csvType.get(Constants.FEES_IMPORT_HEADER5));
					fee.setNote(csvType.get(Constants.FEES_IMPORT_HEADER6));
					if (exchangeManager.getFeeById(new FeeApiId(fee
							.getExchange(), fee.getFeeType())) == null) {
						exchangeManager.create(fee);
						i++;
					} else {
						if ((total - i) < Constants.SKIPPED_SUMMARY_LENGTH) {
							skippedSummary = skippedSummary.concat(fee
									.getExchange().getCode()
									+ " "
									+ fee.getFeeType().getMarket().getCode()
									+ " "
									+ fee.getFeeType().getType()
									+ " "
									+ fee.getUnit()
									+ " "
									+ fee.getValue()
									+ "\\n");
						}
					}
					total++;
				}
				// close the parser
				parser.close();
				addFacesMessage("fees.import.imported");
				addFacesMessage("fees.import.importedSummary", new Object[] { i,
						total - i, skippedSummary });
			} else {
				addFacesError("fees.import.error");
			}
		} catch (IOException e) {
			addFacesError("fees.import.error");
		}
		return "list";
	}

	public String delete() {

		exchangeManager.delete(selectedFee);
		addFacesMessage("fees.deleted");
		return "list";
	}

	public String edit() {

		String typeType = null;
		String typeMarketCode = null;
		if (exchangeCode == null) {
			exchangeCode = new String(getParameter("exchangeCode"));
		}
		if (typeMarketCode == null) {
			typeMarketCode = new String(getParameter("marketCode"));
		}
		if (typeType == null) {
			typeType = new String(getParameter("type"));
		}
		type = typeMarketCode + " - " + typeType;
		Exchange selectedExchange = exchangeManager
				.getExchangeByCode(exchangeCode);
		Market selectedMarket = exchangeManager.getMarketByCode(typeMarketCode);
		FeeApiType feeType = new FeeApiType(typeType, selectedMarket);
		selectedFee = exchangeManager.getFeeById(new FeeApiId(selectedExchange,
				feeType));
		return "edit";
	}

	public String save() {

		String typeType = null;
		String typeMarketCode = null;
		String key = new String("");
		Exchange selectedExchange = exchangeManager
				.getExchangeByCode(exchangeCode);
		if (type != null) {
			if (type.length() > 0) {
				typeMarketCode = type.substring(0, type.indexOf(" - "));
				typeType = type.substring(type.indexOf(" - ") + 3);
			}
		}
		Market selectedMarket = new Market();
		if (exchangeManager.getMarketByCode(typeMarketCode) == null) {
			selectedMarket = new Market(typeMarketCode, typeMarketCode
					+ " market");
			exchangeManager.create(selectedMarket);
		} else {
			selectedMarket = exchangeManager.getMarketByCode(typeMarketCode);
		}
		FeeApiType feeType = new FeeApiType();
		if (exchangeManager.getTypeById(new FeeApiTypeId(typeType,
				selectedMarket)) == null) {
			feeType = new FeeApiType(typeType, selectedMarket);
			exchangeManager.create(feeType);
		} else {
			feeType = exchangeManager.getTypeById(new FeeApiTypeId(typeType,
					selectedMarket));
		}
		selectedFee.setExchange(selectedExchange);
		selectedFee.setFeeType(feeType);
		boolean isUpdate = (exchangeManager.getFeeById(new FeeApiId(selectedFee
				.getExchange(), selectedFee.getFeeType())) == null) ? false
				: true;
		if (!isUpdate) {
			exchangeManager.create(selectedFee);
			key = "fees.added";
		} else {
			exchangeManager.update(selectedFee);
			key = "fees.updated";
		}
		addFacesMessage(key);
		return "list";
	}
}
