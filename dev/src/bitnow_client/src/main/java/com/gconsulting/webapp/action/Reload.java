package com.gconsulting.webapp.action;

import com.gconsulting.webapp.listener.StartupListener;

import java.io.IOException;
import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * JSF Page class to handle reloading options in servlet context.
 *
 * @author Matt Raible
 */
@Scope("request")
@Component("reload")
public class Reload extends BasePage implements Serializable {
	
    private static final long serialVersionUID = 2466200890766730676L;

    public String execute() throws IOException {

        if (log.isDebugEnabled()) {
            log.debug("Entering 'execute' method");
        }
        StartupListener.setupContext(getServletContext());
        addFacesMessage("reload.succeeded"); 
        return "success";
    }

}
