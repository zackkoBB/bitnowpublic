package com.gconsulting.webapp.action;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.gsconsulting.bitnow.model.Role;
import com.gsconsulting.bitnow.model.User;
import com.gsconsulting.bitnow.model.service.UserManager;
import com.gsconsulting.bitnow.model.util.Constants;

@Scope("session")
// @Component("menuController")
@Controller("menuController")
public class MenuController extends BasePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7204659847632369338L;
	private User user = new User();

	public MenuController() {

		super();
	}

	private void loadUser() {

		if (getRequest().getRemoteUser() != null) {
			user = userManager.getUserByUsername(getRequest().getRemoteUser());
		} else {
			user = null;
		}
		if (user != null) {
			if (user.getUsername() != null) {
				user.setConfirmPassword(user.getPassword());
			}
		}
	}

	@PostConstruct
	public void init() {
		loadUser();
	}

	@Override
	@Autowired
	public void setUserManager(@Qualifier("userManager") UserManager userManager) {
		this.userManager = userManager;
	}

	public void registerUserNewsletter() {
		/*
		 * TODO: Register insterted E-Mail and display message (Optional): send
		 * confirmation email.
		 */
		addFacesMessage("website.home.newsletterOK");
	}

	public boolean getUserLoggedIn() {

		if (user != null) {
			return true;
		} else {
			return false;
		}
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getTheme() {

		loadUser();
		return user.getTheme();
	}

	/**
	 * Logout
	 * 
	 * @return
	 */
	public void logout() throws IOException {

		ExternalContext ec = FacesContext.getCurrentInstance()
				.getExternalContext();
		ec.invalidateSession();
		ec.redirect(ec.getRequestContextPath() + "/home");
	}

	public boolean isAdmin() {

		if (user == null) {
			loadUser();
		}
		for (Role role : user.getRoles()) {
			if (role.getName().equalsIgnoreCase(Constants.ADMIN_ROLE)) {
				return true;
			}
		}
		return false;
	}
}
