package com.gconsulting.webapp.action;

import java.io.IOException;
import java.io.Serializable;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.commons.fileupload.util.Streams;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gconsulting.webapp.model.UserView;
import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.Document;
import com.gsconsulting.bitnow.model.File;
import com.gsconsulting.bitnow.model.User;
import com.gsconsulting.bitnow.model.dao.SearchException;
import com.gsconsulting.bitnow.model.enums.AccountStatus;
import com.gsconsulting.bitnow.model.enums.AccountType;
import com.gsconsulting.bitnow.model.service.AccountManager;
import com.gsconsulting.bitnow.model.service.ConfigurationManager;
import com.gsconsulting.bitnow.model.service.DocumentManager;
import com.gsconsulting.bitnow.model.service.FileManager;
import com.gsconsulting.bitnow.model.util.Constants;
import com.gsconsulting.bitnow.model.util.CryptService;

@Scope("session")
@Component("userListAction")
@SuppressWarnings("unchecked")
public class UserList extends BasePage implements Serializable {

	private static final long serialVersionUID = 972359310602744018L;

	private ConfigurationManager configurationManager;
	private AccountManager accountManager;
	private DocumentManager documentManager;
	private FileManager fileManager;

	private List<UserView> users;
	private UserView selectedUser;
	private Document selectedDocument;
	private String query;
	private Integer pollingInterval;

	@Autowired
	public void setConfigurationManager(
			@Qualifier("configurationManager") ConfigurationManager configurationManager) {
		this.configurationManager = configurationManager;
	}

	@Autowired
	public void setAccountManager(
			@Qualifier("accountManager") AccountManager accountManager) {
		this.accountManager = accountManager;
	}

	@Autowired
	public void setDocumentManager(
			@Qualifier("documentManager") DocumentManager documentManager) {
		this.documentManager = documentManager;
	}

	@Autowired
	public void setFileManager(@Qualifier("fileManager") FileManager fileManager) {
		this.fileManager = fileManager;
	}

	public UserList() {
		setSortColumn("username");
	}

	@PostConstruct
	public void init() {

		pollingInterval = new Integer(configurationManager
				.getConfigurationByKey(Constants.UI_POLLING_INTERVAL)
				.getValue());
		loadUsers();
	}

	/**
	 * Save user
	 */
	public void saveUser() {

		if (selectedUser != null) {
			selectedUser.getUser().setCreated(
					selectedUser.getUser().getCreated() / 1000);
			userManager.update(selectedUser.getUser());
			addFacesMessage("user.saved");
		} else {
			addFacesError("errors.general");
		}
		loadUsers();
	}

	/**
	 * Normalize (decrypt + date) account
	 * 
	 * @param account
	 */
	private void decryptAccount(Account account) {

		account.setCreated(account.getCreated() * 1000);
		try {
			account.setId(CryptService.decrypt(account.getId()));
			if (account.getNumber() != null) {
				if (account.getNumber().length() > 0) {
					account.setNumber(CryptService.decrypt(account.getNumber()));
				}
			}
			if (account.getOwner() != null) {
				if (account.getOwner().length() > 0) {
					account.setOwner(CryptService.decrypt(account.getOwner()));
				}
			}
			if (account.getWebUsername() != null) {
				if (account.getWebUsername().length() > 0) {
					account.setWebUsername(CryptService.decrypt(account
							.getWebUsername()));
				}
			}
			if (account.getWebPassword() != null) {
				if (account.getWebPassword().length() > 0) {
					account.setWebPassword(CryptService.decrypt(account
							.getWebPassword()));
				}
			}
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Normalize (encrypt + date) account
	 * 
	 * @param account
	 */
	private void encryptAccount(Account account) {

		account.setCreated(account.getCreated() / 1000);
		try {
			account.setId(CryptService.encrypt(account.getId()));
			if (account.getNumber() != null) {
				if (account.getNumber().length() > 0) {
					account.setNumber(CryptService.encrypt(account.getNumber()));
				}
			}
			if (account.getOwner() != null) {
				if (account.getOwner().length() > 0) {
					account.setOwner(CryptService.encrypt(account.getOwner()));
				}
			}
			if (account.getWebUsername() != null) {
				if (account.getWebUsername().length() > 0) {
					account.setWebUsername(CryptService.encrypt(account
							.getWebUsername()));
				}
			}
			if (account.getWebPassword() != null) {
				if (account.getWebPassword().length() > 0) {
					account.setWebPassword(CryptService.encrypt(account
							.getWebPassword()));
				}
			}
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Set accounts inactive
	 */
	public void disableAccounts() {

		if (selectedUser != null) {
			for (Account account : selectedUser.getAccounts()) {
				if (account.getType() == AccountType.EUR_POSTEPAY.ordinal()) {
				} else if (account.getType() == AccountType.BTC.ordinal()) {
					encryptAccount(account);
					account.setLastReadStatus(AccountStatus.ACCOUNT_NOT_ACTIVE
							.ordinal());
					accountManager.update(account);
				} else if (account.getType() == AccountType.EUR.ordinal()) {
					encryptAccount(account);
					account.setLastReadStatus(AccountStatus.ACCOUNT_NOT_ACTIVE
							.ordinal());
					accountManager.update(account);
				}
			}
			/*
			 * TODO: send email to user
			 */
			addFacesMessage("user.saved");
		} else {
			addFacesError("errors.general");
		}
		loadUsers();
	}

	/**
	 * Set accounts enabled
	 */
	public void enableAccounts() {

		if (selectedUser != null) {
			for (Account account : selectedUser.getAccounts()) {
				if (account.getType() == AccountType.EUR_POSTEPAY.ordinal()) {
					encryptAccount(account);
					account.setLastReadStatus(AccountStatus.ACCOUNT_LAST_READ_ERROR
							.ordinal());
					accountManager.update(account);
				} else if (account.getType() == AccountType.BTC.ordinal()) {
					encryptAccount(account);
					account.setLastReadStatus(AccountStatus.ACCOUNT_LAST_READ_ERROR
							.ordinal());
					accountManager.update(account);
				} else if (account.getType() == AccountType.EUR.ordinal()) {
					encryptAccount(account);
					account.setLastReadStatus(AccountStatus.ACCOUNT_LAST_READ_ERROR
							.ordinal());
					accountManager.update(account);
				}
			}
			/*
			 * TODO: send email to user
			 */
			addFacesMessage("user.saved");
		} else {
			addFacesError("errors.general");
		}
		loadUsers();
	}

	/**
	 * Set accounts seller
	 */
	public void enableAccountsSeller() {

		if (selectedUser != null) {
			for (Account account : selectedUser.getAccounts()) {
				if (account.getType() == AccountType.EUR_POSTEPAY.ordinal()) {
					encryptAccount(account);
					account.setLastReadStatus(AccountStatus.ACCOUNT_LAST_READ_ERROR
							.ordinal());
					accountManager.update(account);
				} else if (account.getType() == AccountType.BTC.ordinal()) {
					encryptAccount(account);
					account.setLastReadStatus(AccountStatus.ACCOUNT_LAST_READ_ERROR
							.ordinal());
					accountManager.update(account);
				} else if (account.getType() == AccountType.EUR.ordinal()) {
					encryptAccount(account);
					account.setLastReadStatus(AccountStatus.ACCOUNT_SELLER
							.ordinal());
					accountManager.update(account);
				}
			}
			/*
			 * TODO: send email to user
			 */
			addFacesMessage("user.saved");
		} else {
			addFacesError("errors.general");
		}
		loadUsers();
	}

	/**
	 * Delete Document
	 * 
	 */
	public void deleteDocument() {

		documentManager.delete(selectedDocument);
		fileManager.delete(selectedDocument.getFile());
		loadUsers();
		addFacesMessage("success.deleteDocument", selectedDocument.getName());
	}

	/**
	 * Download Document
	 * 
	 */

	public void downloadDocument() {

		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		File documentData = fileManager.get(selectedDocument.getFile());
		ec.responseReset();
		ec.setResponseContentType(ec.getMimeType(selectedDocument.getName()));
		try {
			ec.setResponseContentLength((new Long(documentData.getData()
					.length())).intValue());
			ec.setResponseHeader("Content-Disposition",
					"attachment; filename=\"" + selectedDocument.getName()
							+ "\"");
			Streams.copy(documentData.getData().getBinaryStream(),
					ec.getResponseOutputStream(), true);
		} catch (SQLException e) {
			log.error("Downloading: " + e.getMessage());
			addFacesError("errors.downloadingDocument",
					selectedDocument.getName());
		} catch (IOException e) {
			log.error("Downloading: " + e.getMessage());
			addFacesError("errors.downloadingDocument",
					selectedDocument.getName());
		}
		fc.responseComplete();
	}

	/**
	 * Upload Document
	 * 
	 * @param event
	 */

	public void uploadDocument(FileUploadEvent event) {

		if (selectedUser.getDocuments() == null) {
			selectedUser.setDocuments(new ArrayList<>());
		} else if (selectedUser.getDocuments().size() >= Constants.MAX_RECEIPTS_TRANSACTION) {
			addFacesError("errors.uploadDocuments.maxNumber");
		} else {
			UploadedFile documentUploaded = event.getFile();
			Long file = fileManager.create(documentUploaded.getContents());
			Document document = new Document(null,
					documentUploaded.getFileName(), file,
					selectedUser.getUser());
			documentManager.create(document);
			selectedUser.getDocuments().add(document);
			addFacesMessage("success.uploadDocuments", document.getName());
		}
	}

	/**
	 * Refresh UI
	 */
	public void refreshUI() {
		loadUsers();
	}

	/**
	 * Load Users
	 */
	private void loadUsers() {

		List<User> usersModel;
		try {
			usersModel = (List<User>) sort(userManager.search(query));
		} catch (SearchException se) {
			addFacesError(se.getMessage());
			usersModel = (List<User>) sort(userManager.search(query));
		}
		if (usersModel != null) {
			users = new ArrayList<UserView>();
			UserView tmp;
			for (User user : usersModel) {
				/*
				 * Fix dates
				 */
				user.setCreated(user.getCreated() * 1000);
				tmp = new UserView(user);
				tmp.setRoles(user.getRoles());
				List<Account> tmpAccounts = accountManager.getAccountByUser(
						user, null);
				for (Account account : tmpAccounts) {
					decryptAccount(account);
				}
				tmp.setAccounts(tmpAccounts);
				tmp.setDocuments(documentManager.getDocumentByUser(tmp
						.getUser()));
				users.add(tmp);
			}
		}
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getQuery() {
		return query;
	}

	public List<UserView> getUsers() {
		return users;
	}

	public String search() {
		return "success";
	}

	public UserView getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(UserView selectedUser) {
		this.selectedUser = selectedUser;
	}

	public Integer getPollingInterval() {
		return pollingInterval;
	}

	public void setPollingInterval(Integer pollingInterval) {
		this.pollingInterval = pollingInterval;
	}

	public Document getSelectedDocument() {
		return selectedDocument;
	}

	public void setSelectedDocument(Document selectedDocument) {
		this.selectedDocument = selectedDocument;
	}
}
