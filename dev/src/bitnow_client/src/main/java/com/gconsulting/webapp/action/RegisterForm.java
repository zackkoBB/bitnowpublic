package com.gconsulting.webapp.action;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;

import com.gsconsulting.bitnow.model.Account;
import com.gsconsulting.bitnow.model.User;
import com.gsconsulting.bitnow.model.enums.AccountStatus;
import com.gsconsulting.bitnow.model.enums.AccountType;
import com.gsconsulting.bitnow.model.service.AccountManager;
import com.gsconsulting.bitnow.model.service.ConfigurationManager;
import com.gsconsulting.bitnow.model.service.MailEngine;
import com.gsconsulting.bitnow.model.service.RoleManager;
import com.gsconsulting.bitnow.model.service.UserExistsException;
import com.gsconsulting.bitnow.model.util.Constants;
import com.gsconsulting.bitnow.model.util.CryptService;

/**
 * JSF Page class to handle signing up a new user.
 *
 * @author mraible
 */
@Scope("request")
@Component("registerForm")
public class RegisterForm extends BasePage implements Serializable {

	private static final long serialVersionUID = 3524937486662786265L;
	private User user = new User();
	private RoleManager roleManager;
	private ConfigurationManager configurationManager;
	private AccountManager accountManager;
	private Double feeSeller;
	private String contextPath;
	private String key;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Autowired
	public void setRoleManager(@Qualifier("roleManager") RoleManager roleManager) {
		this.roleManager = roleManager;
	}

	@Autowired
	public void setConfigurationManager(
			@Qualifier("configurationManager") ConfigurationManager configurationManager) {
		this.configurationManager = configurationManager;
	}

	@Autowired
	public void setAccountManager(
			@Qualifier("accountManager") AccountManager accountManager) {
		this.accountManager = accountManager;
	}

	@Autowired
	public void setMailEngine(@Qualifier("mailEngine") MailEngine mailEngine) {
		this.mailEngine = mailEngine;
	}

	public String save() throws Exception {

		Long currentDate = Calendar.getInstance().getTimeInMillis() / 1000;
		user.setCreated(currentDate);
		user.setEnabled(false);
		// Set the default user role on this new user
		user.addRole(roleManager.getRole(Constants.USER_ROLE));
		user.setTheme(Constants.DEFAULT_THEME);
		if (!user.getPassword().equalsIgnoreCase(user.getConfirmPassword())) {
			addFacesError("errors.password.confirmPasswordMismatch");
			return "error";
		}
		try {
			user.setSecret(CryptService.getKey().getKey());
			user = userManager.saveUser(user);
			/*
			 * Create EUR and BTC accounts
			 */
			feeSeller = new Double(configurationManager.getConfigurationByKey(
					Constants.FEE_SELLER).getValue());
			contextPath = configurationManager.getConfigurationByKey(
					Constants.CONTEXT_PATH).getValue();
			key = configurationManager.getConfigurationByKey(
					Constants.REST_PRICE_KEY).getValue();
			accountManager.create(new Account(user, CryptService.encrypt(user
					.getEmail()), AccountType.EUR.ordinal(), "", "", "", "",
					0d, 0d, 0l, 0d, AccountStatus.ACCOUNT_NOT_ACTIVE.ordinal(),
					feeSeller, currentDate, currentDate, currentDate));
			accountManager.create(new Account(user, CryptService.encrypt(user
					.getEmail()), AccountType.BTC.ordinal(), "", "", "", "",
					0d, 0d, 0l, 0d, AccountStatus.ACCOUNT_NOT_ACTIVE.ordinal(),
					feeSeller, currentDate, currentDate, currentDate));
			/*
			 * getSession().setAttribute(Constants.REGISTERED, Boolean.TRUE); //
			 * log user in automatically UsernamePasswordAuthenticationToken
			 * auth = new UsernamePasswordAuthenticationToken(
			 * user.getUsername(), user.getConfirmPassword(),
			 * user.getAuthorities()); auth.setDetails(user);
			 * SecurityContextHolder.getContext().setAuthentication(auth);
			 */
			/*
			 * Send an account information e-mail
			 */
			message = new SimpleMailMessage();
			message.setTo(user.getEmail());
			String subject = '[' + getText("webapp.name") + "] "
					+ getText("register.email.subject");
			message.setSubject(subject);
			Map<String, Object> args = new HashMap<>();
			args.put("webappURL", getText("website.url"));
			args.put("webappName", getText("webapp.name"));
			args.put("webappTagline", getText("webapp.tagline"));
			args.put("userRegistered", getText("user.registered"));
			args.put("username", user.getUsername());
			args.put("activationLinkMessage",
					getText("register.activationLink"));
			args.put("activationLink",
					contextPath + "rest/register?" + "key=" + key + "&user="
							+ user.getUsername() + "&otp=" + user.getSecret());
			args.put("emailClose", getText("success.orderSubmitted.emailClose"));
			mailEngine
					.sendHTML(
							message,
							com.gconsulting.webapp.util.Constants.REGISTER_EMAIL_TEMPLATE,
							args);
			addFacesMessage("user.registered");
			addFacesMessage("user.checkEmail", user.getEmail());
		} catch (AccessDeniedException ade) {
			// thrown by UserSecurityAdvice configured in aop:advisor
			// userManagerSecurity
			log.warn(ade.getMessage());
			getResponse().sendError(HttpServletResponse.SC_FORBIDDEN);
			return "error";
		} catch (UserExistsException | DataIntegrityViolationException e) {
			addFacesError("errors.existing.user",
					new Object[] { user.getUsername(), user.getEmail() });
			// redisplay the unencrypted passwords
			user.setPassword(user.getConfirmPassword());
			return "error";
		} catch (MailException e) {
			addFacesError("website.contact.contactUs.error", e.getMessage());
			// e.printStackTrace();
			return "error";
		}
		return "success";
	}
}
