package com.gconsulting.webapp.exception;

public class ClientException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3939035894580338076L;

	public ClientException() {
	}

	public ClientException(String message) {
		super(message);
	}
}
