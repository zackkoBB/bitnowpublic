package com.gconsulting.webapp.rest;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gconsulting.webapp.coge.COGE;
import com.gconsulting.webapp.exception.ClientException;
import com.gconsulting.webapp.model.ExchangeRate;
import com.gsconsulting.bitnow.model.service.ConfigurationManager;
import com.gsconsulting.bitnow.model.util.Constants;

@RestController
public class TickerController {

	protected final Log log = LogFactory.getLog(getClass());

	private ConfigurationManager configurationManager;
	private String key;
	private Double walletBTCBalance;
	private Double walletBTCBalanceEUR;
	private Double priceSellBTC;
	private Double priceBuyBTC;
	private Double feeBuy;
	private Double feeSell;

	@Autowired
	public void setConfigurationManager(
			@Qualifier("configurationManager") ConfigurationManager configurationManager) {
		this.configurationManager = configurationManager;
	}

	/**
	 * Init method called after construct
	 * 
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	@PostConstruct
	public void init() throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException {

		if (configurationManager != null) {
			log.info("Environment: "
					+ configurationManager.getConfigurationByKey(
							Constants.ENVIRONMENT).getValue());
			// key = CryptService
			// .decrypt(configurationManager.getConfigurationByKey(
			// Constants.REST_PRICE_KEY).getValue());
			key = configurationManager.getConfigurationByKey(
					Constants.REST_PRICE_KEY).getValue();
			feeBuy = new Double(configurationManager
					.getConfigurationByKey(Constants.FEE_BUY).getValue());
			feeSell = new Double(configurationManager.getConfigurationByKey(
					Constants.FEE_SELL).getValue());
		}
		getData();
	}

	/**
	 * Get Data from the web
	 */
	private void getData() {

		ExchangeRate exchangeRateRetrieved = COGE.getInstance()
				.getExchangeRate();
		if (exchangeRateRetrieved != null) {
			/*
			 * Adjust exchangeRate to our value
			 */
			priceSellBTC = exchangeRateRetrieved.getRate()
					+ exchangeRateRetrieved.getRate() / 100.00 * feeBuy;
			priceBuyBTC = exchangeRateRetrieved.getRate()
					+ exchangeRateRetrieved.getRate() / 100.00 * feeSell;
		}
		if (COGE.getInstance().getBTCWallet() != null) {
			walletBTCBalance = COGE.getInstance().getBTCWallet().getBalance();
		} else {
		}
		// walletBalanceEUR = new Double(configurationManager
		// .getConfigurationByKey(Constants.POSTE_PAY_BALANCE).getValue());
		walletBTCBalanceEUR = 0D;
	}

	private void drawImage(HttpServletResponse response, String text,
			Paint paint) throws IOException {

		// response.setContentType(MediaType.IMAGE_JPEG_VALUE);
		response.setContentType(MediaType.IMAGE_PNG_VALUE);
		ServletOutputStream out = response.getOutputStream();
		// BufferedImage image = new BufferedImage(200, 40,
		// BufferedImage.TYPE_BYTE_INDEXED);
		BufferedImage image = new BufferedImage(200, 40,
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D graphics = image.createGraphics();
		graphics.setComposite(AlphaComposite.Clear);
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, 200, 40);
		graphics.setPaint(paint);
		// Font font = new Font("Comic Sans MS", Font.PLAIN, 30);
		Font font = new Font("Verdana", Font.PLAIN, 30);
		graphics.setFont(font);
		graphics.setComposite(AlphaComposite.Src);
		graphics.drawString(text, 5, 30);
		graphics.dispose();
		ImageIO.write(image, "png", out);
		out.close();
	}

	private void returnError(HttpServletResponse response, String text)
			throws IOException {
		// GradientPaint gradientPaint = new GradientPaint(10, 5, Color.GREEN,
		// 20,
		// 10, Color.RED, true);
		drawImage(response, text, Color.RED);
	}

	// private void returnBlack(HttpServletResponse response, String text)
	// throws IOException {
	// drawImage(response, text, Color.BLACK);
	// }

	private void returnOk(HttpServletResponse response, String text)
			throws IOException {
		drawImage(response, text, Color.GREEN);
	}

	@RequestMapping(value = "/price", method = RequestMethod.GET)
	public void getPrice(
			HttpServletResponse response,
			@RequestParam(value = "type", defaultValue = Constants.PRICE_SELL_LABEL) String type,
			@RequestParam(value = "coin", defaultValue = Constants.FEE_UNIT_BTC) String coin,
			@RequestParam(value = "quantity", defaultValue = "1") Double quantity,
			@RequestParam(value = "language", defaultValue = "IT") String language,
			@RequestParam(value = "key") String keyReceived)
			throws ClientException, IOException {

		log.info("Parameters: " + type + " " + coin + " " + quantity + " "
				+ keyReceived);
		if (keyReceived.equalsIgnoreCase(key)) {
			NumberFormat df = new DecimalFormat("#,##0.00");
			if (language.equals(Constants.LANGUAGE_IT_LABEL)) {
				DecimalFormatSymbols symbols = new DecimalFormatSymbols();
				symbols.setDecimalSeparator(',');
				symbols.setGroupingSeparator('.');
				df = new DecimalFormat("#,##0.00", symbols);
			}
			getData();
			if (type.equalsIgnoreCase(Constants.PRICE_SELL_LABEL)) {
				returnOk(response, "€" + df.format(priceSellBTC * quantity));
			} else if (type.equalsIgnoreCase(Constants.PRICE_BUY_LABEL)) {
				returnError(response, "€" + df.format(priceBuyBTC * quantity));
			} else {
				// No price
				returnError(response, Constants.PRICE_WRONG_MESSAGE);
			}
		} else {
			// wrong secret
			returnError(response, Constants.KEY_WRONG_MESSAGE);
		}
	}

	@RequestMapping(value = "/liquidity", method = RequestMethod.GET)
	public void getLiquidity(
			HttpServletResponse response,
			@RequestParam(value = "coin", defaultValue = Constants.FEE_UNIT_BTC) String coin,
			@RequestParam(value = "language", defaultValue = "IT") String language,
			@RequestParam(value = "key") String keyReceived)
			throws ClientException, IOException {

		log.info("Parameters: " + coin + " " + language + " " + keyReceived);
		if (keyReceived.equalsIgnoreCase(key)) {
			getData();
			if (coin.equalsIgnoreCase(Constants.FEE_UNIT_BTC)) {
				DecimalFormat df = new DecimalFormat("#,##0.00000000");
				if (language.equals(Constants.LANGUAGE_IT_LABEL)) {
					DecimalFormatSymbols symbols = new DecimalFormatSymbols();
					symbols.setDecimalSeparator(',');
					symbols.setGroupingSeparator('.');
					df = new DecimalFormat("#,##0.00000000", symbols);
				}
				returnOk(response, df.format(walletBTCBalance));
			} else if (coin.equalsIgnoreCase(Constants.MARKET_EUR)) {
				DecimalFormat df = new DecimalFormat("#,##0.00");
				if (language.equals(Constants.LANGUAGE_IT_LABEL)) {
					DecimalFormatSymbols symbols = new DecimalFormatSymbols();
					symbols.setDecimalSeparator(',');
					symbols.setGroupingSeparator('.');
					df = new DecimalFormat("#,##0.00", symbols);
				}
				returnError(response, "€" + df.format(walletBTCBalanceEUR));
			} else {
				// No coin
				returnError(response, Constants.LIQUIDITY_COIN_WRONG_MESSAGE);
			}
		} else {
			// wrong secret
			returnError(response, Constants.KEY_WRONG_MESSAGE);
		}
	}
}